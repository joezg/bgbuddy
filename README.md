# Welcome to bgBuddy #
This is bgBuddy. Hopefully, social network for board-game players with many useful tools for them.

# Getting started: #
* clone repository
* install dependencies

```
#!bash

$ npm install
```

* start application in one of the listed ways:


```
#!bash

LINUX
$ ./cmd/actionhero
$ npm start
$ ./node_modules/.bin/actionhero start
$ ./node_modules/actionhero/bin/actionhero start
$ node ./node_modules/actionhero/bin/actionhero start
```

```
#!bat

WINDOWS
\> cmd\actionhero
\> npm start
\> node_modules\.bin\actionhero start
\> node_modules\actionhero\bin\actionhero start
\> node node_modules\actionhero\bin\actionhero start
```

# Node.js debugging: #
* install node-inspector globally

```
#!bash

$ npm install node-inspector -g
```

* start application in debug mode in one of the listed ways:


```
#!bash

LINUX
$ ./cmd/ahdebug
$ node --debug ./node_modules/actionhero/bin/actionhero start
```

```
#!bat

WINDOWS
\> cmd\ahdebug
\> node --debug node_modules\actionhero\bin\actionhero start
```
* in separate console window start node-inspector

```
#!bash

$ node-inspector
```

* debug application on chrome on listed url

# Helpful tools #
* [DHC - REST HTTP API Client](https://chrome.google.com/webstore/detail/dhc-rest-http-api-client/aejoelaoggembcahagimdiliamlcdmfm?utm_source=gmail) - for testing RESTapi in chrome

# Deploying to Heroku #
* install Heroku toolbelt if not installed
* login to Heroku if needed

```
#!bash

$ heroku login
```
* add Heroku SSH key if needed (Permission denied (publickey))

```
#!bash
$ heroku keys:add

```
* create heroku remote if needed

```
#!bash

$ git remote add heroku git@heroku.com:bgbuddy.git
```

* commit changes to git

```
#!bash

$ git commit -a
```
* push changes to Heroku

```
#!bash

$ git push heroku master
```

# Architecture overview: #
* RESTapi is made using actionhero.js framework served on node.js with included dependencies
    * actionhero - RESTapi framework
    * mongoskin - wrapper around native mongodb api
    * step - control-flow library for chaining callbacks
* Database engine is mongodb
* GUI is served through actionhero file server and is composed from:
    * Durandal.js single page application framework which is build on top of:
        * Bootstrap - responsive CSS framework
        * Font-awesome - icon font
        * jQuery - DOM manipulation framework
        * Knockout.js - MVVM framework
        * Require.js - Javascript modules management framework
    * Utility frameworks:
        * Moment.js - date-time management framework
* Application is hosted on Heroku:
    * [www.bgbuddy.com](www.bgbuddy.com)
    * [bgbuddy.herokuapp.com](http://bgbuddy.herokuapp.com)
* Mongo database is hosted on MongoLab