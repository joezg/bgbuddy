var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill(process.env.MANDRILL_API_KEY);
var _ = require('underscore');
var fs = require('fs');
var path = require('path');

var Email = function(subject, templateName, templateData){
    var template = fs.readFileSync(path.join(__dirname, '../emailTemplates/'+templateName+'.html'), "utf8");
    var compiled = _.template(template);
    var body = compiled(templateData);

    this.message = {
        html: body,
        from_name: 'bgBuddy',
        from_email: 'noreply@bgbuddy.com'
    };
    this.message.html = body;
    this.message.subject = subject;
}

Email.prototype.changeMessage = function(message){
    this.message = _.extend(this.message, message);
}

Email.prototype.addTo = function(address){
    if (!this.message.to)
        this.message.to = [];

    var to = {}
    if (!address.email)
        throw new Error("email is mandatory");

    to.email = address.email;

    if (address.name){
        to.name = address.name;
    }

    to.type = address.type ? address.type : "to";

    this.message.to.push(to);
}

Email.prototype.send = function(next){
    mandrill_client.messages.send({"message": this.message, "async": false }, function(result) {
        next(null);
    }, function(e) {
        next(e);
    });
}

module.exports = Email;