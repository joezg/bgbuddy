var Q = require('q');

var promiseResolve = function(promise, connection, next, success){
    Q(promise).then(
        function resolved(data){
            if (success){
                success(connection.response, data);
            }
            connection.response.success = true;
            next(connection, true);
        },
        function rejected(error) {
            connection.error = error;
            next(connection, true);
        }
    ).done();
}

module.exports = promiseResolve;