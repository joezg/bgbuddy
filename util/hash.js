var bcrypt = require('bcryptjs');
var calculatePasswordHash = function(password, next){
    if (next) {
        //next(err, hash)
        bcrypt.hash(password, 10, next);
    } else {
        return bcrypt.hashSync(password, 10);
    }
};
var checkPasswordHash = function(password, passwordHash, next){
    if (next) {
        //next(err, result)
        bcrypt.compare(password, passwordHash, next);
    } else {
        return bcrypt.compareSync(password, passwordHash);
    }
}

var setCustomCookie = function(connection, cookie){
    var cookieStr = cookie.name + '=' + cookie.value;

    if (cookie.expire){
        var ms = 0;

        ms += cookie.expire.years ? cookie.expire.years * 365 * 24 * 60 * 60000 : 0;
        ms += cookie.expire.days ? cookie.expire.days * 24 * 60 * 60000 : 0;
        ms += cookie.expire.months ? cookie.expire.months * 30 * 24 * 60 * 60000 : 0;
        ms += cookie.expire.hours ? cookie.expire.hours * 60 * 60000 : 0;
        ms += cookie.expire.minutes ? cookie.expire.minutes * 60000 : 0;
        ms += cookie.expire.seconds ? cookie.expire.seconds * 1000 : 0;

        cookieStr +='; expires='+new Date(new Date().getTime() + ms).toUTCString()
    }

    if (!cookie.path)
        cookie.path = '/';

    cookieStr += '; path=' + cookie.path;

    connection.rawConnection.responseHeaders.push(['set-cookie', cookieStr]);
}

module.exports = {
  calculatePasswordHash: calculatePasswordHash,
  setCustomCookie: setCustomCookie,
  checkPasswordHash: checkPasswordHash
}