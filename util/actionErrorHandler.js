var ActionErrorHandler = function(connection, next){
    this.connection = connection;
    this.next = next;
}

ActionErrorHandler.prototype.checkError = function(err){
    if (err){
        this.connection.error = err;
        this.next(this.connection, true);
        return false;
    }

    return true;
}

module.exports = ActionErrorHandler;