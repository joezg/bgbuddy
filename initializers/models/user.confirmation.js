var crypto = require('crypto');
var randomInt = function (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}

exports.get = function(mongoose, api){
    /************************ SCHEMA *********************************/
    var userConfirmationSchema = new mongoose.Schema({
        "_id": { type: String, required: true },
        "userId": { type: String, required: true },
        "initTime": { type: Date, default: Date.now, index : { expires : 86400 /* 1 day */ } }
    }, {collection: 'user.confirmation'});

    /************************ VALIDATORS *****************************/
    /************************ END VALIDATORS *************************/

    /************************ VIRTUALS *******************************/
    userConfirmationSchema.virtual('user').set(function (user){
        this._username = user.username;
        this._email = user.email;
        this.userId = user._id;
    });
    /************************ END VIRTUALS ***************************/

    /************************ METHODS *******************************/
    /************************ END METHODS ***************************/

    /************************ MIDDLEWARE *****************************/
    userConfirmationSchema.pre('save', function(next){
        var Email = require('../../util/email');

        var emailToSend = new Email('Please confirm your account', 'welcome', {
            user: this._username,
            confirmationCode: this._id
        });
        emailToSend.addTo({email: this._email});

        emailToSend.send(next);
    })
    userConfirmationSchema.pre('validate', function(next){
        if (!this._id || this._id.length < 128)
            this._id = crypto.randomBytes(randomInt(64, 73)).toString('hex');
        next();
    })
    /************************ END MIDDLEWARE *************************/

    var UserConfirmation;

    try {
        UserConfirmation = mongoose.model('user.confirmation', userConfirmationSchema);
    }
    catch (e) {
        UserConfirmation = mongoose.model('user.confirmation');
    }

    return UserConfirmation;
}