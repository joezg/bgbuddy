var crypto = require('crypto');

exports.get = function(mongoose, api){
    /************************ SCHEMA *********************************/
    var userPasswordRecoverySchema = new mongoose.Schema({
        "_id": { type: String, required: true },
        "userId": { type: String, required: true },
        "initTime": { type: Date, default: Date.now, index : { expires : 86400 /* 1 day */ } }
    }, {collection: 'user.passwordRecovery'});

    /************************ VALIDATORS *****************************/
    /************************ END VALIDATORS *************************/

    /************************ VIRTUALS *******************************/
    userPasswordRecoverySchema.virtual('user').set(function (user){
        this._username = user.username;
        this._email = user.email;
        this.userId = user._id;
    });
    /************************ END VIRTUALS ***************************/

    /************************ METHODS *******************************/
    /************************ END METHODS ***************************/

    /************************ MIDDLEWARE *****************************/
    userPasswordRecoverySchema.pre('save', function(next){
        var Email = require('../../util/email');

        var emailToSend = new Email('Recover your password', 'passwordRecovery', {
            user: this._username,
            passwordRecoveryCode: this._id
        });
        emailToSend.addTo({email: this._email});

        emailToSend.send(next);
    })
    userPasswordRecoverySchema.pre('validate', function(next){
        if (!this._id || this._id.length < 128)
            this._id = crypto.randomBytes(64).toString('hex');
        next();
    })
    /************************ END MIDDLEWARE *************************/

    var UserPasswordRecovery;

    try {
        UserPasswordRecovery = mongoose.model('user.passwordRecovery', userPasswordRecoverySchema);
    }
    catch (e) {
        UserPasswordRecovery = mongoose.model('user.passwordRecovery');
    }

    return UserPasswordRecovery;
}