var crypto = require('crypto');
var randomInt = function (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}

exports.get = function(mongoose){
    /************************ SCHEMA *********************************/
    var userRememberMeSchema = new mongoose.Schema({
        "_id": { type: String, required: true },
        "userId": { type: String, required: true },
        "lastLoginTime": { type: Date, default: Date.now, index : { expires : 2592000 /* 30 days */ } }
    }, {collection: 'user.rememberMe'});

    /************************ VALIDATORS *****************************/
    /************************ END VALIDATORS *************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/

    /************************ METHODS *******************************/
    /************************ END METHODS ***************************/

    /************************ MIDDLEWARE *****************************/
    userRememberMeSchema.pre('validate', function(next){
        if (!this._id || this._id.length < 256)
            this._id = crypto.randomBytes(randomInt(128, 192)).toString('hex');
        next();
    })
    /************************ END MIDDLEWARE *************************/

    var UserRememberMe;

    try {
        UserRememberMe = mongoose.model('user.rememberMe', userRememberMeSchema);
    }
    catch (e) {
        UserRememberMe = mongoose.model('user.rememberMe');
    }

    return UserRememberMe;
}