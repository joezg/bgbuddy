exports.get = function(mongoose){
    /************************ SCHEMA *********************************/
    var eventOrganizerLibraryBoxSchema = new mongoose.Schema({
        code: { type: String, required: true},
        location: { type: String },
        comment: { type: String },
        eventOrganizerId: { type: mongoose.Schema.Types.ObjectId, required: true }
    }, {collection: 'eventOrganizer.library.box'})
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ METHODS *******************************/
    /************************ END VIRTUALS ***************************/
    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/

    var EventOrganizerLibraryBox;

    try {
        EventOrganizerLibraryBox = mongoose.model('eventOrganizer.library.box', eventOrganizerLibraryBoxSchema);
    }
    catch (e) {
        EventOrganizerLibraryBox = mongoose.model('eventOrganizer.library.box');
    }

    return EventOrganizerLibraryBox;
}
