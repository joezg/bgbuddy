var moment = require('moment');
var Q = require('q');
var _ = require('underscore');

exports.get = function(mongoose, api){
    /************************ SCHEMA *********************************/
    var gameSchema = new mongoose.Schema({
        userId: { type: String, required: true },
        name: { type: String, required: true },
        comment: { type: String },
        owned: { type: Boolean },
        wantToPlay: { type: Boolean },
        favourite: { type: Boolean },
        knowRules: { type: Boolean },
        played: { type: Boolean, default: false },
        dateFirstPlayed: { type: Date },
        dateLastPlayed: { type: Date },
        historicalDateFirstPlayed: { type: Date },
        historicalDateLastPlayed: { type: Date },
        bggId: { type: Number }
    }, {collection: 'user.game'});
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/

    /************************ METHODS *******************************/
    gameSchema.methods.isOutOfDust = function(){
        var lastPlayed = moment(this.dateLastPlayed);
        var monthDifference = moment().diff(lastPlayed, 'months');

        var isOutOfDust = monthDifference >= 6;

        return isOutOfDust;
    };
    gameSchema.methods.recalculateDatesAndUpdateMatchGameInfo = function(){
        var gameId = this._id;
        var gameInfo = {
            newGame: {
                isNewGame: true,
                isGameOutOfDust: false
            },
            outOfDustGame: {
                isNewGame: false,
                isGameOutOfDust: true
            },
            normalGame: {
                isNewGame: false,
                isGameOutOfDust: false
            }
        };

        var checkMatch = function (match, gameInfo){
            var newGameUnequal = match.isNewGame !== gameInfo.isNewGame;
            var oodGameUnequal = match.isGameOutOfDust !== gameInfo.isGameOutOfDust;

            return newGameUnequal || oodGameUnequal;
        }

        return Q(api.mongo.userMatch.find({userId: this.userId, gameId: mongoose.Types.ObjectId(gameId) }).sort({ date: 1 }))
            .then(_.bind(function(matches){
                var promises = [];

                this.played = false;
                this.dateFirstPlayed = null;
                this.dateLastPlayed = null;

                var previousMatch = null;
                matches.forEach(_.bind(function(m){
                    this.played = true;
                    var matchDate = moment(m.date);
                    var saveNeeded = false;
                    if (!previousMatch){
                        var gameLastPlayed = null;
                        if (this.historicalDateLastPlayed){
                            gameLastPlayed = moment(this.historicalDateLastPlayed);
                        } else if (this.historicalDateFirstPlayed){
                            gameLastPlayed = moment(this.historicalDateFirstPlayed);
                        }
                        this.dateFirstPlayed = m.date;

                        if (!gameLastPlayed){
                            saveNeeded = checkMatch(m, gameInfo.newGame);
                            _.extend(m, gameInfo.newGame)
                        } else {
                            var gi = gameInfo.normalGame;
                            if (matchDate.diff(gameLastPlayed, 'months') >= 6){
                                gi = gameInfo.outOfDustGame;
                            }
                            if (matchDate.diff(gameLastPlayed, 's') < 0){
                                gi = gameInfo.newGame;
                            }

                            saveNeeded = checkMatch(m, gi);
                            _.extend(m, gi)
                        }
                    } else {
                        var previousMatchDate = moment(previousMatch.date);

                        var gi = matchDate.diff(previousMatchDate, 'months') >= 6 ? gameInfo.outOfDustGame : gameInfo.normalGame;
                        saveNeeded = checkMatch(m, gi);
                        _.extend(m, gi)
                    }

                    previousMatch = m;

                    if (saveNeeded)
                        promises.push(m.save());
                }, this));

                if (previousMatch){
                    this.dateLastPlayed = previousMatch.date;
                }

                return Q.all(promises);
            }, this));

    }
    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    gameSchema.statics.addGame = function(userId, game, doSendMail){
        var newGame = new this(game);
        newGame.userId = userId;

        if (doSendMail){
            var msgText = "You've played a new game! We added " + game.name + " to your games list. ";

            var newMessage = {
                to: userId,
                from: 'bgBuddy',
                content: msgText
            };

            return api.mongo.userMessages.sendMail(newMessage).then(function(){ return newGame.save(); });
        } else {
            return newGame.save();
        }
    };
    gameSchema.statics.recordPlay = function(userId, _id){
        return this.findOne({userId: userId, _id: _id}).then(function(game){
            game.dateLastPlayed = new Date();
            if (!game.dateFirstPlayed)
                game.dateFirstPlayed = game.dateLastPlayed;
            game.played = true;
            return game.save();
        });
    }
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/
    var UserGame;

    try {
        UserGame = mongoose.model('user.game', gameSchema);
    }
    catch (e) {
        UserGame = mongoose.model('user.game');
    }

    return UserGame;
}
