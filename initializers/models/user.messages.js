var Step = require('step');
var _ = require('underscore');
var Q = require('q');

var errorRaised = function(err, next){
    if (err) {
        next(err);
        return true;
    }

    return false;
}

exports.get = function(mongoose, api){
    /************************ SCHEMA *********************************/
    var userMessagesSchema = new mongoose.Schema({
        _id: { type: String, required: true },
        inbox: [{
            _id: { type: mongoose.Schema.Types.ObjectId, required: true},
            from: { type: String, required: true },
            content: { type: String, required: true },
            dateSent: { type: Date, default: Date.now },
            isUnread: { type: Boolean, default: true }
        }],
        sent: [{
            _id: { type: mongoose.Schema.Types.ObjectId, required: true},
            to: { type: String, required: true },
            content: { type: String, required: true },
            dateSent: { type: Date, default: Date.now },
            delivered: { type: Boolean, default: false }
        }]
    }, {collection: 'user.messages'});
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    userMessagesSchema.virtual('username').get(function () {
        return this._id;
    });

    userMessagesSchema.virtual('username').set(function (username) {
        this._id = username;
    });
    /************************ END VIRTUALS ***************************/

    /************************ METHODS *******************************/
    userMessagesSchema.methods.makMessagesRead = function(messageIds, markAll, next){
        var self = this;
        var toMarkDelivered = {};

        if (this.inbox){
            if (!markAll){
                messageIds.forEach(function(id){
                    var msg = self.inbox.id(id);

                    if (msg){
                        msg.isUnread = false;

                        if (!toMarkDelivered[msg.from]){
                            toMarkDelivered[msg.from] = [];
                        }

                        toMarkDelivered[msg.from].push(id);
                    }
                })
            }
            else {
                this.inbox.forEach(function(msg){
                    if (msg.isUnread){
                        msg.isUnread = false;

                        if (!toMarkDelivered[msg.from]){
                            toMarkDelivered[msg.from] = [];
                        }

                        toMarkDelivered[msg.from].push(msg._id);
                    }
                });
            }

            Step(
                function saveReceiverMessages(){
                    self.save(this)
                },
                function markSenderMessagesDelivered(err){
                    if (err){
                        this(err);
                    } else {
                        _.each(toMarkDelivered, function(ids, sender){
                            var UserMessages = self.constructor;

                            UserMessages.markAsDelivered(sender, ids, this.parallel());
                        }, this)

                    }
                },
                function result(err){
                    next(err);
                }
            )
        } else {
            next(null);
        }
    }

    userMessagesSchema.methods.markMessageRead = function(messageId, next){
        this.makMessagesRead([messageId], false, next);
    };

    userMessagesSchema.methods.markAllMessagesRead = function(next){
        this.makMessagesRead(null, true, next);
    };

    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    userMessagesSchema.statics.markAsDelivered = function(from, messageIds, next){
        var self = this;
        Step(
            function findSender(){
                var um = self.findById(from, this);
            },
            function markAllAsDeliveredAndSave(err, um){
                if (err){
                    this(err);
                } else {
                    if (um.sent){
                        messageIds.forEach(function(id){
                            var msg = um.sent.id(id);
                            msg.delivered = true;
                        });

                        um.save(this);
                    } else {
                        this(null);
                    }
                }
            },
            function result (err){
                next(err);
            }
        );
    }

    userMessagesSchema.statics.sendMail = function(message, next){
        var self = this;

        message._id = mongoose.Types.ObjectId();

        var promise = api.mongo.user.findById(message.to)
            .then(function(user){
                if (!user)
                    throw new Error("user "+message.to+" does not exists.");

                var senderPromise = self.findByIdAndUpdate(
                    message.from,
                    {
                        $push: {
                            sent: {
                                $each: [message],
                                $position: 0
                            }
                        }
                    },
                    { upsert: true }
                );

                var receiverPromise = self.findByIdAndUpdate(
                    message.to,
                    {
                        $push: {
                            inbox: {
                                $each: [message],
                                $position: 0
                            }
                        }
                    },
                    { upsert: true }
                );

                return Q.all([senderPromise, receiverPromise]);
            })

        return promise;
    }
    /************************ END STATICS ****************************/

    var UserMessages;

    try {
        UserMessages = mongoose.model('user.mesages', userMessagesSchema);
    }
    catch (e) {
        UserMessages = mongoose.model('user.mesages');
    }

    return UserMessages;
}