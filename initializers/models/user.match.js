var _ = require('underscore');
var Q = require('q');

exports.get = function(mongoose, api){
    var playerSchema = require('./match.player.js').get(mongoose);
    /************************ SCHEMA *********************************/
    var userMatchSchema = new mongoose.Schema({
        "userId": { type: String, required: true },
        "game": { type: String, required: true },
        "gameId": { type: mongoose.Schema.Types.ObjectId, required: true },
        "date": { type: Date, default: Date.now },
        "location": { type: String },
        "inProgress": { type: Boolean, default: true },
        "isScored": {type: Boolean, default: false},
        "isNewGame": {type: Boolean, default: false},
        "isGameOutOfDust": {type: Boolean, default: false},
        "phases": [
            {
                //do not remove _id - it is used by mongoose for removing sub-document
                "type": { type: Number, required: true },
                "name": { type: String, required: true },
                "start": { type: Date, default: Date.now },
                "end": { type: Date }
            }
        ],
        "players": [playerSchema]
    }, {collection: 'user.match'});
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/
    /************************ METHODS *******************************/
    userMatchSchema.methods.startNewPhase = function(newPhase){
        this.endOngoingPhase(newPhase.start);
        this.phases.push(newPhase);
    }

    userMatchSchema.methods.finishMatch = function(){
        this.endOngoingPhase();
        this.inProgress = false;
    }

    userMatchSchema.methods.endOngoingPhase = function(endTime){
        if (!endTime)
            endTime = new Date();

        for (var i=0;i<this.phases.length;i++){
            var p = this.phases[i];

            if (!p.end) {
                p.end = endTime;
                break;
            }
        }
    }

    userMatchSchema.methods.findPlayer = function(player){
        for (var i=0;i<this.players.length;i++){
            var p = this.players[i];
            if (p.isUser && player.isUser){
                return p;
            }
            if (player.name && p.name == player.name){
                return p;
            }
            if (player.isAnonymous && p.isAnonymous && p._id.toString() == player._id.toString()){
                return p;
            }
        }
    }

    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    userMatchSchema.statics.updatePlayers = function(userId, matchId, players, doAddBuddy){
        var findMatchPromise = api.mongo.userMatch.findOne({userId: userId, _id: matchId});

        var checkBuddyPromises = []
        var buddiesToAdd = [];
        players.forEach(function(p){
            if (p.isNewBuddy){
                if (!p.buddyId){
                    p.buddyId = api.mongo.mongoose.Types.ObjectId();
                }

                if (doAddBuddy){
                    p.isNewBuddy = false;
                    var newBuddy = _.pick(p, 'name');
                    newBuddy._id = p.buddyId;

                    buddiesToAdd.push(newBuddy);
                }

            } else if (!p.isUser && !p.isAnonymous){
                checkBuddyPromises.push(api.mongo.userBuddy.findOne({userId: userId, _id: p.buddyId}).then(function(buddy){
                    if (!buddy)
                        throw new Error("Buddy with a name " + p.name + " does not exists.")
                }));
            }
        });

        var promises = [findMatchPromise];
        promises = promises.concat(checkBuddyPromises);

        if (doAddBuddy && buddiesToAdd.length > 0){
            promises.push(api.mongo.userBuddy.addBuddies(userId, buddiesToAdd));
        }

        return Q.all(promises).then(function(data){
            var match = data[0];

            if (null == match)
                throw new Error("Match with given id don't exists.");

            //updating match
            match.isScored= false;
            for (var i=0;i<players.length;i++){
                var p = players[i];

                if (p.rank || p.result || p.winner){
                    match.isScored = true;
                }
            }
            match.players = {};
            match.players = players;

            return match.save();
        });

    }

    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/
    /************************ VALIDATORS *****************************/
    /************************ END VALIDATORS *************************/
    var UserMatch;

    try {
        UserMatch = mongoose.model('user.match', userMatchSchema);
    }
    catch (e) {
        UserMatch = mongoose.model('user.match');
    }

    return UserMatch;
}
