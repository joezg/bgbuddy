exports.get = function(mongoose){
    /************************ SCHEMA *********************************/
    var errorSchema = new mongoose.Schema({
        date: { type: Date, default: Date.now },
        type: { type: String, required: true},
        severity: { type: String, required: true},
        stack: [],
        extraMessages: []
    }, {collection: 'error', capped: 10485760 });
    
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/

    /************************ METHODS *******************************/
    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/
    var Error;

    try {
        Error = mongoose.model('error', errorSchema);
    }
    catch (e) {
        Error = mongoose.model('error');
    }

    return Error;
}
