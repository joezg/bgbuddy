exports.get = function(mongoose){
    /************************ SCHEMA *********************************/
    var actionLogSchema = new mongoose.Schema({
        date: { type: Date, default: Date.now },
        currentUser: { type: String },
        actionName: { type: String, required: true },
        connection: {}
    }, {collection: 'actionLog', capped: 10485760 });
    
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/

    /************************ METHODS *******************************/
    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/
    var ActionLog;

    try {
        ActionLog = mongoose.model('actionLog', actionLogSchema);
    }
    catch (e) {
        ActionLog = mongoose.model('actionLog');
    }

    return ActionLog;
}
