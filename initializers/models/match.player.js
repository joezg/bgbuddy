exports.get = function(mongoose){
    /************************ SCHEMA *********************************/
    var playerSchema = new mongoose.Schema({
        //do not remove _id - it is used by mongoose for removing sub-document
        "buddyId": { type: mongoose.Schema.Types.ObjectId },
        "name": { type: String },
        "isNewBuddy": {type: Boolean },

        "isUser": { type: Boolean },

        "isAnonymous": { type: Boolean },

        "result": { type: Number },
        "rank": { type: Number },
        "winner": { type: Boolean },
        "firstPlayer": { type: Boolean }
    });

    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/

    /************************ METHODS *******************************/

    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ VALIDATIONS ********************************/
    var checkIllegalProperty = function(property, invalidationProperty){
        if (this[property])
            this.invalidate(invalidationProperty, "Invalid property combination: " + property);
    }
    playerSchema.path('isUser').validate(function (value){
        if (this.isUser){
            checkIllegalProperty.call(this, 'isAnonymous', 'isUser');
            checkIllegalProperty.call(this, 'name', 'isUser');
            checkIllegalProperty.call(this, 'buddyId', 'isUser');
            checkIllegalProperty.call(this, 'isNewBuddy', 'isUser');
        }
    });
    playerSchema.path('name').validate(function (value){
        if (this.name){
            checkIllegalProperty.call(this, 'isAnonymous', 'name');
            checkIllegalProperty.call(this, 'isUser', 'name');

            if (!this.isNewBuddy && !this.buddyId){
                this.invalidate('name', 'If name is set for existing buddy, buddyId should be set, too.');
            }
        }
    });
    playerSchema.path('isAnonymous').validate(function (value){
        if (this.isAnonymous){
            checkIllegalProperty.call(this, 'isUser', 'isAnonymous');
            checkIllegalProperty.call(this, 'name', 'isAnonymous');
            checkIllegalProperty.call(this, 'buddyId', 'isAnonymous');
            checkIllegalProperty.call(this, 'isNewBuddy', 'isAnonymous');
        }
    });
    /************************ END VALIDATIONS ****************************/

    /************************ MIDDLEWARE *****************************/
    playerSchema.pre('validate', function (next) {
        if (!this.isUser && !this.name && !this.isAnonymous)
            next(new Error('One of parameters isUser, isName or isAnonymous must be set.'))
        next();
    });
    /************************ END MIDDLEWARE *************************/

    return playerSchema;
}
