var Q = require("q");

exports.get = function(mongoose, api){
    /************************ SCHEMA *********************************/
    var eventOrganizerSchema = new mongoose.Schema({
        name: { type: String, required: true },
        website: { type: String },
        admins: [{ type: String }]
    }, {collection: 'eventOrganizer'});
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/
    /************************ METHODS *******************************/
    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    eventOrganizerSchema.statics.isUserAdmin = function(user, eventOrganizerId, next){
        this.findOne({
            _id: eventOrganizerId,
            admins: user
        }, function(err, eo){
            if (err) {
                next(err);
            } else {
                if (null == eo){
                    next(null, false);
                } else {
                    next(null, true);
                }
            }
        })
    }
    eventOrganizerSchema.statics.getAllUserAdministered = function(user){
        return this.find(
            {
                admins: { $in: [user] }
            },
            {
                "_id": 1,
                "name": 1,
                "website": 1
            }
        )
    };
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/

    var EventOrganizer;

    try {
        EventOrganizer = mongoose.model('eventOrganizer', eventOrganizerSchema);
    }
    catch (e) {
        EventOrganizer = mongoose.model('eventOrganizer');
    }

    return EventOrganizer;
}
