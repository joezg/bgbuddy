var _ = require('underscore');

exports.get = function(mongoose, api){
    /************************ SCHEMA *********************************/
    var eventMarathonSchema = new mongoose.Schema({
        eventId: { type: mongoose.Schema.Types.ObjectId, required: true },
        eventOrganizerId: { type: mongoose.Schema.Types.ObjectId, required: true },
        name: { type: String, required: true},
        date: { type: Date, required: true},
        participants: [{
            name: { type: String, required: true},
            participantId: { type: String },
            participantsId: [ { type: String } ], //could be teams or pairs
            finalRanking: { type: Number, required: false },
            totalPoints: { type: Number, required: false },
            scores: [
                {
                    gameId: { type: mongoose.Schema.Types.ObjectId, required: true },
                    gameName: { type: String, required: true},
                    bestPosition: { type: Number, required: false },
                    bestPoints: { type: Number, required: false }
                }
            ]
        }],
        isStarted: { type: Boolean, default: false},
        isFinished: { type: Boolean, default: false},
        games: [{
            gameId: { type: mongoose.Schema.Types.ObjectId, required: true },
            name: { type: String, required: true},
            weight: { type: Number, required: true }
        }],
        positionPoints: [{
            position: { type: Number, required: true},
            numberOfPlayers: { type: Number, required: true},
            points: { type: Number, required: true }
        }],
        type: { type: Number, default: 1}

    }, {collection: 'event.tournament'});
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/
    /************************ METHODS *******************************/

    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/

    /************************ HELPERS ********************************/

    eventMarathonSchema.methods.calculateStandings = function(){
        var gameIds = _.map(this.games, function(g){
            return g.gameId;
        });


        return api.mongo.eventMatch.find({eventId: this.eventId, gameId: {$in: gameIds}}).then(_.bind(function(matches){

            for (var i=0; i<matches.length; i++){
                var match = matches[i];

                for (var j=0; j<match.players.length; j++){
                    var player = match.players[j];

                    var participant = _.find(this.participants, function(p){
                        return p.participantId === player.playerId;
                    })

                    if (participant){
                        var game = _.find(this.games, function(g){
                            return g.gameId.toString() === match.gameId;
                        })

                        var score = _.find(participant.scores, function(s){
                            return s.gameId.toString() === game.gameId.toString();
                        })

                        var positionPoints = _.find(this.positionPoints, function(pp){
                            return pp.position === player.rank && pp.numberOfPlayers === match.players.length;
                        }).points;

                        if (!score){
                            score = {
                                gameId: game.gameId,
                                gameName: game.name,
                                bestPosition: player.rank,
                                bestPoints: positionPoints*game.weight
                            }
                            participant.scores.push(score);
                        } else {
                            var newScore = positionPoints*game.weight;

                            if (newScore > score.bestPoints){
                                score.bestPoints = newScore;
                                score.bestPosition = player.rank;
                            }
                        }
                    }
                }
            }

            this.participants.forEach(_.bind(function(p){
                p.totalPoints = 0;
                for (var i = 0; i< p.scores.length; i++){
                    p.totalPoints = p.totalPoints + p.scores[i].bestPoints;
                }
            }, this));


        }, this));

    }

    var EventMarathon;

    try {
        EventMarathon = mongoose.model('event.marathon', eventMarathonSchema);
    }
    catch (e) {
        EventMarathon = mongoose.model('event.marathon');
    }

    return EventMarathon;
}
