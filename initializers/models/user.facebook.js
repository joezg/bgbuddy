exports.get = function(mongoose){
    /************************ SCHEMA *********************************/
    var userFacebookSchema = new mongoose.Schema({
        "_id": { type: String, required: true },
        "username": { type: String},
        "accessToken": { type: String }
    }, {collection: 'user.facebook'});
    /************************ END SCHEMA *****************************/

    /************************ GETTERS / SETTERS **********************/
    /************************ END GETTERS / SETTERS ******************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/

    /************************ METHODS *******************************/
    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ VALIDATORS *****************************/
    /************************ END VALIDATORS *************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/

    var UserFacebook;

    try {
        UserFacebook = mongoose.model('user.facebook', userFacebookSchema);
    }
    catch (e) {
        UserFacebook = mongoose.model('user.facebook');
    }

    return UserFacebook;
}
