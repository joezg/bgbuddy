var validator = require('validator');

exports.get = function(mongoose){
    /************************ SCHEMA *********************************/
    var eventOrganizerParticipantSchema = new mongoose.Schema({
        code: { type: String, required: true },
        eventOrganizerId: { type: mongoose.Schema.Types.ObjectId, required: true },
        name: { type: String, required: true },
        shortName: { type: String, required: false },
        email: { type: String, required: false },
        userId: { type: String, required: false },
        country: { type: String, required: false }
    }, {collection: 'eventOrganizer.participant'});
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/
    /************************ METHODS *******************************/
    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/

    /************************ VALIDATIONS ****************************/
    eventOrganizerParticipantSchema.path('email').validate(function(v) {
        if (v && !validator.isEmail(v)) {
            this.invalidate('email', 'email must be valid email address');
        }
    });
    /************************ END VALIDATIONS ************************/

    var EventOrganizerParticipant;

    try {
        EventOrganizerParticipant = mongoose.model('eventOrganizer.participant', eventOrganizerParticipantSchema);
    }
    catch (e) {
        EventOrganizerParticipant = mongoose.model('eventOrganizer.participant');
    }

    return EventOrganizerParticipant;
}
