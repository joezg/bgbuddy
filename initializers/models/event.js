var Q = require("q");

exports.get = function(mongoose, api){
    /************************ SCHEMA *********************************/
    var eventSchema = new mongoose.Schema({
        name: { type: String, required: true},
        location: { type: String, required: true},
        startDate: { type: Date, required: true},
        endDate: { type: Date, required: true},
        venue: { type: String },
        eventOrganizerId: { type: mongoose.Schema.Types.ObjectId, required: true }
    }, {collection: 'event'});
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/
    /************************ METHODS *******************************/
    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/

    var Event;

    try {
        Event = mongoose.model('event', eventSchema);
    }
    catch (e) {
        Event = mongoose.model('event');
    }

    return Event;
}
