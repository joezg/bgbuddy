var Q = require('q');
var _ = require('underscore')

exports.get = function(mongoose, api){
    /************************ SCHEMA *********************************/
    var buddySchema = new mongoose.Schema({
        userId: { type: String, required: true },
        name: { type: String, required: true },
        username: { type: String },
        comment: { type: String },
        sort: { type: Number },
        like: { type: Boolean, default: false }
    }, {collection: 'user.buddy'});
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/

    /************************ METHODS *******************************/
    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    buddySchema.statics.addBuddies = function(userId, buddies, doSendMail){
        if (buddies.length == 0){
            return Q().thenResolve(buddies);
        }

        var saveBuddyPromises = [];

        buddies.forEach(_.bind(function(b){
            var buddy = new this(b);
            buddy.userId = userId;

            saveBuddyPromises.push(buddy.save());
        }, this))

        return Q.all(saveBuddyPromises).then(function(savedBuddies){
            if (doSendMail){
                var buddiesString = "";
                for (var i=0; i<savedBuddies.length; i++) {
                    buddiesString = buddiesString + savedBuddies[i].name;
                    if (i === savedBuddies.length-2)
                        buddiesString = buddiesString + ' and ';
                    else if (i < savedBuddies.length-2)
                        buddiesString = buddiesString + ', ';
                }
                var msgText = "You've played with some new buddies! We added " + buddiesString + " to your buddy list. ";

                var newMessage = {
                    to: userId,
                    from: 'bgBuddy',
                    content: msgText
                };

                return api.mongo.userMessages.sendMail(newMessage)
                    .then(function(){
                        return savedBuddies;
                    });
            }
            else {
                return savedBuddies;
            }
        })
    }
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/

    var UserBuddy;

    try {
        UserBuddy = mongoose.model('user.buddy', buddySchema);
    }
    catch (e) {
        UserBuddy = mongoose.model('user.buddy');
    }

    return UserBuddy;
}
