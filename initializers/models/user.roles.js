exports.get = function(mongoose){
    /************************ SCHEMA *********************************/
    var userRolesSchema = new mongoose.Schema({
        "_id": { type: String, required: true },
        "roles": [String]
    }, {collection: 'user.roles'});

    /************************ VALIDATORS *****************************/
    /************************ END VALIDATORS *************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/

    /************************ STATICS ********************************/
    userRolesSchema.statics.checkRole = function(id, role, next){
        this.findById(id, function(err, ur){
            if (err){
                next(err);
            } else {
                if (!ur){
                    next(null, false);
                } else {
                    if (ur.roles.indexOf(role) != -1){
                        next(null, true);
                    } else {
                        next(null, false);
                    }
                }
            }
        })
    }
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    userRolesSchema.pre('validate', function(next){
        if (this.roles.indexOf('user') == -1)
            this.roles.push('user');
        next();
    })
    /************************ END MIDDLEWARE *************************/

    var UserRoles;

    try {
        UserRoles = mongoose.model('user.roles', userRolesSchema);
    }
    catch (e) {
        UserRoles = mongoose.model('user.roles');
    }

    return UserRoles;
}
