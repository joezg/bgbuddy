var _ = require('underscore');

exports.get = function(mongoose){
    /************************ SCHEMA *********************************/
    var eventTournamentSchema = new mongoose.Schema({
        eventId: { type: mongoose.Schema.Types.ObjectId, required: true },
        eventOrganizerId: { type: mongoose.Schema.Types.ObjectId, required: true },
        name: { type: String, required: true},
        date: { type: Date, required: true},
        gameId: { type: String, required: false},
        gameName: { type: String, required: false},
        participants: [{
            name: { type: String, required: true},
            participantId: { type: String },
            participantsId: [ { type: String } ], //could be teams or pairs
            isEliminated: { type: Boolean, default: false},
            eliminationRoundNumber: { type: Number, required: false },
            finalRanking: { type: Number, required: false },
            totalPrimaryPoints: { type: Number, required: false },
            totalSecondaryPoints: { type: Number, required: false },
            roundRobinCode: {type: Number, required: false}
        }],
        isStarted: { type: Boolean, default: false},
        isFinished: { type: Boolean, default: false},
        rounds: [ {
            _id: { type: Number, required: true }, //round number
            roundName: { type: String, required: false },
            propositions: {
                tableReposition: { //how do new tables are organized
                    type: String,
                    enum: [
                        'random',
                        'byRank',
                        'snake',
                        'copyPrevious', //tables stays the same
                        'knockout', //players that are promoted from previous from table 1 are combined with players from table 2
                        'pick',
                        'roundRobin'
                    ],
                    default: 'random'
                },
                numberOfTables: { type: Number, default: 2 },
                largerPlayerCountOnLowerTables: { type: Boolean, default: true },
                seating: {  //how do seating on particular table is organized
                    type: String,
                    enum: [
                        'random',
                        'byRank'
                    ],
                    default: 'random'
                },
                carryoverType: {  //how previous round points are carried over to this round
                    type: String,
                    enum: [
                        'continue',
                        'reset',
                        'carryover'
                    ],
                    default: 'continue'
                },
                carryover: { type: Number, default: 0 },
                cutoff: { type: Number, default: 0 },
                cutoffType: {
                    type: String,
                    enum: [
                        'byRankAll', //participants on last positions in ranking are eliminated
                        'byRankTable' //elimination is by rank but on each table separately
                    ],
                    default: 'byRankAll'
                },
                primaryRankingType: {
                    type: String,
                    enum: [
                        'rankPoints', //ranking is mapped to points
                        'scorePoints', //score from the game
                        'percentageOfPointsInGame', //everybody will get percentage of their score against total score
                        'normalizedPercentageOfPoints', //first player will receive 100% and others will get percentage of their points against first player points
                        'wins'   //only wins are worth 1 point
                    ],
                    default: 'rankPoints'
                },
                primaryRankingHigherIsBest: { type: Boolean, default: true},
                secondaryRankingType: {
                    type: String,
                    enum: [
                        'rankPoints', //ranking is mapped to points
                        'scorePoints', //score from the game
                        'percentageOfPointsInGame', //everybody will get percentage of their score against total score
                        'normalizedPercentageOfPoints', //first player will receive 100% and others will get percentage of their points against first player points
                        'byDirectMatch'
                    ],
                    default: 'scorePoints'
                },
                secondaryRankingHigherIsBest: { type: Boolean, default: true},
                rankPointsDistribution: [{
                    _id: { type: Number, required: true },
                    points: { type: Number, required: true }
                }],
                multipleMatches: {
                    isMultiple: { type: Boolean, default: false},
                    isPlayedToNumberOfWins: { type: Boolean, default: true},
                    numberOfMatchesOrWins: { type: Number, default: 2 }
                }
            },
            matches: [{
                tableNumber: { type: Number, required: true },
                isScored: { type: Boolean, required: false},
                gameId: { type: String, required: false},
                date: { type: Date, required: false},
                participants: [{
                    _id: { type: mongoose.Schema.Types.ObjectId, required: true }, //tournament participant id
                    seatingPosition: { type: Number, required: true },
                    name: { type: String, required: true},
                    rank: { type: Number, required: false },
                    result: { type: Number, required: false },
                    winner: { type: Boolean, required: false },
                    primaryPoints: { type: Number, required: false },
                    secondaryPoints: { type: Number, required: false }
                }],
                matchNumber: { type: Number, required: false }
            }],
            ranking: [{
                _id: { type: mongoose.Schema.Types.ObjectId, required: true },
                name: { type: String, required: true},
                previousRoundPrimaryPoints: { type: Number, default: 0 },
                previousRoundSecondaryPoints: { type: Number, default: 0 },
                previousRoundPrimaryCarryover: { type: Number, default: 0 },
                previousRoundSecondaryCarryover: { type: Number, default: 0 },
                primaryPoints: { type: Number, default: 0 },
                secondaryPoints:{ type: Number, default: 0 },
                primaryPointsTotal: { type: Number, default: 0 },
                secondaryPointsTotal: { type: Number, default: 0 },
                isEliminated: { type: Boolean, default: false }
            }],
            isStarted: { type: Boolean, default: false},
            isFinished: { type: Boolean, default: false}
        }],
        type: { type: Number, default: 0}

    }, {collection: 'event.tournament'});
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/
    /************************ METHODS *******************************/
    eventTournamentSchema.methods.getRemainingParticipants = function(){
        var remainingParticipants = [];
        for (var i=0; i<this.participants.length;i++){
            if (!this.participants[i].isEliminated){
                remainingParticipants.push(this.participants[i]);
            }
        }
        return remainingParticipants;
    }
    eventTournamentSchema.methods.calculateTournamentRoundRanking = function(round){
        var previousRound = this.getPreviousRound(round);

        var remainingParticipants = this.getRemainingParticipants();

        remainingParticipants.forEach(function(p){
            var ranking = round.ranking.id(p._id);
            var previousRoundRanking = previousRound ? previousRound.ranking.id(p._id) : null;
            if (null == ranking){
                ranking = {
                    _id: p._id,
                    name: p.name,
                    previousRoundPrimaryPoints: 0,
                    previousRoundSecondaryPoints: 0,
                    previousRoundPrimaryCarryover: 0,
                    previousRoundSecondaryCarryover: 0,
                    primaryPoints: 0,
                    secondaryPoints: 0,
                    primaryPointsTotal: 0,
                    secondaryPointsTotal: 0
                }
                round.ranking.push(ranking);
            }
            if (null != previousRoundRanking){
                ranking.previousRoundPrimaryPoints = previousRoundRanking.primaryPointsTotal;
                ranking.previousRoundSecondaryPoints = previousRoundRanking.secondaryPointsTotal,
                ranking.previousRoundPrimaryCarryover = carryoverTypeFunction[round.propositions.carryoverType](round, previousRoundRanking.primaryPointsTotal);
                ranking.previousRoundSecondaryCarryover = carryoverTypeFunction[round.propositions.carryoverType](round, previousRoundRanking.secondaryPointsTotal);

                if (round.propositions.secondaryRankingType == 'byDirectMatch'){
                    ranking.previousRoundSecondaryCarryover = 0;
                }
            }

            ranking.primaryPoints = 0;
            ranking.secondaryPoints = 0;
        });

        round.matches.forEach(function(m){
            if (m.isScored){
                m.participants.forEach(function(p){
                    var participant = round.ranking.id(p._id);
                    participant.primaryPoints += p.primaryPoints;
                    participant.secondaryPoints += p.secondaryPoints;
                    participant.primaryPointsTotal = participant.primaryPoints + participant.previousRoundPrimaryCarryover;
                    participant.secondaryPointsTotal = participant.secondaryPoints + participant.previousRoundSecondaryCarryover;
                })
            }

        });
    }
    eventTournamentSchema.methods.rearrangeTables = function(roundId, matches){
        var round = this.rounds.id(roundId);

        if (round.propositions.tableReposition != 'pick')
            throw new Error('Tables in this round could not be rearranged mannually because table reposition proposition is not set to pick.');

        round.matches = matches;
    }
    eventTournamentSchema.methods.breakTiesByDirectMatch = function(round){
        var ties = {};

        //find all players with same number of primary points
        for (var i = 0; i<round.ranking.length; i++){
            var numberOfPrimarypoints = round.ranking[i].primaryPointsTotal;

            if (!ties.hasOwnProperty(numberOfPrimarypoints)){
                ties[numberOfPrimarypoints] = [];
            }

            ties[numberOfPrimarypoints].push(round.ranking[i]);
        }

        //find corresponding matches
        var startingRoundNumber = 1; //this could later be a proposition parameter
        var currentRound = this.rounds.id(startingRoundNumber++)

        //loop through all of the rounds
        while (currentRound){
            //loop through all of the round matches
            currentRound.matches.forEach(function(match){
                //loop through all possible ties to see if any is broken in this match
                for (var prop in ties){
                    if (ties.hasOwnProperty(prop)){
                        var tie = ties[prop];
                        //only for real ties (ones that ties more than one player
                        if (tie.length > 1){
                            var positions = [];

                            //pick all positions of tied players
                            for (var i=0; i<tie.length; i++){
                                var matchPlayer = match.participants.id(tie[i]._id);
                                if (null != matchPlayer){
                                    positions.push(matchPlayer);
                                }
                            }

                            if (positions.length > 1){ //... in that case we have a tie breaker
                                for (var i=0; i<positions.length; i++){
                                    for (var j=0; j<positions.length; j++){
                                        if (positions[i].rank < positions[j].rank){
                                            var participant = round.ranking.id(positions[i]._id)
                                            participant.secondaryPoints = participant.secondaryPoints ? participant.secondaryPoints + 1 : 1;
                                            participant.secondaryPointsTotal = participant.secondaryPoints;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });

            currentRound = this.rounds.id(startingRoundNumber++);
        }
    }
    eventTournamentSchema.methods.addMatchScore = function(roundId, scores){
        var round = this.rounds.id(roundId);
        round.isStarted = true;
        var match = round.matches.id(scores._id);
        match.participants = scores.participants;
        match.isScored = true;

        rankingFunctions[round.propositions.primaryRankingType](round, match.participants, 'primaryPoints', this);
        rankingFunctions[round.propositions.secondaryRankingType](round, match.participants, 'secondaryPoints', this);

        //recalculate rankings based on new result
        this.calculateTournamentRoundRanking(round);

        //break ties
        if (round.propositions.secondaryRankingType == 'byDirectMatch') {
            this.breakTiesByDirectMatch(round)
        }

        //sort the ranks
        var primaryHigherIsBetter = round.propositions.primaryRankingHigherIsBest;
        var secondaryHigherIsBetter = round.propositions.secondaryRankingHigherIsBest;
        round.ranking = round.ranking.sort(function(a, b){
            if (a.primaryPointsTotal == b.primaryPointsTotal){
                return (a.secondaryPointsTotal - b.secondaryPointsTotal) * (secondaryHigherIsBetter ? -1 : 1);
            } else {
                return (a.primaryPointsTotal - b.primaryPointsTotal) * (primaryHigherIsBetter ? -1 : 1)
            }
        });

        //if multiple matches, check whether to add matches

        //this one would be called if new match is needed
        var doAddMatch = _.bind(function(match){
            var newMatch = {
                matchNumber: match.matchNumber + 1,
                tableNumber: match.tableNumber,
                gameId: match.gameId,
                date: match.date,
                participants: []
            };

            match.participants.forEach(function(p){
                newMatch.participants.push({
                    _id: p._id,
                    seatingPosition: p.seatingPosition,
                    name: p.name
                })
            })

            round.matches.push(newMatch);
        }, this);

        if (round.propositions.multipleMatches.isMultiple){
            match.matchNumber = match.matchNumber ? match.matchNumber : 1;
            var currentMatchNumber = match.matchNumber;
            if (round.propositions.multipleMatches.isPlayedToNumberOfWins){
                //check number of wins
                var addMatch = true;
                for (var i= 0; i<match.participants.length; i++) {
                    var playerRank = round.ranking.id(match.participants[i]._id);
                    if (playerRank && (playerRank.primaryPointsTotal >= round.propositions.multipleMatches.numberOfMatchesOrWins)){
                        addMatch = false
                        break;
                    }
                }
                if (addMatch)
                    doAddMatch(match);
            } else {
                if (currentMatchNumber < round.propositions.multipleMatches.numberOfMatchesOrWins){
                    doAddMatch(match);
                }
            }
        }
    }
    eventTournamentSchema.methods.getPreviousRound = function(round){
        var roundId = round._id;
        if (roundId != 1){
            var previousRoundId = roundId - 1;
            previousRound = this.rounds.id(previousRoundId);

            if (previousRound == null)
                throw new Error("Invalid state. Round " + previousRoundId + " doesn't exists");
            return previousRound;
        } else
            return null;
    }
    eventTournamentSchema.methods.generateMatches = function(roundId){
        var round = this.rounds.id(roundId);

        if (round.matches.length > 0)
            throw new Error("Matches already generated. Delete matches if you want to generate again.");

        var previousRound = this.getPreviousRound(round);

        //1. table rearrangement
        rearrangementFunctions[round.propositions.tableReposition](this, round);

        //2.tableseating
        seatingFunctions[round.propositions.seating](this, round);

    }
    eventTournamentSchema.methods.endRound = function(roundId){
        var round = this.rounds.id(roundId);

        //1. adjust round flags
        round.isFinished = true;

        //2. cutoff
        cutoffFunctions[round.propositions.cutoffType](round);

        //3. update tournament ranking
        var aliveRank = 1;
        var eliminatedRank = round.ranking.length - round.propositions.cutoff + 1;

        this.participants.forEach(function(p) {
            p.totalPrimaryPoints = null;
            p.totalSecondaryPoints = null;
        })

        round.ranking.forEach(_.bind(function(rank){
            var participantId = rank._id;
            var participant = this.participants.id(participantId);

            participant.isEliminated = rank.isEliminated;
            participant.eliminationRoundNumber = participant.isEliminated ? round._id : null;
            participant.finalRanking = participant.isEliminated ? eliminatedRank++ : aliveRank++;
            participant.totalPrimaryPoints = rank.primaryPointsTotal;
            participant.totalSecondaryPoints = rank.secondaryPointsTotal;
        }, this));

        this.participants = this.participants.sort(function(a, b){
            return a.finalRanking - b.finalRanking;
        });
    }
    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/

    /************************ HELPERS ********************************/
    var randomInt = function (low, high) {
        return Math.floor(Math.random() * (high - low) + low);
    }

    var rearrangementFunctions = {
        random: function(tournament, round){
            var remainingParticipants = tournament.getRemainingParticipants()
            var matches = this.getTablesWithNumberOfPlayers(round, remainingParticipants);

            for (var i=0; i<remainingParticipants.length; i++){
                var tableId = randomInt(0, matches.length);
                var match = matches[tableId];

                match.participants.push({
                    _id: remainingParticipants[i]._id,
                    name: remainingParticipants[i].name
                })

                match.numberOfPlayers--;
                if (match.numberOfPlayers==0){
                    round.matches.push(match);
                    matches.splice(tableId, 1);
                }

            }
        },
        byRank: function(tournament, round){
            var remainingParticipants = tournament.getRemainingParticipants();
            _.sortBy(remainingParticipants, 'finalRanking');
            var matches = this.getTablesWithNumberOfPlayers(round, remainingParticipants);

            for (var i=0; i<remainingParticipants.length; i++){
                var match = matches[0];

                match.participants.push({
                    _id: remainingParticipants[i]._id,
                    name: remainingParticipants[i].name
                })

                match.numberOfPlayers--;
                if (match.numberOfPlayers==0){
                    round.matches.push(match);
                    matches.splice(0, 1);
                }

            }
        },
        snake: function(tournament, round){
            var remainingParticipants = tournament.getRemainingParticipants();
            _.sortBy(remainingParticipants, 'finalRanking');
            var matches = this.getTablesWithNumberOfPlayers(round, remainingParticipants);

            var direction = 1;
            var currentTable = 0;
            for (var i=0; i<remainingParticipants.length; i++){
                var match = matches[currentTable];

                match.participants.push({
                    _id: remainingParticipants[i]._id,
                    name: remainingParticipants[i].name
                })

                if (currentTable == 0 && direction == -1){
                    direction = 0;
                } else if (currentTable == matches.length -1 && direction ==1 ){
                    direction = 0;
                } else if (direction == 0 && currentTable == 0){
                    direction = 1;
                } else if (direction == 0 && currentTable == matches.length -1){
                    direction = -1;
                }

                currentTable += direction;

            }
            round.matches = matches;
        },
        copyPrevious: function(tournament, round){
            var remainingParticipants = tournament.getRemainingParticipants();
            var matches = this.getTablesWithNumberOfPlayers(round, remainingParticipants);

            var previousRound = tournament.getPreviousRound(round);

            for (var i=0; i<previousRound.matches.length; i++){
                for (var j=0; j<previousRound.matches[i].participants.length; j++){
                    var participant = tournament.participants.id(previousRound.matches[i].participants[j]._id);

                    if (!participant.isEliminated){
                        matches[previousRound.matches[i].tableNumber - 1].participants.push({
                            _id: participant._id,
                            name: participant.name
                        });
                    }
                }
            }

            round.matches = matches;
        },
        knockout: function(tournament, round){
            throw new Error("not implemented");
        },
        pick: function(tournament, round){
            for (var i = 1; i <= round.propositions.numberOfTables; i++) {
                var match = {
                    tableNumber: i,
                    participants: []
                }

                round.matches.push(match);
            }
        },
        roundRobin: function(tournament, round){
            //determine number of previous round robin rounds
            var previousRound = tournament.getPreviousRound(round);
            var numberOfPreviousRoundRobinRounds = 0;
            while (previousRound){
                if (previousRound.propositions.tableReposition == 'roundRobin'){
                    numberOfPreviousRoundRobinRounds++;
                    previousRound = tournament.getPreviousRound(previousRound);
                } else {
                    previousRound = null;
                }
            }

            //set array according to roundRobin code
            var remainingParticipants = tournament.getRemainingParticipants();
            if (numberOfPreviousRoundRobinRounds === 0){
                var randomizedList = getShuffledIntegerList(1, remainingParticipants.length)
                for (var j=0;j<remainingParticipants.length;j++){
                    remainingParticipants[j].roundRobinCode = randomizedList.shift();
                }
            }

            remainingParticipants.sort(function(a, b){
                return a.roundRobinCode - b.roundRobinCode;
            });

            if (remainingParticipants.length % 2 == 1)
                remainingParticipants.push("bye");

            //shift players to get to correct round
            for (var i=1; i<=numberOfPreviousRoundRobinRounds;i++){
                var last = remainingParticipants.pop();
                remainingParticipants.splice(1, 0, last);
            }

            //set tables
            var addOne = 1;
            for (var i=0; i<remainingParticipants.length/2; i++){
                if (remainingParticipants[i] != "bye" && remainingParticipants[remainingParticipants.length-(i+1)] != "bye"){
                    var match = {
                        tableNumber: i+addOne,
                        participants: [
                            {
                                _id: remainingParticipants[i]._id,
                                name: remainingParticipants[i].name
                            },
                            {
                                _id: remainingParticipants[remainingParticipants.length-(i+1)]._id,
                                name: remainingParticipants[remainingParticipants.length-(i+1)].name
                            }
                        ]
                    }
                    round.matches.push(match);
                } else {
                    addOne = 0;
                }

            }
        },
        getTablesWithNumberOfPlayers: function (round, remainingParticipants) {
            var numberOfPlayersPerTable = Math.floor(remainingParticipants.length / round.propositions.numberOfTables);
            var diff = remainingParticipants.length - numberOfPlayersPerTable * round.propositions.numberOfTables;

            var matches = [];
            for (var i = 1; i <= round.propositions.numberOfTables; i++) {
                var match = {
                    tableNumber: i,
                    numberOfPlayers: numberOfPlayersPerTable,
                    participants: []
                }

                if (round.propositions.largerPlayerCountOnLowerTables) {
                    if (i >= round.propositions.numberOfTables - diff + 1) {
                        match.numberOfPlayers++;
                    }

                } else {
                    if (i <= diff) {
                        match.numberOfPlayers++;
                    }
                }
                matches.push(match);
            }
            return matches;
        }
    }
    var seatingFunctions = {
        random: function(tournament, round){
            for (var i=0;i<round.matches.length;i++){
                var randomizedList = getShuffledIntegerList(1, round.matches[i].participants.length)
                for (var j=0;j<round.matches[i].participants.length;j++){
                    round.matches[i].participants[j].seatingPosition = randomizedList.shift();
                }
            }
        },
        byRank: function(tournament, round){
            for (var i=0;i<round.matches.length;i++){
                round.matches[i].participants.forEach(function(p){
                    p.seatingPosition = tournament.participants.id(p._id).finalRanking;
                });

                _.sortBy(round.matches[i].participants, 'seatingPosition');

                for (var j=0;j<round.matches[i].participants.length;j++){
                    round.matches[i].participants[j].seatingPosition = j+1;
                }
            }
        }
    }
    var rankingFunctions = {
        rankPoints: function(round, participants, property){
            for (var i = 0; i<participants.length; i++){
                var row = round.propositions.rankPointsDistribution.id(participants[i].rank);
                if (null == row)
                    throw new Error("Number of points for this rank (" + participants[i].rank + ") is not defined in rank table.");
                participants[i][property] =  row.points;
            }
        },
        scorePoints: function(round, participants, property){
            for (var i = 0; i<participants.length; i++){
                participants[i][property] = participants[i].result;
            }
        },
        percentageOfPointsInGame: function(round, participants, property){
            var totalPoints = _.reduce(participants, function(memo, p){
                return memo + p.result;
            }, 0);

            for (var i = 0; i<participants.length; i++){
                participants[i][property] = Math.round(participants[i].result*100*100/totalPoints)/100;
            }
        },
        normalizedPercentageOfPoints: function(round, participants, property){
            var firstPlayer = _.find(participants, function(p){
                return p.winner;
            });

            if (!firstPlayer){
                throw new Error("No winner set for this match. Please add one or more winners.")
            }

            var firstPlayerPoints = firstPlayer.result;

            for (var i = 0; i<participants.length; i++){
                participants[i][property] = Math.round(participants[i].result*100*100/firstPlayerPoints)/100;
            }
        },
        wins: function(round, participants, property){
            for (var i = 0; i<participants.length; i++){
                participants[i][property] = participants[i].winner ? 1 : 0;
            }
        },
        byDirectMatch: function(round, participants, property, tournament){
            // this tiebreaking is made after calculating complete ranking
            for (var i = 0; i<participants.length; i++){
                participants[i][property] = 0;
            }
        }
    }
    var carryoverTypeFunction = {
        continue: function(round, previousRoundScore){
            return previousRoundScore;
        },
        reset: function(round, previousRoundScore){
            return 0;
        },
        carryover: function(round, previousRoundScore){
            return previousRoundScore * round.propositions.carryover;
        }
    }
    var cutoffFunctions = {
        byRankAll: function(round){
            for (var i=0; i<round.ranking.length; i++){
                if (i+round.propositions.cutoff >= round.ranking.length)
                    round.ranking[i].isEliminated = true;
                else
                    round.ranking[i].isEliminated = false;
            }

        },
        byRankTable: function(round){
            round.matches.forEach(function(match){
                for (var i=0; i<match.participants.length; i++){
                    var participant = match.participants[i];

                    if (participant.rank+round.propositions.cutoff > match.participants.length)
                        round.ranking.id(participant._id).isEliminated = true;
                    else
                        round.ranking.id(participant._id).isEliminated = false;
                }
            });
        }
    }

    var getShuffledIntegerList = function(from, to){
        var unshuffledList = []
        for (var i=from; i<= to; i++){
            unshuffledList.push(i);
        }

        var shuffledList = [];
        while (unshuffledList.length > 0){
            var j = randomInt(0, unshuffledList.length);
            shuffledList.push(unshuffledList[j]);
            unshuffledList.splice(j, 1);
        }
        return shuffledList;
    }

    var EventTournament;

    try {
        EventTournament = mongoose.model('event.tournament', eventTournamentSchema);
    }
    catch (e) {
        EventTournament = mongoose.model('event.tournament');
    }

    return EventTournament;
}
