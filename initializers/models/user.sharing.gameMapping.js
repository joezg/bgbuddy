var _ = require('underscore');
var Q = require('q');

exports.get = function(mongoose){
   /************************ SCHEMA *********************************/
    var userSharingGameMappingSchema = new mongoose.Schema({
        "userId": { type: String, required: true },
        "sourceUserId": { type: String, required: false },
        "eventId": { type: mongoose.Schema.Types.ObjectId, required: false },
        "mappings": [
            {
                sourceId: { type: mongoose.Schema.Types.ObjectId, required: true },
                gameId: { type: mongoose.Schema.Types.ObjectId, required: false },
                doAddNew: { type: Boolean, required: false },
                newGameName: { type: String, required: false }
            }
        ]
    }, {collection: 'user.sharingGameMapping'});
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/
    /************************ METHODS *******************************/


    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/
    /************************ VALIDATORS *****************************/
    /************************ END VALIDATORS *************************/
    var UserSharingGameMapping;

    try {
        UserSharingGameMapping = mongoose.model('user.sharingGameMapping', userSharingGameMappingSchema);
    }
    catch (e) {
        UserSharingGameMapping = mongoose.model('user.sharingGameMapping');
    }

    return UserSharingGameMapping;
}
