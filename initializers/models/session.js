var crypto = require('crypto');
var randomInt = function (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}

exports.get = function(mongoose, api){
    /************************ SCHEMA *********************************/
    var sessionSchema = new mongoose.Schema({
        "_id": { type: String, required: true },
        "lastActionTime": { type: Date, index : { expires : 7200 /* 2 hours */ } },
        "googleUserId": { type: String },
        "facebookUserId": { type: String },
        "username": { type: String },
        "isUnconnected": { type: Boolean}
    }, {collection: 'session'});

    /************************ VALIDATORS *****************************/
    /************************ END VALIDATORS *************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/

    /************************ METHODS *******************************/
    sessionSchema.methods.resetTime = function(next){
        this.save(); //in pre save lastActionTime is reset
    }
    /************************ END METHODS ***************************/

    /************************ STATICS *******************************/
    var initSession = function(username, googleUserId, facebookUserId, isUnconnected, next){
        var sessionId = crypto.randomBytes(randomInt(64, 73)).toString('hex');

        var session = this({
            _id: sessionId,
            loggedIn: true
        });
        if (username)
            session.username = username;

        if (googleUserId)
            session.googleUserId = googleUserId;

        if (facebookUserId)
            session.facebookUserId = facebookUserId;

        if (isUnconnected)
            session.isUnconnected = true;

        session.save(function(err){
            if (err)
                next(err);
            else
                next(null, sessionId);
        });
    };
    sessionSchema.statics.initUnconnectedGoogleSession = function(userId, next){
        initSession.call(this, null, userId, null, true, next);
    };
    sessionSchema.statics.initUnconnectedFacebookSession = function(userId, next){
        initSession.call(this, null, null, userId, true, next);
    };
    sessionSchema.statics.logIn = function(username, next){
        initSession.call(this, username, null, null, false, next);
    };
    sessionSchema.statics.logOut = function(sessionId, next){
        this.findById(sessionId, function(err, session){
            session.remove(next);
        });
    };
    sessionSchema.statics.load = function(sessionId, next){
        this.findById(sessionId, next);
    };
    /************************ END STATICS ***************************/
    /************************ MIDDLEWARE *****************************/
    sessionSchema.pre('save', function(next){
        this.lastActionTime = new Date();

        if (!this.googleUserId && !this.username && !this.facebookUserId){
            var err = new Error('One property of googleUserId, facebookUserId or username must be set');
            next(err);
        }
        else {
            next();
        }
    });
    /************************ END MIDDLEWARE *************************/

    var Session;

    try {
        Session = mongoose.model('session', sessionSchema);
    }
    catch (e) {
        Session = mongoose.model('session');
    }

    return Session;
}

