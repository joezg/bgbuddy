var _ = require('underscore');
var Q = require('q');

exports.get = function(mongoose){
   /************************ SCHEMA *********************************/
    var userSharingPlayerMappingSchema = new mongoose.Schema({
        "userId": { type: String, required: true },
        "sourceUserId": { type: String, required: false },
        "eventId": { type: mongoose.Schema.Types.ObjectId, required: false },
        "mappings": [
            {
                sourceId: { type: mongoose.Schema.Types.ObjectId, required: true },
                buddyId: { type: mongoose.Schema.Types.ObjectId, required: false },
                isAnonymous: { type: Boolean, required: false },
                isMe: { type: Boolean, required: false },
                doAddNew: { type: Boolean, required: false },
                newBuddyName: { type: String, required: false },
                importAllSourceMatches: { type: Boolean, default: false }
            }
        ]
    }, {collection: 'user.sharingPlayerMapping'});
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/
    /************************ METHODS *******************************/


    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/
    /************************ VALIDATORS *****************************/
    /************************ END VALIDATORS *************************/
    var UserSharingPlayerMapping;

    try {
        UserSharingPlayerMapping = mongoose.model('user.sharingPlayerMapping', userSharingPlayerMappingSchema);
    }
    catch (e) {
        UserSharingPlayerMapping = mongoose.model('user.sharingPlayerMapping');
    }

    return UserSharingPlayerMapping;
}
