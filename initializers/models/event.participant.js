var Q = require("q");

exports.get = function(mongoose, api){
    /************************ SCHEMA *********************************/
    var eventParticipantSchema = new mongoose.Schema({
        participantName: { type: String, required: true},
        participantShortName: { type: String, required: false},
        participantId: { type: mongoose.Schema.Types.ObjectId, required: true },
        participantCode: { type: String, required: true},
        userId: { type: String, required: false },
        eventName: { type: String, required: true},
        eventId: { type: mongoose.Schema.Types.ObjectId, required: true },
        eventOrganizerId: { type: mongoose.Schema.Types.ObjectId, required: true }
    }, {collection: 'event.participant'});
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/
    /************************ METHODS *******************************/
    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/

    var EventParticipant;

    try {
        EventParticipant = mongoose.model('event.participant', eventParticipantSchema);
    }
    catch (e) {
        EventParticipant = mongoose.model('event.participant');
    }

    return EventParticipant;
}
