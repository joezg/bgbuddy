exports.get = function(mongoose){
    /************************ SCHEMA *********************************/
    var eventMatchSchema = new mongoose.Schema({
        eventId: { type: mongoose.Schema.Types.ObjectId, required: true },//eventId
        game: { type: String, required: true},
        gameId: { type: String, required: true },
        date: { type: Date, required: true },
        players: [{
            name: { type: String, required: true},
            playerId: { type: String, required: true },
            result: { type: Number },
            rank: { type: Number },
            winner: { type: Boolean }
        }]
    }, {collection: 'event.match'});
    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ METHODS *******************************/
    /************************ END VIRTUALS ***************************/
    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/

    var EventMatch;

    try {
        EventMatch = mongoose.model('event.match', eventMatchSchema);
    }
    catch (e) {
        EventMatch = mongoose.model('event.match');
    }

    return EventMatch;
}
