var validator = require('validator');
var hash = require('../../util/hash');
var Step = require('step');

exports.get = function(mongoose ,api){
    /************************ SCHEMA *********************************/
    var userSchema = new mongoose.Schema({
        "_id": { type: String, required: true },
        "username": { type: String, required: true },
        "passwordHash": { type: String, required: true },
        "userWithoutPassword": { type: Boolean, default: false },
        "email": { type: String, required: true },
        "memberSince": { type: Date, default: Date.now },
        "isConfirmed":  { type: Boolean, default: false },
        "color": { type: String, required: false },
        "theme": { type: String, required: false },
        "cookieDisclaimerDismissed": { type: Boolean, default: false },
        "sex": { type: String, required: false } // 'f' or 'm'
    }, {collection: 'user'});
    /************************ END SCHEMA *****************************/

    /************************ GETTERS / SETTERS **********************/
    userSchema.path('username').set(function (username){
        this._id = username.toLowerCase();
        return username;
    });
    userSchema.path('userWithoutPassword').set(function(value){
       if (value){
           this.passwordHash = "userWithoutPassword";
       }
       return value;
    });
    /************************ END GETTERS / SETTERS ******************/

    /************************ VIRTUALS *******************************/
    userSchema.virtual('password').get(function (){
        return this._password;
    })
    userSchema.virtual('password').set(function (password){
        this.passwordHash = hash.calculatePasswordHash(password);
        this._password = password;
    });
    /************************ END VIRTUALS ***************************/

    /************************ METHODS *******************************/
    userSchema.methods.authenticate = function(password, next){
        if (this.userWithoutPassword){
            next(false);
        } else {
            hash.checkPasswordHash(password, this.passwordHash, next);
        }
    }
    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ VALIDATORS *****************************/
    userSchema.path('passwordHash').validate(function(v) {
        if (this._password && !this.userWithoutPassword) {
            if (!validator.isLength(this._password, 6)) {
                this.invalidate('password', 'password must be at least 6 characters.');
            }
        }

        if (this.isNew && !this._password && !this.userWithoutPassword) {
            this.invalidate('password', 'password required');
        }
    });
    userSchema.path('username').validate(function(v) {
        if (!validator.isLength(v, 1, 100)) {
            this.invalidate('username', 'username must be less than 100 characters');
        }
    });
    userSchema.path('email').validate(function(v) {
        if (!validator.isEmail(v)) {
            this.invalidate('email', 'email must be valid email address');
        }
    });
    /************************ END VALIDATORS *************************/

    /************************ MIDDLEWARE *****************************/
    userSchema.pre('save', function(next){
        var self = this;
        if (this.isNew){
            Step(
                function saveConfirmation(){
                    if (!self.userWithoutPassword){
                        var UserConfirmation = api.mongo.userConfirmation;
                        var uc = new UserConfirmation({ user: self });
                        uc.save(this);
                    } else {
                        this(null);
                    }
                },
                function saveRole(err){
                    if (!err){
                        var UserRoles = api.mongo.userRoles;
                        var ur = new UserRoles({ _id: self._id })
                        ur.save(next);
                    } else {
                        next(err);
                    }
                }
            )
        } else {
            next(null);
        }
    })
    /************************ END MIDDLEWARE *************************/

    var User;

    try {
        User = mongoose.model('user', userSchema);
    }
    catch (e) {
        User = mongoose.model('user');
    }

    return User;
}
