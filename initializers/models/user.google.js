exports.get = function(mongoose){
    /************************ SCHEMA *********************************/
    var userGoogleSchema = new mongoose.Schema({
        "_id": { type: String, required: true },
        "username": { type: String},
        "accessToken": { type: String }
    }, {collection: 'user.google'});
    /************************ END SCHEMA *****************************/

    /************************ GETTERS / SETTERS **********************/
    /************************ END GETTERS / SETTERS ******************/

    /************************ VIRTUALS *******************************/
    /************************ END VIRTUALS ***************************/

    /************************ METHODS *******************************/
    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ VALIDATORS *****************************/
    /************************ END VALIDATORS *************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/

    var UserGoogle;

    try {
        UserGoogle = mongoose.model('user.google', userGoogleSchema);
    }
    catch (e) {
        UserGoogle = mongoose.model('user.google');
    }

    return UserGoogle;
}
