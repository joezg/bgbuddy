exports.get = function(mongoose){
    /************************ SCHEMA *********************************/
    var eventOrganizerLibraryGameSchema = new mongoose.Schema({
        code: { type: String, required: true },
        name: { type: String, required: true},
        owner: { type: String },
        isBoxed: { type: Boolean, default: false},
        box: { type: String },
        location: { type: String },
        comment: { type: String },
        eventOrganizerId: { type: mongoose.Schema.Types.ObjectId, required: true },
        eventId: { type: mongoose.Schema.Types.ObjectId },
        notOwnedByOrganizer: { type: Boolean, default: false}
    }, {collection: 'eventOrganizer.library.game'});

    /************************ END SCHEMA *****************************/

    /************************ VIRTUALS *******************************/
    /************************ METHODS *******************************/
    /************************ END VIRTUALS ***************************/
    /************************ END METHODS ***************************/

    /************************ STATICS ********************************/
    /************************ END STATICS ****************************/

    /************************ MIDDLEWARE *****************************/
    /************************ END MIDDLEWARE *************************/

    var EventOrganizerLibraryGame;

    try {
        EventOrganizerLibraryGame = mongoose.model('eventOrganizer.library.game', eventOrganizerLibraryGameSchema);
    }
    catch (e) {
        EventOrganizerLibraryGame = mongoose.model('eventOrganizer.library.game');
    }

    return EventOrganizerLibraryGame;
}
