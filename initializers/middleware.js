var _ = require('underscore');

exports.middleware = function(api, next){

    var verbCheckMiddleware = function(connection, actionTemplate, next){
        if (!actionTemplate.verb){
            connection.error = "This action is invalid, because the verb isn't set";
            next(connection, false);
        } else if (connection.rawConnection.method.toLowerCase() != actionTemplate.verb.toLowerCase()) {
            connection.error = "Wrong verb is used for this action";
            connection.rawConnection.responseHttpCode = 405;
            next(connection, false);
        } else {
            next(connection, true);
        }
    }

    var disabledActionMiddleware = function(connection, actionTemplate, next){
        if (actionTemplate.disabled) {
            connection.error = "This action is disabled";
            next(connection, false);
        } else if (actionTemplate.inDev && !api.config.general.developmentMode){
            connection.error = "This action is available only in development mode";
            next(connection, false);
        } else {
            next(connection, true);
        }
    }

    var sessionMiddleware = function(connection, actionTemplate, next){
        var sessionId = connection.params.sessionId;

        if (sessionId){
            api.mongo.session.load(sessionId, function(err, session){
                if (err){
                    connection.error = err;
                    next(connection, false);
                } else {
                    connection.session = session;
                    if (session){
                        if (session.username)
                            connection.currentUser = session.username;

                        if (session.googleUserId)
                            connection.googleUserId = session.googleUserId;

                        if (session.facebookUserId)
                            connection.facebookUserId = session.facebookUserId;

                        connection.sessionId = sessionId;
                        session.resetTime();
                    }
                    next(connection, true);
                }
            });
        } else {
            next(connection, true);
        }
    }

    var authenticationMiddleware = function(connection, actionTemplate, next){
        if (actionTemplate.public) {
            next(connection, true);
        } else {
            if (!connection.session){
                connection.error = "Not authenticated";
                connection.rawConnection.responseHttpCode = 401;
                next(connection, false);
                return;
            } else if (connection.session.isUnconnected){
                connection.error = "Unconnected session";
                next(connection, false);
            } else {
                next(connection, true);
            }
        }
    }

    var roleAuthorizationMiddleware = function(connection, actionTemplate, next){
        if (actionTemplate.public) {
            next(connection, true);
        } else {
            var actionRole = actionTemplate.role || 'admin';
            var username = connection.currentUser || '';

            api.mongo.userRoles.checkRole(username, actionRole,
                function(err, hasRole){
                    if (err) {
                        connection.error = err;
                        next(connection, false);
                    } else {
                        if (hasRole){
                            next(connection, true);
                        } else {
                            connection.error = 'Insufficient privileges';
                            next(connection, false);
                        }
                    }
                }
            );
        }
    }

    var eventAdministratorActionAuthorization = function(connection, actionTemplate, next){
        if (actionTemplate.eventAdmin == true) {
            var eventOrganizerId = connection.params.eventOrganizerId;

            if (!eventOrganizerId){
                connection.error = 'This action cannot be called without eventOrganizerId parameter';
                next(connection, false);
            } else {
                api.mongo.eventOrganizer.isUserAdmin(connection.currentUser, eventOrganizerId,
                    function(err, isAdmin){
                        if (err) {
                            connection.error = err;
                            next(connection, false);
                        } else {
                            if (!isAdmin){
                                connection.error = "Not an admin of requested event (organizer)";
                                next(connection, false);
                            } else {
                                next(connection, true);
                            }
                        }
                    })
            }
        } else {
            next(connection, true);
        }

    }

    var actionLog = function(connection, actionTemplate) {
        if (api.mongo && api.mongo.actionLog){
            var actionLog = new api.mongo.actionLog();
            
            actionLog.date = new Date();
            actionLog.currentUser = connection.currentUser;
            actionLog.actionName = actionTemplate.name;
            actionLog.connection = _.pick(connection, 'params', 'id', 'remoteIP', 'remotePort', 'type', 'response', 'action');
            actionLog.connection.params = _.omit(actionLog.connection.params, 'password');
            
            actionLog.save();
        }
    }
    
    var actionLogPostProcessMiddleware = function(connection, actionTemplate, toRender, next){
        actionLog(connection, actionTemplate);
        
        next(connection);
    }

//    var actionLogPreProcessMiddleware = function(connection, actionTemplate, next){
//        actionLog(connection, actionTemplate);
//
//        next(connection, true);
//    }

    api.actions.addPreProcessor(disabledActionMiddleware, 10);
    api.actions.addPreProcessor(verbCheckMiddleware, 20);
    api.actions.addPreProcessor(sessionMiddleware, 30);
    api.actions.addPreProcessor(authenticationMiddleware, 40);
    api.actions.addPreProcessor(roleAuthorizationMiddleware, 50);
    api.actions.addPreProcessor(eventAdministratorActionAuthorization, 60);
    //api.actions.addPreProcessor(actionLogPreProcessMiddleware, 60);
    api.actions.addPostProcessor(actionLogPostProcessMiddleware, 10)

    next();
}