var os = require('os');

exports.errorReporters = function(api, next){
    var mongoErrorReporter = function(type, err, extraMessages, severity){
        if (api.mongo && api.mongo.error){
            var error = new api.mongo.error();
            
            error.type = type;
            error.stack = err.stack.split(os.EOL);
            error.date = new Date();
            error.severity = severity;
            error.extraMessages = extraMessages;
            
            error.save();
            
        }
    }

    if (api.config.general.developmentMode)
        api.exceptionHandlers.reporters.push(mongoErrorReporter);   
    else
        api.exceptionHandlers.reporters = [mongoErrorReporter]
    
    next();
}