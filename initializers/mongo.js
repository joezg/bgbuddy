var mongoose = require('mongoose');

exports.mongo = function(api, next){
    api.mongoStarter = {};
    api.mongoStarter._start = function(api, next){
        mongoose.connect(api.config.general.mongoConnectionString);

        var conn = mongoose.connection;
        conn.on('error', console.error.bind(console, 'connection error:'));
        conn.once('open', function callback() {
            console.log('Connection to mongodb opened');
            var mongo = {
                mongoose:  mongoose,
                user: require('./models/user.js').get(mongoose, api),
                matchPlayer: require('./models/match.player.js').get(mongoose, api),
                userMatch: require('./models/user.match.js').get(mongoose, api),
                userGame: require('./models/user.game.js').get(mongoose, api),
                userBuddy: require('./models/user.buddy.js').get(mongoose, api),
                userMessages: require('./models/user.messages.js').get(mongoose, api),
                userConfirmation: require('./models/user.confirmation.js').get(mongoose, api),
                userRememberMe: require('./models/user.rememberMe.js').get(mongoose),
                userPasswordRecovery: require('./models/user.passwordRecovery.js').get(mongoose, api),
                userRoles: require('./models/user.roles.js').get(mongoose),
                userGoogle: require('./models/user.google.js').get(mongoose),
                userFacebook: require('./models/user.facebook.js').get(mongoose),
                userSharingPlayerMapping: require('./models/user.sharing.playerMapping.js').get(mongoose),
                userSharingGameMapping: require('./models/user.sharing.gameMapping.js').get(mongoose),
                session: require('./models/session.js').get(mongoose),
                event: require('./models/event.js').get(mongoose, api),
                eventMatch: require('./models/event.match.js').get(mongoose),
                eventParticipant: require('./models/event.participant.js').get(mongoose),
                eventOrganizer: require('./models/eventOrganizer.js').get(mongoose, api),
                eventOrganizerParticipant: require('./models/eventOrganizer.participant.js').get(mongoose),
                eventOrganizerLibraryGame: require('./models/eventOrganizer.library.game.js').get(mongoose),
                eventOrganizerLibraryBox: require('./models/eventOrganizer.library.box.js').get(mongoose),
                eventTournament: require('./models/event.tournament.js').get(mongoose),
                eventMarathon: require('./models/event.marathon.js').get(mongoose, api),
                error: require('./models/error.js').get(mongoose),
                actionLog: require('./models/actionLog.js').get(mongoose)
            };

            //mongoose.set('debug', true);
            api.mongo = mongo;
        });

        next();
    };

    api.mongoStarter._stop =  function(api, next){
        api.mongo.mongoose.connection.close();
        next();
    };

    next();
}
