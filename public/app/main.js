requirejs.config({
    paths: {
        'text': '../lib/require/text',
        'durandal':'../lib/durandal/js',
        'plugins' : '../lib/durandal/js/plugins',
        'transitions' : '../lib/durandal/js/transitions',
        'knockout': '../lib/knockout/knockout-3.2.0.debug',
        'bootstrap': '../lib/bootstrap/js/bootstrap',
        'jquery': '../lib/jquery/jquery-1.11.1',
        'moment': '../lib/moment/moment-with-locales.min',
        'numeral': '../lib/numeral/numeral.min',
        'q': '../lib/q/q',
        'jquery-color': '../lib/jquery-color/jquery.color',
        'bootstrap-multiselect': '../lib/bootstrap-multiselect/bootstrap-multiselect',
        'bootstrap-datetimepicker': '../lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker',
        'bootstrap-switch': '../lib/bootstrap-switch-master/js/bootstrap-switch.min',
        'jsBarcode': '../lib/jsBarcode/JsBarcode.all.min',
        'i18next': '../lib/i18next/i18next.amd.withJQuery-1.7.3',
        'underscore': "../lib/underscore/underscore",
        'lodash': "../lib/lodash/lodash.min",
        'pdfkit': "../lib/pdfkit/pdfkit",
        'blobStream': "../lib/pdfkit/blob-stream",
        'draggabilly': "../lib/draggabilly/draggabilly.pkgd",
        'typeahead': '../lib/typeahead.js/typeahead.bundle',
        'gapi': 'https://apis.google.com/js/client:platform',
        'facebook': '//connect.facebook.net/en_US/sdk',

        'modals': 'ui/modals',
        'model': 'model',
        'dao': 'dao',
        'util': 'util'
    },
    shim: {
        'bootstrap': {
            deps: ['jquery']
        },
        'jquery-color': {
            deps: ['jquery']
        },
        'bootstrap-multiselect': {
            deps: ['bootstrap', 'jquery', 'knockout']
        },
        'bootstrap-switch': {
            deps: ['jquery']
        },
        'jsBarcode': {
            deps: ['jquery']
        },
        'typeahead': {
            deps: ['jquery']
        },
        'gapi': {
            deps: ['jquery']
        },
        'facebook' : {
            exports: 'FB'
        }
    },
    waitSeconds: 0
});

requirejs.onError = function(err){
    var msg = document.getElementById("requirejs_error");
    var errTxt = document.getElementById("error_text");
    if (errTxt){
        errTxt.innerHTML = err.toString();
        msg.style.display = 'block';
    } else {
        console.dir(err);
    }

}

define(
    ['durandal/system', 'durandal/app', 'durandal/binder', 'durandal/viewLocator', 'q', 'i18next', 'moment', 'plugins/widget', 'knockout', 'util/customBindings'],
    function (system, app, binder, viewLocator, Q, i18n, moment, widget, ko){
        system.debug(true);

        app.configurePlugins({
            router:true,
            dialog: true,
            widget: {
                kinds: ['hub', 'scoreField', 'searchList', 'dateTimePicker', 'scoreTable', 'editableField', 'panel', 'pdf']
            }
        });

        //overriding widget functions for having widgets in different folder
        var mapWidget = function(kind) {
            return 'ui/widgets/' + kind;
        };
        widget.convertKindToModulePath = mapWidget;
        widget.convertKindToViewPath = mapWidget;

        //plugging Q promise mechanism to durandal
        system.defer = function (action) {
            var deferred = Q.defer();
            action.call(deferred, deferred);
            var promise = deferred.promise;
            deferred.promise = function() {
                return promise;
            };

            return deferred;
        };

        var supportedLangs = ['en', 'hr'];
        var lang = window.navigator.userLanguage || window.navigator.language;

        if (supportedLangs.indexOf(lang) == -1 || !lang)
            lang = 'en'

        var i18NOptions = {
            detectFromHeaders: false,
            lng: lang,
            fallbackLng: 'en',
            ns: 'app',
            resGetPath: 'locales/__lng__/__ns__.json',
            useCookie: false
        };

        ko.observableArray.fn.refresh = function (item) {
            if (item){
                var index = this['indexOf'](item);
                if (index >= 0) {
                    this.splice(index, 1);
                    this.splice(index, 0, item);
                }
            } else {
                var items = this();
                this([]);
                this(items);
            }

        }

//        ko.observable.fn.increment = function(){
//            this(this()+1);
//        }

        app.start().then(function() {

            i18n.init(i18NOptions, function () {
                moment.locale(i18n.lng());
                //Call localization on view before binding...
                binder.binding = function (obj, view) {
                    $(view).i18n();
                };

                app.title = i18n.t('app.name');

                //Replace 'viewmodels' in the moduleId with 'views' to locate the view.
                //Look for partial views in a 'views' folder in the root.
                viewLocator.useConvention();

                var sessionId = window.sessionStorage.getItem('sessionId');
                if (null !== sessionId)
                    app.sessionId = sessionId;

                app.setSession = function(sessionId, rememberMeToken, googleSignIn, facebookSignIn){
                    app.sessionId = sessionId;
                    window.sessionStorage.setItem("sessionId", sessionId);
                    if (rememberMeToken)
                        window.localStorage.setItem("rememberMeToken", rememberMeToken);
                    if (googleSignIn)
                        window.sessionStorage.setItem("googleSignIn", true);
                    if (facebookSignIn)
                        window.sessionStorage.setItem("facebookSignIn", true);
                }

                //Show the app by setting the root view model for our application with a transition.
                app.setRoot('ui/shell');
            });
        });
    }
);