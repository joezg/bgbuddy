define(['q', 'plugins/dialog', 'modals/login', 'durandal/app', 'i18next', 'jquery', 'underscore', 'dao/loginDAO'],
    function (Q, dialog, loginDlg, app, i18n, $, _, loginDAO) {
        var loginProcessStarted = false;
        var failedRequests = [];
        var startLogin = function(){
            loginProcessStarted = true;

            var loginSuccessfulCB = function(){
                loginProcessStarted = false;
                while (failedRequests.length > 0){
                    var opts = failedRequests.shift();
                    var original = opts.original;
                    original.data.sessionId = app.sessionId;

                    if (original.type !== "GET")
                        original.data = JSON.stringify(original.data);

                    $.ajax(original);
                }
            };

            var openLoginDialog = function(){
                dialog.show(loginDlg).then(function(){
                    loginSuccessfulCB();
                });
            }

            rememberMeToken = window.localStorage.getItem('rememberMeToken');
            if (rememberMeToken){
                loginDAO.rememberMeLogin(rememberMeToken).then(function(data){
                    app.setSession(data.sessionId, data.rememberMeToken);
                    loginSuccessfulCB();
                }).catch(function(){
                    openLoginDialog();
                });
            } else {
                openLoginDialog();
            }
        }


        var getDefaultJsonParam = function(){
            return {
                dataType : "json",
                contentType: 'application/json',
                error: function(err){
                    if (err.status == 401){
                        failedRequests.push(this);

                        if (!loginProcessStarted)
                            startLogin();

                    } else {
                        app.trigger('msg.new', {error:err, text:i18n.t("err.genericError"), type:"danger"});
                        app.trigger('spinner.stop');
                        this.def.reject(err);
                    }
                },
                success: function(data){
                    app.trigger('spinner.stop');
                    this.def.resolve(data);
                }
            };
        };

        return {
            httpTypes: {
                post: "POST",
                get: "GET",
                put: "PUT",
                delete: "DELETE"
            },
            startAjax: function(params){
                var d = Q.defer();
                $.extend(params, getDefaultJsonParam(), {def: d});
                if (!params.data)
                    params.data = {};
                params.data.sessionId = app.sessionId || null;
                var original = _.clone(params);
                params.original = original;

                if (params.type !== this.httpTypes.get)
                    params.data = JSON.stringify(params.data);

                $.ajax(params);
                app.trigger('spinner.start');

                return d.promise;
            },
            wrapCacheInPromise: function(data){
                var d = Q.defer();
                d.resolve(data);

                return d.promise;
            },
            cancelLogin: function(){
                //all promises dependent of login must be rejected
                while (failedRequests.length > 0){
                    var opts = failedRequests.shift();
                    opts.def.reject(new Error('login cancelled'));
                }
                app.trigger('spinner.stop');
                dialog.close(loginDlg);
            }
        };
    }
);
