define(['dao/daoUtil'],
    function (util) {

        return {
            getMessages: function() { // fetch all user messages
                var params = {
                    url: "/api/messagesGet",
                    data: {},
                    type: util.httpTypes.get
                };
                return util.startAjax(params);
            },
            getUnreadMessages: function() { // fetch unread user messages
                var params = {
                    url: "/api/messagesGetUnread",
                    data: {},
                    type: util.httpTypes.get
                };
                return util.startAjax(params);
            },
            getUnreadMessagesCount: function() { // fetch unread user messages
                var params = {
                    url: "/api/messagesGetUnreadCount",
                    data: {},
                    type: util.httpTypes.get
                };
                return util.startAjax(params);
            },
            getMessagesWithUser: function(username) { // fetch messages with particular user (inbox & sent)
                var params = {
                    url: "/api/messagesGetWithUser",
                    data: {username: username},
                    type: util.httpTypes.get
                };
                return util.startAjax(params);
            },
            getMessage: function(messageId) { // fetch user message by id

                var params = {
                    url: "/api/messageGetById",
                    data: {messageId: messageId},
                    type: util.httpTypes.get
                };
                return util.startAjax(params);
            },
            sendMessage: function(message) { // send message
                var params = {
                    url: "/api/messageSend",
                    data: {to: message.to, content:message.content},
                    type: util.httpTypes.post
                };
                return util.startAjax(params);
            },
            markAsRead: function(messageId) { // mark message with given id as read by user
                var params = {
                    url: "/api/messageMarkRead",
                    data: {messageId: messageId},
                    type: util.httpTypes.post
                };
                return util.startAjax(params);
            },
            markAllAsRead: function() { // mark all user messages as read by user
                var params = {
                    url: "/api/messageMarkAllRead",
                    data: {},
                    type: util.httpTypes.post
                };
                return util.startAjax(params);
            }

        };
    }
);


