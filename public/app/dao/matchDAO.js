define(['dao/daoUtil', 'dao/gameDAO', 'jquery'],
    function (util, gameDAO) {
        return {
            getMatchesForUser: function(count){
                var data = {}
                if (count > 0){
                    data.count = count;
                }

                var params = {
                    url: "/api/matchGetLast",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },

            getMatchesWithBuddy: function(buddyId){

                var params = {
                    url: "/api/matchesWithBuddyGet",
                    data: { buddyId: buddyId },
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },

            getMatchesForGame: function(gameId){

                var params = {
                    url: "/api/matchesForGameGet",
                    data: { gameId: gameId },
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },

            getMatchesNewerThan: function(date){

                var params = {
                    url: "/api/matchesWithinDateIntervalGet",
                    data: { from: date },
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },

            getMatchesWithinDateInterval: function(from, to){
                //console.log("Fetching matches from " + from + " to " + to);
                var params = {
                    url: "/api/matchesWithinDateIntervalGet",
                    data: { from: from, to: to},
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },


            startNewMatch: function(game, gameId, location, phaseType, phaseName, addSelf){
                var data = {
                    game: game,
                    gameId: gameId,
                    inProgress: true,
                    initialPhase: {
                        type: phaseType,
                        name: phaseName
                    },
                    addSelf: addSelf
                };

                if (location)
                    data.location = location;

                var params = {
                    url: "/api/matchAdd",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            },
            getMatch: function(id) {
                var params = {
                    url: "/api/matchGet",
                    data: { matchId: id },
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            updatePhases: function(id, phases) {
                var data = {
                    matchId: id,
                    phases: phases
                };

                var params = {
                    url: "/api/matchUpdatePhases",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            matchStartPhase: function(id, phase) {
                var data = {
                    matchId: id,
                    phaseStart: phase.start,
                    phaseType: phase.type,
                    phaseName: phase.name
                };

                var params = {
                    url: "/api/matchStartPhase",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            matchUpdateInfo: function(match) {
                var data = {
                    _id: match.id,
                    location: match.location,
                    date: match.date,
                    isGameOutOfDust: match.isGameOutOfDust,
                    isNewGame: match.isNewGame
                };

                var params = {
                    url: "/api/matchUpdateInfo",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            matchUpdatePlayers: function(id, players, doAddBuddies) {
                var data = {
                    matchId: id,
                    players: players,
                    doAddBuddies: doAddBuddies
                };

                var params = {
                    url: "/api/matchUpdatePlayers",
                    data: data,
                    type: util.httpTypes.put
                };


                return util.startAjax(params);
            },
            matchRemovePlayer: function(id, playerId) {
                var data = {
                    matchId: id,
                    playerId: playerId
                };

                var params = {
                    url: "/api/matchRemovePlayer",
                    data: data,
                    type: util.httpTypes.put
                };


                return util.startAjax(params);
            },
            finishMatch: function(id){
                var data = {
                    matchId: id
                };

                var params = {
                    url: "/api/matchFinish",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            deleteMatch: function(matchId){
                var data = {
                    _id: matchId
                }
                var params = {
                    url: "/api/matchDelete",
                    data: data,
                    type: util.httpTypes.delete
                };

                return util.startAjax(params).then(function(data){
                    gameDAO.destroyCache();
                    return data;
                });
            },
            matchUpdateGame: function(matchId, newGameId, newGameName, originalGameId){
                var data = {
                    _id: matchId,
                    newGameId: newGameId,
                    newGameName: newGameName,
                    originalGameId: originalGameId
                };

                var params = {
                    url: "/api/matchUpdateGame",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params).then(function(data){
                    gameDAO.destroyCache();
                    return data;
                });
            }
        };
    }
);

