/**
 * @module dao/login
 *
 * This is dao for login/logout
 */
define(['q', 'durandal/app' /*should not have reference to daoUtil because that would create circular reference that includes login modal*/],
    function (Q, app) {
        return {
            login: function(username, password, rememberMe, socialConnect, socialType){
                var data = { username: username, password: password, rememberMe: rememberMe };

                if (app.sessionId)
                    data.sessionId = app.sessionId;

                if (socialConnect) {
                    data.socialConnect = socialConnect;
                    data.socialType = socialType;
                }


                var params = {
                    url: "/api/logIn",
                    data: JSON.stringify(data),
                    type: "POST",
                    dataType : "json",
                    contentType: 'application/json'
                };

                return Q($.ajax(params));
            },
            googleLogin: function(code){
                var data = {token: code};
                var params = {
                    url: "/api/googleSignIn",
                    data: JSON.stringify(data),
                    type: "POST",
                    dataType : "json",
                    contentType: 'application/json'
                };

                return Q($.ajax(params));
            },
            facebookLogin: function(token){
                var data = {token: token};
                var params = {
                    url: "/api/facebookSignIn",
                    data: JSON.stringify(data),
                    type: "POST",
                    dataType : "json",
                    contentType: 'application/json'
                };

                return Q($.ajax(params));
            },
            rememberMeLogin: function(rememberMeToken){
                var params = {
                    url: "/api/rememberMeLogin",
                    data: JSON.stringify({ rememberMeToken: rememberMeToken }),
                    type: "POST",
                    dataType : "json",
                    contentType: 'application/json'
                };

                return Q($.ajax(params));
            },
            logout: function(){
                var data = { sessionId: app.sessionId }
                var rmToken = window.localStorage.getItem("rememberMeToken");
                if (rmToken)
                    data.rememberMeToken = rmToken;

                var params = {
                    url: "/api/logOut",
                    data: JSON.stringify(data),
                    type: "DELETE",
                    dataType : "json",
                    contentType: 'application/json'
                };

                return Q($.ajax(params));
            }

        };
    }
);
