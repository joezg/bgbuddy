define(['dao/daoUtil'],
    function (util) {
        return {
            getEventPlayersMapping: function(eventId){
                var params = {
                    url: "/api/sharingPlayerMappingGet",
                    data: {eventId: eventId},
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            saveEventPlayerMappings: function(mappings, eventId){
                var data = {
                    mappings: mappings,
                    eventId: eventId
                };

                var params = {
                    url: "/api/sharingPlayerMappingSave",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            },
            getUserPlayerMapping: function(userId){
                var params = {
                    url: "/api/sharingPlayerMappingGet",
                    data: {userId: userId},
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            getEventGameMapping: function(eventId){
                var params = {
                    url: "/api/sharingGameMappingGet",
                    data: {eventId: eventId},
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            saveEventGameMappings: function(mappings, eventId){
                var data = {
                    mappings: mappings,
                    eventId: eventId
                };

                var params = {
                    url: "/api/sharingGameMappingSave",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            },
        };
    }
);



