define(['dao/daoUtil'],
    function (util) {

        return {

            sendInviteEmail: function(emails, content) { // send email
                //console.log("Sending email to " + email.to);
                var params = {
                    url: "/api/inviteEmailSend",
                    data: {emails: emails, content:content},
                    type: util.httpTypes.post
                };
                return util.startAjax(params);
            }

        };
    }
);


