define(['dao/daoUtil'],
    function (util) {
        var casheEventOrganizerInfoId = null,
            casheEventOrganizerInfo = null,
            casheEventInfoId = null,
            casheEventInfo = null;

        return {
            getAllUserEvents: function(){
                var params = {
                    url: "/api/userEventGetAll",
                    data: {},
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            getAllUserAdministeredEventOrganizers: function(){
                var params = {
                    url: "/api/userAdministeredEventOrganizerGetAll",
                    data: {},
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            getLibrary: function(eventOrganizerId, onlyFromOrganizer){
                var data = {
                    eventOrganizerId: eventOrganizerId
                };
                if (onlyFromOrganizer){
                    data.onlyFromOrganizer = true;
                }

                var params = {
                    url: "/api/libraryGetAll",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            getLibraryBoxes: function(eventOrganizerId){
                var data = {
                    eventOrganizerId: eventOrganizerId
                };

                var params = {
                    url: "/api/libraryBoxGetAll",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            getBoxItem: function(eventOrganizerId, itemId){
                var data = {
                    eventOrganizerId: eventOrganizerId,
                    itemId: itemId
                };

                var params = {
                    url: "/api/boxItemGet",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },

            saveBoxItem: function(eventOrganizerId, box){
                var data = {
                    eventOrganizerId: eventOrganizerId,
                    itemId: box._id,
                    code: box.code,
                    location: box.location,
                    comment: box.comment
                };

                var params = {
                    url: "/api/boxItemSave",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            },

            addBox: function(eventOrganizerId, box){
                var data = {
                    eventOrganizerId: eventOrganizerId,
                    code: box.code,
                    comment: box.comment,
                    location: box.location
                };

                var params = {
                    url: "/api/boxAdd",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            },

            getLibraryItem: function(eventOrganizerId, itemId){
                var data = {
                    eventOrganizerId: eventOrganizerId,
                    itemId: itemId
                };

                var params = {
                    url: "/api/libraryItemGet",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            saveLibraryItem: function(eventOrganizerId, item){
                var data = {
                    eventOrganizerId: eventOrganizerId,
                    itemId: item.itemId || item._id,
                    name: item.name,
                    isBoxed: item.isBoxed,
                    owner: item.owner,
                    comment: item.comment,
                    code: item.code,
                    notOwnedByOrganizer: item.notOwnedByOrganizer,
                    eventId: item.eventId
                };

                if (item.location)
                    data.location = item.location;

                if (item.box)
                    data.box = item.box.code || item.box;

                var params = {
                    url: "/api/libraryItemSave",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            },
            getAllEventOrganizerParticipants: function(eventOrganizerId){
                var data = {
                    eventOrganizerId: eventOrganizerId
                };

                var params = {
                    url: "/api/eventOrganizerParticipantGetAll",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            getAllEventParticipants: function(eventOrganizerId, eventId){
                var data = {
                    eventId: eventId,
                    eventOrganizerId: eventOrganizerId
                };

                var params = {
                    url: "/api/eventParticipantGetAll",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            getEventOrganizerInfo: function(eventOrganizerId){
                if (eventOrganizerId == casheEventOrganizerInfoId && casheEventOrganizerInfo != null){
                    return util.wrapCacheInPromise(casheEventOrganizerInfo);
                }

                var data = {
                    eventOrganizerId: eventOrganizerId
                };

                var params = {
                    url: "/api/eventOrganizerGetInfo",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params).then(function(result){
                    casheEventOrganizerInfoId = eventOrganizerId;
                    casheEventOrganizerInfo = result;

                    return result;
                });
            },
            getEventInfo: function(eventOrganizerId, eventId){
                if (eventId == casheEventInfoId && casheEventInfo != null){
                    return util.wrapCacheInPromise(casheEventInfo);
                }

                var data = {
                    eventId: eventId,
                    eventOrganizerId: eventOrganizerId
                };

                var params = {
                    url: "/api/eventGetInfo",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params).then(function(result){
                    casheEventInfoId = eventId;
                    casheEventInfo = result;

                    return result;
                });
            },
            getAllEventOrganizerEvents: function(eventOrganizerId){
                var data = {
                    eventOrganizerId: eventOrganizerId
                };

                var params = {
                    url: "/api/eventOrganizerEventGetAll",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            getAllEventOrganizerParticipantEvents: function(eventOrganizerId, participantId){
                var data = {
                    eventOrganizerId: eventOrganizerId,
                    participantId: participantId
                };

                var params = {
                    url: "/api/eventOrganizerEventGetAllForParticipant",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            addEventOrganizerEvent: function(eventOrganizerId, event){
                var data = {
                    eventOrganizerId: eventOrganizerId,
                    name: event.name,
                    location: event.location,
                    venue: event.venue,
                    startDate: event.startDate,
                    endDate: event.endDate
                };

                var params = {
                    url: "/api/eventOrganizerEventAdd",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            getEventMatches: function(eventId){
                var data = {
                    eventId: eventId
                };

                var params = {
                    url: "/api/eventMatchGetAll",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            getAllEventMatchesForParticipants: function(eventId, participantIds){
                var data = {
                    eventId: eventId,
                    participants: JSON.stringify(participantIds)
                };

                var params = {
                    url: "/api/eventMatchGetAllForParticipants",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            addMatch: function(eventId, eventOrganizerId, match){
                var data = {
                    eventId: eventId,
                    eventOrganizerId: eventOrganizerId,
                    game: match.game,
                    gameId: match.gameId,
                    date: match.date,
                    players: match.players
                };

                var params = {
                    url: "/api/eventMatchAdd",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            },
            getParticipant: function(eventOrganizerId, itemId){
                var data = {
                    eventOrganizerId: eventOrganizerId,
                    participantId: itemId
                };

                var params = {
                    url: "/api/eventOrganizerParticipantGet",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            saveParticipant: function(eventOrganizerId, participant){
                var data = {
                    eventOrganizerId: eventOrganizerId,
                    _id: participant._id,
                    code: participant.code,
                    name: participant.name,
                    shortName: participant.shortName,
                    email: participant.email,
                    userId: participant.userId,
                    country: participant.country
                };

                var params = {
                    url: "/api/eventOrganizerParticipantSave",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            addParticipant: function(eventOrganizerId, participant){
                var data = {
                    eventOrganizerId: eventOrganizerId,
                    code: participant.code,
                    name: participant.name,
                    shortName: participant.shortName,
                    email: participant.email,
                    userId: participant.userId,
                    country: participant.country
                };

                var params = {
                    url: "/api/eventOrganizerParticipantAdd",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            },
            addParticipantToEvent: function(eventOrganizerId, eventId, participantId){
                var data = {
                    eventOrganizerId: eventOrganizerId,
                    eventId: eventId,
                    _id: participantId
                };

                var params = {
                    url: "/api/participantAddToEvent",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            removeParticipantFromEvent: function(eventOrganizerId, eventId, participantId){
                var data = {
                    eventOrganizerId: eventOrganizerId,
                    eventId: eventId,
                    _id: participantId
                };

                var params = {
                    url: "/api/participantRemoveFromEvent",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            getEventTournaments: function(eventId){
                var data = {
                    eventId: eventId
                };

                var params = {
                    url: "/api/eventTournamentGetAll",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            getEventTournament: function(eventId, tournamentId){
                var data = {
                    eventId: eventId,
                    tournamentId: tournamentId
                };

                var params = {
                    url: "/api/eventTournamentGet",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            addTournament: function(eventId, eventOrganizerId, tournament){
                var data = {
                    eventOrganizerId: eventOrganizerId,
                    eventId: eventId,
                    name: tournament.name,
                    date: tournament.date,
                    type: tournament.type
                };

                var params = {
                    url: "/api/eventTournamentAdd",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            },
            updateTournament: function(eventId, eventOrganizerId, tournamentId, tournament){
                var data = {
                    tournamentId: tournamentId,
                    eventId: eventId,
                    eventOrganizerId: eventOrganizerId,
                    name: tournament.name,
                    date: tournament.date,
                    gameName: tournament.gameName,
                    gameId: tournament.gameId,
                    type: tournament.type
                };

                var params = {
                    url: "/api/eventTournamentUpdate",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            addTournamentParticipant: function(eventId, eventOrganizerId, tournamentId, type, participant){
                var data = {
                    tournamentId: tournamentId,
                    eventId: eventId,
                    eventOrganizerId: eventOrganizerId,
                    participant: participant,
                    type: type
                };

                var params = {
                    url: "/api/eventTournamentParticipantAdd",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            removeTournamentParticipant: function(eventId, eventOrganizerId, tournamentId, type, participantId){
                var data = {
                    tournamentId: tournamentId,
                    eventId: eventId,
                    eventOrganizerId: eventOrganizerId,
                    participantId: participantId,
                    type: type
                };

                var params = {
                    url: "/api/eventTournamentParticipantRemove",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            calculateMarathonStandings: function(eventId, eventOrganizerId, marathonId){
                var data = {
                    eventId: eventId,
                    marathonId: marathonId,
                    eventOrganizerId: eventOrganizerId
                };

                var params = {
                    url: "/api/eventMarathonCalculateStandings",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            getTournamentRounds: function(eventId, tournamentId){
                var data = {
                    eventId: eventId,
                    tournamentId:tournamentId
                };

                var params = {
                    url: "/api/eventTournamentRoundGetAll",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            addTournamentRound: function(eventId, eventOrganizerId, tournamentId){
                var data = {
                    eventId: eventId,
                    tournamentId:tournamentId,
                    eventOrganizerId: eventOrganizerId
                };

                var params = {
                    url: "/api/eventTournamentRoundAdd",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            updateTournamentRoundPropositions: function(eventId, eventOrganizerId, tournamentId, roundId, propositions){
                var data = propositions;

                data.eventId = eventId;
                data.eventOrganizerId = eventOrganizerId;
                data.tournamentId = tournamentId;
                data.roundId = roundId;

                var params = {
                    url: "/api/eventTournamentRoundPropositionUpdate",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            generateTournamentRoundMatches: function(eventId, eventOrganizerId, tournamentId, roundId){
                var data = {};

                data.eventId = eventId;
                data.eventOrganizerId = eventOrganizerId;
                data.tournamentId = tournamentId;
                data.roundId = roundId;

                var params = {
                    url: "/api/eventTournamentRoundGenerateMatches",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            rearrangeTables: function(eventId, eventOrganizerId, tournamentId, roundId, matches){
                var data = {};

                data.eventId = eventId;
                data.eventOrganizerId = eventOrganizerId;
                data.tournamentId = tournamentId;
                data.roundId = roundId;
                data.matches = matches;

                var params = {
                    url: "/api/eventTournamentRearrangeTables",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            deleteTournamentRoundMatches: function(eventId, eventOrganizerId, tournamentId, roundId){
                var data = {};

                data.eventId = eventId;
                data.eventOrganizerId = eventOrganizerId;
                data.tournamentId = tournamentId;
                data.roundId = roundId;

                var params = {
                    url: "/api/eventTournamentRoundDeleteMatches",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            addTournamentRoundMatchScore: function(eventId, eventOrganizerId, tournamentId, roundId, match){
                var data = {};

                data.eventId = eventId;
                data.eventOrganizerId = eventOrganizerId;
                data.tournamentId = tournamentId;
                data.roundId = roundId;
                data.match = match;

                var params = {
                    url: "/api/eventTournamentRoundAddScore",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            finishRound: function(eventId, eventOrganizerId, tournamentId, roundId){
                var data = {};

                data.eventId = eventId;
                data.eventOrganizerId = eventOrganizerId;
                data.tournamentId = tournamentId;
                data.roundId = roundId;

                var params = {
                    url: "/api/eventTournamentRoundFinish",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            addMarathonGame: function(eventId, eventOrganizerId, marathonId, game, weight){
                var data = {};

                data.eventId = eventId;
                data.eventOrganizerId = eventOrganizerId;
                data.marathonId = marathonId;
                data.gameId = game._id;
                data.gameName = game.name;
                data.weight = weight;

                var params = {
                    url: "/api/eventMarathonGameAdd",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            removeMarathonGame: function(eventId, eventOrganizerId, marathonId, _id){
                var data = {};

                data.eventId = eventId;
                data.eventOrganizerId = eventOrganizerId;
                data.marathonId = marathonId;
                data._id = _id;

                var params = {
                    url: "/api/eventMarathonGameRemove",
                    data: data,
                    type: util.httpTypes.delete
                };

                return util.startAjax(params);
            }
        };
    }
);



