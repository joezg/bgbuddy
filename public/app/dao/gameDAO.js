define(['dao/daoUtil', 'underscore','model/cache', 'jquery'],
    function (util, _ , Cache){

        var cache = new Cache();

        return {
            destroyCache: function(){
                cache.destroy();
            },
            getGamesForUser: function(force){ // fetches ALL games for user
                if (!force && cache.isCacheValid()){
                    var res = {}
                    res.games = cache.getAll();
                    return util.wrapCacheInPromise(res);
                }
                var params = {
                    url: "/api/gamesGet",
                    data: { },
                    type: util.httpTypes.get
                };
                return util.startAjax(params).then(function(res) {
                   // console.log("Returning games from server");
                    if (res.games) {
                        cache.addOrUpdateItems(res.games);
                        cache.setCached();
                    }
                    return res;
                });
            },
            getGameForUser: function(gameId, force) {
                if (!force){
                    var foundGame = cache.getItem(gameId);
                    if (foundGame) {
                        return util.wrapCacheInPromise({game: foundGame});
                    }
                }
                var params = {
                    url: "/api/gameGet",
                    data: {gameId: gameId },
                    type: util.httpTypes.get
                };
                return util.startAjax(params).then(function(res) {
                    //console.log("Returning game from server");
                    if (res.game)
                        cache.addOrUpdateItems(res.game);
                    return res;
                });
            },
            saveNewGame: function(game, doSendMail) {
                var data = {
                    name: game.name,
                    owned: game.owned,
                    played: game.played,
                    dateLastPlayed: game.dateLastPlayed,
                    comment: game.comment,
                    bggId: game.bggId,
                    doSendMail: doSendMail
                };
                var params = {
                    url: "/api/gameAdd",
                    data: data,
                    type: util.httpTypes.post
                };
                return util.startAjax(params).then(function(res){
                    if (res.game)
                        cache.addOrUpdateItems(res.game);
                    return(res);
                });
            },
            updateGame: function(game) {
                var data = {
                    _id: game._id,
                    name: game.name,
                    owned: game.owned,
                    played: game.played,
                    dateLastPlayed: game.dateLastPlayed,
                    comment: game.comment,
                    bggId: game.bggId
                };

                var params = {
                    url: "/api/gameUpdate",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params).then(function(res){
                    if (res.game)
                        cache.addOrUpdateItems(res.game);
                    return(res);
                });
            },
            updateGameLike: function(id, like){
                var data = {
                    _id: id,
                    favourite: like
                };

                var params = {
                    url: "/api/gameUpdate",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params).then(function(res){
                    if (res.game)
                        cache.addOrUpdateItems(res.game);
                    return(res);
                });
            },
            updateGameLikeToPlay: function(id, likeToPlay){
                var data = {
                    _id: id,
                    wantToPlay: likeToPlay
                };

                var params = {
                    url: "/api/gameUpdate",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params).then(function(res){
                    if (res.game)
                        cache.addOrUpdateItems(res.game);
                    return(res);
                });
            },
            updateGameComment: function(id, comment){
                var data = {
                    _id: id,
                    comment: comment
                };

                var params = {
                    url: "/api/gameUpdate",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params).then(function(res){
                    if (res.game)
                        cache.addOrUpdateItems(res.game);
                    return(res);
                });
            },
            updateGameOwned: function(id, owned){
                var data = {
                    _id: id,
                    owned: owned
                };

                var params = {
                    url: "/api/gameUpdate",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params).then(function(res){
                    if (res.game)
                        cache.addOrUpdateItems(res.game);
                    return(res);
                });
            },
            updateGameKnow: function(id, knowRules){
                var data = {
                    _id: id,
                    knowRules: knowRules
                };

                var params = {
                    url: "/api/gameUpdate",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params).then(function(res){
                    if (res.game)
                        cache.addOrUpdateItems(res.game);
                    return(res);
                });
            },
            updateGameDates: function(id, dateFirstPlayed, dateLastPlayed){
                var data = {
                    _id: id,
                    played: true
                };

                if (dateFirstPlayed)
                    data.historicalDateFirstPlayed = dateFirstPlayed;

                if (dateLastPlayed)
                    data.historicalDateLastPlayed = dateLastPlayed;

                var params = {
                    url: "/api/gameUpdate",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params).then(function(res){
                    if (res.game)
                        cache.addOrUpdateItems(res.game);
                    return(res);
                });
            },
            renameGame: function(gameId, newName){
                var data = {
                    gameId: gameId,
                    newName: newName
                }
                var params = {
                    url: "/api/gameRename",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params).then(function(res) {
                    cache.addOrUpdateItems(res.game);
                    return res;
                });
            },
            deleteGame: function(gameId){
                var data = {
                    gameId: gameId
                }
                var params = {
                    url: "/api/gameDelete",
                    data: data,
                    type: util.httpTypes.delete
                };

                return util.startAjax(params).then(function(res){
                    cache.removeItem(gameId)
                    return res;
                });
            },
            updateGameDatesAndMatchGameInfo: function(_id){
                var data = {
                    _id: _id
                }
                var params = {
                    url: "/api/gemeUpdateDatesAndMatchesGameInfo",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            }
        };
    }
);

