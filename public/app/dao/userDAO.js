/**
 * @module dao/user
 *
 * This is dao for user management
 */
define(['dao/daoUtil'],
    function (util) {
        var userData = {};
        var cached = false;

        return {
            getUserData: function(force){
                if (!force && cached){
                    return util.wrapCacheInPromise(userData);
                }

                var params = {
                    url: "/api/getUserData",
                    data: {},
                    type: util.httpTypes.get
                };

                return util.startAjax(params).then(function(data){
                    userData = data.user;
                    return data;
                });
            },
            getUserName: function() {
                var params = {
                    url: "/api/getUserName",
                    data: {},
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            signUp: function(username, email, password, sex){
                var data = {
                    username: username,
                    email: email,
                    password: password,
                    sex: sex
                };

                var params = {
                    url: "/api/userAdd",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            },
            confirmUser: function(code){
                var data = {
                    code: code
                };

                var params = {
                    url: "/api/confirmEmail",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            },
            resendConfirmationCode: function(auth){
                var data = {
                    username: auth.username,
                    password: auth.password
                };

                var params = {
                    url: "/api/resendConfirmationCode",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            },
            changePasswordWithCode: function(password, code){
                var data = {
                    password: password,
                    code: code
                };

                var params = {
                    url: "/api/userChangePasswordWithCode",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params);
            },
            updateUserProfile: function(profile){
                var data = {profile: profile};

                var params = {
                    url: "/api/userProfileUpdate",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            },
            approveCookies: function() {
                var params = {
                    url: "/api/userApproveCookies",
                    data: {},
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            },
            sendPasswordRecoveryRequest: function(username) {
                var data = {
                    username: username
                };
                var params = {
                    url: "/api/userPasswordRecoveryRequest",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            },
            recoverUsername: function(email) {
                var data = {
                    email: email
                };
                var params = {
                    url: "/api/userRecoverUsername",
                    data: data,
                    type: util.httpTypes.get
                };

                return util.startAjax(params);
            },
            createSocialConnectedUser: function(username, type) {
                var data = {
                    username: username
                };
                var params = {
                    url: type == 'fb' ? "/api/facebookNewUserConnect" : "/api/googleNewUserConnect",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params);
            }
        };
    }
);
