define(['dao/daoUtil', 'model/cache'],
    function (util, Cache) {
        var cache = new Cache();

        return {
            getAllBuddies: function(force){
                var params = {
                    url: "/api/buddyGetAll",
                    data: {},
                    type: util.httpTypes.get
                };

                return util.startAjax(params).then(function(res) {
                    if (res.buddies) {
                        cache.addOrUpdateItems(res.buddies);
                        cache.setCached();
                    }
                    return res;
                });
            },
            getBuddyCount: function(force){
                /* if (!force && cache.isCacheValid()){
                    var res = {}
                    res.count = cache.getItemCount();
                    return util.wrapCacheInPromise(res);
                }
                */
                var params = {
                    url: "/api/buddyGetCount",
                    data: {},
                    type: util.httpTypes.get
                };

                return util.startAjax(params).then(function(res) {
                    return res;
                });
            },
            getBuddy: function(buddyId, force){
                if (!force){
                    var foundBuddy = cache.getItem(buddyId);
                    if (foundBuddy) {
                        return util.wrapCacheInPromise({buddy: foundBuddy});
                    }
                }
                var params = {
                    url: "/api/buddyGet",
                    data: {buddyId: buddyId},
                    type: util.httpTypes.get
                };

                return util.startAjax(params).then(function(res) {
                    if (res.buddy)
                        cache.addOrUpdateItems(res.buddy);
                    return res;
                });
            },
            updateBuddyLike: function(buddyId, like){
                var params = {
                    url: "/api/buddyLikeUpdate",
                    data: {buddyId: buddyId, like: like},
                    type: util.httpTypes.post
                };

                return util.startAjax(params).then(function(res) {
                    cache.addOrUpdateItems(res.buddy);
                    return res;
                });
            },
            updateBuddyData: function(buddy){
                var params = {
                    url: "/api/buddyUpdate",
                    data: {buddy: buddy},
                    type: util.httpTypes.post
                };

                return util.startAjax(params).then(function(res) {
                    cache.addOrUpdateItems(res.buddy);
                    return res;
                });
            },
            addBuddy: function(buddy){
                var data = {
                    name: buddy.name,
                    comment: buddy.comment,
                    username: buddy.username,
                    like: buddy.like
                }
                var params = {
                    url: "/api/buddyAdd",
                    data: data,
                    type: util.httpTypes.post
                };

                return util.startAjax(params).then(function(res) {
                    cache.addOrUpdateItems(res.buddy);
                    return res;
                });
            },
            renameBuddy: function(buddyId, newName){
                var data = {
                    buddyId: buddyId,
                    newName: newName
                }
                var params = {
                    url: "/api/buddyRename",
                    data: data,
                    type: util.httpTypes.put
                };

                return util.startAjax(params).then(function(res) {
                    cache.addOrUpdateItems(res.buddy);
                    return res;
                });
            },
            deleteBuddy: function(buddyId, force){
                var data = {
                    buddyId: buddyId,
                    force: force
                }
                var params = {
                    url: "/api/buddyDelete",
                    data: data,
                    type: util.httpTypes.delete
                };

                return util.startAjax(params).then(function(res){
                    if (!res.cannotDelete)
                        cache.removeItem(buddyId);

                    return res;
                });
            }
        };
    }
);


