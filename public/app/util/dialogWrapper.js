define(
    ['plugins/dialog', 'underscore'],
    function (dialog, _){
        var openedDialogs = [];

        return {
            show: function(obj, activationData, context){
                openedDialogs.push(obj);

                return dialog.show(obj, activationData, context).then(function(res){
                    openedDialogs.pop(obj);

                    if (res && res.forced){
                        if (obj.hasOwnProperty('forceCloseResult')){
                            return obj.forceCloseResult;
                        } else {
                            throw "Dialog is forced to close. Results are unpredictable";
                        }
                    } else {
                        return(res);
                    }
                });
            },
            showMessage: function(message, title, options, autoclose, settings){
                openedDialogs.push(dialog.MessageBox);
                return dialog.showMessage(message, title, options, autoclose, settings).then(function(res){
                    openedDialogs.pop(dialog.MessageBox);

                    if (res && res.forced){
                        throw "Dialog is forced to close. Results are unpredictable";
                    }


                    return(res);
                })
            },
            closeAll: function(){
                _.each(openedDialogs, function(dlg){
                    dialog.close(dlg,{forced:true});
                });
            }
        }
    }
);
