define(
    ['require', 'knockout', 'durandal/system', 'i18next', 'durandal/composition', 'moment'],
    function (require, ko, sys, i18n, composition, moment){

        ko.bindingHandlers.moment = {
            update: function(element, valueAccessor, allBindingsAccessor) {
                $(element).text(moment(valueAccessor()).format('LL'));
            }
        }

        ko.bindingHandlers.attrIfDefined = {
            update: function(element, valueAccessor, allBindingsAccessor) {
                try {
                    var hash = valueAccessor().href;
                }
                catch (ex) {
                    sys.log(ex); //if some attribute isn't set, log and ignore
                    return;
                }

                if (hash !== undefined)
                    ko.bindingHandlers.attr.update(element, valueAccessor, allBindingsAccessor);
                else{
                    //do nothing
                }
            }
        };

        ko.bindingHandlers.dismissAlert = {
            init: function(element, valueAccessor){
                var e = $(element);

                var value = valueAccessor();

                var timeout = ko.unwrap(value);

                if (timeout)
                    setTimeout(function(){
                        e.alert('close')
                    }, timeout);

            }
        };

        ko.bindingHandlers.toggleButton = {
            init: function(element, valueAccessor) {
                var $element = $(element);
                var params = valueAccessor();

                var checked = params.checked;
                if (!ko.isWriteableObservable(checked)) {
                    throw "You must pass an writable observable for checked";
                }

                var checkedKey = ko.unwrap(params.checkedKey);
                if (checkedKey)
                    $element.attr("data-checked-text", i18n.t(checkedKey))

                var uncheckedKey = ko.unwrap(params.uncheckedKey);
                if (uncheckedKey)
                    $element.attr("data-unchecked-text", i18n.t(uncheckedKey))

                var toggledClass = ko.unwrap(params.toggledClass) || 'btn-primary';
                var untoggledClass = ko.unwrap(params.untoggledClass) || 'btn-primary';

                if (toggledClass == untoggledClass){
                    toggledClass = ''; //class toggle not needed
                    untoggledClass = '';
                }
                $element.on("click", function() {
                    checked(!checked());
                });
                ko.computed({
                    disposeWhenNodeIsRemoved: element,
                    read: function() {
                        if (checked()){
                            $element.button('checked');
                            $element.addClass("active");
                            $element.addClass(toggledClass);
                            $element.removeClass(untoggledClass);
                        } else {
                            $element.button('unchecked');
                            $element.removeClass("active");
                            $element.removeClass(toggledClass);
                            $element.addClass(untoggledClass);
                        }

                    }
                });
            }
        };

        ko.bindingHandlers.toggleValuesButton = {
            init: function(element, valueAccessor) {
                var $element = $(element);
                var params = valueAccessor();

                //current value
                var currentValue = params.currentValue;
                if (!ko.isWriteableObservable(currentValue)) {
                    throw "You must pass an writable observable currentValue";
                }

                //possible values
                var values = ko.observableArray();
                values.subscribe(function(newValues){
                    for (var i=0; i<newValues.length;i++){
                        var v = newValues[i];
                        //$element.attr("data-"+v+"-text", v);
                        $element.data(v+'Text', v); //the above doesn't work because jquery does not read data attributes to data that are set later
                    }
                })

                if (ko.isObservable(params.values)){
                    params.values.subscribe(function(newValue){
                        values(newValue);
                    });
                }
                values(ko.unwrap(params.values));

                //default text key
                var notSetKey = ko.unwrap(params.notSetKey);
                if (!notSetKey)
                    notSetKey = "common.select";

                $element.attr("data-select-text", i18n.t(notSetKey));

                //behaviour
                $element.on("click", function() {
                    var newIndex = values().indexOf(currentValue()) + 1;
                    if (newIndex >= values().length)
                        newIndex = 0;

                    var val = values()[newIndex];
                    currentValue(val);
                });
                ko.computed({
                    disposeWhenNodeIsRemoved: element,
                    read: function() {
                        if (values.indexOf(currentValue()) == -1)
                             $element.button('select');

                        $element.button(currentValue());
                    }
                });
            }
        };

        ko.bindingHandlers.addButton = {
            init: function(element, valueAccessor) {
                var $element = $(element);
                var params = valueAccessor();

                var target = params.target;
                if (!ko.isWriteableObservable(target)) {
                    throw "You must pass an writable observable currentValue";
                }

                var value = params.value;
                if (!ko.isObservable(value))
                    value= ko.observable(value);

                $element.on("click", function() {
                    var newValue = target() + value();
                    target(newValue);
                });
            }
        };

        var findColorOfFirstParentWithBackgroundColor = function(element){
            var parent = $(element).parent();

            if (null==parent)
                return $.Color('white');

            var color = $.Color(parent, 'background-color');

            if (color.toHexString(true)=="#00000000")
                return findColorOfFirstParentWithBackgroundColor(parent);
            else
                return color;
        };

        //composition.addBindingHandler is used because this binding should be applied after node is attached
        composition.addBindingHandler('freezeColumn', {
            init: function(element, valueAccessor, allBindings, viewModel) {
                require(['jquery-color'], function(){
                    var $e = $(element)
                    var $fixedColumn = null;

                    var freeze = function(){
                        if (null != $fixedColumn){
                            $fixedColumn.remove();
                        }

                        var $table = $e.clone(); //table to freeze first column is cloned
                        $fixedColumn = $table.insertBefore($e).addClass('frozen-column'); //...and inserted before itself

                        $fixedColumn.find('th:not(:first-child),td:not(:first-child)').remove();  //all columns that do not need to be freezed (other than first) are removed from cloned table

                        //each column in frozen table needs some setup
                        $fixedColumn.find('tr').each(function (i, elem) {
                            var originalRow = $e.find('tr:eq(' + i + ')'); //original row in original table - some properties will be taken from this original row

                            $(this).height(originalRow.height()); //...for example height (cloned table only with first column could have smaller rows because of less data in table)

                            //we need to set color for transparent rows because original table will be seen below frozen column
                            var td = originalRow.children('td:first');
                            if ($.Color(td, 'background-color').toHexString(true)=="#00000000") {
                                var color = findColorOfFirstParentWithBackgroundColor(td);

                                $(elem).children('td:first').css('background-color', color);
                            }

                        });

                        var $responsiveTable = $e.closest('.table-responsive');
                        $responsiveTable.scroll(function(e){
                            var position = $responsiveTable.scrollLeft();
                            $table.toggleClass('frozen-column-scrolled', position > 0);
                        })
                    };
                    freeze();

                    //fixed column should be rerendered when table changes
                    $e.bind('DOMNodeInserted', freeze);
                    $e.bind('DOMNodeRemoved', freeze);
                    $e.bind('DOMSubtreeModified', freeze);

                })
            }
        });

        ko.bindingHandlers.tooltip = {
            //initializing bootstrap tooltip
            init: function(element, valueAccessor) {
                var $e = $(element);
                var params = valueAccessor();

                params.trigger = params.trigger || 'hover click';

                if (params.titleKey)
                    params.title = i18n.t(params.titleKey)

                $e.tooltip(params);
            }
        };

        ko.bindingHandlers.popover = {
            //initializing bootstrap popover
            init: function(element, valueAccessor){
                var $e = $(element);
                var params = valueAccessor();

                params.trigger = params.trigger || 'hover click';

                var p = $e.popover(params);

                if (params.popoverClass)
                    p.data('bs.popover').tip().addClass(params.popoverClass);
            }

        }

        ko.bindingHandlers.onLostFocus = {
            init: function(element, valueAccessor, allBindings, viewModel){
                var $e = $(element);
                var callback = valueAccessor();

                $e.on('blur', function () {
                    callback.call(viewModel);
                });
            }

        }

        ko.bindingHandlers.addCssClass = {
            init: function(element, valueAccessor){
                var $e = $(element);
                var cssClass = ko.unwrap(valueAccessor());

                $e.addClass(cssClass);
            }
        }

        ko.bindingHandlers.i18n = {
            update: function(element, valueAccessor){
                var $e = $(element);
                var params = valueAccessor();

                if (ko.isObservable(params.key)){
                    params.key.subscribe(function(newValue){
                        var str = i18n.t(ko.unwrap(newValue), ko.unwrap(params.options))
                        $e.text(str);
                    })
                }

                var str = i18n.t(ko.unwrap(params.key), ko.unwrap(params.options))
                $e.text(str);
            }
        }

        ko.bindingHandlers.switch = {
            init: function(element, valueAccessor, allBindings, viewModel){
                var $e = $(element);
                var params = valueAccessor();
                var options = params && params.options ? params.options : {};
                var value = params && params.value ? params.value : ko.observable();
                var changeCallback = params && params.onChange ? params.onChange : null;

                if (!ko.isObservable(value)){
                    value = ko.observable(value);
                }

                if (options.onTextLabel){
                    options.onText = i18n.t(options.onTextLabel);
                }
                if (options.offTextLabel){
                    options.offText = i18n.t(options.offTextLabel);
                }

                require(['bootstrap-switch'], function(){
                    $e.bootstrapSwitch(options);
                    if (value())
                        $e.bootstrapSwitch('state',true); // Set intial state
                    else
                        $e.bootstrapSwitch('state',false); // Set intial state

                    $e.on('switchChange.bootstrapSwitch', function (e, state) {
                        value(state);
                        if (changeCallback){
                            changeCallback.call(viewModel, state);
                        }
                    }); // Update the model when changed.
                });
            },
            update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                var $e = $(element);
                var params = valueAccessor();
                var value = params && params.value ? params.value : ko.observable();

                var val = ko.isObservable(value) ? value() : value;

                require(['bootstrap-switch'], function(){
                    if(val) {
                        var vStatus = $e.bootstrapSwitch('state');

                        if (vStatus != val) {
                            $e.bootstrapSwitch('state', val);
                        }
                    }
                });
            }
        }

        ko.bindingHandlers.typeahead = {
            init: function(element, valueAccessor, allBindings){
                var $e = $(element);
                var params = valueAccessor();
                var suggestionField = params.suggestionField || 'name';
                var itemId = params.itemId;
                var idField = params.idField || '_id';

                require(['typeahead'], function(){
                    var updateValues = function(e, suggestion){
                        var bindingName = '';
                        if (allBindings.has('textInput')) {
                            bindingName = 'textInput';
                        } else if (allBindings.has('value')){
                            bindingName = 'value';
                        }

                        if (bindingName) {
                            var value = allBindings.get(bindingName);
                            if (ko.isObservable(value)){
                                value(suggestion[suggestionField]);
                            }
                        }

                        if (itemId && ko.isObservable(itemId)){
                            itemId(suggestion[idField]);
                        }
                    };
                    $e.on('keyup', function(e){
                        //some of keys that do not alter the item name. (i.e. arrows, esc, shift, ctrl, tab, alt)
                        if ([9,13,16,17,18,20,27,33,34,35,36,37,38,39,40,42,45,113,114,115,116,117,118,119,120,121,122,123,144].indexOf(e.which)==-1){
                            itemId(null);
                        }

                    });
                    $e.on('typeahead:selected', updateValues);
                    $e.on('typeahead:autocompleted', updateValues);

                })
            },
            update: function(element, valueAccessor, allBindings){
                var $e = $(element);
                var params = valueAccessor();
                var suggestionField = params.suggestionField || 'name';
                var items = ko.unwrap(params.datums);
                require(['typeahead'], function(){
                    // constructs the suggestion engine
                    var set = new Bloodhound({
                        datumTokenizer: Bloodhound.tokenizers.obj.whitespace(suggestionField),
                        queryTokenizer: Bloodhound.tokenizers.whitespace,
                        local: items
                    });

                    // kicks off the loading/processing of `local` and `prefetch`
                    set.initialize();

                    $e.typeahead('destroy');

                    $e.typeahead({
                            hint: false,
                            highlight: true,
                            minLength: 1
                        },
                        {
                            name: 'items',
                            displayKey: suggestionField,
                            source: set.ttAdapter()
                        });
                })
            }
        }

        ko.bindingHandlers.draggable= {
            init: function(element, valueAccessor, allBindings, viewModel){
                require(['draggabilly'], function(){
                    require(['util/dragDrop'], function(dragDrop){
                        var params = valueAccessor();
                        var Draggable = dragDrop.Draggable;

                        !params.options && (params.options = {});
                        !params.draggabillyOptions && (params.draggabillyOption = {});

                        new Draggable(element, params.options, params.draggabillyOptions, viewModel);
                    })
                });
            }
        }

        ko.bindingHandlers.droppable= {
            init: function(element, valueAccessor, allBindings, viewModel){
                require(['draggabilly'], function(){
                    require(['util/dragDrop'], function(dragDrop){
                        var params = valueAccessor();
                        var Droppable = dragDrop.Droppable;

                        !params && (params = {});

                        new Droppable(element, params, viewModel);
                    })
                });
            }
        }

        ko.bindingHandlers.fbSigninButton= {
            init: function(element, valueAccessor, allBindings, root){
                if (window.sessionStorage.getItem("doNotLogFacebookUser") != "true"){
                    var $e = $(element);
                    var params = valueAccessor();
                    var signinCallback = params ? params.signinCallback : null;

                    if (!signinCallback){
                        throw new Error("signinCallback must be set for fbSigninButton");
                    }

                    var cb = function(auth_result){
                        signinCallback.call(root, auth_result);
                    }

                    require(['facebook'], function(){
                        FB.init({
                            appId      : '1600627500203927',
                            cookie     : true,
                            xfbml      : false,
                            version    : 'v2.4'
                        });

                        FB.getLoginStatus(cb);

                        FB.Event.subscribe('auth.login', cb);

                        $e.append('<fb:login-button scope="public_profile,email" data-size="large" />');
                        FB.XFBML.parse();
                    })
                }
                else {
                    window.sessionStorage.removeItem("doNotLogFacebookUser")
                }
            }
        }

        ko.bindingHandlers.gPlusSigninButton = {
            init: function(element, valueAccessor, allBindings, root){
                var $e = $(element);
                var params = valueAccessor();
                var signinCallback = params ? params.signinCallback : null;

                if (!signinCallback){
                    throw new Error("signinCallback must be set for gPlusSigninButton");
                }

                var cb = function(auth_result){
                    signinCallback.call(root, auth_result);
                }

                var gPlusParams = {
                    'theme' : 'dark',
                    'scope': 'https://www.googleapis.com/auth/plus.login email',
                    'clientid': '723477775783-gq41v9q2h00n57v3d52cnuptk3nonk27.apps.googleusercontent.com',
                    'redirecturi': 'postmessage',
                    'accesstpye': 'offline',
                    'cookiepolicy': 'single_host_origin',
                    'callback': cb
                }

                if (window.sessionStorage.getItem("doNotLogGoogleUser") != "true"){
                    require(['gapi'], function(){
                        gapi.signin.render($e.attr('id'), gPlusParams);
                    });
                } else {
                    window.sessionStorage.removeItem("doNotLogGoogleUser")
                }
            }
        }
    }
);