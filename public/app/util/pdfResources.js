define(['q'],function (Q) {
    var getResourcePromise = function(url){
        var defered = Q.defer();

        var oReq = new XMLHttpRequest();
        oReq.open("GET", url, true);
        oReq.responseType = "arraybuffer";

        oReq.onload = _.bind(function (oEvent) {
            var arrayBuffer = oReq.response; // Note: not oReq.responseText
            defered.resolve(arrayBuffer);
        }, this);

        oReq.send(null);

        return defered.promise;
    }

    var Resources = function(resources){
        var promises = []
        resources.forEach(function(url){
            promises.push(getResourcePromise(url));
        });

        return Q.all(promises);
    }

    return Resources;
})