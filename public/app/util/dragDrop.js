define(['knockout', 'lodash', 'draggabilly/draggabilly', 'classie/classie', 'get-style-property/get-style-property'], function (ko, _, Draggabilly, classie, getStyleProperty) {
    //based on http://tympanus.net/codrops/2014/11/11/drag-and-drop-interaction-ideas/

    var is3d = !!getStyleProperty( 'perspective' )
    var docElem = window.document.documentElement;
    function scrollX() { return window.pageXOffset || docElem.scrollLeft; }
    function scrollY() { return window.pageYOffset || docElem.scrollTop; }
    var getOffset = function( el ) {
        var offset = el.getBoundingClientRect();
        return { top : offset.top + scrollY(), left : offset.left + scrollX() }
    }
    function getViewportH() {
        var client = docElem['clientHeight'], inner = window['innerHeight'];
        return client < inner ? inner : client;
    }
    var onEndTransition = function ( el, callback ) {
        var onEndCallbackFn = function( ev ) {
            this.removeEventListener( 'transitionend', onEndCallbackFn );
            this.removeEventListener( 'webkitTransitionEnd', onEndCallbackFn );
            if( callback && typeof callback === 'function' ) { callback.call(); }
        };

        el.addEventListener( 'transitionend', onEndCallbackFn );
        el.addEventListener( 'webkitTransitionEnd', onEndCallbackFn );
    }

    // https://remysharp.com/2010/07/21/throttling-function-calls
    var throttle = function(fn, threshhold, scope) {
        threshhold || (threshhold = 250);
        var last,
            deferTimer;

        return function () {
            var context = scope || this;
            var now = +new Date,
                args = arguments;
            if (last && now < last + threshhold) {
                // hold on to it
                clearTimeout(deferTimer);
                deferTimer = setTimeout(function () {
                    last = now;
                    fn.apply(context, args);
                }, threshhold);
            } else {
                last = now;
                fn.apply(context, args);
            }
        };
    }

    var droppables = {
        default: []
    }


    /********************** DROPPABLE *********************************/
    var Droppable = function ( element, options, viewModel ) {
        this.element = element;
        this.viewModel = viewModel;
        this.options = _.extend({}, this.options);
        _.extend(this.options, options );

        classie.add(this.element, 'droppable');

        droppables.default.push(this);

        ko.utils.domNodeDisposal.addDisposeCallback(this.element, (function() {
            // This will be called when the element is removed by Knockout or
            // if some other part of your code calls ko.removeNode(element)
            var index = droppables.default.indexOf(this);
            if (index >= 0) {
                droppables.default.splice(index, 1);
            }
        }).bind(this));
    }

    Droppable.prototype.options = {
        onDrop : function(droppable, draggable) { return false; }
    }

    // based on http://stackoverflow.com/a/2752387 : checks if the droppable element is ready to collect the draggable: the draggable element must intersect the droppable in half of its width or height.
    Droppable.prototype.isDroppable = function( draggableElement ) {
        var offset1 = getOffset( draggableElement ), width1 = draggableElement.offsetWidth, height1 = draggableElement.offsetHeight,
            offset2 = getOffset( this.element ), width2 = this.element.offsetWidth, height2 = this.element.offsetHeight;

        return !(offset2.left > offset1.left + width1 - width1/2 ||
            offset2.left + width2 < offset1.left + width1/2 ||
            offset2.top > offset1.top + height1 - height1/2 ||
            offset2.top + height2 < offset1.top + height1/2 );
    }

    // highlight the droppable if it's ready to collect the draggable
    Droppable.prototype.highlight = function( draggableElement ) {
        if( this.isDroppable( draggableElement ) )
            classie.add( this.element, 'highlight' );
        else
            classie.remove( this.element, 'highlight' );
    }

    // accepts a draggable element...
    Droppable.prototype.collect = function( draggable ) {
        // remove highlight class from droppable element
        classie.remove( this.element, 'highlight' );
        this.options.onDrop( this, draggable );
    }


    /************************* DRAGGABLE *****************************/
    var Draggable = function ( element, options, draggabillyOptions, viewModel ) {
        this.element = element;
        this.viewModel = viewModel;

        this.options = _.extend({}, this.options);
        _.extend(this.options, options );

        this.draggabillyOptions = _.extend({}, this.draggabillyOptions);
        _.extend(this.draggabillyOptions, draggabillyOptions );

        this.scrollableElement = this.options.scrollable === window ? window : document.querySelector( this.options.scrollable );
        this.scrollIncrement = 0;

        this.draggie = new Draggabilly( this.element, this.draggabillyOptions );
        this.initEvents();
    }

    Draggable.prototype.draggabillyOptions = {
        containment: true
    };

    Draggable.prototype.options = {
        // whether to move element back to original position if not dropped
        returnNonDropped: true,
        // whether to animate return
        returnAnimate: true,
        // callbacks
        onStart : function() { return false; },
        onDrag : function() { return false; },
        onEnd : function(wasDropped) { return false; },
        // auto scroll when dragging
        scroll: true,
        // element to scroll
        scrollable : window,
        // scroll speed (px)
        scrollSpeed : 20,
        // minimum distance to the scrollable element edges to trigger the scroll
        scrollSensitivity : 10
    }

    Draggable.prototype.initEvents = function() {
        var self = this;
        this.draggie.on( 'dragStart', this.onDragStart.bind(this) );
        this.draggie.on( 'dragMove', throttle( this.onDragMove.bind(this), 5 ) );
        this.draggie.on( 'dragEnd', this.onDragEnd.bind(this) );
    }

    Draggable.prototype.onDragStart = function() {
        //callback
        this.options.onStart();
        this.position = { left : this.draggie.position.x, top : this.draggie.position.y };
    }

    Draggable.prototype.onDragMove = function() {
        //callback
        this.options.onDrag();

        // scroll page if at viewport's edge
        if( this.options.scroll ) {
            this.scrollPage();
        }

        // highlight droppable elements if draggables intersect
        this.highlightDroppables();
    }

    Draggable.prototype.onDragEnd = function() {

        // if the draggable && droppable elements intersect then "drop" and move back the draggable
        var dropped = false;
        for( var i = 0; i < droppables.default.length; ++i ) {
            var droppableElement = droppables.default[i];
            if( droppableElement.isDroppable( this.element ) ) {
                dropped = true;
                droppableElement.collect( this );
                break;
            }
        }

        //callback
        this.options.onEnd( dropped );

        if (this.options.returnNonDropped && !dropped){
            this.moveBack();
        }
    }

    Draggable.prototype.highlightDroppables = function( ) {
        for( var i = 0; i < droppables.default.length; ++i ) {
            droppables.default[i].highlight( this.element );
        }
    }

    // move back the draggable to its original position
    Draggable.prototype.moveBack = function() {
        // add animate class (where the transition is defined)
        if( this.options.returnAnimate ) {
            classie.add(this.element, 'drag-drop-animate');
        }
        // reset translation value
        this.element.style.transform = is3d ? 'translate3d(0,0,0)' : 'translate(0,0)'
        // apply original left/top
        this.element.style.left = this.position.left + 'px';
        this.element.style.top = this.position.top + 'px';
        // remove class animate (transition) and is-active to the draggable element (z-index)
        var callback = function() {
            if( this.options.returnAnimate ) {
                classie.remove(this.element, 'drag-drop-animate');
            }
        }.bind( this );

        if( this.options.returnAnimate ) {
            onEndTransition(this.element, callback);
        }
        else {
            callback();
        }
    }

    // check if element is outside of the viewport. TODO: check also for right and left sides
    Draggable.prototype.outViewport = function() {
        var scrollSensitivity = Math.abs( this.options.scrollSensitivity ) || 0,
            scrolled = scrollY(),
            viewed = scrolled + getViewportH(),
            elT = getOffset( this.element ).top,
            belowViewport = elT + this.element.offsetHeight + scrollSensitivity > viewed,
            aboveViewport = elT - scrollSensitivity < scrolled;

        if( belowViewport ) this.scrolldir = 'down';
        else if( aboveViewport ) this.scrolldir = 'up';

        return belowViewport || aboveViewport;
    }

    // force the scroll on the page when dragging..
    Draggable.prototype.scrollPage = function() {
        // check if draggable is "outside" of the viewport
        if( this.outViewport( this.element ) ) {
            this.doScroll();
        }
        else {
            // reset this.scrollIncrement
            this.scrollIncrement = 0;
        }
    }

    // just considering scroll up and down
    // this.scrollIncrement is used to accelerate the scrolling. But mainly it's used to avoid the draggable flickering due to the throttle when dragging
    // todo: scroll right and left
    // todo: draggabilly multi touch scroll: see https://github.com/desandro/draggabilly/issues/1
    Draggable.prototype.doScroll = function() {
        var speed = this.options.scrollSpeed || 20;
        this.scrollIncrement++;
        var val = this.scrollIncrement < speed ? this.scrollIncrement : speed;

        this.scrollableElement === window ?
            this.scrollableElement.scrollBy( 0, this.scrolldir === 'up' ? val * -1 : val ) :
            this.scrollableElement.scrollTop += this.scrolldir === 'up' ? val * -1 : val;
    }

    return {
        Draggable: Draggable,
        Droppable: Droppable
    }
});