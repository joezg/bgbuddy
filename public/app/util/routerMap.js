/**
 * @module helpers/routerMap
 *
 * Router maps are defined here.
 */
define(['i18next'],function (i18n) {
    return {
        main: [
            /**************** HOME ****************/
            {
                route: '',
                title: i18n.t('nav.home'),
                moduleId: 'main/home',
                routerGroup: 'home',
                nav: true
            },
            {
                route: 'home',
                title: i18n.t('nav.home'),
                moduleId: 'main/home',
                routerGroup: 'home',
                nav: false
            },
            /**************** /HOME ****************/

            /**************** SCORER ****************/
            {
                route: 'scorer',
                title: i18n.t('nav.scorer'),
                moduleId: 'scorer/overview',
                routerGroup: 'scorer',
                nav: true
            },
            {
                route: 'scorer/newMatch',
                title: i18n.t('nav.newMatch'),
                moduleId: 'scorer/newMatch',
                routerGroup: 'scorer',
                nav: false
            },


            {
                route: 'scorer/stats',
                title: i18n.t('nav.stats'),
                moduleId: 'scorer/stats',
                routerGroup: 'scorer',
                nav: false
            },
            {
                route: 'scorer/newMatch',
                title: i18n.t('nav.newMatch'),
                moduleId: 'scorer/newMatch',
                routerGroup: 'scorer',
                nav: false
            },
            {
                route: 'scorer/viewMatch/:id',
                title: i18n.t('nav.viewMatch'),
                moduleId: 'scorer/viewMatch',
                routerGroup: 'scorer',
                nav: false
            },
            {
                route: 'scorer/viewFinishedMatch/:id',
                title: i18n.t('nav.viewMatch'),
                moduleId: 'scorer/viewFinishedMatch',
                routerGroup: 'scorer',
                nav: false
            },
            {
                route: 'scorer/lastMatches',
                title: i18n.t('nav.matches'),
                moduleId: 'scorer/lastMatches',
                routerGroup: 'scorer',
                nav: false
            },
            {
                route: 'scorer/buddies',
                title: i18n.t('nav.buddies'),
                moduleId: 'scorer/buddies',
                routerGroup: 'scorer',
                nav: false
            },
            {
                route: 'scorer/buddies/:id',
                title: i18n.t('nav.buddies'),
                moduleId: 'scorer/buddy',
                routerGroup: 'scorer',
                nav: false
            },
            {
                route: 'scorer/findBuddies',
                title: i18n.t('nav.findBuddies'),
                moduleId: 'scorer/findBuddies',
                routerGroup: 'scorer',
                nav: false
            },
            {
                route: 'scorer/buddy/:id',
                title: i18n.t('nav.buddyDetails'),
                moduleId: 'scorer/buddy',
                routerGroup: 'scorer',
                nav: false
            },
            {
                route: 'scorer/newBuddy',
                title: i18n.t('nav.buddyDetails'),
                moduleId: 'scorer/addBuddy',
                routerGroup: 'scorer',
                nav: false
            },
            {
                route: 'scorer/games',
                title: i18n.t('nav.games'),
                moduleId: 'scorer/games',
                routerGroup: 'scorer',
                nav: false
            },
            {
                route: 'scorer/game/:id',
                title: i18n.t('nav.game'),
                moduleId: 'scorer/game',
                routerGroup: 'scorer',
                nav: false
            },
            {
                route: 'scorer/newGame',
                title: i18n.t('nav.games'),
                moduleId: 'scorer/addGame',
                routerGroup: 'scorer',
                nav: false
            },
            {
                route: 'scorer/getSharedMatches/:type/:primaryId/:secondaryId',
                title: i18n.t('nav.scoreSharing'),
                moduleId: 'scorer/getSharedMatches',
                routerGroup: 'scorer',
                nav: false
            },
            /**************** /SCORER ****************/

            /**************** EVENT ****************/
            {
                route: 'event',
                title: i18n.t('nav.events'),
                moduleId: 'event/eventList',
                routerGroup: 'event',
                nav: true,
                hash: '#event'
            },
            {
                route: 'event/admin/library/:id',
                title: i18n.t('nav.library'),
                moduleId: 'event/admin/library',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/checkoutLibrary/:id',
                title: i18n.t('nav.library'),
                moduleId: 'event/admin/checkoutLibrary',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/boxes/:id',
                title: i18n.t('nav.boxes'),
                moduleId: 'event/admin/boxes',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/boxes/:id/:code',
                title: i18n.t('nav.box'),
                moduleId: 'event/admin/box',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/boxnew/:id',
                title: i18n.t('nav.box'),
                moduleId: 'event/admin/box',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/libraryItem/:id/:code',
                title: i18n.t('nav.libraryItem'),
                moduleId: 'event/admin/libraryItem',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/newLibraryItem/:id',
                title: i18n.t('nav.libraryItem'),
                moduleId: 'event/admin/libraryItem',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/participants/:eventOrganizerId/:eventId',
                title: i18n.t('nav.participants'),
                moduleId: 'event/admin/eventParticipants',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/participants/:eventOrganizerId',
                title: i18n.t('nav.participants'),
                moduleId: 'event/admin/participants',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/participant/:eventOrganizerId/:participantId',
                title: i18n.t('nav.participant'),
                moduleId: 'event/admin/participant',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/participant/:eventOrganizerId',
                title: i18n.t('nav.participant'),
                moduleId: 'event/admin/participant',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/events/:id',
                title: i18n.t('nav.eventsAdmin'),
                moduleId: 'event/admin/events',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/:id',
                title: i18n.t('nav.admin'),
                moduleId: 'event/admin',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/event/:eventOrganizerId/:id',
                title: i18n.t('nav.manageEvent'),
                moduleId: 'event/admin/event',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/matches/:eventOrganizerId/:id',
                title: i18n.t('nav.matches'),
                moduleId: 'event/admin/matches',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/tournaments/:eventOrganizerId/:id',
                title: i18n.t('nav.tournaments'),
                moduleId: 'event/admin/tournaments',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/tournament/:eventOrganizerId/:id/:tournamentId',
                title: i18n.t('nav.tournament'),
                moduleId: 'event/admin/tournament',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/marathon/:eventOrganizerId/:id/:tournamentId',
                title: i18n.t('nav.marathon'),
                moduleId: 'event/admin/marathon',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/tournamentRound/:eventOrganizerId/:id/:tournamentId/:roundId',
                title: i18n.t('nav.tournamentRound'),
                moduleId: 'event/admin/tournamentRound',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/newMatch/:eventOrganizerId/:id',
                title: i18n.t('nav.newMatch'),
                moduleId: 'event/admin/newMatch',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/admin/matchStats/:eventOrganizerId/:id',
                title: i18n.t('nav.matchStats'),
                moduleId: 'event/admin/matchStats',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/participant/event/:id/:eventOrganizerId',
                title: i18n.t('nav.event'),
                moduleId: 'event/participant/event',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/participant/library/:id/:eventId',
                title: i18n.t('nav.library'),
                moduleId: 'event/participant/library',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/participant/participants/:id/:eventOrganizerId',
                title: i18n.t('nav.participants'),
                moduleId: 'event/participant/participants',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/participant/matches/:id/:eventOrganizerId',
                title: i18n.t('nav.matches'),
                moduleId: 'event/participant/matches',
                routerGroup: 'event',
                nav: false
            },
            {
                route: 'event/pdf/nameTags/:eventOrganizerId/:eventId',
                title: i18n.t('nav.nameTags'),
                moduleId: 'event/admin/nameTagsPDF',
                routerGroup: 'event',
                nav: false
            },
            /**************** /EVENT ****************/

            /**************** NON-GROUPED ****************/
            {
                route: 'signup',
                title: i18n.t('nav.signUp'),
                moduleId: 'main/signup',
                nav: false,
                public: true
            },
            {
                route: 'confirm/:code',
                title: i18n.t('nav.confirm'),
                moduleId: 'main/confirm',
                nav: false,
                public: true
            },
            {
                route: 'passwordRecovery/:code',
                title: i18n.t('nav.passwordRecovery'),
                moduleId: 'main/passwordRecovery',
                nav: false,
                public: true
            },
            {
                route: 'profile',
                title: i18n.t('nav.profile'),
                moduleId: 'main/profile',
                nav: false
            },
            {
                route: 'messages',
                title: i18n.t('msg.messages'),
                moduleId: 'main/messages',
                nav: false
            },
            {
                route: 'message/:id',
                title: i18n.t('msg.messages'),
                moduleId: 'main/message',
                nav: false
            },
            {
                route: 'messageSend',
                title: i18n.t('msg.messages'),
                moduleId: 'main/messageSend',
                nav: false
            },
            {
                route: 'messageSend/:buddyUsername',
                title: i18n.t('msg.messages'),
                moduleId: 'main/messageSend',
                nav: false
            },
            {
                route: 'cookies',
                title: i18n.t('cookies.cookies'),
                moduleId: 'main/cookies',
                nav: false
            },
            {
                route: 'loginProblem',
                title: i18n.t('nav.loginProblem'),
                moduleId: 'main/loginProblem',
                nav: false,
                public: true
            },
            {
                route: 'socialLoginConnect/:type',
                title: i18n.t('nav.socialLoginConnect'),
                moduleId: 'main/socialLoginConnect',
                nav: false,
                public: true
            },
            {
                route: 'admin',
                title: i18n.t('nav.admin'),
                moduleId: 'admin/admin',
                nav: false,
                public: false
            },
            {
                route: 'importExport',
                title: i18n.t('nav.importExport'),
                moduleId: 'main/importExport',
                nav: false,
                public: false
            }
            /**************** /NON-GROUPED ****************/
        ]
    }
});
