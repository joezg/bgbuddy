define(['knockout', 'q'], function (ko, Q){
        var Stepper = function(boundObject){
            this.nextStep = ko.observable(-1);
            this.steps = [];
            this.boundObject = boundObject;
            this.data = ko.observable();
            this.nextStepName = ko.observable('');
        }

        //adds step with data and callback which will be called when next is called
        Stepper.prototype.addStep = function(name, data, callbackBefore, callbackAfter){
            this.steps.push({
                name: name,
                data: data,
                callbackBefore: callbackBefore,
                callbackAfter: callbackAfter
            });

            if (this.nextStep() === -1){
                prepareNextStep.bind(this)()
            }

        }

        Stepper.prototype.next = function(){
            var boundCallbackAfter = this.steps[this.nextStep()].callbackAfter;
            if (boundCallbackAfter)
                boundCallbackAfter = boundCallbackAfter.bind(this.boundObject);

            runWhenPrerequisiteFulfilled(boundCallbackAfter, function(){
                prepareNextStep.bind(this)()
            }.bind(this))
        }

        var prepareNextStep = function(){
            var nextStep = this.nextStep() + 1

            var boundCallbackBefore = this.steps[nextStep].callbackBefore;
            if (boundCallbackBefore)
                boundCallbackBefore = boundCallbackBefore.bind(this.boundObject);

            runWhenPrerequisiteFulfilled(boundCallbackBefore, function(){
                this.nextStep(nextStep);
                this.data(this.steps[nextStep].data);
                this.nextStepName(this.steps[nextStep].name);
            }.bind(this))
        }

        var runWhenPrerequisiteFulfilled = function(prerequisite, callback){
            var result = null;
            if (prerequisite)
                result = prerequisite();

            if (Q.isPromise(result)){
                result.then(callback).done();
            } else {
                callback()
            }
        }

        return Stepper;
    }
);
