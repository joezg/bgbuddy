define(['i18next'],function (i18n) {
    var matchPhaseEnum = {
        teaching: 0,
        setup: 1,
        playing: 2,
        custom: 3,
        scoring: 4,
        pause: 5
    };
    matchPhaseEnum.meta = {
        0: {type: matchPhaseEnum.teaching, order: 20, name: i18n.t('scorer.explainingRules')},
        1: {type: matchPhaseEnum.setup, order: 30, name: i18n.t('scorer.gameSetup')},
        2: {type: matchPhaseEnum.playing, order: 40, name: i18n.t('scorer.playing')},
        3: {type: matchPhaseEnum.custom, order: 50, name: i18n.t('scorer.customPhase'), customName: true},
        4: {type: matchPhaseEnum.scoring, order: 60, name: i18n.t('scorer.scoringPhase')},
        5: {type: matchPhaseEnum.pause, order: 10, name: i18n.t('scorer.pausePhase')} // BZ issue #3 pause game
    };
    matchPhaseEnum.get = function(type){
        for (var prop in matchPhaseEnum.meta) {
            if (matchPhaseEnum.meta.hasOwnProperty(prop)) {
                var p = matchPhaseEnum.meta[prop];
                if (p.type == type)
                    return p;
            }
        }
        return null;
    };
    matchPhaseEnum.getAll = function(except){
        var types = [];

        if (!except)
            except = [];

        if (!(except instanceof Array))
            except = [except];

        for (var prop in matchPhaseEnum.meta) {
            if (matchPhaseEnum.meta.hasOwnProperty(prop)) {
                var p = matchPhaseEnum.meta[prop];

                if (except.indexOf(p.type)==-1)
                    types.push(p);
            }
        }
        return types;
    };

    return matchPhaseEnum;
});

