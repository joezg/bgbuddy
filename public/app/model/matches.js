define(['model/match'],
    function (Match) {
        return {
            toModel: function(matches){
                var modelMatches = [];
                for (i=0; i<matches.length; i++){
                    modelMatches.push(new Match(matches[i]));
                }

                return modelMatches;
            }
        };
});

