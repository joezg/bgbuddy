define(['i18next'],function (i18n) {
    var get = function(type){
        for (var prop in this.meta) {
            if (this.meta.hasOwnProperty(prop)) {
                var p = this.meta[prop];
                if (p.type == type)
                    return p;
            }
        }
        return null;
    }

    var getAll = function(except){
        var types = [];

        if (!except)
            except = [];

        if (!(except instanceof Array))
            except = [except];

        for (var prop in this.meta) {
            if (this.meta.hasOwnProperty(prop)) {
                var p = this.meta[prop];

                if (except.indexOf(p.type)==-1)
                    types.push(p);
            }
        }
        return types;
    };

    var tableReposition = {
        random: "random",
        byRank: "byRank",
        snake: "snake",
        copyPrevious: "copyPrevious",
        knockout: "knockout",
        pick: "pick",
        roundRobin: "roundRobin",
        meta: {
            "random": {type: "random", name: i18n.t('common.random')},
            "byRank": {type: "byRank", name: i18n.t('event.byRank')},
            "snake": {type: "snake", name: i18n.t('event.snake')},
            "copyPrevious": {type: "copyPrevious", name: i18n.t('event.copyPrevious')},
            "knockout": {type: "knockout", name: i18n.t('event.knockout')},
            "pick": {type: "pick", name: i18n.t('event.pick')},
            "roundRobin": {type: "roundRobin", name: i18n.t('event.roundRobin')}
        },
        get: get,
        getAll: getAll
    };

    var seating = {
        random: "random",
        byRank: "byRank",
        meta: {
            "random": {type: "random", name: i18n.t('common.random')},
            "byRank": {type: "byRank", name: i18n.t('event.byRank')}
        },
        get: get,
        getAll: getAll
    };

    var cutoffType = {
        byRankAll: "byRankAll",
        byRankTable: "byRankTable",
        meta: {
            "byRankAll": {type: "byRankAll", name: i18n.t('event.byTournamentRanking')},
            "byRankTable": {type: "byRankTable", name: i18n.t('event.byTableRanking')}
        },
        get: get,
        getAll: getAll
    };

    var rankingType = {
        rankPoints: "rankPoints",
        scorePoints: "scorePoints",
        percentageOfPointsInGame: "percentageOfPointsInGame",
        normalizedPercentageOfPoints: "normalizedPercentageOfPoints",
        wins: "wins",
        byDirectMatch: "byDirectMatch",
        meta: {
            "rankPoints": {type: "rankPoints", name: i18n.t('event.rankPoints')},
            "scorePoints": {type: "scorePoints", name: i18n.t('event.scorePoints')},
            "percentageOfPointsInGame": {type: "percentageOfPointsInGame", name: i18n.t('event.percentageOfPointsInGame')},
            "normalizedPercentageOfPoints": {type: "normalizedPercentageOfPoints", name: i18n.t('event.normalizedPercentageOfPoints')},
            "wins": {type: "wins", name: i18n.t('event.wins')},
            "byDirectMatch": {type: "byDirectMatch", name: i18n.t('event.byDirectMatch')}
        },
        get: get,
        getAll: getAll
    };

    var carryoverType = {
        continue: "continue",
        reset: "reset",
        carryover: "carryover",
        meta: {
            "continue": {type: "continue", name: i18n.t('event.continue')},
            "reset": {type: "reset", name: i18n.t('event.reset')},
            "carryover": {type: "carryover", name: i18n.t('event.carryover')}
        },
        get: get,
        getAll: getAll
    };

    return {
        tableReposition: tableReposition,
        seating: seating,
        cutoffType: cutoffType,
        rankingType: rankingType,
        carryoverType: carryoverType
    };
});

