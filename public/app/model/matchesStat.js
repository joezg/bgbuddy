define([],
    function () {
        //represents match statistic for an item (game, buddy, ...)
        //i.e. if calculating stats for a buddy, this item could be specific match stat for some game
        // or it could be specific match stat against some buddy
        //
        return function MatchesStat (name){
            var self = this;

            this.name = name;
            this.totalMatches = 0; //total number of matches
            this.totalMatchesWithMe = 0;  //total number of matches in which user played
            this.scoredMatches = 0;  //total scored matches
            this.scoredMatchesWithMe = 0; //total scored matches in which user played
            this.notScoredMatches = 0;  //total not scored matches
            this.notScoredMatchesWithMe = 0; //total not scored matches in which user played
            this.inProgressMatches = 0;  //total matches in progress
            this.inProgressMatchesWithMe = 0;  //total matches in progress in which user played
            this.wins = 0;  //total wins by player
            this.winsWithMe = 0;  //total wins by player when user played
            this.losses = 0;  //total loses by player
            this.lossesWithMe = 0;  //total loses by player when user played

            //in multiplayer games, one player can lose, but not to some other specific player who can loose, too
            //this statistic represents that specific losses
            //which player represents wins and which represents specific wins is determined by the code which is filling this stats
            this.specificLosses = 0;  //multiplayer games could
            this.specificLossesWithMe = 0;  //same as above when user played
            this.specificWins = 0;  //for example if in context of player, specific win is if player in context won against given player (player associated with this stat)
            this.specificWinsWithMe = 0;  //same as above when user played

            this.iWon = 0;  //total number of wins by an user
            this.iLost = 0;  //total number of losses by an user
            this.totalTime = moment.duration(0);   //total time spent with this item
            this.totalTimeWithMe = moment.duration(0)   //total time spent with this item when user played

            var add  = function(property, withMe){
                self[property]++;
                if (withMe)
                    self[property+'WithMe']++;
            };
            this.addMatch = function(withMe){
                add('totalMatches', withMe);
            };
            this.addScored = function(withMe){
                add('scoredMatches', withMe);
            };
            this.addNotScored = function(withMe){
                add('notScoredMatches', withMe);
            };
            this.addInProgress = function(withMe){
                add('inProgressMatches', withMe);
            };
            this.addWin = function(withMe){
                add('wins', withMe);
            };
            this.addLoss = function(withMe){
                add('losses', withMe);
            };
            this.addSpecificLoss = function(withMe){
                add('specificLosses', withMe);
            };
            this.addSpecificWin = function(withMe){
                add('specificWins', withMe);
            };
            this.addWinOrLoss = function(isWin, withMe){
                if (isWin)
                    self.addWin(withMe);
                else
                    self.addLoss(withMe);
            };

            this.addTime = function(time, withMe){
                this.totalTime.add(time);
                if (withMe)
                    this.totalTimeWithMe.add(time);
            };
            this.addIWon = function(){
                this.iWon++;
            };
            this.addILost = function(){
                this.iLost++;
            };

            this.getTotalTime = function(){
                return moment.utc(this.totalTime.asMilliseconds()).format("HH:mm:ss");
            };

            this.getTotalTimeWithMe = function(){
                return moment.utc(this.totalTimeWithMe.asMilliseconds()).format("HH:mm:ss");
            };
        }
    });