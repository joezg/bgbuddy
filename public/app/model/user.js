define(['q', 'dao/userDAO'],
    function (Q, userDAO) {
        var loadedDeferred = Q.defer();

        return {
            name: null,
            username: null,
            memberSince: null,
            color: null,
            email: null,
            loaded: false,
            theme: null,
            sex: null,
            cookieDisclaimerDismissed: false,
            init: function(userData) {
                if (userData) {
                    this.name = userData.name;
                    this.username = userData.username;
                    this.memberSince = userData.memberSince;
                    this.color = userData.color;
                    this.email = userData.email;
                    this.theme = userData.theme || 'default';
                    this.sex = userData.sex || '';
                    this.cookieDisclaimerDismissed = userData.cookieDisclaimerDismissed || false;
                    this.loaded = true;
                    loadedDeferred.resolve(this);
                };
            },
            load: function(){
                if (this.loaded)
                    throw "User already loaded. Use reload if you need to reload."

                var self = this;
                userDAO.getUserData().then(
                    function success(data){
                        self.init(data.user);
                    }
                );
            },
            reload: function(){
              this.destroy();
              this.load();
            },
            destroy: function () {
                this.name = null;
                this.username = null;
                this.memberSince = null;
                this.color = null;
                this.email = null;
                this.loaded = false;
                this.theme = null;
                this.sex = null;
                this.cookieDisclaimerDismissed = false;
                loadedDeferred = Q.defer();
            },
            whenLoaded: function() {
                return loadedDeferred.promise;
            }

        };
    }
);