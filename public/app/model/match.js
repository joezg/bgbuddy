define(['moment', 'model/matchPhaseEnum', 'knockout', 'i18next','underscore'],
    function (moment, matchPhaseEnum, ko, i18n, _) {
        var Match = function(match){
            if (null == match){
                this.matchPhases = [];
            }
            else {
                this.game = match.game;
                this.gameId = match.gameId;
                this.date = new Date(match.date);
                this.id = match._id;
                this.location = match.location;
                this.inProgress = match.inProgress;
                this.matchPhases = match.phases || [];
                this.dateFromNow = moment(match.date).fromNow();
                this.formattedDate = moment(match.date).format('LL');
                this.formattedDate4Sort = moment(match.date).format('YYYYMMDDhhmmss');
                this.players = match.players || [];
                this.isScored = match.isScored || false;
                this.iWon = false; // FIXME think about this. flag showing user won this match
                this.iPlayed = false;
                this.isNewGame = match.isNewGame;
                this.isGameOutOfDust = match.isGameOutOfDust;

                // set flag if user won and if user played
                for (var j=0; j<this.players.length; j++) {
                    if (this.players[j].isUser) {
                        this.iPlayed = true;
                        if (this.players[j].winner && this.isScored) {
                            this.iWon = true;
                        }
                        break;
                    }
                }

                for (var i=0; i<this.matchPhases.length; i++){
                    var p = this.matchPhases[i];

                    if (p.type != matchPhaseEnum.custom) {
                        p.name = matchPhaseEnum.meta[p.type].name;
                        this.matchPhases[i]=p;
                    }
                }

            }
        };

        Match.prototype.setDate = function(newDate){
            this.date = new Date(newDate);
            this.dateFromNow = moment(newDate).fromNow();
            this.formattedDate = moment(newDate).format('LL');
            this.formattedDate4Sort = moment(newDate).format('YYYYMMDDhhmmss');
        }

        // Ribica added prototype function: get winners as pretty array
        Match.prototype.getWinners = function() {
            var winners = [];

            for (var j=0; j<this.players.length; j++){
                 if (this.players[j].winner) {
                    if (this.players[j].isUser)
                        winners.push(i18n.t("scorer.me"));
                     else if (this.players[j].isAnonymous)
                        winners.push(i18n.t("scorer.unknown"));
                     else
                        winners.push(this.players[j].name);
                 }
            }

            return winners;
        }

        Match.prototype.isBuddyWinner = function(buddyId){
            for (var j=0; j<this.players.length; j++){
                if (this.players[j].winner && this.players[j].buddyId == buddyId) {
                    return true;
                }
            }
            return false;
        }

        Match.prototype.didBuddyPlay = function(buddyId){
            for (var j=0; j<this.players.length; j++){
                if (this.players[j].buddyId == buddyId) {
                    return true;
                }
            }
            return false;
        }

        Match.prototype.getAvailableMatchPhaseTypes = function(){
            var phaseTypes = [];

            var currentPhaseType = {order: -1};

            var currentPhase = this.getOngoingPhase(false);
            if (currentPhase) {
                currentPhaseType = matchPhaseEnum.meta[currentPhase.type];
            }

            for (var prop in matchPhaseEnum.meta) {
                if (matchPhaseEnum.meta.hasOwnProperty(prop)) {
                    var p = matchPhaseEnum.meta[prop];
                    if (p.order > currentPhaseType.order || p.type == matchPhaseEnum.custom)
                        phaseTypes.push(p);
                }
            }

            return phaseTypes;

        };

        Match.prototype.getCompletedPhases = function() {
            var phases = [];
            for (var i = 0; i < this.matchPhases.length; i++) {
                var phase = this.matchPhases[i];

                if (i>0)
                    phase.hasPrevious = true;

                if (i<this.matchPhases.length - 1 )
                    phase.hasNext = true;

                if (phase.end){
                    //var duration = moment(phase.end).diff(moment(phase.start), 'second');
                    //var time = moment({second: duration}).format('HH:mm:ss');

                    var time = moment.utc(moment(phase.end).diff(moment(phase.start))).format("HH:mm:ss");
                    phase.time = time;
                    phases.push(phase);

                }
            }


            return phases;
        };

        // calc total completed time
        Match.prototype.getCompletedTotalTime = function (includePause, returnDuration) {
            var ttime = moment.duration(0);
            for (var i = 0; i < this.matchPhases.length; i++) {
                var phase = this.matchPhases[i];

                if (phase.end && ((phase.type != matchPhaseEnum.pause) || includePause)) {
                    //var duration = moment(phase.end).diff(moment(phase.start), 'second');
                    //var time = moment({second: duration}).format('HH:mm:ss');
                    var milliseconds = moment(phase.end).diff(moment(phase.start));
                    ttime.add(milliseconds);
                }
            }

            if (returnDuration)
                return ttime;
            else
                return moment.utc(ttime.asMilliseconds()).format("HH:mm:ss");
        }

        Match.prototype.getOngoingPhase = function(doAddTime){
            for (var i = 0; i < this.matchPhases.length; i++) {
                var phase = this.matchPhases[i];

                if (!phase.end){
                    if (doAddTime){
                        var time = moment.utc(moment().diff(moment(phase.start)));
                        phase.time = time.format("HH:mm:ss");
                        phase.moment = time;
                    }

                    return phase;
                }
            }

            return null;
        };

        // get phase that came before last phase. useful when you need to return to phase after pause
        Match.prototype.getPreviousPhase = function(){
            // first check array length
            if (this.matchPhases.length < 2 )
                return null;

            // sort by phase start time
            var sorted = _.sortBy(this.matchPhases, function(p){ return new Date(p.start) })

            // get 2nd last phase - the one that came before last (ongoing) phase
            return sorted[sorted.length - 2];

        };


        Match.prototype.finishOngoingPhase = function(){
            for (var i = 0; i < this.matchPhases.length; i++) {
                var phase = this.matchPhases[i];

                if (!phase.end){
                    phase.end = new Date();
                    this.phaseToUndoFinish = phase;
                }
            }
        };

        Match.prototype.undoFinishOngoingPhase = function(){
            if (this.phaseToUndoFinish){
                delete this.phaseToUndoFinish.end;
                delete this.phaseToUndoFinish;
            }
        };

        Match.prototype.finishMatch = function(){
            this.finishOngoingPhase();
            this.inProgress = false;
        };

        Match.prototype.undoFinishMatch = function(){
            this.undoFinishOngoingPhase();
            this.inProgress = true;
        };

        Match.prototype.startNewPhase = function(type, name){
            var newPhase = {
                type: type,
                name: name || matchPhaseEnum.meta[type].name,
                start: new Date()
            };
            this.matchPhases.push(newPhase);

            return newPhase;
        };

        Match.prototype.removePhase = function(phase){
            for(var i=0; i<this.matchPhases.length; i++) {
                if(this.matchPhases[i] == phase) {
                    this.matchPhases.splice(i, 1);
                    break;
                }
            }
        };

        Match.prototype.addPlayer = function(player){
            var foundDuplicate = false;
            if (!player.isAnonymous){
                for (var i=0; i<this.players.length; i++) {
                    if(player.name) {
                        if (player.name === this.players[i].name) {
                            foundDuplicate = true;
                            break;
                        }
                    } else {
                        if (player.isUser && this.players[i].isUser) {
                            foundDuplicate = true;
                            break;
                        }
                    }
                }
            }

            if (!foundDuplicate) {
                this.players.push(player);
                this.players.sort(normalSort);
                return true;
            } else
                return false;
        };

        Match.prototype.removePlayer = function(player){
            this.isScored = false;
            var index = -1;
            for (var i=0; i<this.players.length; i++){
                var p = this.players[i];
                if (p==player)
                    index = i;
                else {
                    if (p.hasOwnProperty('rank') || p.hasOwnProperty('result') || p.hasOwnProperty('winner'))
                        this.isScored = true;
                }

                if (this.isScored && index > -1)
                    break;
            }

            if (index > -1) {
                this.players.splice(index, 1);
            }
        };

        Match.prototype.getPlayers = function(){
            this.players.sort(normalSort);

            return this.players;
        };

        // sort by win & ranking
        Match.prototype.getPlayersByRank = function(){
            this.players.sort(sortByRank);

            return this.players;
        };

        Match.prototype.addPhase = function(phaseType, afterPhase, customPhaseName){
            var comparePhase = function(source, target){
                return source.start == target.start && source.end == target.end && source.type == target.type && source.name == target.name;
            }

            var sorted = _.sortBy(this.matchPhases, function(p){ return new Date(p.start) })
            var newPhase = {};
            if (afterPhase.type == -1){
                newPhase = {
                    type: phaseType.type,
                    name: customPhaseName || matchPhaseEnum.meta[phaseType.type].name,
                    start: sorted[0].start,
                    end: sorted[0].start
                };
                this.matchPhases.unshift(newPhase);
            } else {
                newPhases = [];

                for(var i=0;i<sorted.length;i++){
                    if (comparePhase(sorted[i], afterPhase)){
                        newPhases.push(sorted[i]);
                        newPhase = {
                            type: phaseType.type,
                            name: customPhaseName || matchPhaseEnum.meta[phaseType.type].name,
                            start: sorted[i].end,
                            end: sorted[i].end
                        }
                        newPhases.push(newPhase);
                    } else {
                        newPhases.push(sorted[i]);
                    }
                }

                this.matchPhases = newPhases;
            }

            return newPhase;
        }

        var normalSort = function(playerA, playerB){
            if (playerA.isUser && !playerB.isUser){
                return -1;
            }

            if (playerB.isUser && !playerA.isUser){
                return 1;
            }

            if (playerA.isAnonymous && !playerB.isAnonymous){
                return 1;
            }

            if (playerB.isAnonymous && !playerA.isAnonymous){
                return -1;
            }

            return 0;
        };

        var sortByRank = function(playerA, playerB){
            // first check for winner
            if (playerA.winner){
                return -1;
            }
            if (playerB.winner){
                return 1;
            }
            // then sort by ranking
            if (playerA.rank < playerB.rank){
                return -1;
            }
            // if ranks are equal, first show user, then buddies. anonymous come last
            if (playerA.rank === playerB.rank){
                if (playerA.isUser)
                    return -1;
                if (playerB.isUser)
                    return 1;
                if (playerA.isAnonymous)
                    return 1;
                if (playerB.isAnonymous)
                    return -1;
            }
            return 1; // this means (playerA.rank > playerB.rank)
            // isn't it nice when someone explains their code
        }

        return Match;
    });

