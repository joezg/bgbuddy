define(['model/matchesStat', 'i18next'],
    function (MatchesStat, i18n) {
        var StatsByItem = function() {
            this.total = new MatchesStat(i18n.t("scorer.total"));
            this.items = {};
        }

        //match - match to add statistic fot
        //name - name of an item (buddy or game or location...)
        //itemId - id of an item (buddyId, gameId, if user is buddy -1 should be passed)
        //itemIsUser - indicates whether item is user
        //mainId - for this Id wins/losses is calculated
        //mainIsUser - indicates if main is user
        //specificId - for this Id are specific wins/losses against calculated
        //specificIsUser - indicates if specific is user
        //calculates wins - indicates whether wins/losses will be calculated
        StatsByItem.prototype.addStatistic = function(match, itemName, itemId, itemIsUser, calculateWins, mainId, mainIsUser, specificId, specificIsUser){
            //fetching or adding an item for which stat is added
            if (!this.items.hasOwnProperty(itemName)){
                this.items[itemName] = new MatchesStat(itemName);
                this.items[itemName]._id = itemId;
                this.items[itemName].isUser = itemIsUser;
            }

            var currentItem = this.items[itemName];

            if (match.inProgress) {
                //matches in progres
                currentItem.addInProgress(match.iPlayed);
                this.total.addInProgress(match.iPlayed);
            } else {
                //total matches
                currentItem.addMatch(match.iPlayed);
                this.total.addMatch(match.iPlayed);
                //total time
                currentItem.addTime(match.getCompletedTotalTime(false, true), match.iPlayed)
                this.total.addTime(match.getCompletedTotalTime(false, true), match.iPlayed);

                // finished match
                if (match.isScored) {
                    //scored matches
                    currentItem.addScored(match.iPlayed);
                    this.total.addScored(match.iPlayed);

                    if (match.iWon){
                        //matches that user won
                        currentItem.addIWon();
                        this.total.addIWon();
                    } else if (match.iPlayed){
                        //matches that user lost
                        currentItem.addILost();
                        this.total.addILost();
                    }

                    if (calculateWins){
                        //wins/loses calculation

                        //main is winner
                        var mainIsWinner = mainIsUser ? match.iWon : match.isBuddyWinner(mainId);
                        if (mainIsWinner){
                            //main player wins
                            currentItem.addWin(match.iPlayed);
                            this.total.addWin(match.iPlayed);

                            //finding out whether this win was against specific player
                            var specificWasInPlay = specificIsUser ? match.iPlayed : match.didBuddyPlay(specificId);
                            if (specificWasInPlay){
                                var specificIsLooser = specificIsUser ? !match.iWon : !match.isBuddyWinner(specificId);

                                if (specificIsLooser){
                                    //specific wins against an item
                                    currentItem.addSpecificWin(match.iPlayed);
                                    this.total.addSpecificWin(match.iPlayed);
                                }
                            }
                        } else {
                            //player in context lost
                            currentItem.addLoss(match.iPlayed);
                            this.total.addLoss(match.iPlayed);

                            //finding out whether this win was against specific player
                            var specificWasInPlay = specificIsUser ? match.iPlayed : match.didBuddyPlay(specificId);
                            if (specificWasInPlay){
                                var specificIsWinner = specificIsUser ? match.iWon : match.isBuddyWinner(specificId) ;

                                if (specificIsWinner){
                                    //specific wins against an item
                                    currentItem.addSpecificLoss(match.iPlayed);
                                    this.total.addSpecificLoss(match.iPlayed);
                                }
                            }
                        }
                    }


                } else {
                    //total not scored matches
                    currentItem.addNotScored(match.iPlayed);
                    this.total.addNotScored(match.iPlayed);
                }
            }
        };

        function getMost(property, items, onlyWithMe, excludeUser){
            var maxCount = 0;
            var mostlyItems = [];

            for (var name in items) {
                var item = items[name];
                var total = onlyWithMe ? item[property+'WithMe'] : item[property];

                if (!item.isUser || !excludeUser) { // exclude user
                    if (total > maxCount) {
                        mostlyItems = [];
                        mostlyItems.push({name:item.name, count:total, _id:item._id });
                        maxCount = total;
                    } else if (total === maxCount) {
                        mostlyItems.push({name:item.name, count:total, _id:item._id});
                    }
                }
            }

            mostlyItems.sort(function(a, b){
                return a.name > b.name;
            });

            return {items: mostlyItems, count: maxCount};
        }

        StatsByItem.prototype.getMostPlayedItems = function(onlyWithMe, excludeUser){
            return getMost('totalMatches', this.items, onlyWithMe, excludeUser);
        }

        StatsByItem.prototype.getMostWinsByUser = function(){
            //only with me must be false because iWon property is only one and doesn't have WithMe suffix
            return getMost('iWon', this.items, false, true);
        }

        StatsByItem.prototype.getMostLossesByUser = function(){
            //only with me must be false because iWon property is only one and doesn't have WithMe suffix
            return getMost('iLost', this.items, false, true);
        }

        return StatsByItem;
    }
);