define([],
    function () {
        var Cache = function() {
            this.isCached = false;
            this.items = {};
            this.cacheTimestamp = null;
            this.cacheInvalidationTimeout = 120; // minutes
            this.debugMode = true; // set this to get console logs
        };

        Cache.prototype.addOrUpdateItems= function(newItems) { // can pass 1 item or array of items but note! *** requires _id property ***
                    if (this.debugMode) console.log("Cache: Invoked addOrUpdateItems");
                    if (!newItems)
                        return;
                    if( Object.prototype.toString.call( newItems ) !== '[object Array]' ) {
                        newItems = [newItems];
                    }
                    if (this.debugMode) console.log("Cache: Adding or updating " + newItems.length + " items");

                    for (var i=0; i<newItems.length; i++){
                        var item = newItems[i];
                        if (item._id)
                           this.items[item._id] = item;
                    }
                };
        Cache.prototype.setCached= function() { // set cached flag & time
                    if (this.debugMode) console.log("Cache: Invoked setCached");
                     this.isCached = true;
                   this.cacheTimestamp = new Date();
                    if (this.debugMode) console.log("Cache: Set cached flag, cacheTimestamp: " + this.cacheTimestamp);
                };
        Cache.prototype.isCacheValid= function() { // check whether cache is valid (complete)
                    if (this.debugMode) console.log("Cache: Invoked isCached");
                    if (!this.isCached || !this.cacheTimestamp) {
                        if (this.debugMode) console.log("Cache: Not cached or doesn't have timestamp");
                        return false;
                    }


                    var timeDiff = Math.abs(new Date().getTime() - this.cacheTimestamp.getTime());
                    var diffMinutes = Math.ceil(timeDiff / (1000 * 60));
                    if (this.debugMode) console.log("Cache: diffMinutes: " + diffMinutes);

                    if (diffMinutes > this.cacheInvalidationTimeout) {
                        if (this.debugMode) console.log("Cache: diffMinutes: " + diffMinutes + " > cacheInvalidationTimeout: " + this.cacheInvalidationTimeout);
                        return false;
                    }

                    if (this.debugMode) console.log("Cache: is cached, return true");
                    return true;
                };
        Cache.prototype.getItem= function(itemId) { // get particular item from cache (by item id)
                    if (this.debugMode) console.log("Cache: Invoked getItem by id " + itemId);
                    return this.items[itemId];
                };
        Cache.prototype.removeItem= function(itemId) { // remove particular item from cache (by item id)
                    if (this.debugMode) console.log("Cache: Invoked removeItem by id " + itemId);
                    if (this.items.hasOwnProperty(itemId)) {
                        delete this.items[itemId];
                        if (this.debugMode) console.log("Cache: Item found and deleted");
                        return true;
                    }

                    if (this.debugMode) console.log("Cache: Item not found");
                    return false;
                };
        Cache.prototype.getAll= function() { // get all items from cache
                    if (this.debugMode) console.log("Cache: Invoked getAll");
                    var returnItems = [];
                    for (var item in this.items) {
                        if (this.items.hasOwnProperty(item)) {
                            returnItems.push(this.items[item]);
                        }
                    }
                    if (this.debugMode) console.log("Cache: Returning " + returnItems.length + " items");
                    return returnItems;
                };
        Cache.prototype.getItemCount= function() { // get item count
                if (this.debugMode) console.log("Cache: Invoked getItemCount");
                var count = 0;
                for (var item in this.items) {
                    if (this.items.hasOwnProperty(item)) {
                        count++;
                    }
                }
                if (this.debugMode) console.log("Cache: item count: " + count);
                return count;
        };
        Cache.prototype.destroy= function () { // destroy cache
                    if (this.debugMode) console.log("Cache: Invoked destroy");
                    this.isCached = false;
                    this.items = {};
                    this.cacheTimestamp = null;

        };

        return Cache;

});