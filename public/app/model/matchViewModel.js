define(['knockout', 'underscore', 'model/matchPhaseEnum'],
    function (ko, _, matchPhaseEnum) {
        var initPlayer = function(player){
            player.isFirst = ko.observable(player.firstPlayer);
            player.isFirst.subscribe(function(newValue){
                player.firstPlayer = newValue;
            })
        }

        var ctor = function(match){
            this.gameName = ko.observable(match.game);
            this.location = ko.observable(match.location);
            this.location.subscribe(_.bind(function(newValue){
                this.match.location = newValue;
            }, this));
            this._id = match.id;
            this.match = match;
            this.getMatchModel = ko.observable(match);
            this.players = ko.observableArray(this.match.getPlayers());
            this.totalTime = ko.observable(null); //set in setPhases
            this.completedPhases = ko.observableArray([]);
            this.ongoingPhase = ko.observable({name:null, time:null });
            this.gameInfo = ko.observable(null);
            this.gameInfoClass = ko.observable(null);
            this.paused = ko.observable(false);
            this.unpaused = ko.computed(_.bind(function(){
                return !this.paused();
            }, this));

            this.isScored = ko.computed(_.bind(function(){
                return _.any(this.players(), function(p){
                    return p.hasOwnProperty('rank') || p.hasOwnProperty('result') || p.hasOwnProperty('winner');
                })
            }, this));

            this.syncPlayers();
            this.setGameInfo();
            this.setPhases();

            this.playersUndoData = null;
            this.phasesUndoData = null;
        }

        ctor.prototype.reloadGameDataAndInfos = function(match) {
            _.extend(this.match, _.pick(match, 'game', 'gameId', 'isGameOutOfDust', 'isNewGame'));
            this.reloadMatchModel();
            this.gameName(this.match.game);
        }

        ctor.prototype.syncPlayers = function(){
            this.players(this.match.players);
            _.each(this.players(), initPlayer);
        }

        ctor.prototype.setPlayersUndo = function(){
            this.playersUndoData = _.map(this.match.players, _.clone);
        }

        ctor.prototype.deletePlayersUndo = function(){
            this.playersUndoData = null;
        }

        ctor.prototype.undoPlayersChanges = function(){
            if (null == this.playersUndoData)
                throw new Error('There is no undo data. Call setPlayerUndo before trying to undo player changes.')

            this.match.players = this.playersUndoData;
            this.syncPlayers()

            this.playersUndoData = null;
        }

        ctor.prototype.updatePlayersIds = function(players){
            var existingAnonymousIds = _.reduce(this.players(), function(memo, p){
                if (p.isAnonymous && p._id){
                    memo.push(p._id);
                }
                return memo;
            }, []);

            this.players().forEach(_.bind(function(player){
                if (!player._id){
                    var newPlayer = _.find(players, function(p){
                        if (player.isUser)
                            return p.isUser;
                        else if(player.isAnonymous && p.isAnonymous)
                            return _.indexOf(existingAnonymousIds, p._id) == -1;
                        else if (player.buddyId)
                            return p.buddyId == player.buddyId;
                        else
                            return p.name = player.name;

                        return false;
                    });

                    if (!newPlayer)
                        throw new Error('Sync not successful');

                    player._id = newPlayer._id;
                }

            }, this))
        }

        ctor.prototype.addPlayer = function(player){
            if (this.match.addPlayer(player)){
                initPlayer(player);
                this.players.valueHasMutated();
            }
        }

        ctor.prototype.removePlayer = function(player){
            this.match.removePlayer(player);
            this.players.valueHasMutated()
        }

        ctor.prototype.setPhasesUndo = function(){
            this.phasesUndoData = _.map(this.match.matchPhases, _.clone);
        }

        ctor.prototype.undoPhasesChanges = function(){
            if (null == this.phasesUndoData)
                throw new Error('There is no undo data. Call setPhasesUndo before trying to undo phases changes.')

            this.match.matchPhases = this.phasesUndoData;
            this.setPhases();
            this.phasesUndoData = null;
        }

        ctor.prototype.deletePhasesUndo = function(){
            this.phasesUndoData = null;
        }

        ctor.prototype.startNewPhase= function(phaseType, name){
            this.match.finishOngoingPhase();
            var newPhase = this.match.startNewPhase(phaseType, name);

            this.setPhases();

            return newPhase;
        }

        ctor.prototype.setPhases = function(){
            var newPhases = this.match.getCompletedPhases();
            newPhases.forEach(function(p){
                if (!ko.isObservable(p.timeObservable)){
                    p.timeObservable = ko.observable(p.time);
                } else {
                    p.timeObservable(p.time);
                }
            })
            this.completedPhases(newPhases);

            var ongoingPhase = this.match.getOngoingPhase(true);
            if (ongoingPhase) {
                this.ongoingPhase({
                    name: ongoingPhase.name,
                    start: ongoingPhase.start,
                    time: ko.observable(ongoingPhase.time)
                });

                if (ongoingPhase.type == matchPhaseEnum.pause){
                    this.paused(true);
                } else {
                    this.paused(false)
                }
            }

            this.totalTime(this.match.getCompletedTotalTime(false, false));
        }
        ctor.prototype.startTimers = function(){
            this.totalTimeInterval = setInterval(_.bind(function(){
                var time = moment.utc(moment().diff(moment(this.match.date)))
                this.totalTime(time.format('HH:mm:ss'));
            }, this), 1000);

            if (this.ongoingPhase()){
                if (this.ongoingInterval) clearInterval(this.ongoingInterval);
                this.ongoingInterval = setInterval(_.bind(function(){
                    var time = moment.utc(moment().diff(moment(this.ongoingPhase().start)))
                    this.ongoingPhase().time(time.format('HH:mm:ss'));
                }, this), 1000);
            }
        }

        ctor.prototype.stopTimers = function(){
            if (this.totalTimeInterval){
                clearInterval(this.totalTimeInterval);
                this.totalTimeInterval = null;
            }
            if (this.ongoingInterval) {
                clearInterval(this.ongoingInterval);
                this.ongoingInterval = null;
            }
        }

        ctor.prototype.setGameInfo = function(){
            if (this.match.isGameOutOfDust){
                this.gameInfo('scorer.outOfDust');
                this.gameInfoClass('outOfDustLabel');
            }

            if (this.match.isNewGame){
                this.gameInfo('scorer.new');
                this.gameInfoClass('newLabel');
            }
        }

        ctor.prototype.setDate = function(newDate){
            this.match.setDate(newDate);
            this.getMatchModel.valueHasMutated();
        }

        //this will refresh views bound to getMatchModel observable
        ctor.prototype.reloadMatchModel = function(){
            this.getMatchModel.valueHasMutated();
        }

        //reload players
        ctor.prototype.reloadPlayers = function(){
            this.players.refresh();
        }

        ctor.prototype.clean = function(){
            this.gameName(null);
            this.location(null);
            this._id = null;
            this.match = null;
            this.getMatchModel(null);
            this.gameInfo(null);
            this.gameInfoClass(null);
            this.players([]);
            this.totalTime(null);
            this.completedPhases([]);
            this.ongoingPhase({name:null, time:null });
            this.paused(false);
            this.playersUndoData = null;
            this.phasesUndoData = null;

            this.stopTimers();
        }

        return ctor;
    }
);
