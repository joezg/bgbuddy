define(['knockout', 'underscore'], function(ko, _) {
    var ctor = function() { };

    ctor.prototype.activate = function(settings) {
        var self = this;

        this.players = settings.players;
        this.positions = ko.observableArray();
        this.selectCallback = settings.selectCallback || null;
        this.selectKey = settings.selectKey
        this.selectContext = settings.selectContext || null;
        this.nameField = settings.nameField || 'name';

        if (ko.isObservable(this.players)){
            this.players.subscribe(function(newValues){
                self.positions(_.range(1, self.players().length + 1));
            });
        } else {
            this.players = ko.observableArray(this.players);
        }

        this.invoke = function(data){
            self.selectCallback.call(self.selectContext, data);
        }


        this.positions(_.range(1, this.players().length + 1));
    };

    return ctor;
});