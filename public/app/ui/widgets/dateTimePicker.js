define(['jquery', 'knockout', 'moment', 'i18next', 'bootstrap-datetimepicker'],function($, ko, moment, i18n) {
    var ctor = function() { };

    ctor.prototype.activate = function(settings) {
        this.placeholderKey = settings.placeholderKey;
        this.options = settings.options || {};

        if (!ko.isObservable(settings.value)){
            this.value = ko.observable(settings.value);
        } else {
            this.value = settings.value;
        }
    };

    ctor.prototype.attached = function(view){
        var self = this;
        var $dtp = $(view).children(":first")

        $dtp.datetimepicker();
        $dtp.children("input").attr("placeholder", i18n.t(this.placeholderKey));

        var picker = $dtp.data('DateTimePicker');
        this.picker = picker;
        this.picker.options(this.options);

        this.picker.date(moment(this.value()));

        $dtp.on("dp.change",function (e) {
            self.value(self.picker.date().toDate());
        });
    }

    return ctor;
});