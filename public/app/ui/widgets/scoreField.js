define(['knockout'], function(ko) {
    var ctor = function() { };

    ctor.prototype.activate = function(settings) {
        this.score = settings.score;
        if(!ko.isObservable(this.score))
            this.score = ko.observable(this.score);-1

        this.smallDecrease = ko.observable(-1);
        this.largeDecrease = ko.observable(-10);
        this.smallIncrease = ko.observable(1);
        this.largeIncrease = ko.observable(10);

    };

    return ctor;
});