define(['lodash', 'knockout', 'blobStream'],function(_, ko, blobStream) {
    var ctor = function() { };

    ctor.prototype.activate = function(settings) {
        this.url = ko.observable();

        var doc = settings.document;
        var blobStream  = require('blobStream');
        var stream = doc.pipe(blobStream());
        doc.end();

        stream.on('finish', _.bind(function(){
            // or get a blob URL for display in the browser
            var url = stream.toBlobURL('application/pdf');
            this.url(url);
        }, this))
    };

    return ctor;
});