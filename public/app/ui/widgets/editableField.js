define(['knockout', 'i18next', 'moment', 'util/dialogWrapper', 'modals/common/changeValue'],
    function(ko, i18n, moment, dialog, changeValueDlg) {
    var ctor = function() { };

    ctor.prototype.activate = function(settings) {
        //reading settings
        this.icon = settings.icon;
        this.titleKey = settings.titleKey
        this.placeholderKey = settings.placeholderKey
        this.callback = settings.onSave;
        this.value = settings.value;
        this.notSetKey = settings.notSetKey || 'common.notSet';
        this.showNotSet = settings.showNotSet === false ? false : true;
        this.changeKey = settings.changeKey || 'common.change';
        this.dropdownOptions = settings.dropdownOptions || null;
        this.dropdownProperty = settings.dropdownProperty || 'name';
        this.noTitle = settings.noTitle ? settings.noTitle : false;
        this.isBold = true;
        this.isItalic = false;
        if (settings.isBold === false){
            this.isBold = false;
        }
        if (settings.isItalic){
            this.isItalic = true;
            this.isBold = false;
        }

        //setup
        this.isEditing = ko.observable(false);
        this.root = settings.activationData.bindingContext.$root;
        this.title = i18n.t(this.titleKey);
        this.placeholder = i18n.t(this.placeholderKey)
        this.isSelected = ko.observable(false);

        if (!ko.isObservable(this.value)){
            this.value = ko.observable(this.value);
        }

        var self = this;
        var setType = function(type){
            self.isDate = false;
            self.isDropdown = false;
            self.isBoolean = false;

            if (null != type)
                self[type] = true;
        }

        this.showValue = ko.observable();
        if (this.value() instanceof Date){
            setType('isDate');
            this.showValue(moment(this.value()).fromNow());
        } else if (this.dropdownOptions){
            setType('isDropdown');
            this.showValue(this.value()[this.dropdownProperty]);
        } else if ((this.value() || this.value() ===false) && this.value().constructor === Boolean){
            setType('isBoolean');
        } else {
            setType(null);
            this.showValue(this.value());
        }

        this.startChange = function(){
            if (this.isDropdown || this.isDate) {
                var self = this;

                var params = {
                    value: this.value(),
                    label: i18n.t(this.titleKey),
                    isDate: this.isDate,
                    isDropdown: this.isDropdown,
                    options: this.dropdownOptions,
                    optionsCaption: this.placeholderKey,
                    placeholderKey: this.placeholderKey
                }

                dialog.show(changeValueDlg, params).then(function(newValue){
                    if (null != newValue) {
                        self.value(newValue);
                        if (self.isDate){
                            self.showValue(moment(self.value()).fromNow());
                        }
                        self.callback.call(self.root, newValue);
                    }
                }).done();
            } else {
                if (this.previousValue != null)
                    this.value(this.previousValue);
                else
                    this.previousValue = this.value();

                this.isEditing(true);
                this.isSelected(true);
            }
        }

        this.onSwitchChange = function(newValue){
            this.callback.call(this.root, newValue);
        }

        this.cancelEditing = function(){
            this.isEditing(false);
        }

        this.endEditing = function(){
            this.isEditing(false)
            this.previousValue = null;
            this.callback.call(this.root, this.value());
            return false;
        }

    };

    return ctor;
});