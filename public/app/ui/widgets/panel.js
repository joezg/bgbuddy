define(['knockout', 'underscore'], function(ko, _) {
    var ctor = function() {
    };

    ctor.prototype.activate = function(settings) {
        var self = this;

        /*
         //any property should not be expression that includes observables - in that case
         //complete widget would be reloaded because it would change options of panel binding
         //The problem is that panel will open/close based on isOpenByDefault any time one of
         //that observables changes
         headerButton = {
             visible: <observable or scalar>,
             click: <function>,
             clickBubble: true/false,
             tooltipKey: <string>,
             icon: <string>
         }
         button = {
             columnClass: <string>,
             click: <function>,
             enable: <observable or scalar>,,
             titleKey: <string>
         }
         */

        this.panelName = settings.panelName || generateGUID();
        this.headerButtons = settings.headerButtons || [];
        this.headerButtons.forEach(function(button){
            if (typeof button.visible === 'undefined')
                button.visible = true;

            if (typeof button.clickBubble === 'undefined')
                button.clickBubble = false;
        });
        this.headerIcon = settings.headerIcon || null;
        this.titleKey = settings.titleKey;
        this.isOpenByDefault = settings.isOpenByDefault === false ? false : true;
        this.buttons = settings.buttons || [];
        this.buttons.forEach(function(button){
            if (typeof button.enable === 'undefined')
                button.enable = true;
        });
    };

    var generateGUID = function(){
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }

    return ctor;
});