define(['knockout', 'jquery', 'underscore', 'i18next', 'modals/help/searchListHelp', 'util/dialogWrapper','durandal/app'], function(ko, $, _, i18n, searchListHelpDlg, dialog, app) {
    var ctor = function() { };

    var compareCustomField = function(currentValue, targetValue){
        var localeYes = i18n.t("common.yes_lower").toLowerCase();
        var localeNo = i18n.t("common.no_lower").toLowerCase();

        var t = typeof currentValue;

        if (t=='object' && currentValue instanceof Array){
            t='array';
        }

        var matches = false;

        switch (t){
            case 'array':
                var preparedString = targetValue.replace(/=/g,":").replace(/,/g, ";"); //using regex because normal replace only replaces first occurrence
                var conditions = getCustomFilterConditions(preparedString);

                if (conditions.length>0){
                    for (var i=0;i<currentValue.length;i++){
                        var currentItem = currentValue[i];

                        for (var j=0; j<conditions.length;j++){
                            var condition = conditions[j];

                            if (currentItem.hasOwnProperty(condition.field)){
                                matches = compareCustomField(currentItem[condition.field], condition.value);

                                if (!matches)
                                    break;
                            } else {
                                matches = false;
                                break;
                            }
                        }

                        if (matches) //found at least one item in array that satisfies given conditions so skip other items
                            break;
                    }
                }
                break;
            case 'boolean':
                //if ((targetValue.toLowerCase() == 'yes' || targetValue == 1 || targetValue.toLowerCase() == 'true' || targetValue.toLowerCase() == localeYes) && currentValue)
                if (('yes'.slice(0, targetValue.length) == targetValue.toLowerCase()
                    || 'true'.slice(0, targetValue.length) == targetValue.toLowerCase()
                    || localeYes.slice(0, targetValue.length) == targetValue.toLowerCase()
                    || targetValue == 1) && currentValue)
                    matches = true;

                //if ((targetValue.toLowerCase() == 'no' || targetValue == 0 || targetValue.toLowerCase() == 'false'  || targetValue.toLowerCase() == localeNo) && !currentValue)
                if (('no'.slice(0, targetValue.length) == targetValue.toLowerCase()
                    || 'false'.slice(0, targetValue.length) == targetValue.toLowerCase()
                    || localeNo.slice(0, targetValue.length) == targetValue.toLowerCase()
                    || targetValue == 0) && !currentValue)
                    matches = true;

                break;
            case 'number':
                matches = targetValue == currentValue;
                break;
            case 'string':
                if (targetValue.indexOf('~')===0){
                    var newValue = targetValue.substring(1);
                    matches = currentValue.toLowerCase().slice(0, newValue.length) === newValue.toLowerCase();
                } else {
                    matches = currentValue.toLowerCase().indexOf(targetValue.toLowerCase())>-1;
                }
                break;
            default:
                //no match
                break;
        }

        return matches;
    };

    var getSortFunction = function(sortField, sortDirection, sortFunction){
        if (null == sortFunction && null != sortField) {
            sortFunction = function(a, b){
                if (a[sortField] == b[sortField])
                    return 0;

                if (!a.hasOwnProperty(sortField))
                    return 1;

                if (!b.hasOwnProperty(sortField))
                    return -1;

                if (sortDirection == 'desc')
                    var compare = a[sortField] < b[sortField] ? 1 : -1;
                else
                    var compare = a[sortField] > b[sortField] ? 1 : -1;

                return compare;
            }
        }

        return sortFunction;
    }

    var getCustomFilterConditions = function(searchString){
        var conditions = [];

        if (!searchString)
            return [];

        var conditionStrings = searchString.split(";");

        for (var i=0; i<conditionStrings.length; i++){
            var currentString = conditionStrings[i];

            var parts = currentString.split(":");

            if (parts.length>1 && parts[0].trim() && parts[1].trim()){
                conditions.push({
                   field: parts[0].trim(),
                   value: parts[1].trim()
                });
            }
        }

        return conditions;
    };

    ctor.prototype.filter = function(){
        this.showMoreButtonFlag(false); // by dflt set to false
        var str = this.searchString();
        var shownItemsCounter = 0; // shown items counter

        var data = this.data;

        if (this.searchCustomFields)
            var conditions = getCustomFilterConditions(str);

        for (var i=0; i<data.length; i++){
            var d = data[i];

            // if !showAll, don't even check search string, doesn't matter, just set show to false and continue
            if (!this.showAll && (shownItemsCounter >= (this.itemCount+this.itemCountDiff()))) {
                d.show(false);
                this.showMoreButtonFlag(true); // if an item was set not to show because of itemcount limit, only then set flag to true
                continue;
            }

            if (!str){
                d.show(true);
                shownItemsCounter++;
                d.counter(shownItemsCounter);
                continue;
            } else {
                var nameMatches = false;
                if (this.nameField && d[this.nameField]) {
                    nameMatches = d[this.nameField].toLowerCase().slice(0, str.length) == str.toLowerCase();
                }

                var idMatches = false;
                if (!nameMatches && this.searchById && $.isNumeric(str))
                    idMatches = (d[this.idField] - this.idOffset) == str || d[this.idField] == str;

                var customFieldMatches = false;
                if (!nameMatches && !idMatches && this.searchCustomFields){
                    if (conditions.length > 0){
                        for (var j=0; j<conditions.length;j++){
                            var condition = conditions[j];

                            if (d.hasOwnProperty(condition.field)){
                                customFieldMatches = compareCustomField(d[condition.field], condition.value);

                                if (!customFieldMatches)
                                    break;
                            } else {
                                customFieldMatches = false;
                                break;
                            }
                        }

                    }
                }

                if (nameMatches || idMatches || customFieldMatches) {
                    d.show(true);
                    shownItemsCounter++;
                    d.counter(shownItemsCounter);
                } else {
                    d.show(false);
                }
            }
        }

    }

    ctor.prototype.activate = function(settings) {
        var self = this;
        //data to show in searchList
        this.data = ko.unwrap(settings.data);
        //callback that is called when item in searchList is clicked
        this.selectCallback = settings.selectCallback;
        //label key for select action for every element in list
        this.selectKey = settings.selectKey || 'common.select';
        //field in objects in data that represents the name of the item
        this.nameField = settings.nameField || 'name';
        //field in objects in data that represents the id of the item
        this.idField = settings.idField || '_id';
        //indicates whether id will be shown on every item
        this.showId = settings.showId || false;
        //indicates id offset for searching (i.e. if all ids starts from 1000, searching for 25 will match 1025)
        this.idOffset = settings.idOffset || 0;
        //indicates whether search by id field is enabled
        this.searchById = settings.searchById || false;
        //indicates whether search by any field is enabled
        this.searchCustomFields = settings.searchCustomFields || false;
        // show all data from list at once, no pagination
        this.showAll = settings.showAll || false;
        // item number to show initially and add subsequently when user chooses to fetch more. set default number here.
        // irrelevant if showAll flag is set. ribica loves comments.
        this.itemCount = settings.itemCount || 20; // initial item count to show
        //doSort fields indicate whether searchList would be sorted
        if (!settings.hasOwnProperty('doSort'))
        {
            this.doSort = true
        } else
        {
            this.doSort = settings.doSort;
        }
        //by this field, searchList will be sorted, assuming this field is string field
        this.sortField = settings.sortField || 'name';
        //custom sorting function callback
        this.sortFunction = settings.sortFunction || null;
        //indicates sort direction. Must be asc or desc.
        this.sortDirection = settings.sortDirection || 'asc';
        //*** SECONDARY SORTING
        //*** This is only available if primary sort is done through sortField and not through sortFunction.
        //*** If sortFunction is provided that function must implement secondary sort in itself.
        //by this field, searchList will be sorted for all items that are equal by first sortField, assuming this field is string field
        this.secondarySortField = settings.secondarySortField || null;
        //custom sorting function callback for secondary sort
        this.seconsarySortFunction = settings.seconsarySortFunction || null;
        //indicates sort direction for secondary sort. Must be asc or desc.
        this.secondarySortDirection = settings.secondarySortDirection || 'asc';
        //indicates whether add new item button should be visible
        this.addItemButtonVisible = settings.addItemButtonVisible || false;
        this.addItemButtonCallback = function(){
            if (settings.addItemButtonCallback){
                settings.addItemButtonCallback.apply(settings.bindingContext.$root);
            }
        }

        //custom filters
        this.filters = settings.customFilters || [];
        this.clearFilter = function(){
            self.searchString("");
        }
        this.filterSelected = function(filter){
            self.searchString(filter.filter);
        }
        this.customFilterItemClass = settings.customFilterItemClass || 'col-md-3 col-sm-6 col-xs-6';

        //string that is searched in list
        if (!settings.searchString) {
            this.searchString = ko.observable("");
        } else {
           if (!ko.isObservable(settings.searchString)){
               this.searchString = ko.observable(settings.searchString);
           } else {
               this.searchString = settings.searchString;
           }
        }


        this.itemCountDiff = ko.observable(0); // items shown after initial itemCount (number of items to show after 'more...' has been selected). will be incremented upon user selection.
        this.showMoreButtonFlag = ko.observable(false); // i think this flag solves our show more button visibility problem after search

        this.showHelp = function () {
            dialog.show(searchListHelpDlg, { id: this.searchById, fields: this.searchCustomFields});
        };
        this.fetchMore = function() {
            this.itemCountDiff(this.itemCountDiff() + this.itemCount); // increment items to show by initial self.itemCount
        };

        //Preparing the data
        if (this.data.length > 0){
            //adding show field to every element in data
            this.data.forEach(function(d){
                d.show = ko.observable(true);
                d.counter = ko.observable(0);
            });

            //sorting
            if (this.doSort){
                var sortFunction = getSortFunction(this.sortField, this.sortDirection, this.sortFunction);
                var secondarySortFunction = getSortFunction(this.secondarySortField, this.secondarySortDirection, this.seconsarySortFunction);
                //this.data.sort(sortFunction);
                this.data.sort(function(a, b){
                    var result = sortFunction(a, b);

                    if (result == 0 && secondarySortFunction)
                        result = secondarySortFunction(a, b);

                    return result;
                });
            }

            //filtering
            ko.computed(this.filter, this);
        }

        this.select = function(item, context){
            this.selectCallback.call(settings.bindingContext.$root, item, context);
        }
    };

    return ctor;
});