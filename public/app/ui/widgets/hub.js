define(['i18next', 'knockout'],function(i18n, ko) {
    var ctor = function() { };

    ctor.prototype.activate = function(settings) {
        var context = settings.bindingContext;
        this.action = function(){
            var args = arguments;
            var parentContext = context.$parent ? context.$parent : context.$root;
            settings.action.apply(parentContext, args);
        }

        this.actionTextExists = ko.observable(false);

        var self = this;
        if (ko.isObservable(settings.actionTextKey)){
            this.actionText = ko.pureComputed(function(){
                var text = i18n.t(settings.actionTextKey())
                self.actionTextExists(!!text);
                return text;
            });
        } else {
            this.actionText = settings.actionTextKey ? i18n.t(settings.actionTextKey) : '';
            self.actionTextExists(!!this.actionText);
        }


        this.data = settings.data || null;
        this.context = settings.context || null;
    };

    return ctor;
});