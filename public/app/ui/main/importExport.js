define(['knockout', 'dao/bggDAO'],
    function (ko, bggDAO) {
        var code = null;

        return {
            /********************BINDINGS*************************/
            actionPending: ko.observable(false),
            bggUsername: ko.observable(),
            importBggGames: function() {
                bggDAO.importCollection(this.bggUsername()).done();
            },

            /********************LIFECYCLE CALLBACKS**************/
            activate: function() {
                this.bggUsername("");
                this.actionPending(false);
            }
        };
    }
);