define(['knockout', 'dao/userDAO', 'durandal/app','model/user','i18next', 'util/dialogWrapper', 'modals/user/sendInviteModal'],
    function (ko, userDAO, app, user, i18n, dialog, sendInviteDlg) {
        var color = ko.observable();

        return {
            /********************BINDINGS*********************************/
            name: ko.observable(),
            username: ko.observable(),
            color: color,
            email: ko.observable(),
            theme: ko.observable(),
            colors: [
                {color:"red",i18n:"common.red", css:"meepleRed"},
                {color:"orange",i18n:"common.orange", css:"meepleOrange"},
                {color:"yellow",i18n:"common.yellow", css:"meepleYellow"},
                {color:"green",i18n:"common.green", css:"meepleGreen"},
                {color:"blue",i18n:"common.blue", css:"meepleBlue"},
                {color:"pink",i18n:"common.pink", css:"meeplePink"},
                {color:"purple",i18n:"common.purple", css:"meeplePurple"},
                {color:"black",i18n:"common.black", css:"meepleBlack"}
                ],
            themes: ["default", "cerulean", "darkly", "flatly", "sandstone", "slate", "superhero", "united"],
            chooseColor: function(daColor) {
                userDAO.updateUserProfile({color: daColor.color}).then(function(res){

                    if (res.success === true) {
                        //app.trigger('msg.new', {text:"setting color to " + daColor.color, type:"success", timeout: 3000});
                        color(daColor.color);
                        user.reload();
                        app.trigger('themeMeeple.change', daColor.color);
                    }
                    else
                        app.trigger('msg.new', {text:i18n.t("err.couldntSave"), type:"danger", timeout: 3000});


                });

            },
            sendInviteEmail: function() {
                dialog.show(sendInviteDlg, this.username()).then(function(success) {
                    if (success)
                        app.trigger('msg.new', {text:i18n.t("user.invitationEmailsSentSuccessfully"), type:"success", timeout: 3000});
                });

            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (){
                var self = this;
                self.name("");
                self.username("");
                self.email("");
                color("");

                self.theme.subscribe(function(theme){
                    if (self.themeInitialChange){
                        self.themeInitialChange = false;
                    } else {
                        app.trigger('theme.change', theme);
                        userDAO.updateUserProfile({theme: theme})
                            .then(function(){
                                user.reload();
                            });
                    }
                });

                return user.whenLoaded().then(function(u){
                    self.name(u.name);
                    self.username(u.username);
                    self.email(u.email);

                    self.themeInitialChange = true;
                    self.theme(u.theme || 'default');
                    color(u.color);

                })
            }
            
        };
    }
);