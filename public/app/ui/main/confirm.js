define(['knockout', 'dao/userDAO', 'durandal/app', 'i18next', 'plugins/router'],
    function (ko, userDAO, app, i18n, router) {
        var code = null;

        return {
            /********************BINDINGS*************************/
            success: ko.observable(false),
            resendVisible: ko.observable(false),
            actionPending: ko.observable(false),
            resendSuccessful: ko.observable(false),
            username: ko.observable(),
            password: ko.observable(),
            confirm: function() {
                var self = this;

                userDAO.confirmUser(code)
                    .then(function() {
                        self.success(true);
                    })
                    .catch(function(error){
                        if (error.responseJSON.invalidCode){
                            self.resendVisible(true);
                            self.resendSuccessful(false);
                        }
                    });
            },
            start: function(){
                router.navigate("#");
            },
            resend: function(){
                var self = this;
                this.actionPending(true);
                userDAO.resendConfirmationCode({username: this.username(), password: this.password()})
                    .then(function(){
                        self.resendSuccessful(true);
                        self.actionPending(false);
                    }).catch(function(){
                        self.actionPending(false);
                    });
            },

            /********************LIFECYCLE CALLBACKS**************/
            activate: function(confirmationCode) {
                this.success(false);
                this.resendVisible(false);
                this.username("");
                this.password("");
                this.resendSuccessful(false);
                this.actionPending(false);
                code = confirmationCode;

                this.confirm();
            }
        };
    }
);