define(['knockout', 'dao/userDAO', 'durandal/app', 'i18next', 'plugins/router'],
    function (ko, userDAO, app, i18n, router) {
        var code = null;

        return {
            /********************BINDINGS*************************/
            actionPending: ko.observable(false),
            email: ko.observable(),
            username: ko.observable(),
            recoverPassword: function(){
                userDAO.sendPasswordRecoveryRequest(this.username()).then(function(){
                    app.trigger('msg.new', {text: i18n.t('loginProblems.passwordRecoverySuccessMsg'), type: 'success', timeout:10000});
                })
            },
            recoverUsername: function() {
                userDAO.recoverUsername(this.email()).then(function(){
                    app.trigger('msg.new', {text: i18n.t('loginProblems.usernameRecoverySuccessMsg'), type: 'success', timeout:10000});
                })
            },

            /********************LIFECYCLE CALLBACKS**************/
            activate: function() {
                this.email(null);
                this.actionPending(false);
                this.username(null);
            }
        };
    }
);