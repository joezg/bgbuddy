define(
    ['plugins/router', 'knockout', 'dao/userDAO', 'dao/matchDAO', 'model/match', 'durandal/app', 'model/user','dao/msgDAO', 'i18next', 'q'],
    function (router, ko, userDao, matchDAO, Match, app, userModel, msgDAO, i18n, Q) {
        var lastMatch = ko.observable({game:'', inProgress: false, date: null, dateFromNow: null});
        var newMessages = ko.observable();

        return {
            /********************BINDINGS*************************/
            name: ko.observable(),
            newMessages: newMessages,
            lastMatch: lastMatch,
            matchIsLoading: ko.observable(true),
            startNewMatch: function(){
                router.navigate('#scorer/newMatch');
            },
            viewMatch: function(){
                router.navigate('#scorer/viewMatch/'+lastMatch().id);
            },
            viewProfile: function(){
                router.navigate('#profile');
            },
            viewMessages:function(){
                router.navigate('#messages');
            },
            /********************LIFECYCLE CALLBACKS**************/
            activate: function () {
                var self = this;
                this.newMessages(0);

                var userLoadingPromise =  userModel.whenLoaded()
                    .then(function(usr){
                        self.name(usr.username);
                    });
                    
                var unreadMessagesPromise = msgDAO.getUnreadMessages()
                    .then(function(data){
                        self.newMessages((data&&data.unreadMessages)?data.unreadMessages.length:0);
                    });
                    
                var userMatchesPromise = matchDAO.getMatchesForUser(1)
                    .then(function(response){
                        if (response.matches && response.matches.length>=1){
                            var m = new Match(response.matches[0]);
    
                            self.lastMatch(m);
                        } else
                            self.lastMatch(null);
    
                        self.matchIsLoading(false);
                    });

                return Q.all([userLoadingPromise, userMatchesPromise, unreadMessagesPromise]);
            }
        };
    }
);
