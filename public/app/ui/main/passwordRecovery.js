define(['knockout', 'dao/userDAO', 'durandal/app', 'i18next', 'plugins/router'],
    function (ko, userDAO, app, i18n, router) {
        var code = null;

        return {
            /********************BINDINGS*************************/
            actionPending: ko.observable(false),
            password: ko.observable(),
            success: ko.observable(false),
            confirm: function() {
                var self = this;

                userDAO.changePasswordWithCode(this.password(), code)
                    .then(function() {
                        self.success(true);
                    });
            },
            start: function(){
                router.navigate("#");
            },

            /********************LIFECYCLE CALLBACKS**************/
            activate: function(confirmationCode) {
                this.password(null);
                this.actionPending(false);
                this.success(false);
                code = confirmationCode;

            }
        };
    }
);