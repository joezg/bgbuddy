define(['knockout', 'dao/gameDAO', 'plugins/router','model/user', 'durandal/app','i18next','moment','dao/msgDAO'],
    function (ko, gameDAO, router, user, app, i18n, moment, msgDAO) {

        return {
            /********************BINDINGS*********************************/
            messages: ko.observableArray(),
            inboxMessages: [],
            sentMessages: [],
            myFilter: ko.observable(),
            displayFolder: ko.observable('inbox'),
            containsUnreadMessages: ko.observable(false),
            sendMessage: function() {
                router.navigate('#messageSend');
            },
            selectMessage: function(context) {
                router.navigate('#message/'+context.data._id);
            },
            markAllAsRead: function() {
                var self = this;
                msgDAO.markAllAsRead().then(function(data){
                    if (data.success) {
                        self.containsUnreadMessages(false);
                        app.trigger('messages.refresh');
                        self.messages().forEach(function(msg){
                           msg.isUnread(false);
                        });
                    }
                });
            },
            filterUnread:  function() {
                this.myFilter("isUnread:"+i18n.t("common.yes_lower")+";");
            },
            filterContent:  function() {
                this.myFilter("content:");
            },
            filterClear:  function() {
                this.myFilter("");
            },
            showInbox:  function() {
                this.myFilter("");
                this.displayFolder('inbox');
                this.messages(this.inboxMessages);
            },
            showSent:  function() {
                this.myFilter("");
                this.displayFolder('sent');
                this.messages(this.sentMessages);
            },
            toMoment: function(date) {
                return moment(date);
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (){
                var self = this;
                
                self.messages([]);
                self.inboxMessages = [];
                self.sentMessages = [];
                self.containsUnreadMessages(false);

                // get messages
                return self.fetchMessages();
            },
            fetchMessages: function() {
                var self = this;

                var msgsSortFunction = function(a, b) {
                    return (new Date(a.dateSent) <  new Date(b.dateSent))?1:-1; // sort by date descending
                };

                return msgDAO.getMessages().then(function(data){
                    if (data.success) {
                        if (data.inbox) {
                            data.inbox.sort(msgsSortFunction);
                            for (var i=0; i<data.inbox.length; i++) { // check whether inbox contains unread messages to set flag
                                if (data.inbox[i].isUnread) {
                                    self.containsUnreadMessages(true);
                                    break;
                                }
                            }
                        }

                        if (data.sent)
                            data.sent.sort(msgsSortFunction);

                        self.inboxMessages = data.inbox;
                        self.sentMessages = data.sent;

                        self.messages(self.displayFolder()==='sent'?self.sentMessages:self.inboxMessages); // if more folders are added handle that here too

                        self.messages().forEach(function(msg){
                            msg.isUnread = ko.observable(msg.isUnread);
                        })
                    }

                });

            }
            
        };
    }
);