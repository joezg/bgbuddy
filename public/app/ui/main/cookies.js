define(
    ['plugins/router', 'durandal/app', 'knockout', 'dao/userDAO','model/user'],
    function (router, app, ko, userDAO, user) {

        return {
            /********************BINDINGS*************************/
            userApprovedCookies: ko.observable(false),
            sex: ko.observable(),
            cookiesApproved: function() {
                var self = this;
                userDAO.approveCookies().then(function(data) {
                    if (data.success) {
                        self.userApprovedCookies(true);
                    }
                });

            },
            /********************LIFECYCLE CALLBACKS**************/
            activate: function () {
                var self = this;
                
                return user.whenLoaded().then(function(u){
                    self.userApprovedCookies(u.cookieDisclaimerDismissed);
                    self.sex(u.sex);
                });
            }
        };
    }
);
