define(['knockout', 'plugins/router', 'model/user', 'durandal/app','i18next','moment','dao/msgDAO'],
    function (ko, router, user, app, i18n, moment, msgDAO) {

        return {
            /********************BINDINGS*********************************/
            subject: ko.observable(), // field currently not used
            content: ko.observable(),
            to: ko.observable(),
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (buddyUsername){
                this.subject("");
                this.content("");
                this.to("");

                // TODO fetch suggestions perhaps

                if (buddyUsername) {
                    this.to(buddyUsername);
                }
            },

            sendMessage: function() {
                var self = this;


                msgDAO.sendMessage({to:this.to(), content:this.content()}).then(function(data){
                    if (data.success) {
                        // show message and reroute back to message overview
                        app.trigger('msg.new', {text:i18n.t("msg.sentSuccessfully"), type:"success", timeout: 2000});
                        router.navigate('#messages');
                    }

                });

            }

        };
    }
);