define(['knockout', 'plugins/router', 'model/user', 'durandal/app','i18next','moment','dao/msgDAO'],
    function (ko, router, user, app, i18n, moment, msgDAO) {

        var viewModel = {
            /********************BINDINGS*********************************/
            id: ko.observable(),
            subject: ko.observable(), // field currently not used
            content: ko.observable(),
            from: ko.observable(),
            to: ko.observable(),
            dateSent: ko.observable(),
            folder: ko.observable(),
            replyMessage: ko.observable(),
            previousMessages: ko.observableArray(),
            reply: function() {
                var self = this;
                msgDAO.sendMessage({to:this.folder()==='inbox'?this.from():this.to(), content:this.replyMessage()}).then(function(data){
                    if (data.success) {
                        // show message and reroute back to message overview
                        //app.trigger('msg.new', {text:i18n.t("msg.sentSuccessfully"), type:"success", timeout: 2000});
                        //router.navigate('#messages');
                        self.previousMessages.unshift({content: self.replyMessage(), from:self.from(), to:self.to(), dateSent: new Date(), folder: 'sent'});
                        self.replyMessage("");
                    }

                });
            },
            toMoment: function(date) {
                return moment(date);
            },
            selectMessage: function(context) {
                for (var i=0; i<viewModel.previousMessages().length; i++) {
                    if (context._id === viewModel.previousMessages()[i]._id) {
                        viewModel.content(viewModel.previousMessages()[i].content); // display content of selected message
                        viewModel.dateSent(moment(viewModel.previousMessages()[i].dateSent).fromNow()); // show correct date
                        //viewModel.folder(viewModel.previousMessages()[i].folder); // change folder if need be
                        break;
                    }
                }
                // router.navigate('#message/'+context._id); // don't navigate, just show different content
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (id){
                var self = this;
                this.id(id);
                
                self.subject("");
                self.content("");
                self.from("");
                self.dateSent("");
                self.replyMessage("");
                self.previousMessages([]);


                return self.fetchMessage()
                    .then(function(buddy){
                        self.fetchAllMessagesWithUser(buddy); //this function is not passed to then directly because this would be bound to window
                    });

            },

            fetchMessage: function() {
                var self = this;

                return msgDAO.getMessage(self.id()).then(function(data){
                    if (data.success && data.message) {
                        self.content(data.message.content);
                        self.from(data.message.from);
                        self.to(data.message.to);
                        self.folder(data.folder);
                        self.dateSent(moment(data.message.dateSent).fromNow());

                        if (data.message.isUnread)
                            self.markAsRead(); // if successfully fetched and unread - mark as read

                        return self.folder() === 'inbox' ? self.from() : self.to();
                    } else
                        return null;
                });

            },

            fetchAllMessagesWithUser: function(penpal) {
                if (penpal) {
                    var self = this;
                    var msgsSortFunction = function(a, b) {
                        return (new Date(a.dateSent) <  new Date(b.dateSent))?1:-1; // sort by date descending
                    };
    
                    return msgDAO.getMessagesWithUser(penpal).then(function(data) {
    
                        data.inbox.forEach(function(i) {
                            i.folder = 'inbox';
                        });
                        data.sent.forEach(function(s) {
                            s.folder = 'sent';
                        });
                        var allMessages = data.inbox.concat(data.sent);
                        allMessages.sort(msgsSortFunction);
    
                        self.previousMessages(allMessages);
                    });
                }
            },

            markAsRead: function() {
                msgDAO.markAsRead(this.id()).then(function() {
                    app.trigger('messages.refresh');
                });
            }
            
        };

        return viewModel;
    }
);