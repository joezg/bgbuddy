define(['knockout', 'dao/userDAO', 'durandal/app', 'i18next'],
    function (ko, userDAO, app, i18n) {
        return {
            /********************BINDINGS*************************/
            actionPending: ko.observable(false),
            username: ko.observable(),
            email: ko.observable(),
            password: ko.observable(),
            sex: ko.observable(),
            success: ko.observable(false),
            choseFemale: function() {
                this.sex('f');
            },
            choseMale: function() {
                this.sex('m');
            },
            signUp: function() {
                var self = this;

                userDAO.signUp(this.username(), this.email(), this.password(), this.sex())
                    .then(function() {
                        app.trigger('msg.new', {text:i18n.t("signUp.signUpSuccessful"), type:"success", timeout: 2000});
                        self.success(true);
                    });
            },

            /********************LIFECYCLE CALLBACKS**************/
            activate: function() {
                this.success(false);
                this.actionPending(false);
                this.username(null);
                this.email(null);
                this.password(null);
                this.sex(null);
            }
        };
    }
);