define(['durandal/app', 'plugins/router', 'knockout', 'dao/loginDAO', 'dao/userDAO', 'i18next', 'jquery'],
    function (app, router, ko, loginDAO, userDAO, i18n) {
        return {
            /********************BINDINGS*************************/
            actionPending: ko.observable(false),
            username: ko.observable(),
            newUsername: ko.observable(),
            password: ko.observable(),
            success: function(){
                router.navigate('#');
                app.trigger('msg.new', {text:i18n.t("signUp.connectedGoogleAccount", {name: this.type=='fb'?'facebook':'google'}), type:"success", timeout: 2000});
            },
            connect: function() {
                var self = this;
                self.actionPending(true);

                var val = null;
                if (!self.username()){
                    val = $("#username").val();
                    if (!val){
                        self.showError(i18n.t("login.usernameRequired"));
                        return;
                    } else
                        self.username(val);
                }
                if (!self.password()){
                    val = $("#password").val();
                    if (!val) {
                        self.showError(i18n.t("login.passwordRequired"));
                        return;
                    } else
                        self.password(val);
                }

                loginDAO.login(self.username(),self.password(), null, true, self.type)
                    .then(function(data){
                        app.setSession(data.sessionId);
                        self.success();
                    })
                    .catch(function(err){
                        if (err.status === 403) {
                            self.showError(i18n.t("login.invalidCredentials"));
                        } else if (err.status === 412){
                            self.showError(i18n.t("login.userNotConfirmed"));
                        } else {
                            self.showError(i18n.t("err.genericError"), err);
                        }
                    })
            },
            createNew: function() {
                var self = this;
                this.actionPending(true);
                userDAO.createSocialConnectedUser(this.newUsername(), this.type)
                    .then(function(data){
                        self.success();
                    });
            },

            /********************LIFECYCLE CALLBACKS**************/
            activate: function(type) {
                this.type = type;
                this.actionPending(false);
                this.username(null);
                this.newUsername(null);
                this.password(null);
                router.navigate('#socialLoginConnect/'+type, { replace: true, trigger: false });
            },

            /********************UTILITY***************************/
            showError: function(msg, err){
                var msg = {text:msg, type:"danger"}
                if (err)
                    msg.error = err;
                else
                    msg.timeout = 3000;

                app.trigger('msg.new', msg);
                this.actionPending(false);
            }
        };
    }
);