define(['knockout', 'plugins/router', 'dao/eventDAO'],
    function (ko, router, eventDAO) {
        var eventOrganizerId = null;
        return {
            /********************BINDINGS*********************************/
            viewLibrary: function(){
                router.navigate('#event/admin/library/'+eventOrganizerId);
            },
            showParticipants: function(){
                router.navigate('#event/admin/participants/'+eventOrganizerId);
            },
            showEvents: function(){
                router.navigate('#event/admin/events/'+eventOrganizerId);
            },
            name: ko.observable(),

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (id){
                var self = this;
                eventOrganizerId = id;

                return eventDAO.getEventOrganizerInfo(eventOrganizerId)
                    .then(function(result){
                        self.name(result.info.name);
                    });
            }

        };
    }
);
