define(['knockout', 'dao/eventDAO', 'plugins/router', 'q'],
    function (ko, eventDAO, router, Q) {
        return {
            /********************BINDINGS*********************************/
            events: ko.observableArray(),
            administeredEventOrganizers: ko.observableArray(),
            selectAdministeredEventOrganizer: function(hub){
                var id = hub.data._id;

                router.navigate("#event/admin/"+id);
            },
            selectEvent: function(hub){
                var id = hub.data._id;
                var eventOrganizerId = hub.data.eventOrganizerId;

                router.navigate("#event/participant/event/"+id+"/"+eventOrganizerId);
            },

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (){
                var self = this;
                var eventPromise = eventDAO.getAllUserEvents().then(function(data){
                    self.events(data.events);
                });

                var eventAdministrationPromise = eventDAO.getAllUserAdministeredEventOrganizers().then(function(data){
                    self.administeredEventOrganizers(data.eventGroups);
                });
                
                return Q.all([eventPromise, eventAdministrationPromise]);
            }

        };
    }
);