define(['knockout', 'dao/eventDAO', 'q', 'moment', 'i18next', 'plugins/router', 'durandal/app'],
    function (ko, eventDAO, Q, moment, i18n, router, app) {
        var eventId = null;
        var eventOrganizerId = null;

        var removeFromArray = function(array, element){
            var index = array.indexOf(element);
            if (index > -1){
                array.splice(index, 1);
            }
        }

        return {
            /********************BINDINGS*********************************/
            eventName: ko.observable(),
            chosenGame: ko.observable(),
            date: ko.observable(),
            dateString: null,
            games: ko.observableArray(),
            participants: ko.observableArray(),
            players: ko.observableArray(),
            positions: ko.observableArray(),
            resetDate: function(){
              this.date(null);
            },
            removePlayer: function(player){
                this.participants.push(player.participant);
                removeFromArray(this.players, player);
            },
            resetGame: function(){
                this.chosenGame(null);
            },
            selectGame: function(item){
                this.chosenGame(item.data);
            },
            selectParticipant: function(item){
                var newPlayer = {
                    participant: item.data,
                    playerId: item.data._id,
                    result: ko.observable(0),
                    rank: ko.observable(0),
                    winner: ko.observable(false)
                };
                this.players.push(newPlayer);
                removeFromArray(this.participants, item.data);
            },
            saveMatch: function(){
                var match = {
                    date: this.date(),
                    gameId: this.chosenGame()._id,
                    game: this.chosenGame().name,
                    players: []
                }

                for (var i=0;i<this.players().length; i++){
                    var p = this.players()[i];
                    var player = {
                        name: p.participant.participantName,
                        playerId: p.participant.participantId,
                        result: p.result(),
                        rank: p.rank(),
                        winner: p.winner()
                    }
                    match.players.push(player);
                }

                eventDAO.addMatch(eventId, eventOrganizerId, match).then(function(result){
                    app.trigger('msg.new', {text:i18n.t('common.ok'), type:"success", timeout: 3000});
                    router.navigate('#event/admin/matches/'+eventOrganizerId+'/'+eventId);
                })
            },

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (eoId, id){
                var self = this;
                eventId = id;
                eventOrganizerId = eoId;
                this.chosenGame(null);
                this.games([]);
                this.participants([]);
                this.players([]);
                this.positions([]);
                this.date(null);

                this.dateString = ko.computed(function(){
                    if (null == self.date())
                        return null;
                    else
                        return moment(self.date()).calendar();
                });

                var infoPromise = eventDAO.getEventInfo(eventOrganizerId, eventId).then(function(result){
                    self.eventName(result.info.name);
                });

                var gamesPromise = eventDAO.getLibrary(eventOrganizerId).then(function(data){
                    self.games(data.games);
                });

                var participantsPromise = eventDAO.getAllEventParticipants(eventOrganizerId, eventId).then(function(data){
                    self.participants(data.participants);
                });

                return Q.all([infoPromise, gamesPromise, participantsPromise]);
            }
        };
    }
);
