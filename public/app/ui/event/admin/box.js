define(['knockout', 'dao/eventDAO', 'durandal/app', 'plugins/router', 'q', 'i18next'],
    function (ko, eventDAO, app, router, Q, i18n) {
        var eventOrganizerId = null;
        var boxId = null;

        var moveItem = function(game, from, to, isBoxed, boxCode){
            //find game in array
            var i = from.indexOf(game);

            //remove from array
            from.splice(i, 1);
            //add to new array
            to.push(game);
            //unpack game
            game.isBoxed = isBoxed;

            var originalBoxCode = game.box;
            if (isBoxed){
                game.box = boxCode;
            }
            game.isNew = false;

            eventDAO.saveLibraryItem(eventOrganizerId, game)
                .catch(function() {
                    //find game in array
                    var i = to.indexOf(game);

                    //remove from array
                    to.splice(i, 1);
                    //add to new array
                    from.push(game);
                    //unpack game
                    game.isBoxed = !isBoxed;

                    if (isBoxed){
                        game.box = originalBoxCode;
                    }

                }).done()

        }

        return {
            /********************BINDINGS*********************************/
            name: ko.observable(),
            boxCode: ko.observable(),
            boxLocation: ko.observable(),
            boxGames: ko.observableArray(),
            unpackedGames: ko.observableArray(),
            unpackedGameSelectKey: ko.observable(),
            notInBoxGames: ko.observableArray(),
            comment: ko.observable(),
            newBox: ko.observable(true),
            notInBoxFilter: "isBoxed:"+i18n.t('common.no'),
            gotoBoxManagement: function(context){
                router.navigate("#event/admin/boxes/"+eventOrganizerId);
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (id, itemId){
                var self = this;
                eventOrganizerId = id; // id is event id
                self.cleanup();
                self.unpackedGameSelectKey('library.packGame');

                var boxPromise = null;

                if(typeof itemId === 'undefined'){
                    // this is a new box
                    self.newBox(true);
                } else {
                    self.newBox(false);
                    //itemId is box name (id)
                    boxId = itemId;
                    // get box item info
                    boxPromise = eventDAO.getBoxItem(eventOrganizerId, itemId).then(function(data){
                        self.boxLocation(data.box.location);
                        self.comment(data.box.comment);
                        self.boxCode(data.box.code);
                    });
                }

                // get event info
                var infoPromise = eventDAO.getEventOrganizerInfo(eventOrganizerId).then(function(result){
                    self.name(result.info.name);
                });

                var libraryPromise = eventDAO.getLibrary(eventOrganizerId).then(function(result){
                    for (var i=0; i<result.games.length; i++){
                        var g = result.games[i];

                        if (g.isBoxed && g.box == self.boxCode()){
                            self.boxGames.push(g)
                        } else if (!g.isBoxed && g.box == self.boxCode()){
                            self.unpackedGames.push(g)
                        } else {
                            self.notInBoxGames.push(g)
                        }

                    }
                });
                
                return Q.all([infoPromise, boxPromise, libraryPromise]);

            },
            unpackGame: function(item){
                moveItem(item.data, this.boxGames, this.unpackedGames, false, this.boxCode());
            },
            packGame: function(item){
                moveItem(item.data, this.unpackedGames, this.boxGames, true, this.boxCode());
            },
            packNewGame: function(item){
                moveItem(item.data, this.notInBoxGames, this.boxGames, true, this.boxCode());
            },
            saveBoxItem: function(){
                var self = this;
                var b = {_id: boxId, location: self.boxLocation(), comment: self.comment(), code: self.boxCode()};

                //checking if self.boxCode()  is blank, null or undefined. Sexy, I know.
                if (!b.code  || /^\s*$/.test(b.code))
                    app.trigger('msg.new', {text:"The cherry blossom <br /> Tumbles from the highest tree. <br /> One needs to enter box name", type:"danger", timeout: 7000});
                else {
                    if (self.newBox()) {
                        this.addNewBox(b);
                    } else {
                        // updating existing box
                        eventDAO.saveBoxItem(eventOrganizerId, b).then(function(result){
                            app.trigger('msg.new', {text:i18n.t('common.ok'), type:"success", timeout: 3000});
                        });
                    }
                }
            },
            addNewBox: function(box){
                return eventDAO.addBox(eventOrganizerId, box).then(_.bind(function(result){
                    app.trigger('msg.new', {text:i18n.t('common.ok'), type:"success", timeout: 3000});
                    this.newBox(false);
                    router.navigate("#event/admin/boxes/"+eventOrganizerId+"/"+result.box._id, { replace: true, trigger: false });
                }, this)).done();
            },
            cleanup: function(){
                var self = this;
                self.name("");
                boxId = null;
                self.boxCode("");
                self.boxLocation("");
                self.newBox(true);
                self.comment("");
                self.unpackedGames([]);
                self.boxGames([]);
                self.notInBoxGames([]);
            }

        };
    }
);