define(['knockout', 'plugins/router', 'dao/eventDAO'],
    function (ko, router, eventDAO) {
        var eventId = null;
        var eventOrganizerId = null;
        return {
            /********************BINDINGS*********************************/
            showParticipants: function(){
                router.navigate('#event/admin/participants/'+eventOrganizerId+'/'+eventId);
            },
            viewMatches: function(){
                router.navigate('#event/admin/matches/'+eventOrganizerId+'/'+eventId);
            },
            gotoEventsList: function(){
                router.navigate('#event/admin/events/'+eventOrganizerId);
            },
            showTournaments: function() {
                router.navigate('#event/admin/tournaments/'+eventOrganizerId+'/'+eventId);
            },
            name: ko.observable(),

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (eoId, id){
                var self = this;
                eventId = id;
                eventOrganizerId = eoId;

                return eventDAO.getEventInfo(eventOrganizerId, eventId)
                    .then(function(result){
                        self.name(result.info.name);
                    })
            }

        };
    }
);
