define(['knockout', 'dao/eventDAO', 'lodash', 'i18next', 'plugins/router', 'q', 'durandal/app'],
    function (ko, eventDAO, _, i18n, router, Q, app) {
        var eventOrganizerId = null;
        var itemId = ko.observable();
        var boxName = "";
        var indexedBoxes = null;

        return {
            /********************BINDINGS*********************************/
            name: ko.observable(),
            isNew: ko.observable(),
            location: ko.observable(),
            code: ko.observable(),
            comment: ko.observable(),
            box: ko.observable(),
            boxLocation: null, //defined in activate
            isBoxed: ko.observable(),
            owner: ko.observable(),
            festivalName: ko.observable(),
            boxes: ko.observableArray(),
            events: ko.observableArray(),
            ownedByOrganizer: ko.observable(),
            boxOptionsCaption: i18n.t('library.chooseBox'),
            selectEventCaption: i18n.t('event.selectEvent'),
            eventId: ko.observable(null),
            gotoLibrary: function(context){
                router.navigate("#event/admin/library/"+eventOrganizerId);
            },
            saveAndBack: function(){
                this.save(null, null, true);
            },
            save: function(context, event, doGoBack){
                var self = this;

                var item = {
                    name: this.name(),
                    location: this.location(),
                    box: this.box(),
                    isBoxed: this.isBoxed() || false,
                    owner: this.owner(),
                    itemId: itemId(),
                    comment: this.comment(),
                    code: this.code(),
                    notOwnedByOrganizer: !this.ownedByOrganizer(),
                    eventId: this.eventId()
                }

                eventDAO.saveLibraryItem(eventOrganizerId, item)
                    .then(function() {
                        app.trigger('msg.new', {text:i18n.t("common.saveSuccessful"), type:"success", timeout: 3000});
                        if (self.isNew() || doGoBack){
                            self.isNew(false);
                            router.navigate("#event/admin/library/"+eventOrganizerId);
                        }
                    }).done()
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (id, iId){
                var self = this;
                eventOrganizerId = id;
                if (!iId){
                    this.isNew(true);
                    itemId('');
                    this.location('');
                    this.name('');
                    this.comment('');
                    this.box('');
                    this.isBoxed(false);
                    this.owner('');
                    this.code('');
                    this.ownedByOrganizer(true);
                    this.events([]);
                    this.eventId(null);
                } else {
                    this.isNew(false);
                    itemId(iId);
                    this.events([]);
                }

                indexedBoxes = null;
                self.comment("");
                self.festivalName("");

                self.boxLocation = ko.computed(function(){
                    var box = this.box();
                    if (box){
                        return box.location;
                    }
                }, this);

                var itemPromise = null;
                if (!this.isNew()){
                    itemPromise = eventDAO.getLibraryItem(eventOrganizerId, itemId()).then(function(data){
                        self.name(data.game.name);
                        self.owner(data.game.owner);
                        self.location(data.game.location);
                        self.comment(data.game.comment);
                        self.code(data.game.code);
                        boxName = data.game.box;
                        self.setBox(); //both name and boxes should be fetched from db (I think it is possible that boxes are fetched first)
                        self.isBoxed(data.game.isBoxed);
                        self.ownedByOrganizer(data.game.notOwnedByOrganizer === true ? false : true);
                        self.eventId(data.game.eventId || null);
                    });
                }

                var infoPromise =  eventDAO.getEventOrganizerInfo(eventOrganizerId).then(function(result){
                    self.festivalName(result.info.name);
                });

                var boxesPromise = eventDAO.getLibraryBoxes(eventOrganizerId).then(function(result){
                    self.boxes(result.boxes);
                    indexedBoxes = _.indexBy(result.boxes, 'code')
                    self.setBox();
                });

                var eventsPromise = eventDAO.getAllEventOrganizerEvents(eventOrganizerId).then(function(result){
                    self.events(result.events);
                    self.eventId.valueHasMutated(); // if this callback is after the one which sets the id, event wouldn't be selected if this is not called
                });
                
                return Q.all([itemPromise, infoPromise, boxesPromise]);
            },

            /********************UTILITY*********************/
            setBox: function(){
                if (boxName && indexedBoxes != null){
                    var box = indexedBoxes[boxName];
                    if (box)
                        this.box(box);
                }
            }
        };
    }
);