define(['knockout', 'dao/eventDAO', 'moment', 'plugins/router', 'q', 'underscore'],
    function (ko, eventDAO, moment, router, Q, _) {
        var eventId = null;
        var eventOrganizerId= null;

        return {
            /********************BINDINGS*********************************/
            matches: ko.observableArray(),
            viewMatch: function(){
                console.log('selected');
            },
            name: ko.observable(),
            addNew: function(){
                router.navigate("#event/admin/newMatch/"+eventOrganizerId+"/"+eventId);
            },
            gotoEventAdmin: function(){
                router.navigate("#event/admin/event/"+eventOrganizerId+"/"+eventId);
            },
            gotoMatchStats: function(){
                router.navigate("#event/admin/matchStats/"+eventOrganizerId+"/"+eventId);
            },

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (eoId, id){
                eventId = id;
                eventOrganizerId = eoId;
                this.matches([]);

                var matchesPromise = eventDAO.getEventMatches(eventId).then(_.bind(function(data){
                    data.matches.forEach(function(match){
                        match.displayDate = moment(match.date).format('L');

                        for(var i=0; i<match.players.length; i++){
                            var p = match.players[i];
                            if (p.winner) {
                                match.winner = p.name;
                                break;
                            }
                        }
                    });

                    this.matches(data.matches);
                }, this));

                var infoPromise = eventDAO.getEventInfo(eventOrganizerId, eventId).then(_.bind(function(result){
                    this.name(result.info.name);
                }, this));
                
                return Q.all([matchesPromise, infoPromise]);
            }

        };
    }
);
