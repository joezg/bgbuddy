define(['knockout', 'dao/eventDAO', 'plugins/router', 'underscore', 'q'],
    function (ko, eventDAO, router, _, Q) {
        var eventOrganizerId = null;
        var indexedBoxes = null;
        var games = null;

        return {
            /********************BINDINGS*********************************/
            games: ko.observableArray(),
            selectGame: function(context){
                router.navigate("#event/admin/libraryItem/"+eventOrganizerId+'/'+context.data._id);
            },
            gotoBoxManagement: function(context){
                router.navigate("#event/admin/boxes/"+eventOrganizerId);
            },
            gotoAdministration: function(){
                router.navigate("#event/admin/"+eventOrganizerId);
            },
            addNewItem: function(){
                router.navigate("#event/admin/newLibraryItem/"+eventOrganizerId);
            },
            name: ko.observable(),
            viewFilter: ko.observable(),

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (id, evId){
                eventOrganizerId = id;
                this.games([]);

                var libraryPromise = eventDAO.getLibrary(eventOrganizerId, true).then(_.bind(function(data){
                    games = data.games; //should be changed in setBoxes before binding
                    this.setBoxes();
                }, this));

                var infoPromise = eventDAO.getEventOrganizerInfo(eventOrganizerId).then(_.bind(function(result){
                    this.name(result.info.name);
                }, this));

                var boxesPromise = eventDAO.getLibraryBoxes(eventOrganizerId).then(_.bind(function(result){
                        indexedBoxes = _.indexBy(result.boxes, 'code');
                        this.setBoxes();
                    }, this));

                
                return Q.all([libraryPromise, infoPromise, boxesPromise]);
            },

            setBoxes: function(){
                if (games != null && indexedBoxes != null){
                    for (var i = 0; i<games.length; i++){
                        var g = games[i];

                        var box = indexedBoxes[g.box];

                        if (box)
                            g.boxLocation = box.location;
                    }

                    this.games(games);
                    games = null;
                }
            }

        };
    }
);