define(['knockout', 'dao/eventDAO', 'durandal/app', 'plugins/router', 'q', 'i18next', 'lodash', '../../../util/countries'],
    function (ko, eventDAO, app, router, Q, i18n, _, countries) {
        var eventOrganizerId = null;
        var allEvents = null;
        var allParticipations = null;
        var participantId = null;

        var removeItem = function(event, from){
            //find game in array
            var i = from.indexOf(event);

            //remove from array
            from.splice(i, 1);
        }

        return {
            /********************BINDINGS*********************************/
            name: ko.observable(),
            participantName: ko.observable(),
            participantShortName: ko.observable(),
            newParticipant: ko.observable(),
            username: ko.observable(),
            code: ko.observable(),
            participantEvents: ko.observableArray(),
            otherEvents: ko.observableArray(),
            email: ko.observable(),
            country: ko.observable(),
            countries: countries,
            countriesCaption: i18n.t("common.choose"),
            gotoParticipants: function(context){
                router.navigate("#event/admin/participants/"+eventOrganizerId);
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (id, itemId){
                eventOrganizerId = id; // id is event id
                this.cleanup();

                var participantPromise = null;
                if(typeof itemId === 'undefined'){
                    // this is a new participant
                    this.newParticipant(true);
                } else {
                    this.newParticipant(false);
                    participantId = itemId;
                    // get participant info
                    participantPromise = eventDAO.getParticipant(eventOrganizerId, itemId).then(_.bind(function(data){
                        this.participantName(data.participant.name);
                        this.participantShortName(data.participant.shortName);
                        this.code(data.participant.code);
                        this.email(data.participant.email);
                        this.username(data.participant.userId);
                        this.country(data.participant.country);
                    }, this));
                }

                // get event info
                var infoPromise = eventDAO.getEventOrganizerInfo(eventOrganizerId).then(_.bind(function(result){
                    this.name(result.info.name);
                }, this));

                var eventsPromise = eventDAO.getAllEventOrganizerEvents(eventOrganizerId).then(_.bind(function(result){
                    allEvents = _.indexBy(result.events, "_id");
                    this.filterEvents();
                }, this));

                var participantEventsPromise = null;
                if (participantId) {
                    participantEventsPromise = eventDAO.getAllEventOrganizerParticipantEvents(eventOrganizerId, participantId).then(_.bind(function(result){
                        allParticipations = _.indexBy(result.events, "eventId");
                        this.filterEvents();
                    }, this));
                } else {
                    allParticipations = [];
                }


                return Q.all([infoPromise, participantPromise, eventsPromise, participantEventsPromise]);

            },
            filterEvents: function(){
                if (allEvents && allParticipations){
                    _.forOwn(allEvents, _.bind(function(event){
                        if (allParticipations.hasOwnProperty(event._id)){
                            this.participantEvents.push(allEvents[event._id]);
                        } else {
                            this.otherEvents.push(allEvents[event._id]);
                        }
                    }, this));
                }
            },
            addToEvent: function(item){
                eventDAO.addParticipantToEvent(eventOrganizerId, item.data._id, participantId).then(_.bind(function(){
                    removeItem(item.data, this.otherEvents);
                    this.participantEvents.push(allEvents[item.data._id]);
                }, this));
            },
            removeFromEvent: function(item){
                eventDAO.removeParticipantFromEvent(eventOrganizerId, item.data._id, participantId).then(_.bind(function(){
                    removeItem(item.data, this.participantEvents);
                    this.otherEvents.push(allEvents[item.data._id]);
                }, this));

            },
            saveAndBack: function(){
                var goBack = true;
                this.saveParticipant(null, null, goBack);
            },
            saveParticipant: function(context, event, doGoBack){
                var p = {_id: participantId, code: this.code(), name: this.participantName(), shortName: this.participantShortName(), email: this.email(), userId: this.username(), country: this.country()};

                if (this.newParticipant()) {
                    this.addParticipant(p, doGoBack)
                } else {
                    // updating existing participant
                    eventDAO.saveParticipant(eventOrganizerId, p).then(function(result){
                        app.trigger('msg.new', {text:i18n.t('common.ok'), type:"success", timeout: 3000});
                        if (doGoBack){
                            router.navigate("#event/admin/participants/"+eventOrganizerId);
                        }
                    });
                }
            },
            addParticipant: function(participant, doGoBack){
                return eventDAO.addParticipant(eventOrganizerId, participant).then(_.bind(function(result){
                    app.trigger('msg.new', {text:i18n.t('common.ok'), type:"success", timeout: 3000});

                    participantId = result.participant._id;
                    this.newParticipant(false);

                    if (doGoBack)
                        router.navigate("#event/admin/participants/"+eventOrganizerId);
                    else
                        router.navigate("#event/admin/participant/"+eventOrganizerId+"/"+participantId, { replace: true, trigger: false });
                }, this));
            },
            cleanup: function(){
                allEvents = null;
                allParticipations = null;
                participantId = null;
                this.name("");
                this.participantName("");
                this.code("");
                this.newParticipant(true);
                this.email("");
                this.participantEvents([]);
                this.otherEvents([]);
                this.username("");
                this.country("");
                this.participantShortName("");
            }

        };
    }
);