define(['knockout', 'dao/eventDAO', 'plugins/router', 'underscore', 'q'],
    function (ko, eventDAO, router, _, Q) {
        var eventOrganizerId = null;
        var allGames = null;

        return {
            /********************BINDINGS*********************************/
            games: ko.observableArray(),
            selectGame: function(context){
                var index = this.games.indexOf(context.data);
                if (index > -1) {
                    this.games.splice(index, 1);
                }
            },
            uncheckAll: function(game){
                this.games(allGames);
                allGames = allGames.slice(0);
            },
            name: ko.observable(),

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (id, evId){
                eventOrganizerId = id;
                this.games([]);

                var libraryPromise = eventDAO.getLibrary(eventOrganizerId, true).then(_.bind(function(data){
                    this.games(data.games);
                    allGames = data.games.slice(0);
                }, this));

                var infoPromise = eventDAO.getEventOrganizerInfo(eventOrganizerId).then(_.bind(function(result){
                    this.name(result.info.name);
                }, this));

                return Q.all([libraryPromise, infoPromise]);
            }

        };
    }
);