define(['knockout', 'dao/eventDAO', 'q', 'plugins/router'],
    function (ko, eventDAO, Q, router) {
        var eventOrganizerId = null;
        return {
            /********************BINDINGS*********************************/
            participants: ko.observableArray(),
            select: function(hub){
                router.navigate("#event/admin/participant/"+eventOrganizerId+'/'+hub.data._id);
            },
            name: ko.observable(),
            addParticipant: function(data){
                router.navigate("#event/admin/participant/"+eventOrganizerId);
            },
            gotoAdministration: function(){
                router.navigate("#event/admin/"+eventOrganizerId);
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (id){
                var self = this;
                self.participants([]);

                eventOrganizerId = id;

                var participantsPromise = eventDAO.getAllEventOrganizerParticipants(eventOrganizerId).then(function(data){
                    self.participants(data.participants);
                });
                var infoPromise = eventDAO.getEventOrganizerInfo(eventOrganizerId).then(function(result){
                    self.name(result.info.name);
                });

                return Q.all([infoPromise, participantsPromise]);
            }

        };
    }
);
