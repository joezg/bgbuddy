define(['knockout', 'dao/eventDAO', 'lodash', 'i18next', 'plugins/router', 'q', 'durandal/app', 'moment', 'util/dialogWrapper', 'modals/common/changeValue', 'modals/event/selectGameModal', 'modals/event/pickParticipantsModal'],
    function (ko, eventDAO, _, i18n, router, Q, app, moment, dialog, changeValueDlg, selectGameDlg, pickParticipantsDlg) {
        var eventOrganizerId = null;
        var eventId = null;
        var tournamentId = null;

        return {
            /********************BINDINGS*********************************/
            name: ko.observable(),
            date: ko.observable(),
            dateFromNow: ko.observable(),
            festivalName: ko.observable(),
            rounds: ko.observableArray(),
            participants: ko.observableArray(),
            game: ko.observable(),
            gotoTournaments: function(context){
                router.navigate("#event/admin/tournaments/"+eventOrganizerId+'/'+eventId);
            },
            changeDate: function(){
                var self = this;
                var params = {
                    value: this.date(),
                    label: i18n.t("common.date"),
                    isDate: true
                }
                dialog.show(changeValueDlg, params).then(function(newValue){
                    if (null != newValue) {
                        self.date(newValue);
                        self.dateFromNow(moment(newValue).fromNow());

                        eventDAO.updateTournament(eventId, eventOrganizerId, tournamentId, {date: self.date(), type: 0}).done();
                    }
                }).done();
            },
            changeGame: function(){
                var self = this;
                dialog.show(selectGameDlg, eventOrganizerId).then(function(game){
                    if (null != game) {
                        self.game(game.name);

                        var data = {
                            gameName: game.name,
                            gameId: game._id,
                            type: 0
                        }

                        eventDAO.updateTournament(eventId, eventOrganizerId, tournamentId, data).done();
                    }
                }).done();
            },
            addRound: function(){
                var self = this;
                eventDAO.addTournamentRound(eventId, eventOrganizerId, tournamentId).then(function(result){
                    self.rounds(result.rounds);
                })
            },
            showRound: function(hub){
                console.dir(hub);
                router.navigate("#event/admin/tournamentRound/"+eventOrganizerId+'/'+eventId+'/'+tournamentId+'/'+hub.data._id);
            },
            addParticipants: function(){
                var self = this;
                var params = {
                    eventOrganizerId: eventOrganizerId,
                    eventId: eventId,
                    participants: self.participants()
                }
                dialog.show(pickParticipantsDlg, params).then(function(participants){
                    if (null != participants) {
                        for (var i=0; i<participants.length; i++){
                            var participant = participants[i];
                            eventDAO.addTournamentParticipant(eventId, eventOrganizerId, tournamentId, 0, participant).then(function(result){
                                self.participants(result.participants);
                            });
                        }
                    }
                }).done();
            },
            removeParticipant: function(participant){
                var i =  this.participants.indexOf(participant);
                //remove from array
                this.participants.splice(i, 1);
                eventDAO.removeTournamentParticipant(eventId, eventOrganizerId, tournamentId, 0, participant._id).done();
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (eoId, id, tId){
                var self = this;
                eventId = id;
                eventOrganizerId = eoId;
                tournamentId = tId;
                self.festivalName("");
                self.rounds([]);
                self.participants([]);
                self.game('');
                self.name('');

                var tournamentPromise = eventDAO.getEventTournament(eventId, tournamentId).then(function(data){
                        self.name(data.tournament.name);
                        self.date(data.tournament.date);
                        self.dateFromNow(moment(data.tournament.date).fromNow());
                        self.game(data.tournament.gameName);
                        self.participants(data.tournament.participants);
                        self.rounds(data.tournament.rounds);
                    });

                var infoPromise =  eventDAO.getEventInfo(eventOrganizerId, eventId).then(function(result){
                    self.festivalName(result.info.name);
                });
                
                return Q.all([tournamentPromise, infoPromise]);
            }
        };
    }
);