define(['knockout', 'dao/eventDAO', 'plugins/router', 'q', 'lodash'],
    function (ko, eventDAO, router, Q, _) {
        var eventId = null;
        var eventOrganizerId= null;

        return {
            /********************BINDINGS*********************************/
            name: ko.observable(),
            totalGames: 0,
            totalMatches: 0,
            playersStats: ko.observableArray(),
            gameStats: [],
            gotoMatches: function(){
                router.navigate("#event/admin/matches/"+eventOrganizerId+"/"+eventId);
            },
            sortByGames: function(){
                var stats = this.playersStats();
                stats = _.sortByOrder(stats, ['totalGames'], ['desc']);
                this.playersStats(stats);
                this.playersStats.refresh();
            },
            sortByPlayers: function(){
                var stats = this.playersStats();
                stats = _.sortByOrder(stats, ['totalPlayers'], ['desc']);
                this.playersStats(stats);
                this.playersStats.refresh();
            },
            showGames: function(stat){
                stat.gamesHidden(!stat.gamesHidden());
                if (!stat.gamesHidden()){
                    stat.playersHidden(true)
                }
            },
            showPlayers:function(stat){
                stat.playersHidden(!stat.playersHidden());
                if (!stat.playersHidden()){
                    stat.gamesHidden(true)
                }
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (eoId, id){
                eventId = id;
                eventOrganizerId = eoId;

                var matchesPromise = eventDAO.getEventMatches(eventId).then(_.bind(function(data){
                    var matches = data.matches;
                    var gameStats = {};
                    var numberOfGames = 0;
                    this.totalMatches = data.matches.length;
                    var playersStats = {};

                    matches.forEach(function(m){
                        if (!gameStats[m.gameId]){
                            var newGame = {
                                name: m.game,
                                numberOfPlays: 1
                            }
                            gameStats[m.gameId] = newGame;
                            numberOfGames++;
                        } else {
                            gameStats[m.gameId].numberOfPlays++;
                        }

                        m.players.forEach(function(p){
                            var playerStat = playersStats[p.playerId];
                            if (!playerStat){
                                playerStat = {
                                    name: p.name,
                                    games: {},
                                    players: {},
                                    totalGames:0,
                                    totalPlayers: 0
                                }
                                playersStats[p.playerId]=playerStat;
                            }

                            if (!playerStat.games[m.gameId]){
                                playerStat.games[m.gameId] = {
                                    name: m.game,
                                    count: 1
                                }
                                playerStat.totalGames++;
                            } else {
                                playerStat.games[m.gameId].count++;
                            }

                            for (var i=0; i< m.players.length; i++){
                                if (m.players[i].playerId !== p.playerId){
                                    if (!playerStat.players[m.players[i].playerId]){
                                        playerStat.players[m.players[i].playerId] = {
                                            name: m.players[i].name,
                                            count: 1
                                        };
                                        playerStat.totalPlayers++;
                                    } else {
                                        playerStat.players[m.players[i].playerId].count++;
                                    }
                                }
                            }
                        });
                    });

                    this.totalGames = numberOfGames;
                    this.gameStats = _(gameStats).values().sortByOrder(['numberOfPlays'], ['desc']).run();
                    this.playersStats(_.values(playersStats));

                    for (var i = 0;i<this.playersStats().length; i++){
                        this.playersStats()[i].players = _(this.playersStats()[i].players).values().sortByOrder(['count'], ['desc']).run();
                        this.playersStats()[i].games = _(this.playersStats()[i].games).values().sortByOrder(['count'], ['desc']).run();
                        this.playersStats()[i].gamesHidden = ko.observable(true);
                        this.playersStats()[i].playersHidden = ko.observable(true);
                    }



                }, this));

                var infoPromise = eventDAO.getEventInfo(eventOrganizerId, eventId).then(_.bind(function(result){
                    this.name(result.info.name);
                }, this));
                
                return Q.all([matchesPromise, infoPromise]);
            }

        };
    }
);
