define(['knockout', 'dao/eventDAO', 'plugins/router', 'q'],
    function (ko, eventDAO, router, Q) {
        var eventOrganizerId = null;

        return {
            /********************BINDINGS*********************************/
            name: ko.observable(),
            boxes: ko.observableArray(),
            selectBox: function(context){
                router.navigate("#event/admin/boxes/"+eventOrganizerId+'/'+context.data._id);
            },
            gotoLibrary: function(context){
                router.navigate("#event/admin/library/"+eventOrganizerId);
            },
            addNewBox: function(context) {
                router.navigate("#event/admin/boxnew/"+eventOrganizerId);
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (id){
                var self = this;
                eventOrganizerId = id;

                // get event info
                var infoPromise = eventDAO.getEventOrganizerInfo(eventOrganizerId).then(function(result){
                    self.name(result.info.name);
                });

                // get boxes for this event
                var boxesPromise = eventDAO.getLibraryBoxes(eventOrganizerId).then(function(data){
                    self.boxes(data.boxes);
                });
                
                return Q.all([infoPromise, boxesPromise]);
            }

        };
    }
);