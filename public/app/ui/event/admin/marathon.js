define(['knockout', 'dao/eventDAO', 'lodash', 'i18next', 'plugins/router', 'q', 'durandal/app', 'util/dialogWrapper', 'modals/common/changeValue', 'modals/event/selectGameModal', 'modals/event/pickParticipantsModal'],
    function (ko, eventDAO, _, i18n, router, Q, app, dialog, changeValueDlg, selectGameDlg, pickParticipantsDlg) {
        var eventOrganizerId = null;
        var eventId = null;
        var marathonId = null;

        var sortParticipants = function(a, b){
            return b.totalPoints - a.totalPoints;
        }

        return {
            /********************BINDINGS*********************************/
            name: ko.observable(),
            date: ko.observable(),
            festivalName: ko.observable(),
            participants: ko.observableArray(),
            games: ko.observableArray(),
            gotoTournaments: function(context){
                router.navigate("#event/admin/tournaments/"+eventOrganizerId+'/'+eventId);
            },
            changeDate: function(){
                eventDAO.updateTournament(eventId, eventOrganizerId, marathonId, {date: this.date(), type: 1}).done();
            },
            addGame:function(){
                var data = {
                    eventOrganizerId: eventOrganizerId,
                    additionalFieldVisible: true,
                    additionalFieldTitleKey: 'event.gameWeight',
                    additionalFieldPlaceholderKey: "event.enterGameWeight"
                }
                dialog.show(selectGameDlg, data).then(_.bind(function(result){
                    eventDAO.addMarathonGame(eventId, eventOrganizerId, marathonId, result.game, result.additionalFieldValue()).then(_.bind(function(result){
                        this.games(result.games);
                    }, this));

                }, this));
            },
            removeGame: function(hub){
                eventDAO.removeMarathonGame(eventId, eventOrganizerId, marathonId, hub._id).then(_.bind(function(result){
                    this.games(result.games);
                }, this));
            },
            addParticipants: function(){
                var self = this;
                var params = {
                    eventOrganizerId: eventOrganizerId,
                    eventId: eventId,
                    participants: self.participants()
                }
                dialog.show(pickParticipantsDlg, params).then(function(participants){
                    if (null != participants) {
                        for (var i=0; i<participants.length; i++){
                            var participant = participants[i];
                            eventDAO.addTournamentParticipant(eventId, eventOrganizerId, marathonId, 1, participant).then(function(result){
                                self.participants(result.participants);
                            });
                        }
                    }
                }).done();
            },
            removeParticipant: function(participant){
                var i =  this.participants.indexOf(participant);
                //remove from array
                this.participants.splice(i, 1);
                eventDAO.removeTournamentParticipant(eventId, eventOrganizerId, marathonId, 1, participant._id).done();
            },
            calculateStandings: function(){
                eventDAO.calculateMarathonStandings(eventId, eventOrganizerId, marathonId).then(_.bind(function(result){
                    this.participants(result.participants.sort(sortParticipants));
                }, this));
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (eoId, id, tId){
                var self = this;
                eventId = id;
                eventOrganizerId = eoId;
                marathonId = tId;
                self.festivalName("");
                self.participants([]);
                this.games([]);
                self.name('');

                var marathonPromise = eventDAO.getEventTournament(eventId, marathonId).then(function(data){
                        self.name(data.tournament.name);
                        self.date(new Date(data.tournament.date));
                        self.participants(data.tournament.participants.sort(sortParticipants));
                        self.games(data.tournament.games);
                    });

                var infoPromise =  eventDAO.getEventInfo(eventOrganizerId, eventId).then(function(result){
                    self.festivalName(result.info.name);
                });
                
                return Q.all([marathonPromise, infoPromise]);
            }
        };
    }
);