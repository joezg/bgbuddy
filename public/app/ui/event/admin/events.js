define(['knockout', 'dao/eventDAO', 'plugins/router', 'modals/event/addEventModal', 'util/dialogWrapper', 'q'],
    function (ko, eventDAO, router, addEventDlg, dialog, Q) {
        var eventOrganizerId;
        var events = ko.observableArray();

        return {
            /********************BINDINGS*********************************/
            events: events,
            manageEvent: function(hub){
                console.log('manage');
                console.dir(hub);
                router.navigate('#event/admin/event/'+eventOrganizerId+'/'+hub.data._id);
            },
            gotoAdministration: function(){
                router.navigate("#event/admin/"+eventOrganizerId);
            },
            name: ko.observable(),
            addEventDialog: function(){
                dialog.show(addEventDlg).then(function(event){
                    if (null != event) {
                        events.push(event);
                        eventDAO.addEventOrganizerEvent(eventOrganizerId, event)
                            .then(function(result){
                                event._id = result.event._id;
                            })
                            .catch(function(){
                                events.remove(event);
                            });
                    }
                }).done();
            },

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (id){
                var self = this;
                eventOrganizerId = id;
                var eventsPromise = eventDAO.getAllEventOrganizerEvents(eventOrganizerId).then(function(data){
                    self.events(data.events);
                });

                var infoPromise = eventDAO.getEventOrganizerInfo(eventOrganizerId).then(function(result){
                    self.name(result.info.name);
                });
                
                return Q.all([eventsPromise, infoPromise]);
            }

        };
    }
);