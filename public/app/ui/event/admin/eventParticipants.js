define(['knockout', 'dao/eventDAO', 'q', 'plugins/router', 'lodash'],
    function (ko, eventDAO, Q, router, _) {
        var eventOrganizerId = null;
        var eventId = null;
        var allParticipants = null;
        var participations = null;

        var removeItem = function(event, from){
            //find game in array
            var i = from.indexOf(event);

            //remove from array
            from.splice(i, 1);
        }

        return {
            /********************BINDINGS*********************************/
            participants: ko.observableArray(),
            otherParticipants: ko.observableArray(),
            select: function(){
                console.log('selected');
            },
            name: ko.observable(),
            gotoEventAdmin: function(){
                router.navigate("#event/admin/event/"+eventOrganizerId+"/"+eventId);
            },
            gotoNametags: function(){
                router.navigate("#event/pdf/nametags/"+eventOrganizerId+"/"+eventId);
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (eoId, id){
                this.participants([]);
                this.otherParticipants([]);
                allParticipants = null;
                participations = null;

                eventOrganizerId = eoId;
                eventId = id;

                var infoPromise = eventDAO.getEventInfo(eventOrganizerId, eventId).then(_.bind(function(result){
                    this.name(result.info.name);
                }, this));
                var participantsPromise = eventDAO.getAllEventParticipants(eventOrganizerId, eventId).then(_.bind(function(data){
                    participations = _.indexBy(data.participants, "participantId");
                    this.prepareParticipants();
                }, this));
                var eventOrganizerParticipantsPromise = eventDAO.getAllEventOrganizerParticipants(eventOrganizerId).then(_.bind(function(data){
                    allParticipants = _.indexBy(data.participants, "_id");
                    this.prepareParticipants();
                }, this));

                return Q.all([infoPromise, participantsPromise]);
            },
            prepareParticipants: function(){
                if (allParticipants && participations){
                    _.forOwn(allParticipants, _.bind(function(participant){
                        if (participations.hasOwnProperty(participant._id)){
                            this.participants.push(allParticipants[participant._id]);
                        } else {
                            this.otherParticipants.push(allParticipants[participant._id]);
                        }
                    }, this));
                }
            },
            addParticipant: function(hub){
                eventDAO.addParticipantToEvent(eventOrganizerId, eventId, hub.data._id).then(_.bind(function(){
                    removeItem(hub.data, this.otherParticipants);
                    this.participants.push(allParticipants[hub.data._id]);
                }, this));
            },
            removeParticipant: function(hub){
                eventDAO.removeParticipantFromEvent(eventOrganizerId, eventId, hub.data._id).then(_.bind(function(){
                    removeItem(hub.data, this.participants);
                    this.otherParticipants.push(allParticipants[hub.data._id]);
                }, this));
            }

        };
    }
);
