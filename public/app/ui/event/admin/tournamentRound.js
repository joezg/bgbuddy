define(['knockout', 'dao/eventDAO', 'lodash', 'i18next', 'plugins/router', 'q', 'util/dialogWrapper', 'model/tournamentPropositionEnums', 'modals/scorer/enterScoreModal'],
    function (ko, eventDAO, _, i18n, router, Q, dialog, propositionEnums, enterScoreDlg) {
        var eventOrganizerId = null;
        var eventId = null;
        var tournamentId = null;

        var sortMatches = function(round){
            round.matches = _.sortBy(round.matches, 'tableNumber');

            round.matches.forEach(function(m){
                m.participants = _.sortBy(m.participants, 'seatingPosition');
            })
        }

        var updateRoundPropositions = function(round){
            var propositions = _.clone(round.propositions);
            propositions.tableReposition = propositions.tableReposition.type;
            propositions.seating = propositions.seating.type;
            propositions.cutoffType = propositions.cutoffType.type;
            propositions.primaryRankingType = propositions.primaryRankingType.type;
            propositions.secondaryRankingType = propositions.secondaryRankingType.type;
            propositions.carryoverType = propositions.carryoverType.type;

            eventDAO.updateTournamentRoundPropositions(eventId, eventOrganizerId, tournamentId, round._id, propositions).done();
        }

        return {
            /********************BINDINGS*********************************/
            name: ko.observable(),
            tournament: null,//set in activate
            round: ko.observable(),
            festivalName: ko.observable(),
            rankTable: ko.observableArray(),
            currentRankTablePosition: ko.observable(),
            currentRankTablePoints: ko.observable(),
            matches: ko.observableArray(), //computedObservable - set in activate
            ranking: null, //computedObservable - set in activate
            cutoffTypeEnum: propositionEnums.cutoffType.getAll(),
            seatingEnum: propositionEnums.seating.getAll(),
            tableRepositionEnum: propositionEnums.tableReposition.getAll(),
            primaryRankingTypeEnum: propositionEnums.rankingType.getAll(),
            secondaryRankingTypeEnum: propositionEnums.rankingType.getAll(propositionEnums.rankingType.wins),
            carryoverTypeEnum: propositionEnums.carryoverType.getAll(),
            numberOfMatchesLabelKey: ko.observable(),
            numberOfMatchesPlaceholderKey: ko.observable(),
            rearrangeEnabled: ko.observable(false),
            gotoTournament: function(context){
                router.navigate("#event/admin/tournament/"+eventOrganizerId+'/'+eventId+'/'+tournamentId);
            },
            /*******************CHANGES***********************************/
            onMultipleMatchesChanged: function(newValue){
                this.round().propositions.multipleMatches.isMultiple = newValue;
                updateRoundPropositions(this.round());
                this.round.valueHasMutated();
            },
            onPrimaryRankingHigherIsBestChanged: function(newValue){
                this.round().propositions.primaryRankingHigherIsBest = newValue;
                updateRoundPropositions(this.round());
                this.round.valueHasMutated();
            },
            onSecondaryRankingHigherIsBestChanged: function(newValue){
                this.round().propositions.secondaryRankingHigherIsBest = newValue;
                updateRoundPropositions(this.round());
                this.round.valueHasMutated();
            },
            onPlayedToNumberOfWinsChanged: function(newValue){
                this.round().propositions.multipleMatches.isPlayedToNumberOfWins = newValue;
                updateRoundPropositions(this.round());
                this.setNumberOfMatchesKeys(newValue);
                this.round.valueHasMutated();
            },
            onTableRepositionChanged: function(newValue){
                this.round().propositions.tableReposition = newValue;
                updateRoundPropositions(this.round());
                this.round.valueHasMutated();
            },
            onSeatingChanged: function(newValue){
                this.round().propositions.seating = newValue;
                updateRoundPropositions(this.round());
                this.round.valueHasMutated();
            },
            onCutoffChanged: function(newValue) {
                this.round().propositions.cutoff = newValue;
                updateRoundPropositions(this.round());
                this.round.valueHasMutated();
            },
            onCutoffTypeChanged: function(newValue){
                this.round().propositions.cutoffType = newValue;
                updateRoundPropositions(this.round());
                this.round.valueHasMutated();
            },
            onPrimaryRankingTypeChanged: function(newValue){
                this.round().propositions.primaryRankingType = newValue;
                updateRoundPropositions(this.round());
                this.round.valueHasMutated();
            },
            onSecondaryRankingTypeChanged: function(newValue){
                this.round().propositions.secondaryRankingType = newValue;
                updateRoundPropositions(this.round());
                this.round.valueHasMutated();
            },
            onNumberOfMatchesOrWinsChanged: function(newValue){
                this.round().propositions.multipleMatches.numberOfMatchesOrWins = newValue;
                updateRoundPropositions(this.round());
                this.round.valueHasMutated();
            },
            onNumberOfTables: function(newValue){
                this.round().propositions.numberOfTables = newValue;
                updateRoundPropositions(this.round());
                this.round.valueHasMutated();
            },
            onLargerPlayerCountOnLowerTablesChanged: function(newValue){
                this.round().propositions.largerPlayerCountOnLowerTables = newValue;
                updateRoundPropositions(this.round());
                this.round.valueHasMutated();
            },
            onCarryoverTypeChanged: function(newValue){
                this.round().propositions.carryoverType = newValue;
                updateRoundPropositions(this.round());
                this.round.valueHasMutated();
            },
            onCarryoverChanged: function(newValue){
                this.round().propositions.carryover = newValue;
                updateRoundPropositions(this.round());
                this.round.valueHasMutated();
            },
            setNumberOfMatchesKeys: function(isPlayedToNumberOfWins){
                if (isPlayedToNumberOfWins == true){
                    this.numberOfMatchesLabelKey('event.numberOfWins');
                    this.numberOfMatchesPlaceholderKey('event.enterNumberOfWins')
                } else {
                    this.numberOfMatchesLabelKey('event.numberOfMatches');
                    this.numberOfMatchesPlaceholderKey('event.enterNumberOfMatches')
                }
            },
            generateRound: function(){
                eventDAO.generateTournamentRoundMatches(eventId, eventOrganizerId, tournamentId, this.round()._id).then(_.bind(function(data){
                    this.round().matches = data.matches;
                    sortMatches(this.round());
                    this.round.valueHasMutated();

                    this.setPlayerPicking();

                }, this)).done();
            },
            deleteMatches: function(){
                eventDAO.deleteTournamentRoundMatches(eventId, eventOrganizerId, tournamentId, this.round()._id).then(_.bind(function(data){
                    this.round().matches = [];
                    this.round().ranking = [];
                    this.round.valueHasMutated();
                }, this)).done();
            },
            enterScores: function(hub){
                if (!this.rearrangeEnabled()){
                    var match = hub.data;

                    var params = {};
                    params.players = match.participants;

                    dialog.show(enterScoreDlg, params).then(_.bind(function(newValue){
                        if (newValue) {
                            eventDAO.addTournamentRoundMatchScore(eventId, eventOrganizerId, tournamentId, this.round()._id, match).then(_.bind(function(result){
                                if (result.match){
                                    match.participants = result.match.participants;
                                    this.matches.refresh(match);
                                } else {
                                    this.round().matches = result.matches;
                                }

                                this.round().ranking = result.ranking;
                                this.round.valueHasMutated();

                            }, this)).done();
                        }
                    }, this)).done();
                }
            },
            finishRound: function(){
                eventDAO.finishRound(eventId, eventOrganizerId, tournamentId, this.round()._id).then(_.bind(function(result){
                    this.round().ranking = result.roundRankings;
                    this.round.valueHasMutated();
                }, this)).done();
            },
            addRankRow: function(){
                var newRankRow = {
                    _id: this.currentRankTablePosition(),
                    points: this.currentRankTablePoints()
                }

                this.rankTable.push(newRankRow);

                this.currentRankTablePoints("");
                this.currentRankTablePosition("");
            },
            removeRankRow: function(row){
                var index = this.rankTable.indexOf(row);
                if (index > -1) {
                    this.rankTable.splice(index, 1);
                }
            },
            saveRankTable: function(){
                this.round().propositions.rankPointsDistribution = this.rankTable();
                updateRoundPropositions(this.round());
            },
            playerDropped: function(droppable, draggable){
                var participant = draggable.viewModel;
                var table = droppable.viewModel;
                participant.isSelected(true);
                table.participants.push({
                    _id: participant._id,
                    name: participant.name,
                    seatingPosition: table.participants.length+1,
                    tableId: table._id
                });

                this.matches.refresh(table);
            },
            removeFromMatch: function(data){
                this.tournament.participants.forEach(_.bind(function(p){
                    if (p._id == data._id){
                        p.isSelected(false);

                        for (var i=0; i<this.matches().length; i++){
                            var m = this.matches()[i];
                            if (m._id == data.tableId){
                                var index = m.participants.indexOf(data);
                                if (index > -1){
                                    m.participants.splice(index, 1);
                                }

                                this.matches.refresh(m);
                                break;
                            }
                        }
                    }
                }, this));



            },

            finishRearrange: function(){
                eventDAO.rearrangeTables(eventId, eventOrganizerId, tournamentId, this.round()._id, this.matches()).then(_.bind(function(){
                    this.rearrangeEnabled(false);
                }, this));

            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (eoId, id, tId, rId){
                eventId = id;
                eventOrganizerId = eoId;
                tournamentId = tId;
                this.round(null);
                this.festivalName("");
                this.name('');
                this.rankTable([]);
                this.currentRankTablePoints("");
                this.currentRankTablePosition("");
                this.rearrangeEnabled(false);

                var tournamentPromise = eventDAO.getEventTournament(eventId, tournamentId).then(_.bind(function(data){
                    this.tournament = data.tournament;
                    var round = _.find(data.tournament.rounds, function(r){
                        return r._id == rId;
                    });

                    //updatePropositions
                    round.propositions.tableReposition = propositionEnums.tableReposition.get(round.propositions.tableReposition);
                    round.propositions.seating = propositionEnums.seating.get(round.propositions.seating);
                    round.propositions.cutoffType = propositionEnums.cutoffType.get(round.propositions.cutoffType);
                    round.propositions.primaryRankingType = propositionEnums.rankingType.get(round.propositions.primaryRankingType);
                    round.propositions.secondaryRankingType = propositionEnums.rankingType.get(round.propositions.secondaryRankingType);
                    round.propositions.carryoverType = propositionEnums.carryoverType.get(round.propositions.carryoverType);
                    this.setNumberOfMatchesKeys(round.propositions.multipleMatches.isPlayedToNumberOfWins);
                    this.rankTable(round.propositions.rankPointsDistribution);

                    //sort matches
                    sortMatches(round);

                    this.round(round);
                    this.setPlayerPicking();
                    //following is a hack for updating matches every time round changes
                    ko.computed(_.bind(function(){
                        if (this.round())
                            this.matches(this.round().matches);
                        else
                            this.matches([]);
                    }, this));
                    this.ranking = ko.pureComputed(function() {
                        if (!this.round())
                            return [];

                        return this.round().ranking;

                    }, this);
                    this.name(data.tournament.name);

                }, this));

                var infoPromise =  eventDAO.getEventInfo(eventOrganizerId, eventId).then(_.bind(function(result){
                    this.festivalName(result.info.name);
                }, this));
                
                return Q.all([tournamentPromise, infoPromise]);
            },

            setPlayerPicking: function(){
                var tableRepositionEnum = propositionEnums.tableReposition;
                if (this.round().propositions.tableReposition.type == tableRepositionEnum.get(tableRepositionEnum.pick).type){
                    this.tournament.participants.forEach(function(p){
                        p.isSelected = ko.observable(false);
                    });
                    this.rearrangeEnabled(true);
                }
            }
        };
    }
);