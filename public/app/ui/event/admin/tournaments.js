define(['knockout', 'dao/eventDAO', 'moment', 'plugins/router', 'modals/event/addTournamentModal', 'util/dialogWrapper', 'q'],
    function (ko, eventDAO, moment, router, addTournamentDlg, dialog, Q) {
        var eventId = null;
        var eventOrganizerId= null;

        return {
            /********************BINDINGS*********************************/
            tournaments: ko.observableArray(),
            viewTournament: function(hub){
                if (hub.data.type === 0)
                    router.navigate("#event/admin/tournament/"+eventOrganizerId+"/"+eventId+"/"+hub.data._id);
                else
                    router.navigate("#event/admin/marathon/"+eventOrganizerId+"/"+eventId+"/"+hub.data._id);
            },
            name: ko.observable(),
            addNew: function(){
                var self = this;
                dialog.show(addTournamentDlg).then(function(tournament){
                    if (null != tournament) {
                        eventDAO.addTournament(eventId, eventOrganizerId, tournament).then(function(result){
                            self.tournaments.push(result.tournament);
                            if (result.tournament.type === 0)
                                router.navigate("#event/admin/tournament/"+eventOrganizerId+"/"+eventId+"/"+result.tournament._id);
                            else
                                router.navigate("#event/admin/marathon/"+eventOrganizerId+"/"+eventId+"/"+result.tournament._id);
                        }).done();
                    }
                }).done();
            },
            gotoEventAdmin: function(){
                router.navigate("#event/admin/event/"+eventOrganizerId+"/"+eventId);
            },

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (eoId, id){
                var self = this;
                eventId = id;
                eventOrganizerId = eoId;
                self.tournaments([]);

                var tournamentsPromise = eventDAO.getEventTournaments(eventId).then(function(data){
                    self.tournaments(data.tournaments);
                });

                var infoPromise = eventDAO.getEventInfo(eventOrganizerId, eventId).then(function(result){
                    self.name(result.info.name);
                })
                
                return Q.all([tournamentsPromise, infoPromise]);
            }

        };
    }
);
