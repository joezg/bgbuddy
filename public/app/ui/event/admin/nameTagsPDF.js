define(['knockout', 'lodash', 'pdfkit', 'util/pdfResources', 'dao/eventDAO', 'q', 'jsBarcode'],
    function (ko, _, pdfkit, Resources, eventDAO, Q) {
        return {
            /********************BINDINGS*********************************/

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (eventOrganizerId, eventId){
                this.cleanup();

                var PDFDocument = require('pdfkit');
                //var Buffer = require('Buffer');
                var doc = new PDFDocument({size: 'A4', margin:0});

                var eventParticipantsPromise = eventDAO.getAllEventParticipants(eventOrganizerId, eventId);
                var getResourcesPromise = Resources([
                    '/fonts/pupcat.ttf',
                    '/images/logo.png'
                ]);

                return Q.all([eventParticipantsPromise, getResourcesPromise])
                    .then(_.bind(function(result){

                        var buffers = result[1];
                        var participants = result[0].participants;
                        var x = 20;
                        var y = 20;

                        var nameTagJSON = {
                            setup: {
                                pageStart: {x:20, y:20},
                                columns: {count: 2, offset: 280},
                                rows: {count: 4, offset: 190}
                            },
                            context: {
                                participants: participants,
                                paricipant: {
                                    loop: '$participants'
                                }
                            },
                            tag: [
                                {
                                    rect: {     //draws rectangle
                                        x: 10,
                                        y: 10,
                                        width: 249,
                                        height: 161,
                                        lineWidth: 3,
                                        strokeColor: '#b3b300',
                                        temporaryStyle: true   //whether style will be reset after drawing
                                    }
                                },
                                {
                                    image: {
                                        content: '', //data-uri
                                        x: 18.5,
                                        y: 18.5,
                                        width: 57
                                    }
                                },
                                {
                                    text: {
                                        font: '', //base64
                                        fontSize: 40,
                                        lineWith: 1,
                                        content: function(p){
                                            return p.participantShortName ? p.participantShortName : p.participantName.split(' ').slice(0, -1).join(' ');
                                        },
                                        contentContext: '$participant', //something from context
                                        x: 23,
                                        y: 70,
                                        stroke: true,
                                        fill: true,
                                        characterSpacing: 3
                                    }
                                },
                                {
                                    text: {
                                        font: '', //base64
                                        fontSize: 20,
                                        lineWidth: 1,
                                        content: '$participant.participantCode',
                                        x: 220,
                                        y: 85,
                                        stroke: true,
                                        fill: true,
                                        characterSpacing: 1
                                    }
                                },
                                {
                                    text: {
                                        font: '', //base64
                                        fontSize: 32,
                                        fillColor: '#b3b300',
                                        content: 'Your gaming holiday',
                                        x: 23,
                                        y: 105,
                                        temporaryStyle: true   //whether style will be reset after drawing
                                    }
                                },
                                {
                                    text: {
                                        font: '', //base64
                                        fontSize: 12,
                                        content: 'May 26th – May 29th 2016, zagreb, croatia',
                                        x: 26,
                                        y: 135
                                    }
                                },
                                {
                                    text: {
                                        font: '', //base64
                                        fontSize: 14,
                                        content: 'All a-board',
                                        x: 180,
                                        y: 40,
                                        rotate: {
                                            degree: 90,
                                            origin: {
                                                x:220,
                                                y:60
                                            }
                                        },
                                        temporaryStyle: true   //whether style will be reset after drawing
                                    }
                                },
                                {
                                    image: {
                                        content: function(p){
                                            var code = p.participantCode;
                                            var img = $('<img/>').JsBarcode(code, {width:1,height:25});
                                            return img.attr('src');  //data-uri
                                        },
                                        contentContext: '$participant', //something from context
                                        x: 80,
                                        y: 18.5,
                                        width: 150
                                    }
                                }
                            ]

                        }

                        for (var i=0; i<participants.length; i++){
                            var p = participants[i];
                            if (i % 2 === 1)
                                x += 280;

                            if (i!==0 && i%2 ===0){
                                y += 190;
                                x = 20;
                            }

                            if (i !== 0 && i%8 === 0){
                                doc.addPage();
                                y = 20;
                            }

                            doc.save();
                            doc.rect(x+10,y+10, 249 , 161)
                                .lineWidth(3)
                                .strokeColor('#b3b300')
                                .stroke()
                                .restore();

                            doc.image(buffers[1], x+18.5, y+28.5, {width: 57});

                            var name = p.participantShortName ? p.participantShortName : p.participantName.split(' ').slice(0, -1).join(' ');
                            doc.font(buffers[0])
                                .fontSize(40)
                                .lineWidth(1)
                                .text(name, x+23, y+80, {stroke: true, fill:true, characterSpacing: 3});

                            doc.font(buffers[0])
                                .fontSize(20)
                                .lineWidth(1)
                                .text(p.participantCode, x+220, y+95, {stroke: true, fill:true, characterSpacing: 1})

                            doc.save();
                            doc.font(buffers[0])
                                .fontSize(32)
                                .fillColor('#b3b300')
                                .text('Your gaming holiday', x+23, y+115)
                                .restore()

                            doc.font(buffers[0])
                                .fontSize(12)
                                .text('May 26th – May 29th 2016, zagreb, croatia', x+26, y+145)

                            doc.save()
                            doc.font(buffers[0])
                                .fontSize(14)
                                .rotate(90, {origin:[x+225, y+60]})
                                .text('All a-board', x+180, y+40)
                                .restore();

                            //generating bar code
                            var code = p.participantCode;
                            var img = $('<img/>').JsBarcode(code, {width:1,height:25});
                            var dataURI = img.attr('src');
                            doc.image(dataURI, x+80, y+28.5, {width: 150});
                        }

                        doc.end;

                        this.document = doc;
                    }, this));
            },
            cleanup: function(){

            }

        };
    }
);