define(['knockout', 'dao/eventDAO', 'plugins/router', 'underscore', 'q'],
    function (ko, eventDAO, router, _, Q) {
        var eventOrganizerId = null;
        var eventId = null;
        var games = null;

        return {
            /********************BINDINGS*********************************/
            games: ko.observableArray(),
            isAdmin: ko.observable(),
            selectGame: function(context){
                console.log("non admin click");
            },
            gotoEventOverview: function(){
                router.navigate("#event/participant/event/"+eventId+"/"+eventOrganizerId);
            },
            name: ko.observable(),
            viewFilter: ko.observable(),

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (evId, eoId){
                eventOrganizerId = eoId;
                eventId = evId;
                this.games([]);

                var libraryPromise = eventDAO.getLibrary(eventOrganizerId).then(_.bind(function(data){
                    games = data.games; //should be changed in setBoxes before binding
                    this.games(games);
                }, this));

                var infoPromise = eventDAO.getEventOrganizerInfo(eventOrganizerId).then(_.bind(function(result){
                    this.name(result.info.name);
                }, this))

                return Q.all([libraryPromise, infoPromise]);
            }

        };
    }
);