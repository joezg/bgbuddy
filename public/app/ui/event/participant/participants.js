define(['knockout', 'dao/eventDAO', 'q'],
    function (ko, eventDAO, Q) {
        var eventOrganizerId = null;
        var eventId = null;
        return {
            /********************BINDINGS*********************************/
            participants: ko.observableArray(),
            select: function(){
                console.log('selected');
            },
            name: ko.observable(),

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (id, eoId){
                this.participants([]);

                eventOrganizerId = eoId;
                eventId = id;

                var infoPromise = eventDAO.getEventInfo(eventOrganizerId, eventId).then(_.bind(function(result){
                    this.name(result.info.name);
                }, this));
                var participantsPromise = eventDAO.getAllEventParticipants(eventOrganizerId, eventId).then(_.bind(function(data){
                    this.participants(data.participants);
                }, this));

                return Q.all([infoPromise, participantsPromise]);
            }

        };
    }
);
