define(['knockout', 'plugins/router', 'dao/eventDAO'],
    function (ko, router, eventDAO) {
        var eventId = null;
        var eventOrganizerId = null;
        return {
            /********************BINDINGS*********************************/
            viewLibrary: function(){
                router.navigate('#event/participant/library/'+eventId+'/'+eventOrganizerId);
            },
            showParticipants: function(){
                router.navigate('#event/participant/participants/'+eventId+'/'+eventOrganizerId);
            },
            viewMatches: function(){
                router.navigate('#event/participant/matches/'+eventId+'/'+eventOrganizerId);
            },
            name: ko.observable(),

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (id, eoId){
                var self = this;
                eventId = id;
                eventOrganizerId = eoId;

                return eventDAO.getEventInfo(eventOrganizerId, eventId)
                    .then(function(result){
                        self.name(result.info.name);
                    })
            }

        };
    }
);
