define(['knockout', 'plugins/router',
        'util/routerMap',
        'dao/loginDAO',
        'dao/userDAO',
        'dao/msgDAO',
        'durandal/app',
        'dao/daoUtil',
        'model/user',
        'jquery',
        'util/dialogWrapper',
        'bootstrap'],
    function (ko, router, map, loginDAO, userDAO, msgDAO, app, daoUtil, user, $, dialog) {
    return {
        /********************BINDINGS*********************************/
        router: router,
        username: ko.observable(),
        userVisible: ko.observable(),
        messages: ko.observableArray(),
        loading: ko.observable(false),
        currentRouterGroup: ko.observable(''),
        showCookieDisclaimer: ko.observable(),
        newMessages: ko.observable(0),
        isSpinning:ko.observable(),
        logout: function(){
            var self = this;
            loginDAO.logout()
                .then(function(){
                    self.doLogout();
                }).
                catch(function(err){
                    if (err.status == 403){
                        self.doLogout();
                    }
                    self.setMessage({text:err.responseText, type:'danger'});
                });
        },
        viewMessages:function(){
            router.navigate('#messages');
        },
        cookiesShowDetails: function() {
            this.showCookieDisclaimer(false);
            router.navigate('#cookies');
        },
        cookiesApproved: function() {
            var self = this;
            userDAO.approveCookies().then(function(data) {
                if (data.success) {
                    self.showCookieDisclaimer(false);
                }
            });

        },
        /********************LIFECYCLE CALLBACKS**********************/
        activate: function () {
            var self = this;
            this.loading(false);
            this.showCookieDisclaimer(false);
            this.isSpinning(false);
            router.makeRelative({moduleId: 'ui/'});
            router.map(map.main).buildNavigationModel();

            router.on('router:route:activating', function(activatedVM, route){
                    var coll = $("#main-navbar-collapse-1");
                    coll.collapse({toggle: false});
                    coll.collapse('hide');

                    dialog.closeAll();

                    if (route.config.routerGroup)
                        self.currentRouterGroup(route.config.routerGroup);
                    else
                        self.currentRouterGroup('');

                    if(route.config.public){
                        self.userVisible(false);
                        daoUtil.cancelLogin();
                    } else {
                        //  set user data to user singleton. Ribica is pretty.
                        if (!user.loaded) {
                            self.loadUserData();
                        } else {
                            self.username(user.username);
                            self.userVisible(true);
                        };
                        self.fetchUnreadUserMessages();
                    }
                }
            );

            app.on('login.cancel').then(function(){
                daoUtil.cancelLogin();
            });

            app.on('msg.new').then(function(msg){
                self.setMessage(msg); //if setMessage is passed as argument to then, "this" has different meaning in setMessage
            });

            app.on('login.successful').then(function(username){
                self.loadUserData();
            });

            app.on('theme.change').then(function(theme){
                self.changeTheme(theme);
            });

            app.on('themeMeeple.change').then(function(meeple){
                self.changeMeepleTheme(meeple);
            });

            app.on('messages.refresh').then(function(){ // user when user clicks on 'mark all messages read' or smtg
                self.fetchUnreadUserMessages();
            });

            app.on('spinner.start').then(function(){
                self.isSpinning(true);
            });

            app.on('spinner.stop').then(function(){
                self.isSpinning(false);
            });

            router.activate(); //By not returning a promise, shell is sctivated before first view is activated
                               //if router.activate is returned, shell will not be activated and login screen
                               //could not be shown because child view is not activated before login and shell is
                               //not activated before child view.
        },


        /********************UTILITY*************************************/
        loadUserData: function(){
            var self = this;

            function readUserData() {
                self.username(user.username);
                self.userVisible(true);

                self.changeTheme(user.theme);
                self.changeMeepleTheme(user.color);

                if (!user.cookieDisclaimerDismissed) {
                    self.showCookieDisclaimer(true);
                }
            }

            if (!user.loaded)  {
                user.load();
                user.whenLoaded().then(readUserData);
            } else {
                readUserData();
            }
        },
        fetchUnreadUserMessages: function() {
            var self = this;
            msgDAO.getUnreadMessagesCount().then(function(data){
                self.newMessages((data&&data.unreadMessagesCount)?data.unreadMessagesCount:0);
            });
        },
        setMessage: function(msg){
            msg.type = "alert-" + msg.type;

            if (msg.error){
                msg.details = msg.error.responseJSON ? msg.error.responseJSON.error : msg.error;
                msg.detailsDOMId = "details_" + (this.messages().length + 1);
            } else {
                msg.details = null;
            }

            if (!msg.timeout)
                msg.timeout = null;

            this.messages.unshift(msg);
            window.scrollTo(0,0);
        },
        changeTheme: function(theme){
            var href = "../lib/bootstrap/css/bootstrap-theme.css"
            if (theme!="default" && null != theme)
                href = "css/theme/"+theme+"/bootstrap.min.css"

            $('#css_theme').attr("href", href);
        },
        changeMeepleTheme: function(meeple){ // Ribica added this.
            var href = "css/meeple/blue/meeple.css"; // this is default
            if (meeple!="default" && null != meeple)
                href = "css/meeple/"+meeple+"/meeple.css"

            $('#css_meeple_theme').attr("href", href);
        },
        doLogout: function(){
            window.localStorage.removeItem("rememberMeToken");
            window.sessionStorage.removeItem("sessionId");

            if (window.sessionStorage.getItem("googleSignIn")=="true"){
                window.sessionStorage.removeItem("googleSignIn");
                window.sessionStorage.setItem("doNotLogGoogleUser", true);
            }

            if (window.sessionStorage.getItem("facebookSignIn")=="true"){
                window.sessionStorage.removeItem("facebookSignIn");
                window.sessionStorage.setItem("doNotLogFacebookUser", true);
            }

            this.username('');
            this.userVisible(false);
            window.location = "/";
            user.destroy(); // destroy user data
        }
    };
});