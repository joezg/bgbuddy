define(['knockout', 'dao/gameDAO', 'plugins/router', 'model/matches','model/user', 'durandal/app','i18next', 'moment'],
    function (ko, gameDAO, router, matchesModel, user, app, i18n, moment) {

        return {
            /********************BINDINGS*********************************/
            games: ko.observableArray(),
            selectGame: function(context){
                router.navigate('#scorer/game/'+context.data._id);
            },
            addNewGame: function() {
                router.navigate('#scorer/newGame');
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (){
                var self = this;
                self.games([]);

                this.filters = [
                    { titleKey: "scorer.showOnlyGamesIown", filter: "owned:"+i18n.t("common.yes_lower")+";" },
                    { titleKey: "scorer.showGamesIvePlayed", filter: "played:"+i18n.t("common.yes_lower")+";" },
                    { titleKey: "scorer.showGamesIhaventPlayed", filter: "played:"+i18n.t("common.no_lower")+";" }
                ]

                // get matches
                return self.fetchGames();
            },
            fetchGames: function() {
                var self = this;

                return gameDAO.getGamesForUser(false).then(function(data){
                    data.games.forEach(function(game){
                        if (game.dateLastPlayed)
                            game.dateLastPlayedFormatted = moment(game.dateLastPlayed).format('LL');
                    });
                    self.games(data.games);
                });

            }

        };
    }
);