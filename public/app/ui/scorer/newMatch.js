define(['knockout', 'dao/matchDAO', 'dao/gameDAO', 'plugins/router', 'util/dialogWrapper', 'model/matchPhaseEnum', 'modals/scorer/addCustomPhaseModal', 'underscore', 'q'],
    function (ko, matchDAO, gameDAO, router, dialog, matchPhaseEnum, phaseDlg, _, Q) {
        var matchPhaseType = ko.observable(); //in closure because of scope of matchPhaseTypeChosen function

        return {
            /********************BINDINGS*************************/
            savePending: ko.observable(false),
            game: ko.observable(),
            gameId: ko.observable(),
            location: ko.observable(),
            matchPhaseType: matchPhaseType,
            matchPhaseTypes: ko.observableArray(),
            gameSuggestion: ko.observable(),
            locationSuggestion: ko.observable(),
            gameSuggestions: ko.observableArray([]),
            startSave: function(){
                var self = this;
                var newMatchPhaseType = this.matchPhaseType();
                if (newMatchPhaseType.type == matchPhaseEnum.custom){
                    dialog.show(phaseDlg).then(function(name){
                        if (null != name)
                            self.save(newMatchPhaseType.type, name);
                    });
                } else {
                    self.save(newMatchPhaseType.type, newMatchPhaseType.name);
                }
            },
            matchPhaseTypeChosen: function(type){
                matchPhaseType(type);
            },
            selectSuggestedGame: function() {
                this.game(this.gameSuggestion());
            },
            selectSuggestedLocation: function() {
                this.location(this.locationSuggestion());
            },
            /********************LIFECYCLE CALLBACKS***************/
            activate: function() {
                var self = this;
                this.savePending(false);
                this.game(null);
                this.gameId(null);
                this.location(null);
                this.gameSuggestion(null);
                this.locationSuggestion(null);

                this.matchPhaseTypes(matchPhaseEnum.getAll([matchPhaseEnum.scoring, matchPhaseEnum.pause])); // exclude scoring & pause

                this.matchPhaseType(matchPhaseEnum.get(matchPhaseEnum.playing));

                // suggest last game played
                var lastMatchesPromise = matchDAO.getMatchesForUser(1).then(function(data){

                    if (data.matches && data.matches.length>=1) {
                        var lastMatch = data.matches[0];
                        self.gameSuggestion(lastMatch.game);
                        self.locationSuggestion(lastMatch.location);
                    }

                });

                var gamesPromise = gameDAO.getGamesForUser().then(function(data){
                    self.gameSuggestions(data.games);
                });

                return Q.all([lastMatchesPromise, gamesPromise]);

            },
            /********************UTILITY****************************/
            save: function(phaseType, phaseName){
                var self = this;
                this.savePending(true);

                if (!this.gameId()){
                    var foundGame = _.find(this.gameSuggestions(), function (g){
                        return self.game().toLowerCase() == g.name.toLowerCase();
                    })

                    if (foundGame){
                        this.gameId(foundGame._id);
                        this.game(foundGame.name);
                    }

                }

                matchDAO.startNewMatch(self.game(), self.gameId(), self.location(), phaseType, phaseName, true)
                    .then(function(res){
                        router.navigate("#scorer/viewMatch/" + res.match._id, { replace: true, trigger: true });
                    }).done();
            }
        };
    }
);
