define(['knockout', 'plugins/router','durandal/app', 'i18next', 'dao/eventDAO', 'dao/gameDAO', 'dao/buddyDAO', 'dao/sharingDAO', 'dao/matchDAO', 'q', 'lodash', 'util/stepper'],
    function (ko, router, app, i18n, eventDAO, gameDAO, buddyDAO, sharingDAO, matchDAO, Q, _, Stepper) {

        var unwrapMappings = function(mappings){
              return _(mappings).map(function(m){
                  return {
                      sourceId: m.sourceId,
                      sourceName: m.sourceName,
                      buddyId: ko.unwrap(m.buddyId),
                      isAnonymous: ko.unwrap(m.isAnonymous),
                      isMe: ko.unwrap(m.isMe),
                      doAddNew: ko.unwrap(m.doAddNew),
                      newBuddyName: ko.unwrap(m.doAddNew) ? ko.unwrap(m.newBuddyName) : null,
                      importAllSourceMatches: ko.unwrap(m.importAllSourceMatches)
                  }
              }).run();
        };

        var unwrapGameMappings = function(mappings){
            return _(mappings).map(function(m){
                return {
                    sourceId: m.sourceId,
                    sourceName: m.sourceName,
                    gameId: ko.unwrap(m.gameId),
                    doAddNew: ko.unwrap(m.doAddNew),
                    newGameName: ko.unwrap(m.doAddNew) ? ko.unwrap(m.newGameName) : null
                }
            }).run();
        };

        var preparePlayerMappingStep = function() {
            var promises = [this.manager.getSourcePlayers(), buddyDAO.getAllBuddies(), this.manager.getPlayerMappings()];

            return Q.all(promises).then(_.bind(function(results){
                var source = results[0];
                var buddies = results[1].buddies;
                var mappings = results[2].mapping;

                this.buddies(buddies);

                source.forEach(_.bind(function(s){
                    var sourceId = this.manager.getSourceId(s);
                    var oldMapping = _.findWhere(mappings, {sourceId: sourceId});
                    var currentMapItem = {
                        sourceId: sourceId,
                        sourceName: this.manager.getSourceName(s),
                        doAddNew: ko.observable(oldMapping ? oldMapping.doAddNew : false),
                        newBuddyName: ko.observable(oldMapping && oldMapping.newBuddyName ? oldMapping.newBuddyName : this.manager.getSourceName(s)),
                        isAnonymous: ko.observable(oldMapping ? oldMapping.isAnonymous : false),
                        isMe: ko.observable(oldMapping ? oldMapping.isMe : false),
                        buddyId: ko.observable(oldMapping ? oldMapping.buddyId : null),
                        importAllSourceMatches: ko.observable(oldMapping ? oldMapping.importAllSourceMatches : false)
                    }

                    currentMapItem.isMe.subscribe(function(value){
                        if (value === true)
                            currentMapItem.importAllSourceMatches(true);
                    })

                    this.mappings.push(currentMapItem);
                }, this))
            }, this))
        }

        var playerMappingStepFinished = function(){
            return this.manager.saveMappings(this.mappings())
                .then(_.bind(function(){
                    var playersForGettingMatches = _(this.mappings()).reduce(function(result, mapping){
                        if (mapping.importAllSourceMatches())
                            result.push(mapping.sourceId);

                        return result;
                    }, []);

                    return this.manager.getMatchesForPlayers(playersForGettingMatches);
                }, this))
                .then(_.bind(function(result){
                    this.matches(result.matches);
                }, this))
        }

        var prepareGameMappingsStep = function(){
            var promises = [];
            promises.push(gameDAO.getGamesForUser());
            promises.push(this.manager.getGameMappings());

            return Q.all(promises).then(_.bind(function(result){
                var gameMappings = {};
                console.log(result);
                this.games(result[0].games);
                var oldMappings = result[1].mapping;

                this.matches().forEach(_.bind(function(m){
                    var oldMapping = _.findWhere(oldMappings, {sourceId: m.gameId});

                    if (!gameMappings.hasOwnProperty(m.gameId)){
                        var currentMapItem = {
                            sourceId: m.gameId,
                            sourceName: m.game,
                            doAddNew: ko.observable(oldMapping ? oldMapping.doAddNew : false),
                            newGameName: ko.observable(oldMapping && oldMapping.newGameName ? oldMapping.newGameName : m.game),
                            gameId: ko.observable(oldMapping ? oldMapping.gameId : null)
                        }
                        gameMappings[m.gameId] = currentMapItem;
                    }

                }, this));

                this.gameMappings(_.values(gameMappings));
            }, this));
        }

        var gameMappingStepFinished = function(){
            return this.manager.saveGameMappings(this.gameMappings())
        }

        var prepareMatchOverviewStep = function(){
            var reduceFunctionGenerator = function(propertyName){
                return function(total, i){
                    if (!total) total = {};
                    total[i[propertyName]] = i;
                    return total;
                }
            }

            var sorted = _(this.matches()).sortBy('date').each(function(m) { m.included = ko.observable(true); }).run();
            var playerMapping = _(unwrapMappings(this.mappings())).indexBy('sourceId').run();
            var gameMapping = _(unwrapGameMappings(this.gameMappings())).indexBy('sourceId').run();
            var buddyMap = _(this.buddies()).indexBy('_id').run();
            var gameMap = _(this.games()).indexBy('_id').run();

            var preparedMatches = _(sorted).map(_.bind(function(match){
                var newMatch = { included: true }; //TODO check if already imported

                newMatch.gameMapping = gameMapping[match.gameId];
                newMatch.sharing = this.manager.getSharingDetails();
                newMatch.sharing.matchId = match._id;

                newMatch.game = newMatch.gameMapping.doAddNew ? newMatch.gameMapping.newGameName : gameMap[newMatch.gameMapping.gameId].name;
                newMatch.gameId = newMatch.gameMapping.gameId ? newMatch.gameMapping.gameId : null;
                newMatch.date = match.date;
                newMatch.location = this.manager.getLocation();
                newMatch.isScored = false;

                newMatch.players = [];

                match.players.forEach(_.bind(function(p){
                    if (p.hasOwnProperty('winner') && !newMatch.isScored){
                        newMatch.isScored = true;
                    }

                    var newPlayer = {};

                    newPlayer.playerMapping = playerMapping[this.manager.getMatchPlayerId(p)];

                    newPlayer.isUser = false;
                    newPlayer.isAnonymous = false;
                    newPlayer.buddyId = null;
                    newPlayer.name = null;

                    if (newPlayer.playerMapping.isMe){
                        newPlayer.isUser = true;
                    } else if (newPlayer.playerMapping.isAnonymous){
                        newPlayer.isAnonymous = true;
                    } else if (newPlayer.playerMapping.doAddNew){
                        newPlayer.name = newPlayer.playerMapping.newBuddyName;
                        newPlayer.isNewBuddy = true;
                    } else {
                        newPlayer.buddyId = newPlayer.playerMapping.buddyId;
                        newPlayer.name = buddyMap[newPlayer.playerMapping.buddyId].name;
                    }

                    newPlayer.result = p.result;
                    newPlayer.rank = p.rank;
                    newPlayer.winner = !!p.winner;
                    newPlayer.firstPlayer = !!p.firstPlayer;

                    newMatch.players.push(newPlayer);
                }, this));

                return newMatch;
            }, this)).run();

            this.matches(preparedMatches);
        }

        var managers = {
            event: function(eventOrganizerId, eventId){
                var manager = {
                    eventOrganizerId: eventOrganizerId,
                    eventId: eventId,
                    getSourcePlayers: function(){
                        return eventDAO.getAllEventParticipants(this.eventOrganizerId, this.eventId).then(function(result){
                            return result.participants;
                        });
                    },
                    getPlayerMappings: function() {
                        return sharingDAO.getEventPlayersMapping(this.eventId);
                    },
                    saveMappings: function(mappings){
                        return sharingDAO.saveEventPlayerMappings(unwrapMappings(mappings), this.eventId);
                    },
                    getGameMappings: function() {
                        return sharingDAO.getEventGameMapping(this.eventId);
                    },
                    saveGameMappings: function(mappings){
                        return sharingDAO.saveEventGameMappings(unwrapGameMappings(mappings), this.eventId);
                    },
                    getSourceId: function(source) {
                        return source.participantId;
                    },
                    getSourceName: function(source) {
                        return source.participantName;
                    },
                    getMatchPlayerId: function(p){
                        return p.playerId;
                    },
                    getMatchesForPlayers: function(participantIds){
                        return eventDAO.getAllEventMatchesForParticipants(this.eventId, participantIds);
                    },
                    getLocation: function() {
                        return this.eventName;
                    },
                    getSharingDetails: function() {
                        return {
                            eventOrganizerId: this.eventOrganizerId,
                            eventId: this.eventId
                        }
                    }
                }

                eventDAO.getEventInfo(manager.eventOrganizerId, manager.eventId).then(_.bind(function(data){
                    this.eventName = data.info.name;
                    this.eventLocation = data.info.location;
                }, manager));

                return manager;
            },
            user: function(userId){
                return {
                    userId: userId,
                    getSourcePlayers: function(){
                        //return buddyDAO.getAnotherUserBuddies(userId);
                        console.log('not implemented');
                    },
                    getPlayerMappings: function() {
                        return sharingDAO.getUserPlayerMapping(this.userId);
                    },
                    saveMappings: function(mappings){
                        //return sharingDAO.saveUserPlayerMappings(unwrapMappings(mappings), this.userId);
                        console.log('not implemented');
                    },
                    getSourceId: function(source) {
                        return source.buddyId;
                    },
                    getSourceName: function(source) {
                        return source.name;
                    },
                    getMatchesForPlayers: function(userIds){
                        console.log('not implemented');
                    },
                    getLocation: function() {
                        console.log('not implemented');
                    }
                }
            }
        }

        return {
            /********************BINDINGS*********************************/
            mappings: ko.observableArray(),
            gameMappings: ko.observableArray(),
            buddies: ko.observableArray(),
            games: ko.observableArray(),
            matches: ko.observableArray(),
            actionButtonKey: ko.observable(),
            stepper: null,
            clickNext: function(){
                this.stepper.next();
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (type, primaryId, secondaryId){
                this.matches([]);
                this.gameMappings([]);
                this.mappings([]);
                this.buddies([]);
                this.games([]);
                this.manager = managers[type](primaryId, secondaryId);

                this.stepper = new Stepper(this);
                this.stepper.addStep('playerMapping', {buttonKey: 'scoreSharing.nextGames'}, preparePlayerMappingStep, playerMappingStepFinished);
                this.stepper.addStep('gameMapping', {buttonKey: 'scoreSharing.nextMatches'}, prepareGameMappingsStep, gameMappingStepFinished);
                this.stepper.addStep('matchOverview', {buttonKey: 'scoreSharing.importMatches'}, prepareMatchOverviewStep, null);
            }
        };
    }
);