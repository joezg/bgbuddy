define(['knockout', 'dao/buddyDAO', 'plugins/router','durandal/app', 'i18next'],
    function (ko, buddyDAO, router, app, i18n) {

        return {
            /********************BINDINGS*********************************/
            name: ko.observable(),
            comment: ko. observable(),
            save: function() {
                var self = this;
                buddyDAO.addBuddy({name: this.name(), comment:this.comment()}).then(function(data) {
                    if (data.success) {
                        // show message and reroute back to buddy details
                        app.trigger('msg.new', {text:i18n.t("common.dataUpdatedSuccessfully"), type:"success", timeout: 3000});
                        router.navigate('#scorer/buddy/'+data.buddy._id);
                    } else {
                        app.trigger('msg.new', {text:i18n.t("err.couldntUpdate"), type:"danger", timeout: 3000});
                    }
                }).done();
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (){
                this.name("");
                this.comment("");
            }
        };
    }
);