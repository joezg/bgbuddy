define(['knockout', 'dao/buddyDAO'],
    function (ko, buddyDAO) {

        return {
            /********************BINDINGS*********************************/
            name: ko.observable(),
            buddies: ko.observableArray(),
            selectBuddy: function(context){
                //TODO send a friend request that receiver should confirm?
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (){
                var self = this;
                self.buddies([]);

                // TODO what now? Fetch all? Fetch none? Only fetch upon search query string? Ask user to enter valid email address or username?
                return self.fetchBuddies();

            },
            fetchBuddies: function() {
                var self = this;

                return buddyDAO.getAllBuddies(true).then(function(data){
                    if (data.buddies) {
                        self.buddies(data.buddies);
                    }
                });


            }

        };
    }
);