define(['knockout', 'dao/matchDAO', 'plugins/router', 'model/matches', 'i18next'],
    function (ko, matchDAO, router, matchesModel, i18n) {

        return {
            /********************BINDINGS*********************************/
            name: ko.observable(),
            matches: ko.observableArray(),
            selectMatch: function(context){
                // show different match views depending on whether match is in progress
                if (context.data.inProgress)
                    router.navigate('#scorer/viewMatch/'+context.data.id);
                else
                    router.navigate('#scorer/viewFinishedMatch/'+context.data.id);
            },
            gotoOverview: function(context){
                router.navigate("#scorer");
            },
            startNewMatch: function(){
                router.navigate("#scorer/newMatch");
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (){
                var self = this;
                self.matches([]);

                this.filters = [
                    { titleKey: "scorer.showOnlyScored", filter: "isScored:"+i18n.t("common.yes_lower")+";" },
                    { titleKey: "scorer.showOnlyInProgress", filter: "inProgress:"+i18n.t("common.yes_lower")+";" },
                    { titleKey: "scorer.showOnlyMatchesIWon", filter: "iWon:"+i18n.t("common.yes_lower")+";" }
                ]

                // get matches
                return self.fetchMatches();
            },
            fetchMatches: function() {

                var self = this;
                return matchDAO.getMatchesForUser().then(function(data){
                    var ms = matchesModel.toModel(data.matches);
                    self.matches(ms);
                });

            }

        };
    }
);