define(['knockout', 'dao/buddyDAO','dao/matchDAO', 'dao/msgDAO' , 'plugins/router', 'model/matches','durandal/app', 'i18next','moment', 'q', 'underscore', 'model/statsByItem', 'modals/scorer/itemOptionsModal', 'util/dialogWrapper'],
    function (ko, buddyDAO, matchDAO, msgDAO, router, matchesModel, app, i18n, moment, Q, _, StatsByItem, itemOptionsDlg, dialog) {
        return {
            /********************BINDINGS*********************************/
            _id: ko.observable(),
            name: ko.observable(),
            buddyUsername: ko.observable(),
            comment: ko.observable(),
            matches: ko.observableArray(),
            showStatsFlag: ko.observable(false),
            likeFlag: ko.observable(false),
            showBuddyDetailsFlag: ko.observable(false),
            mostlyPlay: ko.observableArray(),
            mostlyWon: ko.observableArray(),
            mostlyLost: ko.observableArray(),
            mostlyPlayWith: ko.observableArray(),
            messages: ko.observableArray(),
            msgsError: ko.observable(),
            gameStats: ko.observableArray(),
            gameStatsTotal: ko.observable(),
            playerStats: ko.observableArray(),
            resultsByGameOnlyWithUser: ko.observable(),
            resultsByBuddyOnlyWithUser: ko.observable(),
            matchesOnlyWithUser: ko.observable(),
            matchesFilter: ko.observable(''),
            toggleResultsByGameOnlyWithUser: function(){
                this.resultsByGameOnlyWithUser(!this.resultsByGameOnlyWithUser());
            },
            toggleResultsByBuddyOnlyWithUser: function(){
                this.resultsByBuddyOnlyWithUser(!this.resultsByBuddyOnlyWithUser());
            },
            toggleMatchesOnlyWithUser: function(){
                this.matchesOnlyWithUser(!this.matchesOnlyWithUser());

                if (this.matchesOnlyWithUser())
                    this.matchesFilter('iPlayed:1');
                else
                    this.matchesFilter('');

            },
            openPlayerOptions: function(){
                var self = this;
                dialog.show(itemOptionsDlg).then(function(result){
                    if (result.action == 'rename'){
                        var originalName = self.name();
                        self.name(result.newName);
                        buddyDAO.renameBuddy(self._id(), result.newName).catch(function(){
                            self.name(originalName);
                        });
                    } else if (result.action == 'delete'){
                        buddyDAO.deleteBuddy(self._id()).then(function(result){
                            if (result.cannotDelete){
                                var message = i18n.t("scorer.buddyParticipatedInMatch", {name: self.name()});
                                var title = i18n.t("scorer.buddyParticipatedInMatchTitle");
                                var options = [
                                    {text: i18n.t('common.ok'), value: 1},
                                    {text: i18n.t('common.cancel'), value: 0}
                                ];
                                dialog.showMessage(message, title, options).then(function(answer){
                                    if (1 === answer) {
                                        buddyDAO.deleteBuddy(self._id(), true).then(function(){
                                            router.navigate('#scorer/buddies');
                                        }).done();
                                    }
                                });
                            } else {
                                router.navigate('#scorer/buddies');
                            }
                        }).done();
                    }
                });
            },
            selectMatch: function(context){
                // show different match views depending on whether match is in progress
                if (context.data.inProgress)
                    router.navigate('#scorer/viewMatch/'+context.data.id);
                else
                    router.navigate('#scorer/viewFinishedMatch/'+context.data.id);
            },
            selectBuddy: function(context){
                router.navigate('#scorer/buddy/'+context._id);
            },
            sendMessage: function() {
                if (this.buddyUsername())
                    router.navigate('#messageSend/' + this.buddyUsername());
                else
                    router.navigate('#messageSend/');
            },
            saveComment: function(comment){
                this.comment(comment);
                buddyDAO.updateBuddyData({_id:this._id(), comment:this.comment()}).done();
            },
            saveUsername: function(username){
                this.buddyUsername(username);
                buddyDAO.updateBuddyData({_id:this._id(), username:this.buddyUsername()}).done();
            },
            selectMessage: function(context) {
                router.navigate('#message/'+context._id);
            },
            toMoment: function(date) {
                return moment(date);
            },
            selectGame: function(context){
                router.navigate('#scorer/game/'+context._id);
            },
            toggleBuddyDetails: function() {
               this.showBuddyDetailsFlag(!this.showBuddyDetailsFlag());
            },
            toggleStats: function() {
                this.showStatsFlag(!this.showStatsFlag());
            },
            toggleLike: function() {
                var self = this;
                buddyDAO.updateBuddyLike(self._id(), !self.likeFlag()).then(function(data){
                    if (data.success) {
                        self.likeFlag(!self.likeFlag());
                    } else {
                        app.trigger('msg.new', {text:i18n.t("err.couldntSave"), type:"danger", timeout: 3000});
                    }

                }).done();

            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (id){
                var self = this;
                self._id(id);

                self.cleanup();

                // fetch buddy details
                var detailsPromise = self.fetchBuddyDetails();

                // get history: games played, messages etc., when u became buddies, most games played...
                var gamesPromise = self.fetchGamesPlayed();

                return Q.all([detailsPromise, gamesPromise]).then(function(){
                    self.calcStats();

                    // fetch messages with selected buddy
                    return self.fetchMessages();
                });
            },

            fetchBuddyDetails: function() {
                var self = this;
                return buddyDAO.getBuddy(self._id())
                    .then(function(data){
                        if(data.buddy) {
                            self.name(data.buddy.name);
                            self.buddyUsername(data.buddy.username);
                            self.likeFlag(data.buddy.like);
                            self.comment(data.buddy.comment);
                        }
                    });
            },

            fetchMessages: function() {
                var self = this;
                var msgsSortFunction = function(a, b) {
                    return (new Date(a.dateSent) <  new Date(b.dateSent))?1:-1; // sort by date descending
                };

                if (self.buddyUsername()) {
                    return msgDAO.getMessagesWithUser(self.buddyUsername()).then(function(data) {

                        data.inbox.forEach(function(i) {
                            i.folder = 'inbox';
                        });
                        data.sent.forEach(function(s) {
                            s.folder = 'sent';
                        });
                        var allMessages = data.inbox.concat(data.sent);
                        allMessages.sort(msgsSortFunction);

                        self.messages(allMessages);
                    });
                } else {
                    self.msgsError(i18n.t("msg.needToKnowUsernameError"));
                }
            },

            fetchGamesPlayed: function() {
                var self = this;

                return matchDAO.getMatchesWithBuddy(self._id()).then(function(data){
                    var ms = matchesModel.toModel(data.matches);
                    //console.log("Fetched " + ms?ms.length:"0" + " matches.");
                    if (ms) {
                        self.matches(ms);
                    }
                    else {
                        self.matches([]);
                    }
                });

            },

            calcStats: function() {
                var self = this;
                //stats by game
                var gameStats = new StatsByItem();
                //stats by player
                var playerStats = new StatsByItem();

                var nameOfMe = i18n.t('scorer.me');

                this.matches().forEach(function(m) {
                    //when calculating game stats for a buddy, specific wins/losses are always against user
                    //        addStatistic(match, itemName, itemId,   itemIsUser, calculateWins, mainId,     mainIsUser, specificId, specificIsUser)
                    gameStats.addStatistic(m,     m.game,   m.gameId, false,      true,          self._id(), false,      -1,         true);

                    if (m.players && m.players.length>0) {
                        m.players.forEach(function(p) {
                            if (!p.isAnonymous && p.buddyId !== self._id()) {
                                var name = p.name ? p.name : nameOfMe;
                                var id = p.isUser ? -1 : p.buddyId;
                                var itemIsUser = p.isUser ? true : false;

                                //when calculating stats against other buddies for a buddy, specific wins/losses are against that buddy
                                //          addStatistic(match, itemName, itemId, itemIsUser, calculateWins, mainId,     mainIsUser, specificId, specificIsUser)
                                playerStats.addStatistic(m,     name,     id,     itemIsUser, true,          self._id(), false,      id,         itemIsUser);
                            }
                        });
                    }

                });

                //convert associative array(object) to array
                self.gameStats(_.values(gameStats.items));
                self.playerStats(_.values(playerStats.items));

                self.gameStatsTotal(gameStats.total);

                // see which buddies user played with most
                var mostlyPlayedWith = playerStats.getMostPlayedItems(true, true);
                self.mostlyPlayWith(mostlyPlayedWith.items);

                //see top games
                var mostlyPlayed = gameStats.getMostPlayedItems(true, false);
                self.mostlyPlay(mostlyPlayed.items);

                var mostWins = gameStats.getMostWinsByUser();
                self.mostlyWon(mostWins.items);
                var mostLoses = gameStats.getMostLossesByUser();
                self.mostlyLost(mostLoses.items)

            },

            cleanup: function (){
                var self = this;
                // clear list
                self.matches([]);
                self.messages([]);
                self.mostlyWon([]);
                self.mostlyLost([]);
                self.mostlyPlayWith([]);
                self.showStatsFlag(false);
                self.likeFlag(false);
                self.showBuddyDetailsFlag(false);
                self.mostlyPlay([]);
                self.msgsError("");
                self.gameStats([]);
                self.gameStatsTotal((new StatsByItem()).total);
                self.playerStats([]);
                self.resultsByBuddyOnlyWithUser(false);
                self.resultsByGameOnlyWithUser(false);
                self.matchesOnlyWithUser(false);
                self.matchesFilter('');
                self.name('');
                self.buddyUsername('');
                self.comment('');
            }

        };
    }
);