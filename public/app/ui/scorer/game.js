define(['knockout', 'dao/gameDAO', 'dao/matchDAO', 'plugins/router', 'model/matches','model/user', 'durandal/app', 'i18next', 'model/statsByItem', 'underscore', 'util/dialogWrapper', 'modals/common/changeValue', 'modals/scorer/itemOptionsModal'],
    function (ko, gameDAO, matchDAO, router, matchesModel, user, app, i18n, StatsByItem, _, dialog, changeValueDlg, itemOptionsDlg) {

        var renameMatchesGame = function(matches, newName){
            matches().forEach(function(match){
                match.game = newName;
            });
            matches.refresh();
        };

        return {
            /********************BINDINGS*********************************/
            _id: ko.observable(),
            name: ko.observable(),
            comment: ko.observable(),
            bggId: ko.observable(),
            played: ko.observable(false),
            dateLastPlayed: ko.observable(null),
            dateFirstPlayed: ko.observable(null),
            historicalDateLastPlayed: ko.observable(null),
            historicalDateFirstPlayed: ko.observable(null),
            firstPlayedDisplay: ko.observable(null),
            lastPlayedDisplay: ko.observable(null),
            historicalDataExists: ko.observable(),
            firstDateDetailsVisible: ko.observable(false),
            lastDateDetailsVisible: ko.observable(false),
            matches: ko.observableArray(),
            statsTotal: ko.observable(),
            stats: ko.observableArray([]),
            mostlyPlayWith: ko.observableArray([]),
            mostlyWon: ko.observableArray([]),
            mostlyLost: ko.observableArray([]),
            likeFlag: ko.observable(false),
            likeToPlayFlag: ko.observable(false),
            ownedFlag: ko.observable(false),
            knowGameFlag: ko.observable(false),
            matchesOnlyWithUser: ko.observable(false),
            matchesFilter: ko.observable(''),
            resultsByBuddyOnlyWithUser: ko.observable(false),
            toggleOwned:function() {
                var self = this;

                gameDAO.updateGameOwned(self._id(), !self.ownedFlag()).then(function(data){
                    if (data.success) {
                        self.ownedFlag(!self.ownedFlag());
                    } else {
                        app.trigger('msg.new', {text:i18n.t("err.couldntSave"), type:"danger", timeout: 3000});
                    }

                }).done();

            },
            toggleLike: function() {
                var self = this;

                gameDAO.updateGameLike(self._id(), !self.likeFlag()).then(function(data){
                    if (data.success) {
                        self.likeFlag(!self.likeFlag());
                    } else {
                        app.trigger('msg.new', {text:i18n.t("err.couldntSave"), type:"danger", timeout: 3000});
                    }

                }).done();

            },
            toggleLikeToPlay: function() {
                var self = this;

                gameDAO.updateGameLikeToPlay(self._id(), !self.likeToPlayFlag()).then(function(data){
                    if (data.success) {
                        self.likeToPlayFlag(!self.likeToPlayFlag());
                    } else {
                        app.trigger('msg.new', {text:i18n.t("err.couldntSave"), type:"danger", timeout: 3000});
                    }

                }).done();

            },
            toggleKnowGame: function() {
                var self = this;

                gameDAO.updateGameKnow(self._id(), !self.knowGameFlag()).then(function(data){
                    if (data.success) {
                        self.knowGameFlag(!self.knowGameFlag());
                    } else {
                        app.trigger('msg.new', {text:i18n.t("err.couldntSave"), type:"danger", timeout: 3000});
                    }

                }).done();

            },
            toggleMatchesOnlyWithUser: function(){
                this.matchesOnlyWithUser(!this.matchesOnlyWithUser());

                if (this.matchesOnlyWithUser())
                    this.matchesFilter('iPlayed:1');
                else
                    this.matchesFilter('');

            },
            toggleResultsByBuddyOnlyWithUser: function(){
                this.resultsByBuddyOnlyWithUser(!this.resultsByBuddyOnlyWithUser());
            },
            saveComment: function(comment){
                this.comment(comment);
                gameDAO.updateGameComment(this._id(), this.comment()).done();
            },
            changeDate: function(date, key){
                var self = this;
                var params = {
                    value: date(),
                    label: i18n.t(key),
                    isDate: true
                }
                return dialog.show(changeValueDlg, params).then(function(newValue){
                    if (null != newValue) {
                        date(moment(newValue).format('LL'));

                        gameDAO.updateGameDates(self._id(), self.historicalDateFirstPlayed(), self.historicalDateLastPlayed()).done();
                        return date();
                    }
                });
            },
            historicalFirstDateChange: function(){
                this.changeDate(this.historicalDateFirstPlayed, 'scorer.dateFirstPlaying')
                    .then(_.bind(function(date){
                        if (this.dateFirstPlayed()){
                            var historical = moment(this.historicalDateFirstPlayed());
                            var actual = moment(this.dateFirstPlayed());

                            if (historical.diff(actual, 's') < 0){
                                this.firstPlayedDisplay(date);
                            } else {
                                this.firstPlayedDisplay(this.dateFirstPlayed());
                            }

                        } else {
                            this.firstPlayedDisplay(date);
                        }
                    }, this));
            },
            historicalLastDateChange: function(){
                this.changeDate(this.historicalDateLastPlayed, 'scorer.dateLastPlaying')
                    .this(_.bind(function(date){
                        if (!this.dateLastPlayed())
                            this.lastPlayedDisplay(date);
                    }, this))
            },
            selectMatch: function(context){
                // show different match views depending on whether match is in progress
                if (context.data.inProgress)
                    router.navigate('#scorer/viewMatch/'+context.data.id);
                else
                    router.navigate('#scorer/viewFinishedMatch/'+context.data.id);
            },
            selectBuddy: function(context){
                router.navigate('#scorer/buddy/'+context._id);
            },
            setNotPlayed: function(){
              this.played(false);
              gameDAO.updateGame({_id: this._id(), played: this.played()}).done();
            },
            openGameOptions: function(){
                dialog.show(itemOptionsDlg).then(_.bind(function(result){
                    if (result.action == 'rename'){
                        var originalName = this.name();
                        this.name(result.newName);
                        renameMatchesGame(this.matches, result.newName);
                        gameDAO.renameGame(this._id(), result.newName).catch(_.bind(function(){
                            this.name(originalName);
                        }, this));
                    } else if (result.action == 'delete'){
                        gameDAO.deleteGame(this._id()).then(_.bind(function(result){
                            if (result.cannotDelete){
                                var message = i18n.t('scorer.gameFoundInMatches');
                                var title = i18n.t('scorer.gameFoundInMatchesTitle');
                                var options = [
                                    {text: i18n.t('common.ok'), value: 1}
                                ];
                                dialog.showMessage(message, title, options).done();
                            } else {
                                router.navigate('#scorer/games');
                            }
                        }, this)).done();
                    }
                }, this));
            },
            recalculateGameDates: function(){
                gameDAO.updateGameDatesAndMatchGameInfo(this._id()).then(_.bind(function(data){
                    this.dateFirstPlayed(data.game.dateFirstPlayed!=null?moment(data.game.dateFirstPlayed).format('LL'):null);
                    this.dateLastPlayed(data.game.dateLastPlayed!=null?moment(data.game.dateLastPlayed).format('LL'):null);
                }, this));
            },
            toggleFirstDateDetailsVisible: function(){
                this.firstDateDetailsVisible(!this.firstDateDetailsVisible());
            },
            toggleLastDateDetailsVisible: function(){
                this.lastDateDetailsVisible(!this.lastDateDetailsVisible());
            },
            enterHistoricalData: function(){
                this.historicalDataExists(true);
                this.firstDateDetailsVisible(true);
                this.lastDateDetailsVisible(true);
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (id){
                var self = this;
                self.cleanup();

                self._id(id);
                return self.fetchGameDetails();
            },
            fetchGameDetails: function() {
                var self = this;

                return gameDAO.getGameForUser(self._id()).then(function(data) {
                    if (data.game) {
                        self.name(data.game.name);
                        self.bggId(data.game.bggId);
                        self.comment(data.game.comment);
                        self.played(data.game.played);
                        self.historicalDateLastPlayed(data.game.historicalDateLastPlayed!=null?moment(data.game.historicalDateLastPlayed).format('LL'):null);
                        self.historicalDateFirstPlayed(data.game.historicalDateFirstPlayed!=null?moment(data.game.historicalDateFirstPlayed).format('LL'):null);
                        self.historicalDataExists(!!data.game.historicalDateLastPlayed || !!data.game.historicalDateFirstPlayed);
                        self.dateFirstPlayed(data.game.dateFirstPlayed!=null?moment(data.game.dateFirstPlayed).format('LL'):null);
                        self.dateLastPlayed(data.game.dateLastPlayed!=null?moment(data.game.dateLastPlayed).format('LL'):null);

                        self.firstPlayedDisplay(self.dateFirstPlayed());
                        if (data.game.historicalDateFirstPlayed){
                            if(!data.game.dateFirstPlayed){
                                self.firstPlayedDisplay(self.historicalDateFirstPlayed());
                            } else {
                                historical = moment(data.game.historicalDateFirstPlayed);
                                actual = moment(data.game.dateFirstPlayed);

                                if (historical.diff(actual, 's')<0){
                                    self.firstPlayedDisplay(self.historicalDateFirstPlayed());
                                }
                            }
                        }

                        self.lastPlayedDisplay(self.historicalDateLastPlayed());
                        if (data.game.dateLastPlayed){
                            self.lastPlayedDisplay(self.dateLastPlayed());
                        }


                        self.likeFlag(data.game.favourite);
                        self.ownedFlag(data.game.owned);
                        self.likeToPlayFlag(data.game.wantToPlay);
                        self.knowGameFlag(data.game.knowRules);
                        // game is found, fetch matches for stats
                        return self.fetchGameMatches();
                    }
                });
            },

            fetchGameMatches: function() {
                var self = this;

                return matchDAO.getMatchesForGame(self._id()).then(function(data) {
                    //console.log("Fetched " + data.matches.length + " matches for game " + self.name());
                    var ms = matchesModel.toModel(data.matches);
                    self.matches(ms);

                    var nameOfMe = i18n.t('scorer.me');
                    var playerStats = new StatsByItem();
                    var gameStats = new StatsByItem();

                    self.matches().forEach(function(m) {
                        //        addStatistic(match, itemName, itemId,   itemIsUser, calculateWins, mainId,     mainIsUser, specificId, specificIsUser)
                        gameStats.addStatistic(m,     m.game,   m.gameId, false,      false);

                        var itemIsPlayer = true;
                        if (m.players && m.players.length>0) {
                            m.players.forEach(function(p) {
                                if (!p.isAnonymous) {
                                    var name = p.name ? p.name : nameOfMe;
                                    var id = p.isUser ? -1 : p.buddyId;
                                    var itemIsUser = p.isUser ? true : false;

                                    //when calculating stats against other buddies for a buddy, specific wins/losses are against that buddy
                                    //          addStatistic(match, itemName, itemId,   itemIsUser, calculateWins, mainId,     mainIsUser, specificId, specificIsUser)
                                    playerStats.addStatistic(m,     name,     id,       itemIsUser, true,          id,         itemIsUser, -1,         true);
                                }
                            });
                        }
                    });

                    self.statsTotal(gameStats.total);
                    self.stats(_.values(playerStats.items));

                    self.mostlyWon(playerStats.getMostWinsByUser().items);
                    self.mostlyLost(playerStats.getMostLossesByUser().items);

                    self.mostlyPlayWith(playerStats.getMostPlayedItems(true, true).items)
                });
            },

            cleanup: function (){
                this._id(null);
                this.name("");
                this.comment("");
                this.bggId(null);
                this.played(false);
                this.dateFirstPlayed(null);
                this.dateLastPlayed(null);
                this.matches([]);
                this.statsTotal(null);
                this.stats([]);
                this.mostlyWon([]);
                this.mostlyLost([]);
                this.likeFlag(false);
                this.likeToPlayFlag(false);
                this.knowGameFlag(false);
                this.mostlyPlayWith([]);
                this.firstDateDetailsVisible(false);
                this.lastDateDetailsVisible(false);
                this.historicalDateLastPlayed(null);
                this.historicalDateFirstPlayed(null);
                this.firstPlayedDisplay(null);
                this.lastPlayedDisplay(null);
                this.historicalDataExists(false);
            }

        };
    }
);