define(['plugins/router', 'knockout', 'dao/matchDAO', 'dao/buddyDAO', 'dao/gameDAO', 'model/matches', 'durandal/app','moment', 'q'],
    function (router, ko, matchDao, buddyDAO, gameDAO, matchesModel, app, moment, Q) {
        //in closure because viewMatch function in called in scope of a hub widget
        var lastMatch = ko.observable();

        return {
            /********************BINDINGS*********************************/
            lastMatch: lastMatch, //in closure because viewMatch function is called in scope of a hub widget
            matches: ko.observableArray(),
            buddyCount: ko.observable(0),
            stats: ko.observable(),
            games: ko.observableArray(),
            gamesPlayed: ko.observable(0),
            gamesOwned: ko.observable(0),
            startNewMatch: function(){
                router.navigate('#scorer/newMatch');
            },
            viewMatch: function(){
                router.navigate('#scorer/viewMatch/'+lastMatch().id);
            },
            viewAllMatches: function(){
                router.navigate('#scorer/lastMatches');
            },
            buddies: function(){
                router.navigate('#scorer/buddies');
            },
            viewStats: function() {
                router.navigate('#scorer/stats');
            },
            viewGames: function() {
                router.navigate('#scorer/games');
            },

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (){
                var self = this;

                this.matches([]);
                this.games([]);
                this.buddyCount(0);
                this.stats({});
                this.gamesPlayed(0);
                this.gamesOwned(0);
                this.lastMatch({game:''});
                
                var buddyPromise = buddyDAO.getBuddyCount()
                    .then(function(data) {
                        self.buddyCount(data.count || 0);
                    });
                    
                var gamesPromise = gameDAO.getGamesForUser()
                    .then(function(data){
                        self.games(data.games);
                        var ownedCount = 0;
                        var playedCount = 0;
                        self.games().forEach(function(g) {
                            if (g.owned)
                                ownedCount++;
                            if (g.played)
                                playedCount++;
                        });
    
                        self.gamesPlayed(playedCount);
                        self.gamesOwned(ownedCount);
                        
                    });
                    
                var matchPromise = matchDao.getMatchesForUser(3)
                    .then(function(data){
                        var matches = matchesModel.toModel(data.matches);
                        self.matches(matches);
    
                        if (matches.length > 0) {
                            self.lastMatch(self.matches()[0]);
                        } else {
                            self.lastMatch({
                                game: '',
                                date: '',
                                inProgress: false
                            })
                        }
                        
                        return self; //returnirg right context for next function in chain
    
                    })
                
                return Q.all([buddyPromise, gamesPromise, matchPromise, self.fetchAndCalcStats()]);
            },

            fetchAndCalcStats: function() {
                var self = this;
                var m = moment(); // now
                m.startOf('month');
                m.hour(0).minute(0).second(0).millisecond(0); // reset time too
                var dateBack = m.toDate(); // matches from this point in time will be fetched for stats

                //  data
                var statistics4Month = {totalGamesPlayed: 0, mostPlayedGame: [], mostPlayedGameCount: 0, diffGamesCount: 0, mostPlayedBuddy: [], mostPlayedBuddyCount: 0};
                return matchDao.getMatchesNewerThan(dateBack)
                    .then(function(data){
                        var ms = matchesModel.toModel(data.matches);
    
                        // calc stats for hub
                        statistics4Month.totalGamesPlayed = ms.length;
                        var games = {};
                        var buddies = {};
                        ms.forEach(function(m) {
                            if (games.hasOwnProperty(m.game)) {
                                games[m.game] = games[m.game] + 1;
                            } else {
                                games[m.game] = 1;
                            }
    
                            m.players.forEach(function(p) {
                                if (p.name) { // don't include user & anonymous
                                    if (buddies.hasOwnProperty(p.name)) {
                                        buddies[p.name] = buddies[p.name] + 1;
                                    } else {
                                        buddies[p.name] = 1;
                                    }
                                }
                            });
                        });
    
                        var maxGames = 0;
                        for(var g in games) {
                            statistics4Month.diffGamesCount++;
                            if (games[g] > maxGames) {
                                statistics4Month.mostPlayedGame = [g];
                                statistics4Month.mostPlayedGameCount = games[g];
                                maxGames = games[g];
                            } else if (games[g] === maxGames) {
                                statistics4Month.mostPlayedGame.push(g);
                            }
                        }
    
                        var maxBuddies = 0;
                        for(var b in buddies) {
                            if (buddies[b] > maxBuddies) {
                                statistics4Month.mostPlayedBuddy = [b];
                                statistics4Month.mostPlayedBuddyCount = buddies[b];
                                maxBuddies = buddies[b];
                            } else if (buddies[b] === maxBuddies) {
                                statistics4Month.mostPlayedBuddy.push(b);
                            }
                        }

                        self.stats(statistics4Month);
                    });
            }
        };
    }
);
