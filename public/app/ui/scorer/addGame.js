define(['knockout', 'dao/gameDAO', 'plugins/router', 'durandal/app', 'i18next'],
    function (ko, gameDAO, router, app, i18n) {

        return {
            /********************BINDINGS*********************************/
            name: ko.observable(),
            actionPending: ko.observable(false),
            owner: ko.observable(false),
            saveGame: function() {
                var self = this;
                this.actionPending(true)
                gameDAO.saveNewGame({name: this.name(), owned: this.owner()}, false).then(function(data) {
                    if (data.success) {
                        app.trigger('msg.new', {text:i18n.t("scorer.gameSavedSuccessfully", {game: self.name()}), type:"success", timeout: 3000});
                        router.navigate('#scorer/game/'+data.game._id);
                    } else {
                        app.trigger('msg.new', {text:i18n.t("err.couldntSave"), type:"error", timeout:3000});
                    }
                }).done();
            },

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (){
                var self = this;
                self.cleanup();
            },

            cleanup: function (){
                this.name("");
                this.owner(false);
                this.actionPending(false);
            }

        };
    }
);