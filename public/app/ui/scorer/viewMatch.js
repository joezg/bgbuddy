define(['knockout', 'dao/matchDAO', 'dao/gameDAO', 'plugins/router', 'modals/scorer/addPlayerModal','modals/scorer/setFirstPlayerModal', 'util/dialogWrapper', 'moment', 'model/match', 'model/matches', 'model/matchPhaseEnum', 'modals/scorer/addPhaseModal', 'durandal/app', 'modals/scorer/enterScoreModal', 'i18next', 'underscore', 'model/matchViewModel'],
    function (ko, matchDAO, gameDAO, router, playerDlg, firstPlayerDlg, dialog, moment, Match, matchesModel, matchPhaseEnum, phaseDlg, app, scoreDialog, i18n, _, matchVM) {
        var noScoring = false;

        return {
            /********************BINDINGS*********************************/
            match: null,
            buddySuggestion: ko.observableArray([]),
            suggestBuddiesFlag: ko.observable(false), // flag indicating whether to suggest buddies. Maybe this is user's first game, no buddies to suggest. or maybe he chose this. through settings.

            addPlayerDialog: function(){
                dialog.show(playerDlg, {players: this.match.players()}).then(_.bind(function(players){
                    if (null != players) {
                        this.match.setPlayersUndo();
                        for (var i=0; i<players.length; i++)
                            this.match.addPlayer(players[i]);

                        this.updatePlayers();
                    }
                }, this));
            },
            setFirstPlayer: function() {
                this.match.setPlayersUndo();
                // remove previous isFirst flag if exists
                this.match.players().forEach(function(p) {
                    p.isFirst(false);
                });

                // open setFirstPlayerModal
                dialog.show(firstPlayerDlg, {players: this.match.players()}).then(_.bind(function(player){
                    if (null != player) {
                        player.isFirst(true);
                    }
                    matchDAO.matchUpdatePlayers(this.match._id, this.match.players(), false).done();
                }, this));
            },
            getRandomBuddy: function() {
                var r = Math.floor(Math.random() * this.match.players().length);

                var randomPlayer = this.match.players()[r];
                var name =  randomPlayer.name;
                var msg = '';

                if (randomPlayer.isUser)
                    msg = i18n.t('scorer.iAmRandomPlayer');
                else if (!name)
                    name = i18n.t('scorer.unknown') + "!";

                if (!msg) {
                    //msg = i18n.t('scorer.xIsRandomPlayer', {buddy:name});
                    msg = name + "!";
                }


                var title = i18n.t('scorer.randomPlayer');

                dialog.showMessage(msg, title, [i18n.t('common.ok')], true);


            },
            selectSuggestedBuddies: function() {
                this.match.setPlayersUndo();
                this.buddySuggestion().forEach(_.bind(function(b) {
                    b = _.pick(b, ['isUser', '_id', 'buddyId', 'name', 'isAnonymous'])
                    this.match.addPlayer(b);
                }, this));
                this.updatePlayers();

                this.suggestBuddiesFlag(false);
            },
            dismissSuggestedBuddies: function() {
                this.suggestBuddiesFlag(false);
            },
            removePlayer: function(hub) {
                var player = hub.data;

                this.match.setPlayersUndo();
                this.match.removePlayer(player);
                matchDAO.matchRemovePlayer(this.match._id, player._id, false).catch(_.bind(function(err){
                    this.match.undoPlayersChanges();
                }, this));
            },
            pauseToggle: function() {
                if (!this.match.paused()){
                    this.startNewPhase(matchPhaseEnum.pause);
                } else {
                    var previousPhase = this.match.getMatchModel().getPreviousPhase();
                    this.startNewPhase(previousPhase.type, previousPhase.name);
                }

            },
            matchPhaseStart: function(){
                dialog.show(phaseDlg,
                    {
                        phases: _.map(this.match.getMatchModel().matchPhases, _.clone),
                        isNext: true,
                        availablePhases: this.match.getMatchModel().getAvailableMatchPhaseTypes()
                    }).then(_.bind(function(data){
                        if (null != data) {
                            this.startNewPhase(data.phase.type, data.name);
                            if (data.phase.type == matchPhaseEnum.scoring){
                                this.askForScoring(false);
                            }
                        }

                    }, this));

            },
            finishMatchClick: function() {
                if (!noScoring)
                    this.askForScoring(true);
                else
                    this.finishMatch();
            },


            /********************LIFECYCLE CALLBACKS************************/
            deactivate: function(){
                if (this.match) this.match.stopTimers();
            },
            activate: function(id) {
                noScoring = false;

                if (this.match) this.match.clean();

                // need last game played to suggest buddies from last game
                // TODO test & check will it work with multiple in progress games
                return matchDAO.getMatchesForUser(2).then(_.bind(function(data){
                    var ms = matchesModel.toModel(data.matches);

                    var activeMatchFound = false;
                    ms.forEach(_.bind(function(m) {
                       if (id === m.id) {
                           activeMatchFound = true;
                           this.match = new matchVM(m);
                           this.match.startTimers();
                       } else {
                           this.buddySuggestion(m.getPlayers());
                           this.suggestBuddiesFlag(true);
                       }
                    }, this));

                    if (!activeMatchFound)
                        return this.fetchMatchById(id);
                }, this));

            },



            /********************UTILITY************************************/
            fetchMatchById: function(id) {
                return matchDAO.getMatch(id).then(_.bind(function(data){
                    this.match = new matchVM(data.match);
                    this.match.startTimers();
                }, this));
            },
            updatePlayers: function(){
                return matchDAO.matchUpdatePlayers(this.match._id, this.match.players(), false)
                    .then(_.bind(function(data){
                        this.match.updatePlayersIds(data.players);
                        this.match.deletePlayersUndo()
                    }, this))
                    .catch(_.bind(function(err){
                        this.match.undoPlayersChanges();
                    }, this));
            },
            startNewPhase: function(phaseType, name){
                this.match.setPhasesUndo();
                var newPhase = this.match.startNewPhase(phaseType, name);
                matchDAO.matchStartPhase(this.match._id, newPhase).catch(_.bind(function(err){
                    this.match.undoPhasesChanges();
                }, this));
            },
            askForScoring: function(doFinishMatch){
                var options = [
                    {text: i18n.t('common.yes'), value: 1},
                    {text: i18n.t('scorer.noIDontCare'), value: 0},
                    {text: i18n.t('common.cancel'), value: -1}
                ];
                dialog.showMessage(i18n.t("scorer.doYouWantToScoreTheMatch"), i18n.t('scorer.scoring'), options).then(_.bind(function(answer){
                    if (1 === answer) {
                        this.enterScoreDialog(doFinishMatch);
                    } else if (0 === answer) {
                        noScoring = true;
                        if (doFinishMatch)
                            this.finishMatch();
                    }
                }, this));
            },
            enterScoreDialog: function(doFinishMatch){
                dialog.show(scoreDialog, this.match.getMatchModel()).then(_.bind(function(finished){
                    if (finished) {
                        this.updatePlayers().then(_.bind(function(){
                            if (doFinishMatch){
                                this.finishMatch();
                            } else {
                                noScoring = true;
                                var options = [
                                    {text: i18n.t('common.yes'), value: 1},
                                    {text: i18n.t('common.no'), value: 0}
                                ];
                                dialog.showMessage(i18n.t("scorer.doYouWantToFinishTheMatch"), i18n.t('scorer.finishMatch'), options).then(_.bind(function(answer){
                                    if (1 === answer) {
                                        this.finishMatch();
                                    }
                                }, this));
                            }
                        }, this));
                    }
                }, this));
            },
            finishMatch: function() {
                this.match.getMatchModel().finishMatch();
                matchDAO.finishMatch(this.match._id)
                    .then(_.bind(function(){
                        if (this.match.getMatchModel().isNewGame){
                            gameDAO.destroyCache();
                        }

                        router.navigate('#scorer');
                    }, this))
                    .catch(function(err){
                        this.match.getMatchModel().undoFinishMatch();
                    });
            }

        };
    }
);

