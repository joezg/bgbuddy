define(['knockout', 'dao/buddyDAO', 'plugins/router', 'durandal/app', 'i18next'],
    function (ko, buddyDAO, router, app, i18n) {

        return {
            /********************BINDINGS*********************************/
            name: ko.observable(),
            buddies: ko.observableArray(),
            myFilter: ko.observable(),
            selectBuddy: function(context){
                router.navigate('#scorer/buddy/'+context.data._id);
            },
            findBuddies: function(){
                router.navigate('#scorer/findBuddies');
            },
            addNewBuddy: function(){
                router.navigate('#scorer/newBuddy');
            },
            buddySortFunction: function(ba, bb){
                //returning 0 is needed for secondary sort
                if (ba.like && bb.like)
                    return 0;
                if (!ba.like && !bb.like)
                    return 0;

                if (ba.like)
                    return -1;
                if (bb.like)
                    return 1;

                return 0;
            },
            /********************ACTIVATION CALLBACKS*********************/
            activate: function (){
                var self = this;
                self.buddies([]);

                this.filters = [
                    { titleKey: "scorer.showFavorites", filter: "like:"+i18n.t("common.yes_lower")+";" }
                ]

                // get buddies
                return self.fetchBuddies();

            },
            fetchBuddies: function() {
                var self = this;

                return buddyDAO.getAllBuddies().then(function(data){
                    if (data.buddies) {
                        self.buddies(data.buddies);
                    }

                });


            }

        };
    }
);