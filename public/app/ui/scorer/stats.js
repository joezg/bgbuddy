define(['knockout', 'dao/buddyDAO','dao/matchDAO', 'plugins/router', 'model/matches','durandal/app', 'i18next', 'model/user','moment', 'q', 'lodash', 'modals/common/changeValue', 'util/dialogWrapper', 'model/statsByItem'],
    function (ko, buddyDAO, matchDAO, router, matchesModel, app, i18n, user, moment, Q, _, changeValueDlg, dialog, StatsByItem) {
        var monthActionLabels = {
            2: "scorer.viewBuddies",
            3: "scorer.viewStats",
            0: "scorer.viewGames",
            1: "scorer.viewHotlist"
        }
        var statistics = {}; // initialize monthly statistics object

        return  {
            /********************BINDINGS*********************************/
            name: ko.observable(), // user name
            currentYear: ko.observable(), // display data for this year in year view
            monthsStats: ko.observableArray([]),
            yearDetailsStats: ko.observable(),
            availableYears: [], // years available for stats
            detailsType: ko.observable(0),
            detailsAction: ko.observable(),
            personalGameStats: ko.observableArray(),
            personalPlayerStats: ko.observableArray(),
            personalGameStatsTotal: ko.observableArray(),
            bgUserSex: "",
            selectGame: function(context){
                router.navigate('#scorer/game/'+context.id);
            },
            selectBuddy: function(context){
                router.navigate('#scorer/buddy/'+context.id);
            },
            selectYear: function() {
                dialog.show(changeValueDlg, {
                    isDropdown: true,
                    options: this.availableYears,
                    label: i18n.t('scorer.chooseStatisticsYear'),
                    title: i18n.t('scorer.statisticsFilter')
                }).then(_.bind(function(result){
                    this.setupYear(result.value);
                }, this));
            },
            selectMonth: function(context) {
                var newType = (this.detailsType()+1) % 4;
                this.detailsType(newType);
                this.detailsAction(monthActionLabels[newType]);
            },

            /********************ACTIVATION CALLBACKS*********************/
            activate: function (){
                this.cleanup();

                // set user data
                var userPromise = user.whenLoaded().then(_.bind(function(u){
                    this.name(u.username);
                    this.bgUserSex = u.sex;
                }, this));

                // get history: games played
                var gamesPromise = this.fetchGamesPlayed();

                return Q.all([userPromise, gamesPromise])
            },


            // user chose to display data for different year
            setupYear: function (year) {
                this.currentYear(year);
                var currentYearStats;
                if (-1 == year) {
                    currentYearStats = statistics;
                } else {
                    currentYearStats = _.where(statistics, {year:year});
                }

                this.monthsStats(currentYearStats);

                // determine this after filling monthsStats
                var yds = {
                    total: 0,
                    differentGames: 0,
                    wins: 0,
                    newGames: 0,
                    outOfDustGames:0,
                    games: [],
                    buddies: [],
                    mostlyPlayed: [],
                    mostlyPlayedMatchesCount: 0,
                    mostlyPlayedWith: [],
                    mostlyPlayedWithMatchesCount: 0
                };

                var gamesPlayed = {}; // e.g. {"GT": 42}
                var buddiesPlayedWith = {}; // e.g. {"Filip": 42}

                for (var i=0; i<currentYearStats.length; i++) {
                    yds.total = yds.total + currentYearStats[i].totals.total;
                    yds.wins = yds.wins + currentYearStats[i].totals.wins;
                    yds.newGames = yds.newGames + currentYearStats[i].totals.newGames;
                    yds.outOfDustGames = yds.outOfDustGames + currentYearStats[i].totals.outOfDustGames;
                    for (var j=0; j<currentYearStats[i].buddies.length; j++) {
                        var buddy = currentYearStats[i].buddies[j];

                        if (!buddiesPlayedWith.hasOwnProperty(buddy.name)) {
                            buddiesPlayedWith[buddy.name] = {name:buddy.name, id: buddy.id, count:0};
                        }

                        buddiesPlayedWith[buddy.name].count += buddy.gameCount;

                    };
                    for (var k=0; k<currentYearStats[i].games.length; k++) {
                        var game = currentYearStats[i].games[k];

                        if (!gamesPlayed.hasOwnProperty(game.game)) {
                            gamesPlayed[game.game] = {name:game.game, id: game.id, count:0};
                        }

                        gamesPlayed[game.game].count += game.count;

                    };
                }

                yds.buddies = _.values(buddiesPlayedWith);
                yds.games = _.values(gamesPlayed);
                yds.differentGames = yds.games.length;

                // determine most played games (yearly stats)
                var maxPlayed = 0;
                for (var property in gamesPlayed) {
                    if (gamesPlayed.hasOwnProperty(property)) {
                        //console.log(property + ": " + gamesPlayed[property]);
                        if (gamesPlayed[property].count > maxPlayed) {
                            yds.mostlyPlayed = [gamesPlayed[property]];
                            maxPlayed = gamesPlayed[property].count;
                        } else if (gamesPlayed[property].count === maxPlayed){
                            yds.mostlyPlayed.push(gamesPlayed[property]);
                        }
                    }
                }
                yds.mostlyPlayedMatchesCount = maxPlayed;

                // determine buddies most played with (yearly stats)
                var maxBuddiesPlayed = 0;
                for (var property in buddiesPlayedWith) {
                    if (buddiesPlayedWith.hasOwnProperty(property)) {
                        if (buddiesPlayedWith[property].count > maxBuddiesPlayed) {
                            yds.mostlyPlayedWith = [buddiesPlayedWith[property]];
                            maxBuddiesPlayed = buddiesPlayedWith[property].count;
                        } else if (buddiesPlayedWith[property].count === maxBuddiesPlayed){
                            yds.mostlyPlayedWith.push(buddiesPlayedWith[property]);
                        }
                    }
                }
                yds.mostlyPlayedWithMatchesCount = maxBuddiesPlayed;

                this.yearDetailsStats(yds);
            },

            fetchGamesPlayed: function() {
                return matchDAO.getMatchesForUser().then(_.bind(function(data){
                    var ms = matchesModel.toModel(data.matches);

                    if (ms.length > 0){
                        // sort matches by date
                        ms.sort(function (ma, mb){
                            if (mb.date > ma.date)
                                return -1;
                            return 1;
                        });

                        var firstYear = moment(ms[0].date).year();
                        var lastYear = moment().year();
                        var tempYear = firstYear;

                        this.availableYears.push({name: i18n.t('common.allTime'), value: -1});
                        while (tempYear <= lastYear){
                            this.availableYears.push({name: tempYear, value: tempYear});
                            tempYear++;
                        }

                        this.calcStats(ms);
                    }
                }, this));


            },

            // calc monthly & yearly stats
            calcStats: function(matches) {
                //personal stats by game
                var personalGameStats = new StatsByItem();
                //personal stats by player
                var personalPlayerStats = new StatsByItem();

                // put stats for month in question here
                // var is outside for loop for efficiency's sake: matches are sorted.
                // so: first consult currStats, then loop through statistics if necessary
                // Ribica is soooo amazing
                var currStats;
                for (var m=0; m<matches.length; m++) {
                    // analyzing matches[m]
                    var match = matches[m];

                    if (match.date && match.iPlayed) {
                        var month = moment(match.date).month();
                        var year = moment(match.date).year();
                        var index = year.toString()+month.toString();
                        currStats = statistics[index];

                        if (!currStats){
                            currStats = {
                                year: year,
                                month: {id:month, name:moment.months(month), nameShort:moment.monthsShort(month)},
                                games: {}, // e.g. {game:"I love my wife", count: 66, isNew: true, isOutOfDust: false},
                                buddies: {}, // e.g. {name: "Filip", gameCount: 16},
                                totals: {total: 0, wins: 0, newGames:0, outOfDustGames:0, scoredMatches: 0}
                            };
                            statistics[index] = currStats;
                        }

                        // now we have data for month in currStats: do your magic
                        currStats.totals.total++; // count total matches played
                        // increment user wins
                        if (match.iWon)
                            currStats.totals.wins++;

                        if (match.isScored)
                            currStats.totals.scoredMatches++;

                        if (!currStats.games.hasOwnProperty(match.gameId)){
                            currStats.games[match.gameId] = {game: match.game, count: 0, isNew: false, isOutOfDust: false, id: match.gameId};
                        }

                        currStats.games[match.gameId].count++;

                        if (match.isNewGame){
                            currStats.games[match.gameId].isNew = true;
                            currStats.totals.newGames++;
                        }
                        if (match.isGameOutOfDust){
                            currStats.games[match.gameId].isOutOfDust = true;
                            currStats.totals.outOfDustGames++
                        }

                        for (var pl=0; pl<match.players.length; pl++) {
                            var buddy = match.players[pl];
                            if (buddy.name){
                                if (!currStats.buddies.hasOwnProperty(buddy.buddyId)){
                                    currStats.buddies[buddy.buddyId] = {name: buddy.name, gameCount: 0, id: buddy.buddyId}
                                }

                                currStats.buddies[buddy.buddyId].gameCount++;

                                //PERSONAL PLAYER STATS
                                //when calculating stats against other buddies for a buddy, specific wins/losses are against that buddy
                                //                  addStatistic(match,     itemName,     itemId,         itemIsUser, calculateWins, mainId,     mainIsUser, specificId,     specificIsUser)
                                personalPlayerStats.addStatistic(match,     buddy.name,   buddy.buddyId,  false,      true,          -1,         true,       buddy.buddyId,  false);
                            }
                        }

                        //PERSONAL GAME STATS
                        //when calculating game stats for myself, there is no specific wins/losses
                        //                addStatistic(match, itemName, itemId,   itemIsUser, calculateWins, mainId,     mainIsUser, specificId, specificIsUser)
                        personalGameStats.addStatistic(match,     match.game,   match.gameId, false,      true,          -1,         true,       null,         false);
                    }
                };

                this.personalGameStats(_(personalGameStats.items).values().sortByOrder('totalMatches', 'desc').value());
                this.personalGameStatsTotal(personalGameStats.total);
                this.personalPlayerStats(_(personalPlayerStats.items).values().sortByOrder('totalMatches', 'desc').value());

                statistics = _.values(statistics);
                statistics.sort(monthSortFunction);

                //calculate hotness
                for (var i = 0; i<statistics.length;i++){
                    for (var j=i; j<statistics.length;j++){
                        var monthsDiff = statistics[j].month.id-statistics[i].month.id;
                        monthsDiff += (statistics[j].year-statistics[i].year)*12;
                        var coef = 1 - (monthsDiff)*0.1;

                        if (coef > 0){
                            for (var id in statistics[i].games) {
                                var game = statistics[i].games[id];
                                if (!statistics[j].hotlist){
                                    statistics[j].hotlist = {};
                                }

                                if (!statistics[j].hotlist[game.id]){
                                    statistics[j].hotlist[game.id] = {game: game.game, count: 0};
                                }

                                var newCount = _.round(statistics[j].hotlist[game.id].count + game.count*coef, 2);
                                statistics[j].hotlist[game.id].count = newCount;
                            };
                        }
                    }
                }

                // sort
                statistics.forEach(function(s){
                    s.games = _.values(s.games);
                    s.buddies = _.values(s.buddies);
                    s.hotlist = _.values(s.hotlist);

                    s.games.sort(gameSortFunction);
                    s.buddies.sort(buddySortFunction);
                    s.hotlist.sort(gameSortFunction);
                    s.hotlist = _.take(s.hotlist, 10);
                })

                this.setupYear(moment().year());

                function gameSortFunction(gameA, gameB){
                    return gameB.count - gameA.count;
                }

                function buddySortFunction(buddyA, buddyB){
                    return buddyB.gameCount - buddyA.gameCount;
                }

                function monthSortFunction(monthA, monthB){
                    if (monthA.year != monthB.year){
                        return monthA.year - monthB.year;
                    } else {
                        return monthA.month.id - monthB.month.id;
                    }
                }
            },

            cleanup: function (){
                // clear data
                this.name("");
                this.monthsStats([]);
                this.yearDetailsStats({
                    total: 0,
                    differentGames: 0,
                    wins: 0,
                    newGames: 0,
                    outOfDustGames:0,
                    games: [],
                    buddies: [],
                    mostlyPlayed: [],
                    mostlyPlayedMatchesCount: 0,
                    mostlyPlayedWith: [],
                    mostlyPlayedWithMatchesCount: 0
                });
                this.currentYear(null);
                this.availableYears = [];
                this.detailsType(0);
                this.detailsAction(monthActionLabels[this.detailsType()]);
                this.bgUserSex = "";
                this.personalGameStats([]);
                this.personalGameStatsTotal(null);
                this.personalPlayerStats([]);
            }

        };
    }
);