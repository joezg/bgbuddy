define(['knockout', 'dao/matchDAO', 'dao/gameDAO', 'plugins/router', 'modals/scorer/addPlayerModal', 'modals/scorer/enterScoreModal', 'modals/scorer/addPhaseModal', 'modals/scorer/editPhaseModal', 'util/dialogWrapper', 'moment', 'model/match', 'underscore', 'q', 'model/matchViewModel', 'modals/scorer/itemOptionsModal'],
    function (ko, matchDAO, gameDAO, router, playerDlg, scoreDlg, addPhaseDlg, editPhaseDlg, dialog, moment, Match, _, Q, matchVM, itemOptionsDlg) {
        return {
            /********************BINDINGS*********************************/
            match: null,//matchViewModel
            editingPlayers: ko.observable(),
            editingPhases: ko.observable(),

            undoPlayersAvailable: ko.observable(false),
            undoPhasesAvailable: ko.observable(),
            playerActionKey: null, //set up in activate

            saveLocation: function(newValue){
                this.match.location(newValue);
                matchDAO.matchUpdateInfo(this.match.getMatchModel()).done();
            },
            saveDate: function(newValue) {
                this.match.setDate(newValue);
                matchDAO.matchUpdateInfo(this.match.getMatchModel()).then(_.bind(function(data){
                    this.match.reloadGameDataAndInfos(data.match);
                    gameDAO.destroyCache();
                }, this));
            },
            toggleOutOfDust: function(){
                this.match.getMatchModel().isGameOutOfDust = !this.match.getMatchModel().isGameOutOfDust;

                if (this.match.getMatchModel().isGameOutOfDust)
                    this.match.getMatchModel().isNewGame = false;

                this.match.reloadMatchModel();
                matchDAO.matchUpdateInfo(this.match.getMatchModel()).catch(_.bind(function(){
                    this.match.getMatchModel().isGameOutOfDust = !this.match.getMatchModel().isGameOutOfDust;
                    this.match.reloadMatchModel();
                }, this));
            },
            toggleNewGame: function(){
                this.match.getMatchModel().isNewGame = !this.match.getMatchModel().isNewGame;

                if (this.match.getMatchModel().isNewGame)
                    this.match.getMatchModel().isGameOutOfDust = false;

                this.match.reloadMatchModel();
                matchDAO.matchUpdateInfo(this.match.getMatchModel()).catch(_.bind(function(){
                    this.match.getMatchModel().isNewGame = !this.match.getMatchModel().isNewGame;
                    this.match.reloadMatchModel();
                }, this));
            },
            editPhases: function(){
                if (this.editingPhases()){
                    this.editingPhases(false);
                    this.match.deletePhasesUndo();
                }
                else {
                    this.undoPhasesAvailable(false);
                    this.editingPhases(true);
                    this.match.setPhasesUndo();
                }
            },
            addPhase: function(){
                dialog.show(addPhaseDlg, {phases: _.map(this.match.getMatchModel().matchPhases, _.clone)}).then(_.bind(function(data){
                    if (data) {
                        this.undoPhasesAvailable(true);
                        var newPhase = this.match.getMatchModel().addPhase(data.phase, data.after, data.name);
                        this.match.reloadMatchModel();
                        matchDAO.updatePhases(this.match.getMatchModel().id, this.match.getMatchModel().matchPhases).catch(_.bind(function(err){
                            this.match.getMatchModel().removePhase(newPhase);
                            this.match.reloadMatchModel();
                        }, this));
                    }
                }, this));
            },
            editPhase: function(hub){
                dialog.show(editPhaseDlg, hub.data).then(_.bind(function(phase){
                    if (phase){
                        this.undoPhasesAvailable(true);
                        for (var i=0; i<this.match.getMatchModel().matchPhases.length;i++){
                            var p = this.match.getMatchModel().matchPhases[i];

                            if (p === phase){
                                if (phase.hasPrevious){
                                    var currentStartDate = moment(phase.start);
                                    for (var j=i-1;j>=0;j--){
                                        var over = false;
                                        var previousPhase = this.match.getMatchModel().matchPhases[j];
                                        var startDate = moment(previousPhase.start);
                                        var endDate = moment(previousPhase.end);

                                        if (phase.changeGameTime) {
                                            var difference = endDate.diff(currentStartDate);
                                            startDate.subtract(difference, 'ms');
                                            endDate = currentStartDate;
                                            currentStartDate = startDate;
                                        } else {
                                            endDate = currentStartDate;
                                            if (startDate.isAfter(endDate)){
                                                startDate = currentStartDate;
                                            } else {
                                                over = true;
                                            }
                                        }

                                        previousPhase.start = startDate.toDate();
                                        previousPhase.end = endDate.toDate();

                                        if (over)
                                            break;
                                    }
                                }
                                if (phase.hasNext){
                                    var currentEndDate = moment(phase.end);
                                    for (var j=i+1;j<this.match.getMatchModel().matchPhases.length;j++){
                                        var over = false;
                                        var nextPhase = this.match.getMatchModel().matchPhases[j];
                                        var startDate = moment(nextPhase.start);
                                        var endDate = moment(nextPhase.end);

                                        if (phase.changeGameTime) {
                                            var difference = currentEndDate.diff(startDate);
                                            endDate.add(difference, 'ms');
                                            startDate = currentEndDate;
                                            currentEndDate = endDate;
                                        } else {
                                            startDate = currentEndDate;
                                            if (endDate.isBefore(startDate)){
                                                endDate = currentEndDate;
                                            } else {
                                                over = true;
                                            }
                                        }

                                        nextPhase.start = startDate.toDate();
                                        nextPhase.end = endDate.toDate();

                                        if (over)
                                            break;
                                    }
                                }
                                break;
                            }
                        }
                        matchDAO.updatePhases(this.match.getMatchModel().id, this.match.getMatchModel().matchPhases).catch(_.bind(function(err){
                            this.match.undoPhasesChanges();
                        }, this));
                        this.match.setPhases();
                    }
                }, this));
            },
            undoPhaseChanges: function(){
                this.match.undoPhasesChanges();

                matchDAO.updatePhases(this.match.getMatchModel().id, this.match.getMatchModel().matchPhases).then(function(){
                    this.editingPhases(false);
                    this.undoPhasesAvailable(false);
                });

            },
            editPlayers: function(){
                if (this.editingPlayers()){
                    this.editingPlayers(false);
                    this.match.deletePlayersUndo();
                }
                else {
                    this.undoPlayersAvailable(false);
                    this.editingPlayers(true);
                    this.match.setPlayersUndo();
                }
            },
            undoPlayerChanges: function(){
                this.match.undoPlayersChanges();

                matchDAO.matchUpdatePlayers(this.match._id, this.match.players())
                    .then(_.bind(function(){
                        this.editingPlayers(false);
                        this.undoPlayersAvailable(false);
                    }, this));
            },
            addPlayer: function(){
                dialog.show(playerDlg, {players: this.match.players()}).then(_.bind(function(players){
                    if (null != players) {
                        this.undoPlayersAvailable(true);
                        for (var i=0; i<players.length; i++)
                            this.match.addPlayer(players[i]);

                        matchDAO.matchUpdatePlayers(this.match._id, this.match.players(), true)
                            .then(_.bind(function(data){
                                this.match.updatePlayersIds(data.players);
                            }, this))
                            .catch(function(){
                                this.match.undoPlayersChanges();
                            });
                    }
                }, this));
            },
            editScores : function(){
                dialog.show(scoreDlg, this.match.getMatchModel()).then(_.bind(function(finished){
                    if (finished) {
                        this.match.reloadPlayers();
                        this.undoPlayersAvailable(true);
                        matchDAO.matchUpdatePlayers(this.match._id, this.match.players()).done()
                    }
                }, this)).done();
            },
            removePlayer: function(player){
                this.match.removePlayer(player);
                this.undoPlayersAvailable(true);
                matchDAO.matchRemovePlayer(this.match._id, player._id).catch(function(err){
                    this.match.addPlayer(player);
                });
            },
            selectPlayer: function(player) {
                var self = this;
                if (!player.isAnonymous && !player.isUser && player.buddyId) {
                    router.navigate('#scorer/buddy/'+player.buddyId);
                };
            },
            playerAction: function(hub){
                if (this.editingPlayers()){
                    this.removePlayer(hub.data);
                } else {
                    this.selectPlayer(hub.data);
                }
            },
            openMatchOptions: function(){
                gameDAO.getGamesForUser().then(_.bind(function(data){
                    var gameSuggestions = data.games;

                    dialog.show(itemOptionsDlg, {delete: true, rename: true, suggestions: gameSuggestions, renameKey: "scorer.changeGame", renamePlaceholderKey: "common.enterGameName"}).then(_.bind(function(result){
                        if (result.action == 'delete'){
                            matchDAO.deleteMatch(this.match._id).then(_.bind(function(result){
                                router.navigate('#scorer/lastMatches');
                            }, this));
                        } else if (result.action == 'rename'){
                            matchDAO.matchUpdateGame(this.match._id, result.itemId, result.newName, this.match.getMatchModel().gameId)
                                .then(_.bind(function(data){
                                    this.match.reloadGameDataAndInfos(data.match);
                                }, this));
                        }
                    }, this));
                }, this));
            },
            goToGame: function(){
                router.navigate('#scorer/game/' + this.match.getMatchModel().gameId);
            },

            /********************LIFECYCLE CALLBACKS************************/
            activate: function(id) {
                if (this.match) this.match.clean();

                this.playerActionKey = ko.computed(_.bind(function(){
                   return this.editingPlayers() ? 'common.remove' : 'scorer.viewBuddy';
                }, this));

                return matchDAO.getMatch(id, true).then(_.bind(function(data){
                    var match = new Match(data.match);
                    this.match = this.match = new matchVM(match);
                }, this));
            }

        };
    }
);

