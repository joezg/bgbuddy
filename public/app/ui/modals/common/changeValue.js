define(['knockout', 'plugins/dialog', 'i18next'], function (ko, dialog, i18n) {
        return {
            /********************BINDINGS*************************/
            value: ko.observable(),
            ok: function(){
                dialog.close(this, this.value());
            },
            cancel: function(){
                dialog.close(this, null);
            },


            /********************LIFECYCLE CALLBACK******************/
            activate: function(params){
                this.value(params.value);
                this.label = params.label;
                this.title = params.title || i18n.t('common.changeValue');
                this.placeholder = i18n.t(this.placeholderKey);
                this.isDate = params.isDate || false;
                this.isDropdown = params.isDropdown || false;
                this.placeholderKey = params.placeholderKey || ( this.isDropdown ? 'common.choose'  : 'common.enterValue');

                if (this.isDropdown){
                    this.options = params.options;
                    this.optionsCaption = i18n.t(this.placeholderKey);
                }
            }
        };
    }
);
