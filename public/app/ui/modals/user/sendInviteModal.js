define(['knockout', 'plugins/dialog', 'i18next', 'dao/emailDAO'], function (ko, dialog, i18n, emailDAO) {
        var emails = ko.observable();
        var message = ko.observable();

        return {
            /********************BINDINGS*************************/
            emails: emails,
            message: message,
            canSubmit: ko.computed(function() {
                if (emails() && message())
                    return true;
                return false;
            }, this),
            ok: function() {
               var recipients = emails().split(',');
               var msg =  message().split(/\n/);
               emailDAO.sendInviteEmail(recipients, msg);

               dialog.close(this, true);

            },
            cancel: function(){
                dialog.close(this, false);
            },


            /********************LIFECYCLE CALLBACK******************/
            activate: function(username){
                this.emails('');
                this.message("Hi!,\nI'm using a great new app for keeping scores of my board game matches.\nIt's called bgBuddy, check it out at www.bgbuddy.com \n\n Cheers, " + username);
            }
        };
    }
);
