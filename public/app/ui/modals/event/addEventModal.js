define(['knockout', 'plugins/dialog'], function (ko, dialog) {
        var name = ko.observable();
        var location = ko.observable();
        var startDate = ko.observable();
        var endDate = ko.observable();

        return {
            /********************BINDINGS*************************/
            name: name,
            location: location,
            venue: ko.observable(),
            startDate: startDate,
            endDate: endDate,
            canSubmit: ko.computed(function() {
                if (name() && location() && startDate() && endDate())
                    return true;

                return false;
            }, this),
            ok: function(){
                var newEvent = {
                    name: name(),
                    location: location(),
                    venue: this.venue(),
                    startDate: startDate(),
                    endDate: endDate()
                }
                dialog.close(this, newEvent);
            },
            cancel: function(){
                dialog.close(this, null);
            },


            /********************LIFECYCLE CALLBACK******************/
            activate: function(){
                name('');
                location('');
                this.venue('');
                startDate(null);
                endDate(null);
            }
        };
    }
);
