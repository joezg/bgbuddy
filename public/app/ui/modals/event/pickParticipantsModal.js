define(['knockout', 'plugins/dialog', 'dao/eventDAO', 'underscore'], function (ko, dialog, eventDAO, _) {
        var moveItem = function(item, from, to){
            //find item in array
            var i = from.indexOf(item);

            //remove from array
            from.splice(i, 1);
            //add to new array
            to.push(item);
        }

        return {
            /********************BINDINGS*************************/
            participants: ko.observableArray(),
            selectedParticipants: ko.observableArray(),
            addAsTeam: ko.observable(false),
            teamName: ko.observable(''),
            selectParticipant: function(hub){
                moveItem(hub.data, this.participants, this.selectedParticipants);
            },
            removeParticipant: function(hub){
                moveItem(hub.data, this.selectedParticipants, this.participants);
            },
            ok: function(){
                if (this.addAsTeam()){
                    var participant = {
                        participantName: this.teamName(),
                        participants: this.selectedParticipants(),
                        isTeam: true
                    }
                    dialog.close(this, [participant]);
                } else {
                    dialog.close(this, this.selectedParticipants());
                }
            },
            cancel: function(){
                dialog.close(this, null);
            },
            /********************LIFECYCLE CALLBACK******************/
            activate: function(params){
                var self = this;
                this.participants([]);
                this.selectedParticipants([]);
                this.addAsTeam(false);
                this.teamName('');

                return eventDAO.getAllEventParticipants(params.eventOrganizerId, params.eventId).then(function(result){
                    var ids = _.indexBy(params.participants, "participantId");

                    result.participants = _.reject(result.participants, function(p){
                        return ids.hasOwnProperty(p.participantId);
                    })

                    self.participants(result.participants);
                });
            }
        };
    }
);
