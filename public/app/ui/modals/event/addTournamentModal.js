define(['knockout', 'plugins/dialog', 'i18next'], function (ko, dialog, i18n) {
        var name = ko.observable();
        var date = ko.observable();
        return {
            /********************BINDINGS*************************/
            name: name,
            date: date,
            type: ko.observable(),
            types: [
                { id: 0, name: i18n.t('event.normalTournament') },
                { id: 1, name: i18n.t('event.marathon') },
            ],
            canSubmit: ko.computed(function() {
                if (name() && date())
                    return true;

                return false;
            }, this),
            ok: function(){
                var newTournament = {
                    name: this.name(),
                    date: this.date(),
                    type: this.type().id
                }
                dialog.close(this, newTournament);
            },
            cancel: function(){
                dialog.close(this, null);
            },


            /********************LIFECYCLE CALLBACK******************/
            activate: function(){
                name('');
                date(null);
                this.type(0);

            }
        };
    }
);
