define(['knockout', 'plugins/dialog', 'dao/eventDAO', 'i18next'], function (ko, dialog, eventDAO, i18n) {
        return {
            /********************BINDINGS*************************/
            games: ko.observableArray(),
            additionalFieldValue: ko.observable(),
            selectGame: function(hub){
                var value = hub.data;

                if (this.additionalFieldVisible){
                    value = {
                        game: hub.data,
                        additionalFieldValue: this.additionalFieldValue
                    }
                }

                dialog.close(this, value);
            },
            cancel: function(){
                dialog.close(this, null);
            },
            /********************LIFECYCLE CALLBACK******************/
            activate: function(params){
                var self = this;
                this.games([]);
                var eventOrganizerId = params;

                if (_.isObject(params)){
                    eventOrganizerId = params.eventOrganizerId;
                    this.additionalFieldVisible = params.additionalFieldVisible;
                    this.additionalFieldTitleKey = params.additionalFieldTitleKey;
                    this.additionalFieldValue(null);
                    this.additionalFieldPlaceholder = i18n.t(params.additionalFieldPlaceholderKey);
                } else {
                    this.additionalFieldVisible = false;
                }

                return eventDAO.getLibrary(eventOrganizerId).then(function(result){
                    self.games(result.games);
                });
            }
        };
    }
);
