define(['knockout', 'plugins/dialog', 'dao/loginDAO', 'durandal/app', 'i18next', 'plugins/router', 'underscore', 'jquery'],
    function (ko, dialog, loginDAO, app, i18n, router, _) {
        return {
            /********************BINDINGS*************************/
            actionPending: ko.observable(false),
            rememberMe: ko.observable(false),
            username: ko.observable(),
            password: ko.observable(),
            signUp: function() {
                this.cancel('#signup');
            },
            facebookSignIn: function(response){
                console.dir(response);
                if (!this.actionPending() && response.status == 'connected'){
                    this.actionPending(true);
                    loginDAO.facebookLogin(response.authResponse.accessToken)
                        .then(_.bind(function(data){
                            app.setSession(data.sessionId, null, false, true);
                            if (data.unconnectedUser) {
                                this.cancel('#socialLoginConnect/fb');
                            } else {
                                dialog.close(this, null);
                            }
                        }, this))
                        .catch(_.bind(function(err){
                            this.showError(i18n.t("err.genericError"), err);
                            this.actionPending(false);
                        }, this));
                }
            },
            googleSignIn: function(authResult){
                var self = this;

                if (!self.actionPending() && authResult.status.google_logged_in && authResult.status.signed_in){
                    self.actionPending(true);
                    loginDAO.googleLogin(authResult.code)
                        .then(function(data){
                            app.setSession(data.sessionId, null, true, false);
                            if (data.unconnectedUser) {
                                self.cancel('#socialLoginConnect/gp');
                            } else {
                                dialog.close(self, null);
                            }
                        })
                        .catch(function(err){
                            self.showError(i18n.t("err.genericError"), err);
                            self.actionPending(false);
                        });
                }
            },
            login: function(){
                var self = this;
                self.actionPending(true);

                var val = null;
                if (!self.username()){
                    val = $("#username").val();
                    if (!val){
                        self.showError(i18n.t("login.usernameRequired"));
                        return;
                    } else
                        self.username(val);
                }
                if (!self.password()){
                    val = $("#password").val();
                    if (!val) {
                        self.showError(i18n.t("login.passwordRequired"));
                        return;
                    } else
                        self.password(val);
                }

                loginDAO.login(self.username(),self.password(), self.rememberMe())
                    .then(function(data){
                        app.setSession(data.sessionId, data.rememberMeToken);
                        app.trigger('login.successful', self.username());
                            app.trigger('msg.new', {text:i18n.t("login.loginSuccessful"), type:"success", timeout: 2000});

                        dialog.close(self, null);
                    })
                    .catch(function(err){
                        if (err.responseJSON.isInvalidCredentials) {
                            self.showError(i18n.t("login.invalidCredentials"));
                        } else if (err.responseJSON.isNotConfirmed){
                            self.showError(i18n.t("login.userNotConfirmed"));
                        } else {
                            self.showError(i18n.t("err.genericError"), err);
                        }
                    })


            },
            loginProblems: function(){
                this.cancel('#loginProblem');
            },

            cancel: function(newRoute) {
                if (router.isNavigating()){
                    var subscription = router.on('router:navigation:cancelled', function(){
                        router.navigate(newRoute);
                        subscription.off();
                    });
                    app.trigger("login.cancel");
                } else {
                    app.trigger("login.cancel"); //login should be cancelled and all promises dependent on login rejected
                    router.navigate(newRoute);

                }
            },

            /********************LIFECYCLE CALLBACKS**************/
            activate: function() {
                this.actionPending(false);
                this.rememberMe(false);
                this.username(null);
                this.password(null);
            },



            /********************UTILITY***************************/
            showError: function(msg, err){
                var msg = {text:msg, type:"danger"}
                if (err)
                    msg.error = err;
                else
                    msg.timeout = 3000;

                app.trigger('msg.new', msg);
                this.actionPending(false);
            }
        };
    }
);

