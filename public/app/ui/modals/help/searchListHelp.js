define(['knockout', 'plugins/dialog'], function (ko, dialog) {
        return {
            /********************BINDINGS*************************/
            showCode: ko.observable(false),
            showFields: ko.observable(false),
            close: function(){
                dialog.close(this, null);
            },

            /********************LIFECYCLE CALLBACK******************/
            activate: function(options){
                this.showCode(options.code);
                this.showFields(options.fields);
            }
        };
    }
);

