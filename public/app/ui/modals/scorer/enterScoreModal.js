//TODO translate
define(['knockout', 'plugins/dialog', 'underscore', 'jquery'], function (ko, dialog, _, $) {

        return {
            /********************BINDINGS*************************/
            players: null,
            unwrapScores: function(){
                var players = this.players;

                for (var i=0; i<players.length; i++){
                    var player = players[i];

                    player.result = ko.unwrap(player.result);
                    player.rank = ko.unwrap(player.rank);
                    player.winner = ko.unwrap(player.winner);
                }
            },
            reset: function(){
                for (var i=0; i<this.players.length; i++){
                    var player = this.players[i];

                    player.result(0);
                    player.rank(0);
                    player.winner(false);
                }
            },
            cancel: function(){
                this.unwrapScores();
                dialog.close(this, false);
            },
            done: function(){
                this.unwrapScores();
                dialog.close(this, true);
            },


            /********************LIFECYCLE CALLBACK******************/
            activate: function(data){
                this.players = data.players;

                for (var i=0; i<this.players.length; i++){
                    var p = this.players[i];

                    p.result = ko.observable(p.result || 0);
                    p.rank = ko.observable(p.rank || 0);
                    p.winner = ko.observable(p.winner || false);


                }
            },

            deactivate: function(){
                this.unwrapScores();
            }
        };
    }
);
