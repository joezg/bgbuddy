define(['knockout', 'plugins/dialog'],
    function (ko, dialog) {
        var firstPlayer = ko.observable();
        var choseRandom = ko.observable();
        return {
            /********************BINDINGS*************************/
            players: ko.observable(), // all players
            firstPlayer: firstPlayer, // first player - selected or randomized
            choseRandom: choseRandom,
            ok: function(){
                dialog.close(this, this.firstPlayer()); // close dialog, return first player (can be user or anonymous)this.firstPlayer()
            },
            cancel: function(){
                dialog.close(this, null);
            },
            selectPlayer: function(player) {
                firstPlayer(player);
                choseRandom(false);
            },
            randomize: function() {
                var r = Math.floor(Math.random() * this.players().length);
                this.firstPlayer(this.players()[r]);
                this.choseRandom(true);
            },
            /********************LIFECYCLE CALLBACKS*****************/
            activate: function(data) {
                var self = this;

                this.players(data.players);
                // first randomize
                self.randomize();

            }

        };
    }
);