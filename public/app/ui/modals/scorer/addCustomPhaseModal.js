define(['knockout', 'plugins/dialog'], function (ko, dialog) {
        var name = ko.observable();

        return {
            /********************BINDINGS*************************/
            name: name,
            canSubmit: ko.computed(function() {
                if (name())
                    return true;

                return false;
            }, this),
            ok: function(){
                dialog.close(this, this.name().trim());
            },
            cancel: function(){
                dialog.close(this, null);
            },

            forceCloseResult: null, //returned in promise when dialog is forced to close
            /********************LIFECYCLE CALLBACK******************/
            activate: function(){
                this.name('');
            }
        };
    }
);
