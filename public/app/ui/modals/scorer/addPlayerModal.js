define(['knockout', 'plugins/dialog', 'dao/buddyDAO', 'i18next', 'underscore', 'bootstrap-multiselect'], function (ko, dialog, buddyDAO, i18n, _) {
        return {
            /********************BINDINGS*************************/
            nameEntered: ko.observable(),
            namesPicked: ko.observable(),
            myselfAvailable: ko.observable(false),
            buddies: ko.observableArray(),
            selectAllLabel: i18n.t('scorer.selectAllOfThem'),
            noneSelectedLabel: i18n.t('common.noneSelected'),
            filterPlaceholderLabel: i18n.t('common.search'),
            duplicateNameFound: ko.observable(),
            duplicateNameMessage: ko.observable(),
            anonymousClicked: function() {
                dialog.close(this, [{isAnonymous: true}]);
            },
            myselfClicked: function() {
                dialog.close(this, [{isUser: true}]);
            },
            entered: function(){
                // enable entering multiple names  Issue #46
                var buds = this.nameEntered().split(",").map(function(name){
                    return {name: name.trim(), isNewBuddy: true};
                });
                dialog.close(this, buds);
            },
            picked: function(){
                var self = this;
                var buds = _.filter(this.buddies(), function(b){
                    return _.contains(self.namesPicked(), b._id);
                });

                buds = _.map(buds, function(b){
                    return {name: b.name, buddyId: b._id, isNewBuddy: false}
                })
                dialog.close(this, buds);
            },
            cancel: function(){
                dialog.close(this, null);
            },


            /********************LIFECYCLE CALLBACKS*****************/
            activate: function(data) {
                var self = this;

                var includedPlayers = data.players;

                var meIncluded = includedPlayers.some(function(player){
                    if (player.isUser)
                        return true;
                    return false;
                });

                this.duplicateNameFound(false);
                this.duplicateNameMessage("");

                this.myselfAvailable(!meIncluded);

                this.nameEntered('');
                this.nameEntered.subscribe(function(namesStr){
                    var names = namesStr.split(",");
                    for (var i=names.length-1; i>=0; i--){
                        var name = names[i].trim().toLowerCase();
                        if (_.some(this.buddies(), function(buddy){
                            return buddy.name.toLowerCase() === name;
                        })){
                            this.duplicateNameMessage(i18n.t('scorer.duplicateBuddyName', {name: names[i].trim()}))
                            this.duplicateNameFound(true);
                            return;
                        }
                    }
                    this.duplicateNameMessage("");
                    this.duplicateNameFound(false);

                }, this);

                this.namesPicked([]);

                return buddyDAO.getAllBuddies().then(function(res){
                    var bud = res.buddies.sort(function(a, b){
                        return (b.sort || 0) - (a.sort || 0);
                    });

                    bud = bud.filter(function(buddy){
                       var found = false;

                       includedPlayers.some(function(findBuddy){
                            if (findBuddy.name == buddy.name){
                                found = true;
                                return true;
                            }
                           return false;
                       });

                       return found !== true;
                    });

                    self.buddies(bud);
                });
            }
        };
    }
);