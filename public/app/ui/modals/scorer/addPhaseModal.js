define(['knockout', 'plugins/dialog', 'i18next', 'model/matchPhaseEnum'], function (ko, dialog, i18n, phaseEnum) {
        return {
            /********************BINDINGS*************************/
            phases: {},
            existingPhases: {},
            customPhaseName: ko.observable(),
            chosenPhase: ko.observable(),
            afterPhase: ko.observable(),
            isNext: ko.observable(),
            phasesCaption: i18n.t('scorer.choosePhaseType'),
            canSubmit: null,
            ok: function(){
                dialog.close(this, {
                    phase: this.chosenPhase(),
                    after: this.afterPhase(),
                    name: this.customPhaseName().trim()
                });
            },
            cancel: function(){
                dialog.close(this, null);
            },

            forceCloseResult: null, //returned in promise when dialog is forced to close
            /********************LIFECYCLE CALLBACK******************/
            activate: function(data){
                var self = this;
                this.chosenPhase(null);
                this.customPhaseName('');

                this.isNext(!!data.isNext);
                this.existingPhases = data.phases;

                if (this.isNext()){
                    this.phases = data.availablePhases;
                } else {
                    this.phases = phaseEnum.getAll();
                    this.existingPhases.unshift({
                        name: i18n.t('scorer.beginningOfGame'),
                        type: -1
                    });
                }

                this.canSubmit = ko.computed(function() {
                    if(self.chosenPhase() && (self.afterPhase() || self.isNext())){
                        if (self.chosenPhase().type !== phaseEnum.custom){
                            return true;
                        } else if(self.customPhaseName()){
                            return true;
                        }
                    }
                    return false;
                }, this)
            }
        };
    }
);
