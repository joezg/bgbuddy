define(['knockout', 'plugins/dialog', 'i18next', 'model/matchPhaseEnum', 'underscore'], function (ko, dialog, i18n, phaseEnum, _) {
        var currentPhase = {};
        var endTimeSubscrition = null;
        var startTimeSubscription = null;

        return {
            /********************BINDINGS*************************/
            newName: ko.observable(),
            toConfirm: ko.observable(false),
            itemSuggestions: ko.observableArray([]),
            itemId: ko.observable(null),
            cancel: function(){
                dialog.close(this, null);
            },
            rename: function(){
                if (this.itemSuggestions().length>0 && !this.itemId()){
                    var foundItem = _.find(this.itemSuggestions(), _.bind(function (item){
                        return this.newName().toLowerCase() == item.name.toLowerCase();
                    }, this));

                    if (foundItem){
                        this.itemId(foundItem._id);
                        this.newName(foundItem.name);
                    }
                }
                dialog.close(this, {action: "rename", newName: this.newName(), itemId: this.itemId()});
            },
            startDelete: function(){
              this.toConfirm(true);
            },
            confirmedDelete: function(){
                dialog.close(this, {action: "delete"});
            },

            forceCloseResult: null, //returned in promise when dialog is forced to close
            /********************LIFECYCLE CALLBACK******************/
            activate: function(options){
                this.newName("");
                this.toConfirm(false);
                this.itemId(null);
                this.itemSuggestions([]);

                if (options){
                    this.renameVisible = !!options.rename;
                    this.deleteVisible = !!options.delete;
                    this.itemSuggestions(options.suggestions || []);
                    this.renameLabel = i18n.t(options.renameKey || "common.rename");
                    this.renamePlaceholder = i18n.t(options.renamePlaceholderKey || "common.enterNewName");
                } else {
                    this.renameVisible = true;
                    this.deleteVisible = true;
                    this.itemSuggestions([]);
                    this.renameLabel = i18n.t("common.rename");
                    this.renamePlaceholder = i18n.t("common.enterNewName");
                }


            }
        };
    }
);
