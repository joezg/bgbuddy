define(['knockout', 'plugins/dialog', 'i18next', 'model/matchPhaseEnum'], function (ko, dialog, i18n, phaseEnum) {
        var currentPhase = {};
        var endTimeSubscrition = null;
        var startTimeSubscription = null;

        return {
            /********************BINDINGS*************************/
            phases: {},
            startDateTime: ko.observable(),
            endDateTime: ko.observable(),
            customPhaseName: ko.observable(),
            chosenPhase: ko.observable(),
            phasesCaption: i18n.t('scorer.choosePhaseType'),
            changeGameTime: ko.observable(),
            showChangeGameTime: ko.observable(),
            canSubmit: null,
            ok: function(){
                currentPhase.type = this.chosenPhase().type;
                currentPhase.name = currentPhase.type == phaseEnum.custom ? this.customPhaseName() : phaseEnum.meta[currentPhase.type].name;
                currentPhase.start = this.startDateTime();
                currentPhase.end = this.endDateTime();
                currentPhase.changeGameTime = this.changeGameTime();

                dialog.close(this, currentPhase);
            },
            cancel: function(){
                dialog.close(this, null);
            },

            forceCloseResult: null, //returned in promise when dialog is forced to close
            /********************LIFECYCLE CALLBACK******************/
            activate: function(data){
                var self = this;

                if (endTimeSubscrition)
                    endTimeSubscrition.dispose();

                if (startTimeSubscription)
                    startTimeSubscription.dispose();

                currentPhase = data;
                this.customPhaseName(data.name);
                this.phases = phaseEnum.getAll();
                this.chosenPhase(phaseEnum.meta[data.type]);
                this.startDateTime(new Date(data.start));
                this.endDateTime(new Date(data.end));
                this.changeGameTime(true);
                this.showChangeGameTime(false);

                endTimeSubscrition = this.endDateTime.subscribe(function(){
                    if (data.hasNext){
                        self.showChangeGameTime(true);
                    };
                });

                startTimeSubscription = this.startDateTime.subscribe(function(){
                    if (data.hasPrevious){
                        self.showChangeGameTime(true);
                    };
                });

                this.canSubmit = ko.computed(function() {
                    if(self.chosenPhase()){
                        if (self.chosenPhase().type !== phaseEnum.custom){
                            return true;
                        } else if(self.customPhaseName()){
                            return true;
                        }
                    }
                    return false;
                }, this)
            }
        };
    }
);
