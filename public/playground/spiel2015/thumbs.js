var games = [];

$(function() {
    Offline.options = {
        checks: {xhr: {url: '/api/checkConnection'}},
        reconnect: false
    };

    games = null;
    if (window.localStorage){
        games = JSON.parse(window.localStorage.getItem('games'));
        var title = window.localStorage.getItem('title');
        $('#header').text(title);
    }

    if (null === games){
        fetchFromBGG(function(data){
            showUI();

            games = formatBGGData(data.geeklist.geeklist);
            writeData(games);
        })
    } else {
        showUI();
        writeData(games);
    }

    $('#refresh').click(function(){
        if (Offline.state === 'up'){
            hideUI();
            fetchFromBGG(function(data){
                showUI();
                games = formatBGGData(data.geeklist.geeklist);

                var $table = $('#data');
                $table.empty();
                writeData(games);
            })
        } else {
            $('#offline_msg').append($('<div id="offline_msg" class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong> You cannot refresh the data while offline.</div>'));
        }
    });

    $('#filter')[0].onchange = function(){
        var $table = $('#data');
        $table.empty();
        writeData(games);
    };
});

var showUI = function(){
    $('#spinner').hide();
    $('#main').show();
}

var hideUI = function(){
    $('#spinner').show();
    $('#main').hide();
}

var formatBGGData = function(list){
    $('#header').text(list.title);
    window.localStorage.setItem('title', list.title);

    var games = _(list.item).map(function(item){
        var value = item.$;
        value.thumbs = parseInt(value.thumbs);

        return value;
    }).sortByOrder(['thumbs'], ['desc']).run();

    window.localStorage.setItem('games', JSON.stringify(games));
    return games;
}

var writeData = function(games){
    var $table = $('#data');
    var counter = 1;
    var filter = $('#filter')[0].selectedIndex;
    games.forEach(function(g){
        var $row = $('<tr/>');

        $row.append($('<td/>').text(counter++));

        var $a = $('<a/>', {
            href: 'https://bgg.cc/geeklist/193588/item/'+ g.id+'#item'+ g.id,
            target: '_blank'
        }).text(g.objectname);

        var $descriptionDiv = $('<div/>');
        $descriptionDiv.append($a);

        $descriptionDiv.append('<br>');
        var $button = $('<button>detalji</button>');
        $button.click(function(){
            fetchObjectFromBgg(g.objectid, function(result){
                console.log(result);
                if (result.object.boardgames.boardgame[0].image){
                    $descriptionDiv.append('<p><img width="200" src="' + result.object.boardgames.boardgame[0].image[0] + '"/></p>');
                }
                $descriptionDiv.append('<p/>').append('<strong>Designers:</strong>');
                result.object.boardgames.boardgame[0].boardgamedesigner.forEach(function(designer){
                    $descriptionDiv.append('<p/>').append(designer._);
                })
                $descriptionDiv.append('<p/>').append('<strong>Publishers:</strong>');
                result.object.boardgames.boardgame[0].boardgamepublisher.forEach(function(publisher){
                    $descriptionDiv.append('<p/>').append(publisher._);
                });

            });
        })
        $descriptionDiv.append($button);


        $row.append($('<td/>').append($descriptionDiv));
        $row.append($('<td/>').text(g.thumbs));

        var $checked = $('<select class="form-control"><option>Not interested</option><option>Must buy</option><option>Play before buy</option><option>Check</option></select>');

        var status = itemStatus(g.objectid)
        if (typeof status !== 'undefined'){
            $checked[0].selectedIndex = status;
        }

        $checked[0].onchange = function(){
            itemStatus(g.objectid, $checked[0].selectedIndex)
        };
        $row.append($('<td/>').append($checked));

        var $customData = $('<input type="text" class="form-control" placeholder="enter data">')
        var customData = itemData(g.objectid)
        if (typeof customData != 'undefined'){
            $customData[0].value = customData;
        }
        $customData[0].onchange = function(){
            itemData(g.objectid, $customData[0].value);
        };
        $row.append($('<td/>').append($customData));

        $table.append($row);
        if (filter !== 0 && filter !==4 && filter !== status){
            $row.hide();
        } else if (filter === 4 && !status){
            $row.hide();
        }
        else {
            $row.show();
        }
    });
}

var fetchFromBGG = function(doneCallback){
    $.ajax({
        dataType : "json",
        contentType: 'application/json',
        url: "/api/getGeeklistJS",
        data: {geeklistId: 193588},
        type: "GET",
        error: function(err){
            console.log(err);
        },
        success: doneCallback

    })
}

var fetchObjectFromBgg = function(objectId, doneCallback){
    $.ajax({
        dataType : "json",
        contentType: 'application/json',
        url: "/api/getObjectJS",
        data: {objectId: objectId},
        type: "GET",
        error: function(err){
            console.log(err);
        },
        success: doneCallback

    })
}


var itemStatus = function(itemId, newValue){
    var statuses = JSON.parse(window.localStorage.getItem('statuses'));
    if (typeof newValue === 'undefined'){
        if (statuses)
            return statuses[itemId];
        else
            return;
    } else {
        !statuses && (statuses={});
        statuses[itemId] = newValue;
        window.localStorage.setItem('statuses', JSON.stringify(statuses));
    }
}

var itemData = function(itemId, newValue){
    var statuses = JSON.parse(window.localStorage.getItem('customData'));
    if (typeof newValue === 'undefined'){
        if (statuses)
            return statuses[itemId];
        else
            return;
    } else {
        !statuses && (statuses={});
        statuses[itemId] = newValue;
        window.localStorage.setItem('customData', JSON.stringify(statuses));
    }
}