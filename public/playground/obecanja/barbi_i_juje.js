var Person = function(name){
    this.name = name;
    console.log("New baby is born. We'll call it " + name);
}

Person.prototype.receiveKiss = function(numberOfKisses){
    console.log(this.name + ' receives ' + numberOfKisses + ' kisses.');
}

Person.prototype.wakeUp = function(numberOfKisses){
    var self = this;
    var wakeUpDeferred = Q.defer();
    this.nudge(function(foot){
        var numberOfKissesToBeloved;

        try {
            if (foot === 'right')
                numberOfKissesToBeloved= 10;
            else
                throw new Error(self.name + " wakes on left foot and is not feeling good");
        }
        catch(err) {
            wakeUpDeferred.reject(err.message);
        }

        wakeUpDeferred.resolve(numberOfKissesToBeloved); //promise je fulfilled s vrijednošću numberOfKissesToBeloved
    });

    return wakeUpDeferred.promise;
}

Person.prototype.nudge = function(cb){
    var self = this;
    console.log(this.name + ' is nudged to wake up. We are waiting patiently...')
    setTimeout(function(){
        var foot = 'left';

        if (Math.random() > 0.4)
            foot = 'right';

        console.log('Finally, '+ self.name + ' wakes up on his '+ foot + ' foot');

        cb(foot);

    }, 5000);

}

Person.prototype.eatBreakfast = function(what){
    console.log(this.name + ' eats ' + what);
}

Person.prototype.makeSad = function(){
    console.log(this.name + ' is sad.');
}

function startStory() {
    var juje = new Person('Juje');self
    var barbi = new Person('Barbi');

    console.log('Many years later...');
    var kissBarbi = function(numberOfKisses) {
        barbi.receiveKiss(numberOfKisses);
    }

    var someProblems = function(err){
        console.log('Because ' + err + ', he will not kiss Barbi');
        barbi.makeSad();
    }

    var wakeUpPromise = juje.wakeUp();

    wakeUpPromise.then(kissBarbi, someProblems);
    wakeUpPromise.then(
        function(){
            juje.eatBreakfast('jaje na oko');
        },
        function(){
            console.log('Juje eats nothing.')
        }
    );
}

