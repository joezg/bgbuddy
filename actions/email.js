var _ = require('underscore');
var Email = require('../util/email');

exports.inviteEmailSend = {
    name: "inviteEmailSend",
    description: "Sends an invite email to provided address(es) in array",
    inputs: {
        required: ['emails', 'content'],
        optional: []
    },
    outputExample: {
        success: true
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    run: function(api, connection, next){
        var emails = connection.params.emails; // emails array
        var content = connection.params.content; // email content array
        if( typeof content === 'string' ) { // if not array, convert to array
            content = [ content ];
        }

        var email = new Email('bgBuddy invitation', 'invite4bgBuddy', {content: content, user: connection.currentUser});

        for(var i=0;i<emails.length;i++){
            email.addTo({
                email: emails[i],
                type: 'bcc'
            })
        }

        email.send(function(err) {
            if (!err) {
                connection.response.success = true;
            }
            else {
                connection.error = err;
            }
            next(connection, true);
        });

    }
}

exports.contactEmail = {
    name: "contactEmail",
    description: "Sends an email to contact@bgbuddy.com",
    inputs: {
        required: ['name', 'from', 'message'],
        optional: []
    },
    outputExample: {
        success: true
    },
    version: 1.0,
    verb: 'post',
    public: true,
    run: function(api, connection, next){
        var name = connection.params.name;
        var from = connection.params.from;
        var message = connection.params.message;

        var email = new Email('Message from ' + name, 'contact_email', {message: message, name: name});
        email.changeMessage({from_name: name, from_email: from})
        email.addTo({email: 'contact@bgbuddy.com'});

        email.send(function(err) {
            if (!err) {
                connection.response.success = true;
            }
            else {
                connection.error = err;
            }
            next(connection, true);
        });

    }
}