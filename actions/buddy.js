var _ = require('underscore');
var resolve = require('../util/promiseResolve.js');
var Q = require('q');

exports.buddyGetAll = {
    name: "buddyGetAll",
    description: "Get all buddies of logged user",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var promise = api.mongo.userBuddy.find({userId: connection.currentUser});

        resolve(promise, connection, next, function(response, buddies){
            response.buddies = buddies;
        });
    }
};

exports.buddyAdd = {
    name: "buddyAdd",
    description: "Adds buddy for logged user",
    inputs: {
        required: ['name'],
        optional: ['username', 'sort', 'like', 'comment']
    },
    outputExample: {
        success: true
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    run: function(api, connection, next){
        var newBuddy = _.pick(connection.params, 'name', 'username', 'sort', 'like', 'comment');
        newBuddy.userId = connection.currentUser;
        newBuddy = new api.mongo.userBuddy(newBuddy);

        resolve(newBuddy.save(), connection, next, function(response, buddy){
            response.buddy = buddy;
        });
    }
};

exports.buddyGet = {
    name: "buddyGet",
    description: "Get buddy of logged user",
    inputs: {
        required: ["buddyId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var buddyId = api.mongo.mongoose.Types.ObjectId(connection.params.buddyId);

        var promise = api.mongo.userBuddy.findOne({userId: connection.currentUser, _id: buddyId})

        resolve(promise, connection, next, function(response, buddy){
            response.buddy = buddy;
        });
    }
};
exports.buddyGetCount = {
    name: "buddyGetCount",
    description: "Get buddy count of logged user",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var promise = api.mongo.userBuddy.count({userId: connection.currentUser});

        resolve(promise, connection, next, function(response, count){
            response.count = count;
        });
    }
};

exports.buddyLikeUpdate = {
    name: "buddyLikeUpdate",
    description: "Update buddy like flag",
    inputs: {
        required: ["buddyId", "like"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    run: function(api, connection, next){
        var buddyId = api.mongo.mongoose.Types.ObjectId(connection.params.buddyId);
        var like = connection.params.like;

        var promise = api.mongo.userBuddy.findOne({userId: connection.currentUser, _id: buddyId})
            .then(function(buddy){
                if (!buddy)
                    throw new Error("Buddy not found, buddy id: " + buddyId);

                buddy.like = like;

                return buddy.save();
            });

        resolve(promise, connection, next, function(response, buddy){
            response.buddy = buddy;
        });
    }
};

exports.buddyUpdate = {
    name: "buddyUpdate",
    description: "Update buddy data (username & comment)",
    inputs: {
        required: ["buddy"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    run: function(api, connection, next){
        var newBuddyData = connection.params.buddy;
        var buddyId = api.mongo.mongoose.Types.ObjectId(newBuddyData._id);

        var promise = api.mongo.userBuddy.findOne({userId: connection.currentUser, _id: buddyId})
            .then(function(buddy){
                if (!buddy)
                    throw new Error("Buddy not found, buddy id: " + buddyId);

                buddy = _.extend(buddy, newBuddyData);

                return buddy.save();
            });

        resolve(promise, connection, next, function(response, buddy){
            response.buddy = buddy;
        });
    }
};

exports.buddyRename = {
    name: "buddyRename",
    description: "Renames buddy",
    inputs: {
        required: ["buddyId", "newName"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    run: function(api, connection, next){
        var buddyId = api.mongo.mongoose.Types.ObjectId(connection.params.buddyId);

        var promise = api.mongo.userBuddy.findOne({userId: connection.currentUser, _id: buddyId})
            .then(function(buddy){
                if (!buddy)
                    throw new Error("Buddy with given Id doesn't exists");

                buddy.name = connection.params.newName;

                var renameBuddiesInMatchesPromise = api.mongo.userMatch.update(
                    {
                        userId: connection.currentUser,
                        'players.buddyId': api.mongo.mongoose.Types.ObjectId(connection.params.buddyId)
                    },
                    {
                        $set: {
                            'players.$.name': connection.params.newName
                        }
                    },
                    {
                        multi: 1
                    }
                );

                return Q.all([buddy.save(), renameBuddiesInMatchesPromise]);
            });

        resolve(promise, connection, next, function(response, data){
            response.buddy = data[0];
        });
    }
};

exports.buddyDelete = {
    name: "buddyDelete",
    description: "Deletes buddy",
    inputs: {
        required: ["buddyId"],
        optional: ["force"]
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'delete',
    role: 'user',
    run: function(api, connection, next){
        var force = connection.params.force || false;
        var buddyId = api.mongo.mongoose.Types.ObjectId(connection.params.buddyId);

        var promise = api.mongo.userMatch.find({userId: connection.currentUser, 'players.buddyId': buddyId})
            .then(function(matches){
                if (matches.length > 0 && !force){
                    return { cannotDelete: true }
                } else {
                    var setBuddyToAnonymousInMatches = api.mongo.userMatch.update(
                        {
                            userId: connection.currentUser,
                            'players.buddyId': buddyId
                        },
                        {
                            $set: {
                                'players.$.isAnonymous': true
                            },
                            $unset: {
                                'players.$.name': "",
                                'players.$.buddyId': "",
                                'players.$.isNewBuddy': "",
                                'players.$.isBuddy': ""
                            }
                        },
                        {
                            multi: 1
                        }
                    );

                    var removeBuddyPromise = api.mongo.userBuddy.remove({userId: connection.currentUser, _id: buddyId});

                    return Q.all([removeBuddyPromise, setBuddyToAnonymousInMatches]);
                }
            })

        resolve(promise, connection, next, function(response, result){
            if (result.cannotDelete){
                response.cannotDelete = true;
            }
        });
    }
};