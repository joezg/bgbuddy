var Step = require('step');

exports.googleSignIn = {
    name: "googleSignIn",
    description: "Receives one-time token for google authentication",
    inputs: {
        required: ['token'],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    public: true,
    run: function(api, connection, next){
        var ActionErrorHandler = require('../util/actionErrorHandler'),
            handler = new ActionErrorHandler(connection, next);

        var CLIENT_ID = "723477775783-gq41v9q2h00n57v3d52cnuptk3nonk27.apps.googleusercontent.com",
            CLIENT_SECRET = "LKNtvz1LGP6EBaobUENn8Ujp",
            REDIRECT_URI = "postmessage";

        var google = require('googleapis'),
            OAuth2 = google.auth.OAuth2,
            oauth2 = google.oauth2('v2');

        var client = new OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URI);

        var accessToken, userId;

        Step(
            function getTokens(){
                client.getToken(connection.params.token, this);
            },
            function checkTokenAndGetUserId(err, tokens) {
                if (handler.checkError(err)) {
                    accessToken = tokens.access_token;
                    var params = {
                        access_token: accessToken
                    }
                    oauth2.tokeninfo(params, this);
                }
            },
            function findUserById(err, tokenInfo) {
                if (handler.checkError(err)){
                    userId = tokenInfo.user_id;
                    api.mongo.userGoogle.findById(userId, this);
                }
            },
            function checkUserAndStartSession(err, userGoogle) {
                if (handler.checkError(err)) {
                    if (!userGoogle || !userGoogle.username) {
                        api.mongo.session.initUnconnectedGoogleSession(userId, this.parallel());

                        //will be overwritten if already exists
                        if (!userGoogle)
                            userGoogle = new api.mongo.userGoogle();

                        userGoogle._id = userId;
                        userGoogle.accessToken = accessToken;
                        userGoogle.save(this.parallel());
                    } else {
                        api.mongo.session.logIn(userGoogle.username, function(err, sessionId){
                            if (handler.checkError(err)){
                                connection.response.sessionId = sessionId;
                                next(connection, true);
                            }
                        });
                    }
                }
            },
            function sendUnconnectedUserMessage(err, sessionId){
                if (handler.checkError(err)) {
                    connection.response.unconnectedUser = true;
                    connection.response.sessionId = sessionId;
                    next(connection, true);
                }
            }
        );
    }
};

exports.googleNewUserConnect = {
    name: "googleNewUserConnect",
    description: "Creates a new bgbuddy user and connects it to google user",
    inputs: {
        required: ['username'],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    public: true,
    run: function(api, connection, next){
        var ActionErrorHandler = require('../util/actionErrorHandler'),
            handler = new ActionErrorHandler(connection, next);

        var google = require('googleapis'),
            plus = google.plus('v1');

        var userGoogle = null;

        if (!connection.session){
            connection.error = "Session expired.";
            next(connection, true);
            return;
        }

        Step(
            function getUserGoogle() {
                var userId = connection.googleUserId;
                api.mongo.userGoogle.findById(userId, this);
            },
            function getUserDataFromGoogle(err, uG) {
                if (handler.checkError(err)){
                    userGoogle = uG;
                    if (null == userGoogle){
                        connection.error = "Google userid doesn't exists";
                        next(connection, true);
                    } else {
                        var params = {
                            access_token: userGoogle.accessToken,
                            userId: userGoogle._id
                        }
                        plus.people.get(params, this);
                    }
                }
            },
            function saveUserAndUpdateGoogleUserAndSession(err, userData) {
                if (handler.checkError(err)) {
                    var user = new api.mongo.user();
                    user.username = connection.params.username;
                    user.email = userData.emails[0].value;
                    user.isConfirmed = true;
                    user.sex = userData.gender == "female" ? "f" : "m";
                    user.userWithoutPassword = true;
                    user.save(this.parallel());

                    //update userGoogle
                    userGoogle.username = user._id;
                    userGoogle.save(this.parallel());

                    //find session
                    api.mongo.session.update({_id: connection.sessionId}, { isUnconnected: false, username: user._id}, this.parallel())
                }
            },
            function result(err, user, uG, session){
                if (handler.checkError(err)) {
                    connection.response.success = true;
                    next(connection, true);
                }
            }
        );
    }
};