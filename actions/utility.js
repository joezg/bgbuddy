exports.checkConnection = {
    name: "checkConnection",
    description: "This methods returns success: true",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
        success: true
    },
    version: 1.0,
    verb: 'head',
    public: true,
    run: function(api, connection, next){
        connection.response.success = true;
        next(connection, true);
    }
};