var Q = require('q');
var _ = require('underscore');
var resolve = require('../util/promiseResolve.js');

exports.eventTournamentGetAll = {
    name: "eventTournamentGetAll",
    description: "Get tournaments of an event",
    inputs: {
        required: ["eventId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){

        var promise = api.mongo.eventTournament.find({
            eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId)
        });

        resolve(promise, connection, next, function(response, tournaments){
            response.tournaments = tournaments;
        });
    }
};

exports.eventTournamentGet = {
    name: "eventTournamentGet",
    description: "Get tournament of an event",
    inputs: {
        required: ["eventId", "tournamentId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){

        var promise = api.mongo.eventTournament.findOne({
            eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
            _id: api.mongo.mongoose.Types.ObjectId(connection.params.tournamentId)
        });

        resolve(promise, connection, next, function(response, tournament){
            response.tournament = tournament
        });
    }
};

exports.eventTournamentAdd = {
    name: "eventTournamentAdd",
    description: "Adds tournament to event",
    inputs: {
        required: ["eventId", "name", "date", "eventOrganizerId", "type"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var newTournament = {
            eventId: connection.params.eventId,
            eventOrganizerId: connection.params.eventOrganizerId,
            name: connection.params.name,
            date: connection.params.date
        }

        var promise = null;
        if (connection.params.type === 0) { //normal tournament
            promise = api.mongo.eventTournament.create(newTournament);
        } else { //marathon
            var marathon = new api.mongo.eventMarathon(newTournament);
            promise = marathon.save();
        }


        resolve(promise, connection, next, function(response, tournament){
            response.tournament = tournament
        });
    }
};

exports.eventTournamentUpdate = {
    name: "eventTournamentUpdate",
    description: "Updates tournament of an event",
    inputs: {
        required: ["eventId", "tournamentId", "eventOrganizerId", "type"],
        optional: ["name", "date", "gameId", 'gameName']
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var newTournament = _.pick(connection.params, 'name', 'date', 'gameId', 'gameName');

        var model = connection.params.type === 0 ? api.mongo.eventTournament : api.mongo.eventMarathon;

        var promise = model.findOne({
                eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
                eventOrganizerId: api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId),
                _id: api.mongo.mongoose.Types.ObjectId(connection.params.tournamentId)
            })
            .then(function(tournament){
                if (tournament == null){
                    throw new Error("There is no tournament with id: " + connection.params.tournamentId);
                } else {
                    _.extend(tournament, newTournament);
                    return tournament.save();
                }
            });

        resolve(promise, connection, next);

    }
};

exports.eventTournamentParticipantAdd = {
    name: "eventTournamentParticipantAdd",
    description: "Adds participant to tournament of an event",
    inputs: {
        required: ["eventId", "tournamentId", "participant", "eventOrganizerId", "type"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var newParticipant = {
            name: connection.params.participant.participantName
        };
        if (!connection.params.participant.isTeam){
            newParticipant.participantId = connection.params.participant.participantId;
        } else {
            newParticipant.participantsId = [];
            connection.params.participant.participants.forEach(function(p){
                newParticipant.participantsId.push(p.participantId);
            })
        }

        var model = connection.params.type === 0 ? api.mongo.eventTournament : api.mongo.eventMarathon;

        var promise = model.findOne({
            eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
            eventOrganizerId: api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId),
            _id: api.mongo.mongoose.Types.ObjectId(connection.params.tournamentId)
        })
            .then(function(tournament){
                if (tournament == null){
                    throw new Error("There is no tournament with id: " + connection.params.tournamentId);
                } else {
                    if (tournament.isStarted)
                        throw new Error("Cannot add participant to the ongoing tournament.")

                    tournament.participants.push(newParticipant);
                    return tournament.save();
                }
            });

        resolve(promise, connection, next, function(response, tournament){
            response.participants = tournament.participants;
        });
    }
};

exports.eventTournamentParticipantRemove = {
    name: "eventTournamentParticipantRemove",
    description: "Removes participant from tournament of an event",
    inputs: {
        required: ["eventId", "tournamentId", "participantId", "eventOrganizerId", "type"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var model = connection.params.type === 0 ? api.mongo.eventTournament : api.mongo.eventMarathon;

        var promise = model.findOne({
            eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
            eventOrganizerId: api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId),
            _id: api.mongo.mongoose.Types.ObjectId(connection.params.tournamentId)
        })
            .then(function(tournament){
                if (tournament == null){
                    throw new Error("There is no tournament with id: " + connection.params.tournamentId);
                } else {
                    if (tournament.isStarted)
                        throw new Error("Cannot remove participant from the ongoing tournament.")

                    tournament.participants.remove(connection.params.participantId);
                    return tournament.save();
                }
            });

        resolve(promise, connection, next);

    }
};

exports.eventTournamentRoundGetAll = {
    name: "eventTournamentRoundGetAll",
    description: "Gets all rounds of a tournament of an event",
    inputs: {
        required: ["eventId", "tournamentId", "eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var promise = api.mongo.eventTournament.findOne({
            eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
            eventOrganizerId: api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId),
            _id: api.mongo.mongoose.Types.ObjectId(connection.params.tournamentId)
        })
            .then(function(tournament){
                if (tournament == null){
                    throw new Error("There is no tournament with id: " + connection.params.tournamentId);
                } else {
                    return tournament;
                }
            });

        resolve(promise, connection, next, function(response, tournament){
            response.rounds = tournament.rounds;
        });
    }
};

exports.eventTournamentRoundAdd = {
    name: "eventTournamentRoundAdd",
    description: "Adds new round to the tournament of an event",
    inputs: {
        required: ["eventId", "tournamentId", "eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var promise = api.mongo.eventTournament.findOne({
            eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
            eventOrganizerId: api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId),
            _id: api.mongo.mongoose.Types.ObjectId(connection.params.tournamentId)
        })
            .then(function(tournament){
                if (tournament == null){
                    throw new Error("There is no tournament with id: " + connection.params.tournamentId);
                } else {
                    if (tournament.isFinished)
                        throw new Error("Cannot add new round to the finished tournament.")

                    var roundId = tournament.rounds.length + 1;
                    tournament.rounds.push({ _id: roundId });
                    return tournament.save();
                }
            });

        resolve(promise, connection, next, function(response, tournament){
            response.rounds = tournament.rounds;
        });
    }
};

exports.eventTournamentRoundPropositionUpdate = {
    name: "eventTournamentRoundPropositionUpdate",
    description: "Updates round proposition of a tournament of an event",
    inputs: {
        required: ["eventId", "tournamentId", "roundId", "eventOrganizerId"],
        optional: ["tableReposition", "seating", "cutoff", "cutoffType", "carryoverType", "carryover",
            "primaryRankingType", "primaryRankingHigherIsBest", "secondaryRankingType",
            "secondaryRankingHigherIsBest", "multipleMatches", "rankPointsDistribution",
            "numberOfTables", "largerPlayerCountOnLowerTables", "rankPointsDistribution"]
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var newPropositions = _.pick(connection.params, this.inputs.optional);

        var promise = api.mongo.eventTournament.findOne({
            eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
            eventOrganizerId: api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId),
            _id: api.mongo.mongoose.Types.ObjectId(connection.params.tournamentId)
        })
            .then(function(tournament){
                if (tournament == null){
                    throw new Error("There is no tournament with id: " + connection.params.tournamentId);
                }

                var round = tournament.rounds.id(connection.params.roundId);

                if (null == round){
                    throw new Exception("Round " + connection.params.roundId + "doesn't exists in this tournament.")
                }

                _.extend(round.propositions, newPropositions);
                return tournament.save();
            });

        resolve(promise, connection, next);

    }
};

exports.eventTournamentRoundGenerateMatches = {
    name: "eventTournamentRoundGenerateMatches",
    description: "Generates matches for round of a tournament of an event",
    inputs: {
        required: ["eventId", "tournamentId", "roundId", "eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var promise = api.mongo.eventTournament.findOne({
            eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
            eventOrganizerId: api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId),
            _id: api.mongo.mongoose.Types.ObjectId(connection.params.tournamentId)
        })
            .then(function(tournament){
                if (tournament == null){
                    throw new Error("There is no tournament with id: " + connection.params.tournamentId);
                }

                tournament.generateMatches(connection.params.roundId);

                return tournament.save();
            });

        resolve(promise, connection, next, function(response, tournament){
            response.matches = tournament.rounds.id(connection.params.roundId).matches;
        });
    }
};

exports.eventTournamentRoundDeleteMatches = {
    name: "eventTournamentRoundDeleteMatches",
    description: "Deletes matches for round of a tournament of an event",
    inputs: {
        required: ["eventId", "tournamentId", "roundId", "eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var promise = api.mongo.eventTournament.findOne({
            eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
            eventOrganizerId: api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId),
            _id: api.mongo.mongoose.Types.ObjectId(connection.params.tournamentId)
        })
            .then(function(tournament){
                if (tournament == null){
                    throw new Error("There is no tournament with id: " + connection.params.tournamentId);
                }

                var round = tournament.rounds.id(connection.params.roundId);
                round.matches = [];
                round.ranking = [];

                return tournament.save();
            });

        resolve(promise, connection, next);
    }
};

exports.eventTournamentRoundAddScore = {
    name: "eventTournamentRoundAddScore",
    description: "Adds score for a match of a round of a tournament of an event",
    inputs: {
        required: ["eventId", "tournamentId", "roundId", "match", "eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var promise = api.mongo.eventTournament.findOne({
            eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
            eventOrganizerId: api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId),
            _id: api.mongo.mongoose.Types.ObjectId(connection.params.tournamentId)
        })
            .then(function(tournament){
                if (tournament == null){
                    throw new Error("There is no tournament with id: " + connection.params.tournamentId);
                }

                tournament.addMatchScore(connection.params.roundId, connection.params.match);

                return tournament.save();
            });

        resolve(promise, connection, next, function(response, tournament){
            var round = tournament.rounds.id(connection.params.roundId);

            if (round.propositions.multipleMatches.isMultiple){
                response.matches = round.matches;
            } else {
                response.match = round.matches.id(connection.params.match._id);
            }
            response.ranking = round.ranking;
        });
    }
};

exports.eventTournamentRearrangeTables = {
    name: "eventTournamentRearrangeTables",
    description: "Rearranges players on tables in a given round for a given tournament",
    inputs: {
        required: ["eventId", "tournamentId", "roundId", "matches", "eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var promise = api.mongo.eventTournament.findOne({
            eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
            eventOrganizerId: api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId),
            _id: api.mongo.mongoose.Types.ObjectId(connection.params.tournamentId)
        })
            .then(function(tournament){
                if (tournament == null){
                    throw new Error("There is no tournament with id: " + connection.params.tournamentId);
                }

                tournament.rearrangeTables(connection.params.roundId, connection.params.matches);

                return tournament.save();
            });

        resolve(promise, connection, next);
    }
};

exports.eventTournamentRoundFinish = {
    name: "eventTournamentRoundFinish",
    description: "Adds score for a match of a round of a tournament of an event",
    inputs: {
        required: ["eventId", "tournamentId", "roundId", "eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var promise = api.mongo.eventTournament.findOne({
            eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
            eventOrganizerId: api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId),
            _id: api.mongo.mongoose.Types.ObjectId(connection.params.tournamentId)
        })
            .then(function(tournament){
                if (tournament == null){
                    throw new Error("There is no tournament with id: " + connection.params.tournamentId);
                }

                tournament.endRound(connection.params.roundId);

                return tournament.save();
            });

        resolve(promise, connection, next, function(response, tournament){
            response.tournamentParticipants = tournament.participants;
            response.roundRankings = tournament.rounds.id(connection.params.roundId).ranking;
        });
    }
};

exports.eventMarathonGameAdd = {
    name: "eventMarathonGameAdd",
    description: "Adds a game with specific weight to a marathon of an event",
    inputs: {
        required: ["eventId", "marathonId", "gameId", "gameName", "weight", "eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var promise = api.mongo.eventMarathon.findOne({
            eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
            eventOrganizerId: api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId),
            _id: api.mongo.mongoose.Types.ObjectId(connection.params.marathonId)
        })
            .then(function(marathon){
                if (marathon == null){
                    throw new Error("There is no marathon with id: " + connection.params.marathonId);
                }

                marathon.games.push({
                    gameId: connection.params.gameId,
                    name: connection.params.gameName,
                    weight: connection.params.weight
                })

                return marathon.save();
            });

        resolve(promise, connection, next, function(response, marathon){
            response.games = marathon.games
        });
    }
};

exports.eventMarathonGameRemove = {
    name: "eventMarathonGameRemove",
    description: "Removes a game with given id from a marathon of an event",
    inputs: {
        required: ["eventId", "marathonId", "_id", "eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'delete',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var promise = api.mongo.eventMarathon.findOne({
            eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
            eventOrganizerId: api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId),
            _id: api.mongo.mongoose.Types.ObjectId(connection.params.marathonId)
        })
            .then(function(marathon){
                if (marathon == null){
                    throw new Error("There is no marathon with id: " + connection.params.marathonId);
                }

                marathon.games.id(connection.params._id).remove();

                return marathon.save();
            });

        resolve(promise, connection, next, function(response, marathon){
            response.games = marathon.games
        });
    }
};

exports.eventMarathonCalculateStandings = {
    name: "eventMarathonCalculateStandings",
    description: "Calculate a standings for a marathon",
    inputs: {
        required: ["eventId", "marathonId", "eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var promise = api.mongo.eventMarathon.findOne({
            eventId: api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
            eventOrganizerId: api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId),
            _id: api.mongo.mongoose.Types.ObjectId(connection.params.marathonId)
        })
            .then(function(marathon){
                if (marathon == null){
                    throw new Error("There is no marathon with id: " + connection.params.marathonId);
                }

                return marathon.calculateStandings().then(function(){
                    return marathon.save();
                });
            });

        resolve(promise, connection, next, function(response, marathon){
            response.participants = marathon.participants
        });
    }
};



