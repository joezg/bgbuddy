var Step = require('step');
var _ = require('underscore');

exports.userAdd = {
    name: "userAdd",
    description: "Adding user (user registration)",
    inputs: {
        required: ["username", "password", "email"],
        optional: ["sex"]
    },
    outputExample: {
        success: true
    },
    version: 1.0,
    verb: 'post',
    public: true,
    run: function(api, connection, next){
        var user = {
            username: connection.params.username,
            password: connection.params.password,
            email: connection.params.email,
            sex: connection.params.sex
        };
        var User = api.mongo.user;

        var user = new User(user);
        user.save(function (err){
            connection.error = err;

            if (!err)
                connection.response.success = true;

            next(connection, true);
        });
    }
};

exports.resendConfirmationCode = {
    name: "resendConfirmationCode",
    description: "Resends email confirmation code",
    inputs: {
        required: ["username", "password"],
        optional: []
    },
    outputExample: {
        success: true
    },
    version: 1.0,
    verb: 'post',
    public: true,
    run: function(api, connection, next){
        var auth = {
            username:connection.params.username.toLowerCase(),
            password:connection.params.password
        }

        var currentUser = null;

        var invalidCredentialsMessage = "Invalid username/password combination";

        Step(
            function getUser(){
                api.mongo.user.findById(auth.username, this);
            },
            function checkUser(err, user){
                if(err){
                    connection.error = err;
                    next(connection, true);
                }else if(user == null){
                    connection.error = invalidCredentialsMessage;
                    next(connection, true);
                }else if (user.isConfirmed){
                    connection.error = "Your user already confirmed. You can log in to bgbuddy.";
                    next(connection, true);
                }else{
                    currentUser = user;
                    user.authenticate(auth.password, this)
                }
            },
            function authenticationResult(err, success){
                if (err){
                    connection.error = err;
                    next(connection, true);
                    return;
                }

                if (!success){
                    connection.error = invalidCredentialsMessage;
                    next(connection, true);
                } else {
                    var UserConfirmation = api.mongo.userConfirmation;
                    var uc = new UserConfirmation({ user: currentUser });
                    uc.save(this);
                }
            }, function sessionSaveResult(err, sessionId, userRM){
                if (err){
                    connection.error = err;
                    next(connection, true);
                } else {
                    connection.response.success = true
                    next(connection, true);
                }
            }
        )
    }
};

exports.userPasswordRecoveryRequest = {
    name: "userPasswordRecoveryRequest",
    description: "Action generates password recovery code and sends it to user's email",
    inputs: {
        required: ["username"],
        optional: []
    },
    outputExample: {
        success: true
    },
    version: 1.0,
    verb: 'post',
    public: true,
    run: function(api, connection, next){
        var username = connection.params.username.trim().toLowerCase();

        Step(
            function fetchUser(){
                api.mongo.user.findById(username, this);
            },
            function savePasswordRecoveryCode(err, user){
                if (err){
                    connection.error = err;
                    next(connection, true);
                } else if (null == user) {
                    connection.error = "Username doesn't exist";
                    next(connection, true);
                } else {
                    var userPasswordRecovery = new api.mongo.userPasswordRecovery();
                    userPasswordRecovery.user = user;
                    userPasswordRecovery.save(this);
                }
            },
            function checkResult(err){
                if (!err)
                    connection.response.success = true;

                connection.error = err;
                next(connection, true);
            }
        );

    }
};

exports.confirmEmail = {
    name: "confirmEmail",
    description: "Confirming user email address",
    inputs: {
        required: ["code"],
        optional: []
    },
    outputExample: {
        success: true
    },
    version: 1.0,
    verb: 'post',
    public: true,
    run: function(api, connection, next){
        var confirm = {
            code: connection.params.code,
            password: connection.params.password
        };

        var failMessage = "Invalid confirmation code.";
        var UserConfirmation = api.mongo.userConfirmation;
        var User = api.mongo.user;

        var userConfirmation = null;

        Step(
            function findConfirmation(){
                UserConfirmation.findById(confirm.code, this);
            },
            function findUser(err, confirmation) {
                if (err){
                    connection.error = err;
                    next(connection, true);
                } else if (null == confirmation) {
                    connection.error = failMessage;
                    connection.response.invalidCode = true;
                    next(connection, true);
                } else {
                    userConfirmation = confirmation;
                    User.findById(confirmation.userId, this);
                }
            },
            function updateUser(err, user){
                if (err){
                    connection.error = err;
                    next(connection, true);
                } else if (null == user) {
                    connection.error = failMessage;
                    connection.response.invalidCode = true;
                    next(connection, true);
                } else {
                    user.isConfirmed = true;
                    userConfirmation.remove(); //don't care if error occured because document will be removed
                    user.save(this);
                }
            },
            function saveComplete (err){
                if (!err)
                    connection.response.success = true;

                connection.error = err;
                next(connection, true);
            }
        )
    }
};

exports.userChangePasswordWithCode = {
    name: "userChangePasswordWithCode",
    description: "Changes user's password wit password recovery code",
    inputs: {
        required: ["code", "password"],
        optional: []
    },
    outputExample: {
        success: true
    },
    version: 1.0,
    verb: 'put',
    public: true,
    run: function(api, connection, next){
        var recovery = {
            code: connection.params.code,
            password: connection.params.password
        };

        var userPasswordRecovery = null;

        Step(
            function findPasswordRecovery(){
                api.mongo.userPasswordRecovery.findById(recovery.code, this);
            },
            function findUser(err, userPR) {
                if (err){
                    connection.error = err;
                    next(connection, true);
                } else if (null == userPR) {
                    connection.error = 'could not find recovery code';
                    next(connection, true);
                } else {
                    userPasswordRecovery = userPR;
                    api.mongo.user.findById(userPR.userId, this);
                }
            },
            function authenticate(err, user){
                if (err){
                    connection.error = err;
                    next(connection, true);
                } else if (null == user) {
                    connection.error = 'could not find user associated with given code';
                    next(connection, true);
                } else {
                    user.password = recovery.password; //this is virtual setter so password will be hashed
                    user.save(this);
                }
            },
            function saveComplete (err){
                if (!err) {
                    connection.response.success = true;
                    userPasswordRecovery.remove(); //don't care if error occurred because document will be removed
                }

                connection.error = err;
                next(connection, true);
            }
        )
    }
};

exports.logIn = {
    name: "logIn",
    description: "User log in",
    inputs: {
        "required" : ["username", "password"],
        "optional" : ["rememberMe", "socialConnect", "socialType"]
    },
    blockedConnectionTypes: [],
    outputExample: {
        auth: true,
        sessionId: '[session_id]'
    },
    verb: 'post',
    public: true,
    run: function(api, connection, next){
        var self = this;
        if(connection.currentUser){
            connection.error = "Multiple logins not allowed.";
            next(connection, true);
        } else {
            connection.error = null;
            connection.response.auth = false;

            var auth = {
                username:connection.params.username.toLowerCase(),
                password:connection.params.password
            }

            var invalidCredentialsMessage = "Invalid username/password combination";

            Step(
                function getUser(){
                    api.mongo.user.findById(auth.username, this);
                },
                function checkUser(err, user){
                    if(err){
                        connection.error = err;
                        next(connection, true);
                    }else if(user == null){
                        connection.error = invalidCredentialsMessage;
                        connection.response.isInvalidCredentials = true;
                        next(connection, true);
                    }else if (!user.isConfirmed){
                        connection.error = "Your user is not confirmed. Please check your inbox for confirmation email.";
                        connection.response.isNotConfirmed = true;
                        next(connection, true);
                    }else{
                        user.authenticate(auth.password, this)
                    }
                },
                function authenticationResult(err, success){
                    if (err){
                        connection.error = err;
                        next(connection, true);
                        return;
                    }

                    if (!success){
                        connection.error = invalidCredentialsMessage;
                        connection.response.isInvalidCredentials = true;
                        next(connection, true);
                    } else {
                        if (connection.params.socialConnect){
                            self.connectSocialAccount(api, connection, auth.username, connection.params.socialType, next);
                        } else {
                            api.mongo.session.logIn(auth.username, this.parallel());
                            if(connection.params.rememberMe){
                                var userRM = new api.mongo.userRememberMe();
                                userRM.userId = auth.username;
                                userRM.save(this.parallel());
                            }
                        }
                    }
                }, function sessionSaveResult(err, sessionId, userRM){
                    if (err){
                        connection.error = err;
                        next(connection, true);
                    } else {
                        connection.response.auth = true;
                        connection.response.sessionId = sessionId;
                        if (userRM)
                            connection.response.rememberMeToken = userRM._id;

                        next(connection, true);
                    }
                }
            )
        }
    },
    connectSocialAccount: function(api, connection, username, socialType, next){
        var ActionErrorHandler = require('../util/actionErrorHandler'),
            handler = new ActionErrorHandler(connection, next);
        Step(
            function getSession(){
                api.mongo.session.findById(connection.sessionId, this);
            },
            function updateSession(err, session){
                if (handler.checkError(err)){
                    session.isUnconnected = false;
                    session.username = username;
                    session.save(this);
                }
            },
            function get(err){
                if (handler.checkError(err)){
                    if (socialType=='fb')
                        api.mongo.userFacebook.findById(connection.facebookUserId, this);
                    else if (socialType=='gp')
                        api.mongo.userGoogle.findById(connection.googleUserId, this);
                    else {
                        connection.error = "Wrong social network type";
                        next(connection, true)
                    }
                }
            },
            function update(err, social){
                if (handler.checkError(err)){
                    social.username = username;
                    social.save(this);
                }
            },
            function result(err){
                if (handler.checkError(err)){
                    connection.response.auth = true;
                    connection.response.sessionId = connection.sessionId;

                    next(connection, true);
                }
            }
        );
    }
};

exports.rememberMeLogin = {
    name: "rememberMeLogin",
    description: "User log in by remember me token",
    inputs: {
        "required" : ["rememberMeToken"],
        "optional" : []
    },
    blockedConnectionTypes: [],
    outputExample: {
        auth: true,
        sessionId: '[session_id]'
    },
    verb: 'post',
    public: true,
    run: function(api, connection, next){
        if(connection.currentUser){
            connection.error = "Multiple logins not allowed.";
            next(connection, true);
        } else {
            connection.error = null;
            connection.response.auth = false;

            var token = connection.params.rememberMeToken;

            Step(
                function getUserRM(){
                    api.mongo.userRememberMe.findById(token, this);
                },
                function checkUserRM(err, userRM){
                    if(err){
                        connection.error = err;
                        next(connection, true);
                    }else if(userRM == null){
                        connection.error = 'wrong token';
                        next(connection, true);
                    }else{
                        api.mongo.session.logIn(userRM.userId, this.parallel());

                        var newUserRM = new api.mongo.userRememberMe();
                        newUserRM.userId = userRM.userId;
                        newUserRM.save(this.parallel());

                        userRM.remove();
                    }
                }, function sessionSaveResult(err, sessionId, userRM){
                    if (err){
                        connection.error = err;
                        next(connection, true);
                    } else {
                        connection.response.auth = true;
                        connection.response.sessionId = sessionId;
                        if (userRM)
                            connection.response.rememberMeToken = userRM._id;

                        next(connection, true);
                    }
                }
            )
        }
    }
};

exports.logOut = {
    name: "logOut",
    description: "Logged user log out",
    inputs: {
        "required" : ['sessionId'],
        "optional" : ['rememberMeToken']
    },
    blockedConnectionTypes: [],
    outputExample: {
        sucess: true
    },
    verb: 'delete',
    role: 'user',
    run: function(api, connection, next){

        Step(
            function removeTokens(){
                api.mongo.session.logOut(connection.params.sessionId, this.parallel());
                if (connection.params.rememberMeToken) {
                    var id = connection.params.rememberMeToken;
                    api.mongo.userRememberMe.remove({"_id":id}, this.parallel());
                }
            },
            function results(err){
                if (null == err){
                    connection.response.success = true;
                    next(connection, true);
                } else {
                    connection.error = err;
                    next(connection, true);
                }
            }
        )
    }
};

exports.getUserData = {
    name: "getUserData",
    description: "Gets logged user data",
    inputs: {
        "required" : [],
        "optional" : []
    },
    blockedConnectionTypes: [],
    outputExample: {},
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        api.mongo.user.findById(connection.currentUser).select('memberSince username color theme sex email cookieDisclaimerDismissed -_id').exec(function(error, userData){
            connection.error = error;

            if (null === error) {
                connection.response.user = userData;
            }
            next(connection, true);
        });

    }
};


exports.getUserName = {
    name: "getUserName",
    description: "Gets logged user name",
    inputs: {
        "required" : [],
        "optional" : []
    },
    blockedConnectionTypes: [],
    outputExample: {},
    inDev: true,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        connection.response.username = connection.currentUser;
        next(connection, true);
    }
};

exports.userProfileUpdate = {
    name: "userProfileUpdate",
    description: "Update user profile",
    inputs: {
        required: ["profile"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    run: function(api, connection, next){
        var profile = connection.params.profile;

        api.mongo.user.findById(connection.currentUser, function(err, user){
            if(err){
                connection.error = err;
                next(connection, true);
            } else {
                _.extend(user, profile);

                user.save(function(err){
                    if(!err){
                        connection.response.success = true;
                    }
                    connection.error = err;
                    next(connection, true);
                })
            }
        });
    }
};

exports.userApproveCookies = {
    name: "userApproveCookies",
    description: "User approved use of cookies, update flag in db",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    run: function(api, connection, next){

        api.mongo.user.findById(connection.currentUser, function(err, user){
            if(err){
                connection.error = err;
                next(connection, true);
            } else {
                //_.extend(user, {cookieDisclaimerDismissed: true});
                user.cookieDisclaimerDismissed = true;

                user.save(function(err){
                    if(!err){
                        connection.response.success = true;
                    }
                    connection.error = err;
                    next(connection, true);
                })
            }
        });
    }
};

exports.userRecoverUsername = {
    name: "userRecoverUsername",
    description: "Sends an email to provided email address with users registered with provided email address",
    inputs: {
        required: ["email"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    public: true,
    run: function(api, connection, next){
        var email = connection.params.email;
        api.mongo.user.find({"email": email}, function(err, users){
            if(err){
                connection.error = err;
                next(connection, true);
            } else {
                var usernames = [];

                if (users != null)
                    users.forEach(function(user){
                        usernames.push(user._id);
                    });


                if (usernames.length > 0){
                    var Email = require('../util/email');

                    var emailToSend = new Email('bgBuddy usernames recovery', 'usernameRecovery', {usernames: usernames, user: usernames[0]});
                    emailToSend.addTo({email: email});

                    emailToSend.send(function(err) {
                        if (!err) {
                            connection.response.success = true;
                        }
                        else {
                            connection.error = err;
                        }
                        next(connection, true);
                    });
                } else {
                    connection.response.success = true;
                    next(connection, true);
                }

            }
        });
    }
};