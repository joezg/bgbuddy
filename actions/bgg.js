var querystring = require('querystring');
var http = require('http');
var Step = require('step');
var moment = require('moment');
var xml2js = require('xml2js');
var Q = require('q');

exports.bggImportCollection = {
    name: "bggImportCollection",
    description: "Imports collection of bgg user to current user games",
    inputs: {
        required: ['bggUsername'],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        // Build the post string from an object
        var query = querystring.stringify({
            username:connection.params.bggUsername
        });

        // An object of options to indicate where to post to
        var requestOptions = {
            host: 'boardgamegeek.com',
            port: '80',
            path: '/xmlapi2/collection?'+query,
            method: 'GET'
        };

        // Set up the request
        var queryRequest = http.get(requestOptions, function(res) {
            res.setEncoding('utf8');
            var body = '';
            res.on('data', function (chunk) {
                body += chunk;
            });
            res.on('end', function () {
                var parser = new xml2js.Parser();
                parser.parseString(body, function (err, result) {
                    var games = result.items.item;

                    var promises = [];
                    games.forEach(function(game){
                        var newGame = {
                            bggId: game.$.objectid,
                            type: game.$.subtype,
                            name: game.name[0]._,
                            owned: game.status[0].$.own,
                            userId: connection.currentUser
                        }
                        game.comment && (newGame.comment = game.comment[0]);

                        var gameModel = new api.mongo.userGame(newGame);
                        promises.push(gameModel.save());
                    });

                    Q.all(promises).then(function() {
                        connection.response.success = true;
                        next(connection, true);
                    });
                });
            });
        }).end();
    }
}

exports.bggPostPlay = {
    name: "bggPostPlay",
    description: "Posts a play to BGG for provided user",
    inputs: {
        required: ['bggUsername', 'bggPassword', 'matchId', 'bggId'],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    run: function(api, connection, next){

        var loginToBgg = function(next){
            //login to bgg
            var loginData = querystring.stringify({
                lasturl: '/',
                username: connection.params.bggUsername,
                password: connection.params.bggPassword
            });

            var loginOptions = {
                host: 'boardgamegeek.com',
                port: '80',
                path: '/login',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'Content-Length': loginData.length
                }
            };
            var loginReq = http.request(loginOptions, function(res){
                next(null, res); //because of step.parallel() first argument in callback must be err - in this case null.
            });

            // post the data
            loginReq.write(loginData);
            loginReq.end();
        }


        Step(
            function prepare(){
                //find match
                api.mongo.userMatch.findOne({ useId: connection.currentUser, _id: connection.params.matchId }, this.parallel());
                loginToBgg(this.parallel())

            },
            function postPlay(err, match, res){
                if (!err && res.statusCode == 302){
                    var cookie = res.headers['set-cookie'].join(';');

                    if (!match){
                        connection.error = "Match doesn't exists"
                        next(connection, true);
                        return;
                    }

                    // Build the post string from an object
                    var playData = {
                        ajax:1,
                        action:'save',
                        comments:'',
                        version:2,
                        objecttype:'thing',
                        objectid:connection.params.bggId,
                        playid:'',
                        playdate:moment(match.date).format('YYYY-M-D'),
                        dateinput:moment(match.date).format('YYYY-M-D'),
                        YUIButton:'',
                        twitter:0,
                        location:match.location ? match.location : '',
                        quantity:1,
                        length:0,
                        incomplete:match.inProgress ? 1 : 0,
                        nowinstats:0
                    };

                    var counter = 0;
                    match.players.forEach(function(p){
                        counter++;
                        playData['players['+counter+'][playerid]'] = '';
                        playData['players['+counter+'][name]'] = p.isUser ? 'me' : (p.isAnnonymous ? 'unknown': p.name);
                        playData['players['+counter+'][username]'] = '';
                        playData['players['+counter+'][color]'] = '';
                        playData['players['+counter+'][position]'] = '';
                        playData['players['+counter+'][score]'] = p.result ? p.result : 0;
                        playData['players['+counter+'][rating]'] = '';
                        playData['players['+counter+'][new]'] = '';
                        playData['players['+counter+'][win]'] = p.winner ? 1 : 0;

                    });

                    playData = querystring.stringify(playData);

                    // An object of options to indicate where to post to
                    var playOptions = {
                        host: 'boardgamegeek.com',
                        port: '80',
                        path: '/geekplay.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                            'Content-Length': playData.length,
                            'Cookie': cookie
                        }
                    };

                    // Set up the request
                    var playRequest = http.request(playOptions, function(res) {
                        res.setEncoding('utf8');
                        res.on('data', function (chunk) {
                            connection.response.success = true;
                            next(connection, true);
                        });
                    });

                    // post the data
                    playRequest.write(playData);
                    playRequest.end();
                }
            }
        )
    }
};

exports.bggSearch = {
    name: "bggSearch",
    description: "Search for a game in BGG",
    inputs: {
        required: ['query'],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    public: true,
    run: function(api, connection, next){
        // Build the post string from an object
        var query = querystring.stringify({
            query:connection.params.query
        });

        // An object of options to indicate where to post to
        var requestOptions = {
            host: 'boardgamegeek.com',
            port: '80',
            path: '/xmlapi2/search?'+query,
            method: 'GET'
        };

        // Set up the request
        var queryRequest = http.get(requestOptions, function(res) {
            res.setEncoding('utf8');
            var body = '';
            res.on('data', function (chunk) {
                body += chunk;
            });
            res.on('end', function () {
                console.log('BODY: ' + body);

                var parser = new xml2js.Parser();
                parser.parseString(body, function (err, result) {
                    console.dir(result);
                    connection.response.result = result;
                    next(connection, true);
                });
            });
        }).end();
    }
};

exports.getGeeklistJS = {
    name: "getGeeklistJS",
    description: "Returns XML from geeklist with given id",
    inputs: {
        required: ['geeklistId'],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    public: true,
    run: function(api, connection, next){
        var requestOptions = {
            host: 'boardgamegeek.com',
            port: '80',
            path: '/xmlapi/geeklist/'+connection.params.geeklistId,
            method: 'GET'
        };

        // Set up the request
        var queryRequest = http.get(requestOptions, function(res) {
            res.setEncoding('utf8');
            var body = '';
            res.on('data', function (chunk) {
                body += chunk;
            });
            res.on('end', function () {
                var parser = new xml2js.Parser();
                parser.parseString(body, function (err, result) {
                    connection.response.geeklist = result;
                    next(connection, true);
                });
            });
        }).end();
    }
}

exports.getObjectJS = {
    name: "getObjectJS",
    description: "Returns XML for object with given id",
    inputs: {
        required: ['objectId'],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    public: true,
    run: function(api, connection, next){
        var requestOptions = {
            host: 'boardgamegeek.com',
            port: '80',
            path: '/xmlapi/boardgame/'+connection.params.objectId,
            method: 'GET'
        };

        // Set up the request
        var queryRequest = http.get(requestOptions, function(res) {
            res.setEncoding('utf8');
            var body = '';
            res.on('data', function (chunk) {
                body += chunk;
            });
            res.on('end', function () {
                var parser = new xml2js.Parser();
                parser.parseString(body, function (err, result) {
                    connection.response.object = result;
                    next(connection, true);
                });
            });
        }).end();
    }
}