var _ = require('underscore');
var resolve = require('../util/promiseResolve.js');

exports.messageSend = {
    name: "messageSend",
    description: "Sends a message to user",
    inputs: {
        required: ['to', 'content'],
        optional: []
    },
    outputExample: {
        success: true
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    run: function(api, connection, next){
        var UserMessages = api.mongo.userMessages;

        var newMessage = {
            to: connection.params.to,
            from: connection.currentUser,
            content: connection.params.content
        };

        var promise = UserMessages.sendMail(newMessage);

        resolve(promise, connection, next);
    }
};

exports.messagesGet = {
    name: "messagesGet",
    description: "Gets messages for logged user",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var UserMessages = api.mongo.userMessages;

        UserMessages.findById(connection.currentUser, function(err, um){
            if (null == err) {
                connection.response.success = true;
                connection.response.inbox = um?um.inbox:[];
                connection.response.sent = um?um.sent:[];
            }
            connection.error = err;
            next(connection, true);
        });
    }
};

exports.messagesGetUnread = {
    name: "messagesGetUnread",
    description: "Gets unread messages for logged user",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var UserMessages = api.mongo.userMessages;

        UserMessages.findById(connection.currentUser, function(err, um){
            if (null == err) {
                connection.response.success = true;

                var unreadMessages = [];
                if (um) {
                    unreadMessages = _.filter(um.inbox, function(message){
                        return message.isUnread;
                    });
                }
                connection.response.unreadMessages = unreadMessages;
            }
            connection.error = err;
            next(connection, true);
        })
    }
};

exports.messagesGetUnreadCount = {
    name: "messagesGetUnreadCount",
    description: "Gets unread messages count for logged user",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var UserMessages = api.mongo.userMessages;

        UserMessages.findById(connection.currentUser, function(err, um){
            if (null == err) {
                connection.response.success = true;

                var unreadMessages = [];
                if (um) {
                    unreadMessages = _.filter(um.inbox, function(message){
                        return message.isUnread;
                    });
                }
                connection.response.unreadMessagesCount = unreadMessages?unreadMessages.length:0;
            }
            connection.error = err;
            next(connection, true);
        })
    }
};

exports.messagesGetWithUser = {
    name: "messagesGetWithUser",
    description: "Gets messages sent or received from given user for logged user",
    inputs: {
        required: ['username'],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var username = connection.params.username;

        var UserMessages = api.mongo.userMessages;

        UserMessages.findById(connection.currentUser, function(err, um){
            if (null == err) {
                connection.response.success = true;

                var inbox = [];
                var sent = [];

                if (um) {
                    inbox = _.filter(um.inbox, function(message){
                        return message.from == username;
                    });
                    sent = _.filter(um.sent, function(message){
                        return message.to == username;
                    });
                }

                connection.response.inbox = inbox;
                connection.response.sent = sent;
            }
            connection.error = err;
            next(connection, true);
        });
    }
};

exports.messagesGetCounts = {
    name: "messagesGetCounts",
    description: "Gets message counts for logged user",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var UserMessages = api.mongo.userMessages;

        UserMessages.findById(connection.currentUser, function(err, um){
            if (null == err) {
                connection.response.success = true;

                var inboxCounts = 0;
                var sentCounts = 0;

                if (um)
                {
                    inboxCounts = _.countBy(um.inbox, function(msg){
                        return msg.isUnread ? 'unread' : 'read';
                    });
                    sentCounts = _.countBy(um.inbox, function(msg){
                        return msg.delivered ? 'delivered' : 'undelivered';
                    });
                }

                connection.response.inboxCounts = inboxCounts;
                connection.response.sentCounts = sentCounts;
            }
            connection.error = err;
            next(connection, true);
        });
    }
};

exports.messageGetById = {
    name: "messageGetById",
    description: "Gets message with given id for logged user. Looks in inbox & sent folder.",
    inputs: {
        required: ['messageId'],
        optional: ['collection']
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var UserMessages = api.mongo.userMessages;
        //var collection = connection.params.collection || 'inbox';

     /*   if (['inbox', 'sent'].indexOf(collection) == -1){
            connection.error = "Wrong collection: " + collection;
            next(connection, true);
            return;
        }
*/
        UserMessages.findById(connection.currentUser, function(err, um){
            if (null == err) {
                if (null != um){
                    var foundInFolder = 'inbox';
                    var message = um['inbox'].id(connection.params.messageId);
                    if (!message) { // msg not found in inbox, look in sent msgs
                        message = um['sent'].id(connection.params.messageId);
                        foundInFolder = 'sent';
                    }
                    if (message) {
                        // var returnValue = _.extend(message, {folder: foundInFolder});
                        // message.folder = foundInFolder;
                        connection.response.message = message;
                        connection.response.folder = foundInFolder;
                        connection.response.success = true;
                    } else {
                        connection.error = "User doesn't have message with given id"
                    }

                } else {
                    connection.error = "user.messages don't exists for given user"
                }
            } else {
                connection.error = err;
            }

            next(connection, true);

        });
    }
};

exports.messageMarkRead = {
    name: "messageMarkRead",
    description: "Marks specific message as read for logged user",
    inputs: {
        required: ['messageId'],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    run: function(api, connection, next){
        var UserMessages = api.mongo.userMessages;

        UserMessages.findById(connection.currentUser, function(err, um){
            if (null == err) {
                um.markMessageRead(connection.params.messageId, function(err, um){
                    connection.error = err;
                    if (!err)
                        connection.response.success = true;

                    next(connection, true);
                });
            } else {
                connection.error = err;
                next(connection, true);
            }
        });
    }
};

exports.messageMarkAllRead = {
    name: "messageMarkAllRead",
    description: "Marks all messages as read for logged user",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    run: function(api, connection, next){
        var UserMessages = api.mongo.userMessages;

        UserMessages.findById(connection.currentUser, function(err, um){
            if (null == err) {
                um.markAllMessagesRead(function(err){
                    connection.error = err;
                    if (!err)
                        connection.response.success = true;

                    next(connection, true);
                });
            } else {
                connection.error = err;
                next(connection, true);
            }
        });
    }
};
