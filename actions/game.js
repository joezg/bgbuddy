var _ = require('underscore');
var Q = require('q');
var resolve = require('../util/promiseResolve.js');

exports.gamesGet = {
    name: "gamesGet",
    description: "Get all games of logged in user",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var promise = api.mongo.userGame.find({userId: connection.currentUser});

        resolve(promise, connection, next, function(response, games){
            response.games = games;
        })
    }
};

exports.gameAdd = {
    name: "gameAdd",
    description: "Adds game for logged user",
    inputs: {
        required: ['name'],
        optional: ['owned', 'played', 'comment', 'bggId', 'doSendMail']
    },
    outputExample: {
        success: true
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    run: function(api, connection, next){
        var newGame = _.pick(connection.params, 'name', 'owned', 'played', 'comment', 'bggId');
        var doSendMail = connection.params.doSendMail;

        var promise = api.mongo.userGame.addGame(connection.currentUser, newGame, doSendMail);

        resolve(promise, connection, next, function(response, game){
            response.game = game;
        })
    }
};

exports.gameUpdate = {
    name: "gameUpdate",
    description: "Updates game for logged user",
    inputs: {
        required: ['_id'],
        optional: ['name', 'owned', 'played', 'comment', 'historicalDateFirstPlayed', 'historicalDateLastPlayed', 'bggId', 'favourite', 'wantToPlay', 'knowRules']
    },
    outputExample: {
        success: true
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    run: function(api, connection, next){
        var daGame = _.pick(connection.params, '_id', 'name', 'owned', 'played', 'comment', 'historicalDateLastPlayed', 'historicalDateFirstPlayed', 'bggId', 'favourite', 'wantToPlay', 'knowRules');

        var doUpdateMatchesGameInfo = false;
        if (daGame.historicalDateFirstPlayed || daGame.historicalDateLastPlayed){
            doUpdateMatchesGameInfo = true;
        }

        var promise = api.mongo.userGame.findOne({userId: connection.currentUser, _id: daGame._id})
            .then(function(game){
                game = _.extend(game, daGame);

                if (doUpdateMatchesGameInfo){
                    return game.recalculateDatesAndUpdateMatchGameInfo()
                        .then(function(){
                            return game.save();
                        });
                } else {
                    return game.save();
                }
            });


        resolve(promise, connection, next, function(response, game){
            response.game = game;
        });
    }
};

exports.gameGet = {
    name: "gameGet",
    description: "Get game of logged user with a given id",
    inputs: {
        required: ["gameId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var gameId = connection.params.gameId;

        var promise = api.mongo.userGame.findOne({userId: connection.currentUser, _id: gameId});

        resolve(promise, connection, next, function(response, game){
            response.game = game;
        })
    }
};

exports.gameRename = {
    name: "gameRename",
    description: "Renames a game",
    inputs: {
        required: ["gameId", "newName"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    run: function(api, connection, next){
        var gameId = connection.params.gameId;
        var promise = api.mongo.userGame.findOne({userId: connection.currentUser, _id: gameId})
            .then(function(game){
                if (!game)
                    throw new Error("Game with given Id doesn't exists");

                game.name = connection.params.newName;

                var renameGameInMatchesPromise = api.mongo.userMatch.update(
                    {
                        userId: connection.currentUser,
                        gameId: api.mongo.mongoose.Types.ObjectId(gameId)
                    },
                    {
                        $set: {
                            game: connection.params.newName
                        }
                    },
                    {
                        multi: 1
                    }
                );

                return Q.all([game.save(), renameGameInMatchesPromise]);
            });

        resolve(promise, connection, next, function(response, data){
            response.game = data[0];
        });
    }
};

exports.gemeDelete = {
    name: "gameDelete",
    description: "Deletes a game",
    inputs: {
        required: ["gameId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'delete',
    role: 'user',
    run: function(api, connection, next){
        var gameId = api.mongo.mongoose.Types.ObjectId(connection.params.gameId);
        var promise = api.mongo.userGame.findOne({userId: connection.currentUser, _id: gameId})
            .then(function(game){
                return api.mongo.userMatch.find({userId: connection.currentUser, gameId: gameId})
                    .then(function(matches){
                        if (matches.length > 0){
                            return { cannotDelete: true }
                        }

                        var removeGamePromise = api.mongo.userGame.remove({userId: connection.currentUser, _id: gameId} );

                        return removeGamePromise;
                    })

            });

        resolve(promise, connection, next, function(response, result){
            if (result.cannotDelete){
                response.cannotDelete = true;
            }
        });
    }
};

exports.gemeUpdateDatesAndMatchesGameInfo = {
    name: "gemeUpdateDatesAndMatchesGameInfo",
    description: "Update game dates according to match records. Also updates mathes game info.",
    inputs: {
        required: ["_id"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    run: function(api, connection, next){
        var gameId = api.mongo.mongoose.Types.ObjectId(connection.params._id);
        var promise = api.mongo.userGame.findOne({userId: connection.currentUser, _id: gameId})
            .then(function(game){
                return game.recalculateDatesAndUpdateMatchGameInfo().then(function(){
                    return game.save();
                });
            });

        resolve(promise, connection, next, function(response, game){
            response.game = game;
        });
    }
};