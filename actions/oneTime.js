var Q = require('q');
var _ = require('underscore');
var resolve = require('../util/promiseResolve.js');

exports.changeLibrarySchema = {
    name: "changeLibrarySchema",
    description: "",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'admin',
    disabled: true,
    run: function(api, connection, next){
        var promise = api.mongo.eventOrganizerLibrary.find({}).then(function(eol){
            var savePromises = [];
            eol.forEach(function(lib){
                var eventOrganizerId = lib._id;
                lib.games.forEach(function(game){
                    var oldGame = game._doc;

                    oldGame.code = oldGame._id;
                    delete oldGame._id;

                    var newGame = new api.mongo.eventOrganizerLibraryGame(oldGame);
                    newGame.eventOrganizerId = eventOrganizerId;

                    savePromises.push(newGame.save());
                })

                lib.boxes.forEach(function(box){
                    var oldBox = box._doc;
                    oldBox.code = oldBox._id;
                    delete oldBox._id;
                    var newBox = new api.mongo.eventOrganizerLibraryBox(oldBox);
                    newBox.eventOrganizerId = eventOrganizerId;

                    savePromises.push(newBox.save());
                })
            })

            return Q.all(savePromises);
        })

        resolve(promise, connection, next);
    }
};

exports.changeTournamentMatchSchema = {
    name: "changeTournamentMatchSchema",
    description: "",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'admin',
    disabled: true,
    run: function(api, connection, next){
        var promise = api.mongo.eventTournament.find({}).then(function(ts){
            var savePromises = [];
            ts.forEach(function(t){
                t.rounds.forEach(function(r){
                    r.matches.forEach(function(m){
                        if (!m.tableNumber){
                            var id = m._id.toString().split('/');
                            m.tableNumber = id[0];

                            if (id.length > 1){
                                m.matchNumber = id[1];
                            }

                            m._id = api.mongo.mongoose.Types.ObjectId();
                        }
                    })
                })

                savePromises.push(t.save());
            })

            return Q.all(savePromises);
        })

        resolve(promise, connection, next);
    }
};


exports.changeUserBuddySchema = {
    name: "changeUserBuddySchema",
    description: "",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'admin',
    disabled: true,
    run: function(api, connection, next){
        var promise = api.mongo.userBuddies.find({}).then(function(userBuddies){
            var savePromises = [];
            userBuddies.forEach(function(ub){
                var userId = ub._id;
                ub.buddies.forEach(function(buddy){
                    var newBuddy = new api.mongo.userBuddy(buddy);
                    newBuddy.userId = userId;

                    savePromises.push(newBuddy.save());
                })
            })

            return Q.all(savePromises);
        })

        resolve(promise, connection, next);
    }
};

exports.changeUserGameSchema = {
    name: "changeUserGameSchema",
    description: "",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'admin',
    disabled: true,
    run: function(api, connection, next){
        var promise = api.mongo.userGames.find({}).then(function(userGames){
            var savePromises = [];
            userGames.forEach(function(ug){
                var userId = ug._id;
                ug.games.forEach(function(game){
                    var newGame = new api.mongo.userGame(game);
                    newGame.userId = userId;

                    savePromises.push(newGame.save());
                })
            })

            return Q.all(savePromises);
        })

        resolve(promise, connection, next);
    }
};

exports.changeUserMatchSchema = {
    name: "changeUserMatchSchema",
    description: "",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'admin',
    disabled: true,
    run: function(api, connection, next){
        var promise = api.mongo.userMatches.find({}).then(function(userMatches){
            var savePromises = [];
            userMatches.forEach(function(um){
                var userId = um._id;
                um.matches.forEach(function(match){
                    var newMatch = new api.mongo.userMatch(match);
                    newMatch.userId = userId;

                    savePromises.push(newMatch.save());
                })
            })

            return Q.all(savePromises);
        }).then(function(){
            return api.mongo.userMatch.find({}).then(function(matches){
                var savePromises = [];
                matches.forEach(function(match){
                    match.players.forEach(function(player){
                        if (player.name){
                            player.buddyId = player._id;
                            player._id = api.mongo.mongoose.Types.ObjectId();
                        }
                    })
                    savePromises.push(match.save());
                })
                return Q.all(savePromises);
            })
        });

        resolve(promise, connection, next);
    }
};