var Q = require('q');
var resolve = require('../util/promiseResolve.js');

exports.userEventGetAll = {
    name: "userEventGetAll",
    description: "Get all events of all event groups related to the logged user",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var promise = api.mongo.eventParticipant.find({userId: connection.currentUser}, {eventId:1})
            .then(function(ep){
                var eventIds = [];
                ep.forEach(function(p){
                    eventIds.push(p.eventId);
                })

                return api.mongo.event.find({_id: {$in: eventIds}});
            });

        resolve(promise, connection, next, function(response, events){
            response.events = events;
        });
    }
};

exports.eventGetInfo = {
    name: "eventGetInfo",
    description: "Get info about an event",
    inputs: {
        required: ["eventId", "eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var eventId = api.mongo.mongoose.Types.ObjectId(connection.params.eventId);
        var eventOrganizerId = api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId);

        var promise = api.mongo.event.findOne({eventOrganizerId: eventOrganizerId, _id: eventId});

        resolve(promise, connection, next, function(response, event){
            response.info = event;
        })
    }
};

exports.eventParticipantGetAll = {
    name: "eventParticipantGetAll",
    description: "Get all participants of specific event of an event organizer",
    inputs: {
        required: ["eventId", "eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var eventId = api.mongo.mongoose.Types.ObjectId(connection.params.eventId),
            eventOrganizerId = api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId);

        var promise = api.mongo.eventParticipant.find({eventOrganizerId: eventOrganizerId, eventId: eventId});

        resolve(promise, connection, next, function(response, participants){
            response.participants = participants;
        });
    }
};

exports.eventMatchGetAll = {
    name: "eventMatchGetAll",
    description: "Get matches of an event",
    inputs: {
        required: ["eventId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var eventId = api.mongo.mongoose.Types.ObjectId(connection.params.eventId);

        var promise = api.mongo.eventMatch.find({eventId: eventId});
        resolve(promise, connection, next, function(response, matches){
            response.matches = matches;
        });
    }
};

exports.eventMatchGetAllForParticipants = {
    name: "eventMatchGetAllForParticipants",
    description: "Get matches of an event for selected participants",
    inputs: {
        required: ["eventId", "participants"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var eventId = api.mongo.mongoose.Types.ObjectId(connection.params.eventId);

        var promise = api.mongo.eventMatch.find({
            eventId: eventId,
            "players.playerId": {
                "$in": JSON.parse(connection.params.participants)
            }
        });
        resolve(promise, connection, next, function(response, matches){
            response.matches = matches;
        });
    }
};

exports.eventMatchAdd = {
    name: "eventMatchAdd",
    description: "Add match to events",
    inputs: {
        required: ["eventId", "date", "game", "gameId", "players"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var eventId = api.mongo.mongoose.Types.ObjectId(connection.params.eventId);
        var match = {
            game: connection.params.game,
            gameId: connection.params.gameId,
            date: connection.params.date,
            players: connection.params.players,
            eventId: eventId
        }

        var newMatch = new api.mongo.eventMatch(match);

        resolve(newMatch.save(), connection, next, function(response, match){
            response.match = match;
        });

    }
};