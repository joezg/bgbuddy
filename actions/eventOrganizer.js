var _ = require('underscore');
var Q = require('q');
var resolve = require('../util/promiseResolve.js');

exports.userAdministeredEventOrganizerGetAll = {
    name: "userAdministeredEventOrganizerGetAll",
    description: "Get all event organizers administered by the current user",
    inputs: {
        required: [],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var promise = api.mongo.eventOrganizer.getAllUserAdministered(connection.currentUser);

        resolve(promise, connection, next, function(response, eventOrganizers){
            response.eventGroups = eventOrganizers;
        });
    }
};

exports.eventOrganizerGetInfo = {
    name: "eventOrganizerGetInfo",
    description: "Get info about an event organizer",
    inputs: {
        required: ["eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var eventOrganizerId = connection.params.eventOrganizerId
        var promise = api.mongo.eventOrganizer.findById(eventOrganizerId, "name website");

        resolve(promise, connection, next, function(response, eo){
            response.info = eo;
        });
    }
};

exports.eventOrganizerParticipantGetAll = {
    name: "eventOrganizerParticipantGetAll",
    description: "Get all participants of an event organizer",
    inputs: {
        required: ["eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var eventOrganizerId = connection.params.eventOrganizerId;

        var promise = api.mongo.eventOrganizerParticipant.find({eventOrganizerId: eventOrganizerId});

        resolve(promise, connection, next, function(response, participants){
            response.participants = participants;
        });
    }
};

exports.eventOrganizerParticipantGet = {
    name: "eventOrganizerParticipantGet",
    description: "Get a participant of an event organizer",
    inputs: {
        required: ["eventOrganizerId", 'participantId'],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var eventOrganizerId = api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId);
        var participantId = api.mongo.mongoose.Types.ObjectId(connection.params.participantId);

        var promise = api.mongo.eventOrganizerParticipant.findOne({eventOrganizerId: eventOrganizerId, _id:participantId});

        resolve(promise, connection, next, function(response, participant){
            response.participant = participant;
        });
    }
};

exports.eventOrganizerEventGetAllForParticipant = {
    name: "eventOrganizerEventGetAllForParticipant",
    description: "Get all events of this organizers for given participantId",
    inputs: {
        required: ["eventOrganizerId", 'participantId'],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var eventOrganizerId = api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId);
var participantId = api.mongo.mongoose.Types.ObjectId(connection.params.participantId);

        var promise = api.mongo.eventParticipant.find({eventOrganizerId: eventOrganizerId, participantId:participantId});

        resolve(promise, connection, next, function(response, events){
            response.events = events;
        });
    }
};

exports.eventOrganizerParticipantSave = {
    name: "eventOrganizerParticipantSave",
    description: "Saves the participant of an event organizer",
    inputs: {
        required: ["eventOrganizerId", '_id', 'name', 'code'],
        optional: ['email', 'userId', 'country', 'shortName']
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var eventOrganizerId = api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId);
        var participantId = api.mongo.mongoose.Types.ObjectId(connection.params._id);
        var newItem = {
            code: connection.params.code,
            name: connection.params.name,
            userId: connection.params.userId,
            email: connection.params.email,
            country: connection.params.country,
            shortName: connection.params.shortName
        };

        var promise = api.mongo.eventOrganizerParticipant.findOne({eventOrganizerId: eventOrganizerId, _id: participantId})
            .then(function(participant){
                if (null == participant)
                    throw new Error ("Participant doesn't exists");

                _.extend(participant, newItem);
                return participant.save();
            });

        promise.then(function(p){
            api.mongo.eventParticipant.find({eventOrganizerId: p.eventOrganizerId, participantId: p._id})
                .then(function(participations){
                    return participations.forEach(function(part){
                        part.participantName = p.name;
                        part.participantShortName = p.shortName;
                        part.participantCode = p.code;
                        part.userId = p.userId;
                        part.save();
                    })
                })
                .catch(function(err){
                    console.log(err);
                });
        });

        resolve(promise, connection, next);
    }
};

exports.participantRemoveFromEvent = {
    name: "participantRemoveFromEvent",
    description: "Removes the participant from indicated event",
    inputs: {
        required: ["eventOrganizerId", "eventId", '_id'],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var eventOrganizerId = connection.params.eventOrganizerId;
        var eventId = connection.params.eventId;
        var _id =  connection.params._id;

        var promise = api.mongo.eventParticipant.remove({eventOrganizerId: eventOrganizerId, eventId: eventId, participantId: _id});

        resolve(promise, connection, next);
    }
};

exports.participantAddToEvent = {
    name: "participantAddToEvent",
    description: "Adds the participant to indicated event",
    inputs: {
        required: ["eventOrganizerId", "eventId", '_id'],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var eventOrganizerId = api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId);
        var eventId = api.mongo.mongoose.Types.ObjectId(connection.params.eventId);
        var _id =  api.mongo.mongoose.Types.ObjectId(connection.params._id);

        var getEventPromise = api.mongo.event.findOne({eventOrganizerId: eventOrganizerId, _id: eventId});
        var getParticipantPromise = api.mongo.eventOrganizerParticipant.findOne({eventOrganizerId: eventOrganizerId, _id: _id})

        var promise = Q.all([getEventPromise, getParticipantPromise]).then(function(data){
            var event = data[0];
            var participant = data[1];

            var participation = new api.mongo.eventParticipant({
                eventOrganizerId: event.eventOrganizerId,
                eventId: event._id,
                participantId: participant._id,
                participantName: participant.name,
                participantShortName: participant.shortName,
                participantCode: participant.code,
                userId: participant.userId,
                eventName: event.name
            })

            return participation.save();
        })

        resolve(promise, connection, next);

    }
};

exports.eventOrganizerParticipantAdd = {
    name: "eventOrganizerParticipantAdd",
    description: "Adds the participant of an event organizer",
    inputs: {
        required: ["eventOrganizerId", 'code', 'name'],
        optional: ['email', 'userId', 'country', 'shortName']
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var eventOrganizerId = api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId);
        var newItem = {
            code: connection.params.code,
            name: connection.params.name,
            eventOrganizerId: eventOrganizerId,
            userId: connection.params.userId,
            email: connection.params.email,
            country: connection.params.country,
            shortName: connection.params.shortName
        };

        var promise = api.mongo.eventOrganizerParticipant.findOne({eventOrganizerId: eventOrganizerId, code: newItem.code})
            .then(function(alreadyExists){
                if (alreadyExists)
                    throw new Error("Participant with code: " + newItem.code + " already exists");

                var newParticipant = new api.mongo.eventOrganizerParticipant(newItem);
                return newParticipant.save();

            });

        resolve(promise, connection, next, function(response, participant){
            response.participant = participant;
        })

    }
};

exports.eventOrganizerEventGetAll = {
    name: "eventOrganizerEventGetAll",
    description: "Get all events of an event organizer",
    inputs: {
        required: ["eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var eventOrganizerId = connection.params.eventOrganizerId

        var promise = api.mongo.event.find({eventOrganizerId: eventOrganizerId});

        resolve(promise, connection, next, function(response, events){
            response.events = events;
        });
    }
};

exports.eventOrganizerEventAdd = {
    name: "eventOrganizerEventAdd",
    description: "Save new event of an event organizer",
    inputs: {
        required: ["eventOrganizerId", "name", "location", "startDate", "endDate"],
        optional: ["venue"]
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var event = {
            name: connection.params.name,
            location: connection.params.location,
            startDate: connection.params.startDate,
            endDate: connection.params.endDate,
            eventOrganizerId: connection.params.eventOrganizerId
        }

        if (connection.params.venue)
            event.venue = connection.params.venue

        var eventModel = new api.mongo.event(event);

        resolve(eventModel.save(), connection, next, function(response, e){
            response.event = e;
        })
    }
};

exports.libraryGetAll = {
    name: "libraryGetAll",
    description: "Get all games in event organizer's library",
    inputs: {
        required: ["eventOrganizerId"],
        optional: ["onlyFromOrganizer"]
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var eventOrganizerId = connection.params.eventOrganizerId;

        var filter = {eventOrganizerId: eventOrganizerId};
        if (connection.params.onlyFromOrganizer){
            filter.notOwnedByOrganizer = {$ne: true};
        }

        var promise = api.mongo.eventOrganizerLibraryGame.find(filter);

        resolve(promise, connection, next, function(response, games){
            response.games = games;
        })
    }
};

exports.libraryBoxGetAll = {
    name: "libraryBoxGetAll",
    description: "Get all defined boxes for the event organizer library",
    inputs: {
        required: ["eventOrganizerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var eventOrganizerId = connection.params.eventOrganizerId;

        var promise = api.mongo.eventOrganizerLibraryBox.find({eventOrganizerId: eventOrganizerId});

        resolve(promise, connection, next, function(response, boxes){
            response.boxes = boxes;
        })
    }
};

exports.libraryItemGet = {
    name: "libraryItemGet",
    description: "Get an item in event organizer library",
    inputs: {
        required: ["eventOrganizerId", "itemId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var eventOrganizerId = api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId);
        var itemId = api.mongo.mongoose.Types.ObjectId(connection.params.itemId);

        var promise = api.mongo.eventOrganizerLibraryGame.findOne({eventOrganizerId: eventOrganizerId, _id: itemId})

        resolve(promise, connection, next, function(response, game){
            response.game = game;
        })
    }
};

exports.boxItemGet = {
    name: "boxItemGet",
    description: "Get an item in event group boxes",
    inputs: {
        required: ["eventOrganizerId", "itemId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var eventOrganizerId = api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId);
        var itemId = api.mongo.mongoose.Types.ObjectId(connection.params.itemId);

        var promise = api.mongo.eventOrganizerLibraryBox.findOne({eventOrganizerId: eventOrganizerId, _id: itemId})

        resolve(promise, connection, next, function(response, box){
            response.box = box;
        })
    }
};

exports.libraryItemSave = {
    name: "libraryItemSave",
    description: "Saves the item of the event organizer library",
    inputs: {
        required: ["eventOrganizerId", 'name', 'code', 'owner', 'isBoxed'],
        optional: ['itemId', 'location', 'box', 'comment', 'notOwnedByOrganizer', 'eventId']
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var eventOrganizerId = api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId);
        var itemId = connection.params.itemId ? api.mongo.mongoose.Types.ObjectId(connection.params.itemId) : null;

        var promise = Q.fcall(function(){
            var newItem = {
                name: connection.params.name,
                code: String(connection.params.code),
                owner: connection.params.owner,
                isBoxed: connection.params.isBoxed,
                comment: connection.params.comment,
                box: connection.params.box,
                location: connection.params.location,
                notOwnedByOrganizer: !!connection.params.notOwnedByOrganizer
            };

            if (newItem.notOwnedByOrganizer){
                if (!connection.params.eventId){
                    throw new Error("When item is not owned by organizer eventId must be set");
                }
                newItem.eventId = connection.params.eventId;
            }

            if (newItem.isBoxed && !newItem.box) {
                throw new Error("When item is boxed, box must be set");
            }

            if (itemId){
                return api.mongo.eventOrganizerLibraryGame.findOne({eventOrganizerId: eventOrganizerId, _id: itemId})
                    .then(function(game){
                        if (null == game){
                            throw new Error("Item with id " + itemId.toString() + " does not exists.");
                        }

                        _.extend(game, newItem);
                        return game.save();
                    })
            } else {
                newItem.eventOrganizerId = eventOrganizerId;
                var game = new api.mongo.eventOrganizerLibraryGame(newItem);
                return game.save();
            }
        })

        resolve(promise, connection, next, function(response, game){
            response.game = game;
        })

    }
}

exports.boxItemSave = {
    name: "boxItemSave",
    description: "Saves the item of the event group boxes",
    inputs: {
        required: ["eventOrganizerId", "itemId", "code"],
        optional: ["location", "comment"]
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var eventOrganizerId = api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId);
        var itemId = api.mongo.mongoose.Types.ObjectId(connection.params.itemId);

        var newItem = {
            location: connection.params.location,
            comment:  connection.params.comment,
            code: connection.params.code
        };

        var promise = api.mongo.eventOrganizerLibraryBox.findOne({eventOrganizerId: eventOrganizerId, _id: itemId})
            .then(function(item){
                if (null == item)
                    throw new Error("Box with id " + itemId.toString() + " doesn't exists.");

                _.extend(item, newItem);

                return item.save();
            });

        resolve(promise, connection, next, function(response, box){
            response.box = box;
        })
    }
}

exports.boxAdd = {
    name: "boxAdd",
    description: "Adds new box to event group boxes",
    inputs: {
        required: ["eventOrganizerId", "code"],
        optional: ["location", "comment"]
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    eventAdmin: true,
    run: function(api, connection, next){
        var eventOrganizerId = api.mongo.mongoose.Types.ObjectId(connection.params.eventOrganizerId);

        var newItem = {
            location: connection.params.location,
            comment:  connection.params.comment,
            code: connection.params.code,
            eventOrganizerId: eventOrganizerId
        };

        var box = new api.mongo.eventOrganizerLibraryBox(newItem);

        resolve(box.save(), connection, next, function(response, box){
            response.box = box;
        })
    }
}


