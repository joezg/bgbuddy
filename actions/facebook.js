var Step = require('step');

exports.facebookSignIn = {
    name: "facebookSignIn",
    description: "Receives short-term token for facebook authentication",
    inputs: {
        required: ['token'],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    public: true,
    run: function(api, connection, next){
        var APP_ID = "1600627500203927",
            APP_SECRET = "8ae7a9c75892c7786d56f2be615d364c";

        var FB = require('fb');

        Step(
            function getApplicationAccessToken(){
                FB.api('oauth/access_token', {
                    client_id: APP_ID,
                    client_secret: APP_SECRET,
                    grant_type: 'client_credentials'
                }, this);
            },
            function debugToken(res){
                if(!res || res.error) {
                    connection.error = res.err;
                    next(connection, true);
                    return;
                }

                FB.api('/debug_token?input_token='+connection.params.token+'&access_token='+res.access_token, this);
            },
            function checkTokenAndFindFacebookUser(res){
                if(!res || res.error) {
                    connection.error = res.err;
                    next(connection, true);
                    return;
                }
                if (res.data.app_id != APP_ID){
                    connection.error = "Invalid access token. Access token is not for this application.";
                    next(connection, true);
                    return;
                }

                //TODO maybe get a long-term token?

                var userId = res.data.user_id;
                api.mongo.userFacebook.findById(userId, this.parallel());

                this.parallel()(null, userId);
                this.parallel()(null, connection.params.token);


            },
            function checkUserAndStartSession(err, uf, userId, accessToken){
                if(err) {
                    connection.error = err;
                    next(connection, true);
                    return;
                }

                if (!uf || !uf.username) {
                    api.mongo.session.initUnconnectedFacebookSession(userId, this.parallel());

                    //will be overwritten if already exists
                    if (!uf)
                        uf = new api.mongo.userFacebook();

                    uf._id = userId;
                    uf.accessToken = accessToken;
                    uf.save(this.parallel());
                } else {
                    api.mongo.session.logIn(uf.username, function(err, sessionId){
                        if(err) {
                            connection.error = err;
                            next(connection, true);
                            return;
                        }

                        connection.response.sessionId = sessionId;
                        next(connection, true);
                    });
                }

            },
            function sendUnconnectedUserMessage(err, sessionId){
                if(err) {
                    connection.error = err;
                    next(connection, true);
                    return;
                }
                connection.response.unconnectedUser = true;
                connection.response.sessionId = sessionId;
                next(connection, true);
            }
        )
    }
};

exports.facebookNewUserConnect = {
    name: "facebookNewUserConnect",
    description: "Creates a new bgbuddy user and connects it to facebook user",
    inputs: {
        required: ['username'],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    public: true,
    run: function(api, connection, next){
        var ActionErrorHandler = require('../util/actionErrorHandler'),
            handler = new ActionErrorHandler(connection, next);

        var APP_ID = "1600627500203927",
            APP_SECRET = "8ae7a9c75892c7786d56f2be615d364c";

        var FB = require('fb');

        var userFacebook = null;

        if (!connection.session){
            connection.error = "Session expired.";
            next(connection, true);
            return;
        }

        Step(
            function getUserFacebook() {
                var userId = connection.facebookUserId;
                api.mongo.userFacebook.findById(userId, this);
            },
            function getUserDataFromFacebook(err, uF) {
                if (handler.checkError(err)){
                    userFacebook = uF;
                    if (null == userFacebook){
                        connection.error = "Facebook userid doesn't exists";
                        next(connection, true);
                    } else {
                        var url = '/v2.4/me?';
                        url+='access_token='+uF.accessToken;
                        url+='&fields=name,id,gender,email';

                        FB.api(url, this);
                    }
                }
            },
            function saveUserAndUpdateFacebookUserAndSession(result) {
                if (result.error){
                    connection.error = result.error.message;
                    next(connection, true);
                    return;
                }

                var user = new api.mongo.user();
                user.username = connection.params.username;
                user.email = result.email;//TODO change
                user.isConfirmed = true;
                user.sex = result.gender == "female" ? "f" : "m";//TODO change
                user.userWithoutPassword = true;
                user.save(this.parallel());

                //update userGoogle
                userFacebook.username = user._id;
                userFacebook.save(this.parallel());

                //find session
                api.mongo.session.update({_id: connection.sessionId}, { isUnconnected: false, username: user._id}, this.parallel())
            },
            function result(err, user, uG, session){
                if (handler.checkError(err)) {
                    connection.response.success = true;
                    next(connection, true);
                }
            }
        );
    }
};