var _ = require('underscore');
var resolve = require('../util/promiseResolve.js');
var Q = require('q');

exports.sharingPlayerMappingGet = {
    name: "sharingPlayerMappingGet",
    description: "Get all player mappings for sharing matches",
    inputs: {
        required: [],
        optional: ['eventId']
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var promise;

        if (connection.params.eventId)
            promise = api.mongo.userSharingPlayerMapping.findOne({userId: connection.currentUser, eventId: connection.params.eventId});

        resolve(promise, connection, next, function(response, data){
            response.mapping = data.mappings;
        });
    }
};

exports.sharingPlayerMappingSave = {
    name: "sharingPlayerMappingSave",
    description: "Save all player mappings for sharing matches",
    inputs: {
        required: ['mappings'],
        optional: ['eventId', 'sourceUserId']
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    run: function(api, connection, next){
        var promise = null

        if (connection.params.eventId)
            promise = api.mongo.userSharingPlayerMapping.findOne({userId: connection.currentUser, eventId: connection.params.eventId});

        promise = promise.then(function(spm){
            if (!spm){
                spm = new api.mongo.userSharingPlayerMapping();
                spm.userId = connection.currentUser;
                connection.params.eventId && (spm.eventId = connection.params.eventId);
                connection.params.sourceUserId && (spm.sourceUserId = connection.params.sourceUserId);
            }

            spm.mappings = connection.params.mappings;

            return spm.save();
        })

        resolve(promise, connection, next);
    }
};

exports.sharingGameMappingSave = {
    name: "sharingGameMappingSave",
    description: "Save all game mappings for sharing matches",
    inputs: {
        required: ['mappings'],
        optional: ['eventId', 'sourceUserId']
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    run: function(api, connection, next){
        var promise = null

        if (connection.params.eventId)
            promise = api.mongo.userSharingGameMapping.findOne({userId: connection.currentUser, eventId: connection.params.eventId});

        promise = promise.then(function(sgm){
            if (!sgm){
                sgm = new api.mongo.userSharingGameMapping();
                sgm.userId = connection.currentUser;
                connection.params.eventId && (sgm.eventId = connection.params.eventId);
                connection.params.sourceUserId && (sgm.sourceUserId = connection.params.sourceUserId);
            }

            sgm.mappings = connection.params.mappings;

            return sgm.save();
        })

        resolve(promise, connection, next);
    }
};

exports.sharingGameMappingGet = {
    name: "sharingGameMappingGet",
    description: "Get all game mappings for sharing matches",
    inputs: {
        required: [],
        optional: ['eventId']
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var promise;

        if (connection.params.eventId)
            promise = api.mongo.userSharingGameMapping.findOne({userId: connection.currentUser, eventId: connection.params.eventId});

        resolve(promise, connection, next, function(response, data){
            response.mapping = data.mappings;
        });
    }
};