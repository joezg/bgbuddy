var _ = require('underscore');
var Q = require('q');
var resolve = require('../util/promiseResolve.js');

exports.matchAdd = {
    name: "matchAdd",
    description: "Adding match for logged user",
    inputs: {
        required: ["game"],
        optional: ["gameId", "inProgress", "initialPhase", "date", "location", "addSelf"]
    },
    outputExample: {
        success: true
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    run: function(api, connection, next){
        var match = {
            game: connection.params.game,
            date: connection.params.date || new Date(),
            location: connection.params.location,
            inProgress: connection.params.inProgress || false,
            userId: connection.currentUser,
            phases: [],
            isScored: false
        };
        if (connection.params.addSelf){
            match.players = [{isUser: true}];
        }
        if (connection.params.initialPhase){
            match.phases.push(connection.params.initialPhase);
        }

        var gameId = api.mongo.mongoose.Types.ObjectId(connection.params.gameId);

        var promise = api.mongo.userGame.findOne({ userId: connection.currentUser, _id: gameId }).then(function(game){
            if (game){
                match.gameId = game._id;
                match.isNewGame = !game.played;
                if (!match.isNewGame)
                    match.isGameOutOfDust = game.isOutOfDust();
            } else {
                match.gameId = api.mongo.mongoose.Types.ObjectId();
                match.isNewGame = true;
                match.isOutOfDust = false;
            }

            var newMatch = api.mongo.userMatch(match);
            return newMatch.save();
        })

        resolve(promise, connection, next, function(response, match){
            response.match = match;
        });

    }
};

exports.matchesImport = {
    name: "matchesImport",
    description: "Import collection of matches",
    inputs: {
        required: ["matches"],
        optional: []
    },
    outputExample: {
        success: true
    },
    version: 1.0,
    verb: 'post',
    role: 'user',
    run: function(api, connection, next){
        var matchPromises = [];

        connection.params.matches.forEach(function(m){
            //TODO test this

            //Add user
            m.userId = connection.currentUser;

            //TODO if gameId do not exists check for a game

            //TODO check player and insert then if necessary

            //TODO add game phase: playing

            var newMatch = new api.mongo.userMatch(m);
            matchPromises.push(newMatch.save());
        });

        resolve(Q.all(matchPromises), connection, next);

    }
};

exports.matchGetLast = {
    name: "matchGetLast",
    description: "Get defined number of last matches of logged user",
    inputs: {
        required: [],
        optional: ["count"]
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var count = connection.params.count || 0;

        var promise = api.mongo.userMatch.find({userId: connection.currentUser}).sort({'date': -1}).limit(count);

        resolve(promise, connection, next, function(response, data){
            response.matches = data;
        })
    }
};

exports.matchStartPhase = {
    name: "matchStartPhase",
    description: "Finish ongoing phase and start a new one",
    inputs: {
        required: ["matchId", "phaseName", "phaseType"],
        optional: ["phaseStart"]
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    run: function(api, connection, next){
        var matchId = connection.params.matchId;
        var phase = {
            name: connection.params.phaseName,
            type: connection.params.phaseType
        };

        if (connection.params.phaseStart){
            phase.start = connection.params.phaseStart;
        }

        var promise = api.mongo.userMatch.findOne({userId: connection.currentUser, _id: matchId})
            .then(function(match){
                if (null == match)
                    throw new Error("Match with given id doesn't exists for current user.")

                match.startNewPhase(phase)
                return match.save();
            });

        resolve(promise, connection, next);
    }
};

exports.matchFinish = {
    name: "matchFinish",
    description: "Finishes a match",
    inputs: {
        required: ["matchId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    run: function(api, connection, next){
        var matchId = connection.params.matchId;

        var promise = api.mongo.userMatch.findOne({userId: connection.currentUser, _id: matchId})
            .then(function(match){
                if (null == match)
                    throw new Error("Match with given id doesn't exists for current user.");

                match.finishMatch();

                // save new buddies
                var buddiesToSave = [];
                match.players.forEach(function(p) {
                    if (p.isNewBuddy) {
                        var newBuddy = _.pick(p, 'name');
                        newBuddy._id = p.buddyId;
                        buddiesToSave.push(newBuddy);
                        p.isNewBuddy = false;
                    }
                });
                var saveBuddiesPromise = api.mongo.userBuddy.addBuddies(connection.currentUser, buddiesToSave, true);

                //update game details in parallel
                var updateGamePromise = null;
                if (!match.isNewGame){
                    updateGamePromise = api.mongo.userGame.recordPlay(connection.currentUser, match.gameId);
                } else {
                    var currentDate = new Date();
                    var game = {
                        name: match.game,
                        _id: match.gameId,
                        played: true,
                        dateLastPlayed: currentDate,
                        dateFirstPlayed: currentDate
                    }
                    updateGamePromise = api.mongo.userGame.addGame(connection.currentUser, game, true);
                }

                return Q.all([saveBuddiesPromise, updateGamePromise, match.save()]);
            });

        resolve(promise, connection, next);
    }
};

exports.matchUpdatePlayers = {
    name: "matchUpdatePlayers",
    description: "Updates players data for a match",
    inputs: {
        required: ["matchId"],
        optional: ["doAddBuddies", "players"]
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    run: function(api, connection, next){
        var matchId = connection.params.matchId;
        var players = connection.params.players || [];
        var doAddBuddies = connection.params.doAddBuddies === false ? false : true;

        var promise = api.mongo.userMatch.updatePlayers(connection.currentUser, matchId, players, doAddBuddies);

        resolve(promise, connection, next, function(response, data){
            response.players = data.players;
        });
    }
};

exports.matchRemovePlayer = {
    name: "matchRemovePlayer",
    description: "Removes player from a match",
    inputs: {
        required: ["matchId", "playerId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    run: function(api, connection, next){
        var matchId = connection.params.matchId;
        var _id = connection.params.playerId;

        var promise = api.mongo.userMatch.update(
            {
                userId: connection.currentUser,
                _id: api.mongo.mongoose.Types.ObjectId(matchId)
            },
            {
                $pull: { players: {_id: api.mongo.mongoose.Types.ObjectId(_id) } }
            }
        );

        resolve(promise, connection, next);
    }
};

exports.matchGet = {
    name: "matchGet",
    description: "Get match of logged user with a given id",
    inputs: {
        required: ["matchId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var matchId = connection.params.matchId;

        var promise = api.mongo.userMatch.findOne({userId: connection.currentUser, _id: matchId});

        resolve(promise, connection, next, function(response, match){
            response.match = match;
        });
    }
};

exports.matchUpdateInfo = {
    name: "matchUpdateInfo",
    description: "Updates info data of the match",
    inputs: {
        required: ["_id"],
        optional: ["date", "location", "isGameOutOfDust", "isNewGame"]
    },
    outputExample: {
        success: true
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    run: function(api, connection, next){
        var newMatch = {
            _id: api.mongo.mongoose.Types.ObjectId(connection.params._id)
        };

        if (connection.params.date !== undefined){
            newMatch.date = connection.params.date;
        }

        if (connection.params.location !== undefined)
            newMatch.location = connection.params.location;

        if (connection.params.isGameOutOfDust !== undefined)
            newMatch.isGameOutOfDust = connection.params.isGameOutOfDust;

        if (connection.params.isNewGame !== undefined)
            newMatch.isNewGame = connection.params.isNewGame;

        var promise = api.mongo.userMatch.findOne({userId: connection.currentUser, _id: newMatch._id}).then(function(match){
            if (null == match)
                throw new Error("Match with given id doesn't exists for current user.");

            var recalculateGameDatesNeeded = false;
            if (match.date.valueOf() != new Date(newMatch.date).valueOf())
                recalculateGameDatesNeeded = true;

            _.extend(match, newMatch);

            var promises = [
                match.save()
            ]

            if (recalculateGameDatesNeeded)
                promises.push(api.mongo.userGame.findOne({userId: connection.currentUser, _id: match.gameId}));

            return Q.all(promises);
        }).then(function(data){
            var match = data[0];
            var game = data[1] || null;

            if (game) {
                return game.recalculateDatesAndUpdateMatchGameInfo()
                    .then(function(changedMatches)
                    {
                        //it is needed to check if match is among the changed ones so it could be returned later
                        changedMatches.forEach(function(m){
                            if (m._id.toString() == match._id.toString()){
                                match = m;
                            }
                        });
                        return game.save()
                    }).then(function(){
                        return match;
                    });
            }
            else {
                return match;
            }
        });

        resolve(promise, connection, next, function(response, match){
            response.match = match;
        });
    }
};

exports.matchUpdatePhases = {
    name: "matchUpdatePhases",
    description: "Replaces all match phases with provided ones",
    inputs: {
        required: ["matchId", "phases"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    run: function(api, connection, next){
        var matchId = connection.params.matchId;
        var phases = connection.params.phases;

        var promise = api.mongo.userMatch.findOne({userId: connection.currentUser, _id: matchId}).then(function(match){
            if (null == match)
                throw new Error("Match with given id doesn't exists for current user.");

            match.phases = phases;

            return match.save();
        });

        resolve(promise, connection, next);
    }
};

exports.matchesWithBuddyGet = {
    name: "matchesWithBuddyGet",
    description: "Get matches of logged in user with defined buddy name",
    inputs: {
        required: ["buddyId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var buddyId = connection.params.buddyId;

        var promise = api.mongo.userMatch.find({userId: connection.currentUser, 'players.buddyId': buddyId});

        resolve(promise, connection, next, function(response, matches){
            response.matches = matches;
        });
    }
};

exports.matchesForGameGet = {
    name: "matchesForGameGet",
    description: "Get matches of logged in user with certain game",
    inputs: {
        required: ["gameId"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var gameId = connection.params.gameId;

        var promise = api.mongo.userMatch.find({userId: connection.currentUser, gameId: gameId});

        resolve(promise, connection, next, function(response, matches){
            response.matches = matches;
        });
    }
};

exports.matchesWithinDateIntervalGet = {
    name: "matchesWithinDateIntervalGet",
    description: "Get matches of logged in user within given date interval",
    inputs: {
        required: [],
        optional: ["from", "to"]
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'get',
    role: 'user',
    run: function(api, connection, next){
        var from = connection.params.from;
        var to = connection.params.to;

        if (!from)
            from = new Date(-8640000000000000);
        else
            from = new Date(from);

        if (!to)
            to = new Date();
        else
            to = new Date(to);

        var promise = api.mongo.userMatch.find({userId: connection.currentUser, date:{$gte: from, $lt: to}});

        resolve(promise, connection, next, function(response, matches){
            response.matches = matches;
        });
    }
};

exports.matchDelete = {
    name: "matchDelete",
    description: "Deletes a match",
    inputs: {
        required: ["_id"],
        optional: []
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'delete',
    role: 'user',
    run: function(api, connection, next){
        var _id = api.mongo.mongoose.Types.ObjectId(connection.params._id);

        var promise = api.mongo.userMatch.findOne({userId: connection.currentUser, _id : _id})
            .then(function(match){
                var gameId = match.gameId;

                var promises = [
                    api.mongo.userGame.findOne({userId: connection.currentUser, _id : gameId}),
                    api.mongo.userMatch.remove({userId: connection.currentUser, _id : _id})
                ]

                return Q.all(promises);
            })
            .then(function(data){
                var game = data[0];
                return game.recalculateDatesAndUpdateMatchGameInfo()
                    .then(function(){
                        return game.save();
                    })
            })

        resolve(promise, connection, next);
    }
};

exports.matchUpdateGame = {
    name: "matchUpdateGame",
    description: "Update game of a match",
    inputs: {
        required: ["_id", "originalGameId"],
        optional: ["newGameId", "newGameName"]
    },
    outputExample: {
    },
    version: 1.0,
    verb: 'put',
    role: 'user',
    run: function(api, connection, next){
        var matchId = connection.params._id;
        var newGameName = connection.params.newGameName;
        var newGameId = api.mongo.mongoose.Types.ObjectId(connection.params.newGameId);
        var originalGameId = api.mongo.mongoose.Types.ObjectId(connection.params.originalGameId);

        var getMatchPromise = api.mongo.userMatch.findOne({userId: connection.currentUser, _id: api.mongo.mongoose.Types.ObjectId(matchId)});
        var getNewGamePromise = api.mongo.userGame.findOne({userId: connection.currentUser, _id: newGameId});
        var getOriginalGamePromise = api.mongo.userGame.findOne({userId: connection.currentUser, _id: originalGameId});

        var promise = Q.all([getMatchPromise, getNewGamePromise, getOriginalGamePromise]).then(function(data){
            var match = data[0];
            var newGame = data[1];
            var originalGame = data[2];

            var updatePromises = [];
            if (!newGame){
                newGame = api.mongo.userGame({name: newGameName, _id: api.mongo.mongoose.Types.ObjectId(), userId: connection.currentUser});
                updatePromises.push(newGame.save());
            }

            match.gameId = newGame._id;
            match.game = newGame.name;

            updatePromises.unshift(match.save());

            return Q.all(updatePromises).then(function(data){
                var match = data[0];
                var gameNew = data[1] || newGame;
                var recalculatePromises = [
                    gameNew.recalculateDatesAndUpdateMatchGameInfo()
                        .then(function(changedMatches)
                        {
                            //it is needed to check if match is among the changed ones so it could be returned later
                            changedMatches.forEach(function(m){
                                if (m._id.toString() == match._id.toString()){
                                    match = m;
                                }
                            });
                            return gameNew.save()
                        }),
                    originalGame.recalculateDatesAndUpdateMatchGameInfo().then(function(){return originalGame.save()})
                ]
                return Q.all(recalculatePromises).then(function(){
                    return match;
                })
            })
        });

        resolve(promise, connection, next, function(response, match){
            response.match = match;
        });
    }
};
